
**NOTE: To get an introduction of the AUTO-XTEND toolchain, please refer to the [main repository](https://bitbucket.org/syed_ali_abbas_jaffari/autoxtend).**

# Introduction to ArcCore 12.0

This repository contains an extension of the open-source Arctic Core operating system for AUTOSAR, which is distributed under the GNU General Public License by ArcCore AB. Arctic Core provides an OSEK/VDX compliant RTOS and supports several platform architectures including ARM, PPC and Renesas. It provides MCALs for more than 25 boards and ECUs. Board specific debugging services (i.e., T32, UDE, WinIdea and SCI) along with CLib port are also available as part of Arctic Core. Compared to the core available on Arctic Core website, this repository includes a port on Raspberry Pi 1,2 and Zero (i.e. BCM2835) and its UART based debugging service.

# Getting Started

**Note: Only tested for Linux (Ubuntu 16.04)**

## Compile
Requirements: Platform dependent cross-compiler (e.g., for raspberry pi, arm-none-eabi-gcc).

Goto the core folder, i.e. core-v12_0_1/core, and type the following in the terminal:

make BOARDDIR=bcm2835 CROSS_COMPILE=/usr/bin/arm-none-eabi- BDIR=../examples/OsSimple SELECT_CONSOLE=TTY_SERIAL_BCM2835 SELECT_OPT=OPT_DEBUG all

This generates a binary (i.e. kernel.img/OsSimple.elf) for the OsSimple example from ArcCore at core-v12_0_1/core/binaries/bcm2835.

## Run on Raspberry Pi (1, 2 and Zero)

**Note: The acknowledge LED in Raspberry Pi Zero and 1/2 are different. To make sure that the correct LED is used to indicate code execution, check core-v12_0_1/core/arch/arm/arm_v6/kernel/sys_tick.c. In case this file needs to be modified, the binary needs to be recompiled (see previous sub section).**

### Prepare SD card:

RPi is usually used with linux images (e.g. raspbian). In order to install these linux distributions on the SD card, one needs to have 2 partitions at the minimum. The first partition (called boot) is responsible for initializing so that RPi is ready to load the OS. If the SD card is new, probably it won't have any partition. To create these partitions, use some freely available tools (e.g. GParted). This boot partition is either FAT32 or FAT16 and is usually 60MB. Since the binary which we are going to run will not be more than 59 MB, boot partition is enough for our use. Hence, the task of this tutorial is to prepare boot partition to run our code.
To execute some code from the boot partition, we need 3 files, i.e. start.elf, bootcode.bin and kernel.img. The first 2 files can be found on raspberry pi bootloader repos (e.g. [https://github.com/raspberrypi/firmware](https://github.com/raspberrypi/firmware)). The first and foremost thing is to copy these files in boot partition. The third file is the binary of the code. Once the binary file kernel.img is generated (see compilation step above), just copy it to the boot partition. Safely remove the SD card reader (e.g. use 'sync' command on the terminal). Plug it in the RPi, power up and the code starts running (indicated by continous flashing of the on-board yellow acknowledge LED).
To avoid changing the SD card everytime a modification in the code is made, use the serial bootloader from [https://github.com/mrvn/raspbootin](https://github.com/mrvn/raspbootin). Note that this type of bootloader requires serial connection as mentioned in the next sub section and might not work on all binaries since it only copies a part of the binary to the SD card.

### Connecting board for debugging
Output (written using printf/stdlib) can be seen by connecting a serial-to-USB cable. The UART configuration on the PC should be 115200-8-N-1 (use any serial terminal, e.g. GTKterm). The device is most probably called /dev/ttyUSB0. If the cable is disconnected, you probably need to setup the configuration again.

USB to Serial converter pin connections (See GPIO connector pinouts - http://www.megaleecher.net/Raspberry_Pi_GPIO_Pinout_Helper):
Cable types:

1. Wires colors: Black, Orange, Yellow (e.g. http://www.ftdichip.com/Support/Documents/DataSheets/Cables/DS_TTL-232R_RPi.pdf)

	Connections: RPi pin 6 is connected to black wire, 8 to yellow and 10 to orange.

2. Wires colors: Black, White, Green, Red

	Connections: RPi pin 6 is connected to black wire, 8 to white and 10 to green. Red can be left open (recommended).

3. Wires colors: Black, Brown, Orange, Red (e.g. https://www.sparkfun.com/products/12977)

	Connections: RPi pin 6 is connected to black wire, 8 to brown and 10 to orange. Red can be left open (recommended).

# Wish to contribute?

You are welcome to contribute in improving and extending the software. To do that, you can request for a branch or report bugs (with procedure to reproduce the bug) to the author, Ali Syed @ [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com). 

# Copyright and License

Copyright (C) 2016, Ali Syed, Germany
Contact: [syed.ali.abbas.jaffari@gmail.com](mailto:syed.ali.abbas.jaffari@gmail.com)

AUTO-XTEND is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. AUTO-XTEND is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU General Public License](https://www.gnu.org/licenses/gpl.txt) for more details.

**Disclaimer:** The rights for the operating system core from ArcCore and AUTOSAR meta-model provided as part of this toolchain are reserved by ArcCore AB, Sweden. The licensing statement from ArcCore is provided as under:

Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
Contact: [contact@arccore.com](mailto:contact@arccore.com)
You may ONLY use this file:

1. if you have a valid commercial ArcCore license and then in accordance with the terms contained in the written license agreement between you and ArcCore, or alternatively
2. if you follow the terms found in GNU General Public License version 2 as published by the Free Software Foundation and appearing in the file LICENSE.GPL included in the packaging of this file or [here](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt).

