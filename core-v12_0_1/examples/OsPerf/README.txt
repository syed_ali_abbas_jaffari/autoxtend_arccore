OsOrti
========
This example shows the following features:
  - SchM module
  - Timer (Timer_xx API)
  - The Task "TaskLoad" that increases the load by 10% every 10ms until it hits 90%.
  - ORTI

Supported boards:
  - mpc5516it

Files:
 - src/tasks.c            - implementation of tasks
 - src/system_hoooks.c    - implementation of hooks
 - OsPerf_Generic.arxml   - generic BSW modules
 - OsPerf_mpc551it.arxml  - configuration referencing generic modules and board specific modules.
 - build_config.mk        - the build configuration
 - makefile               - the makefile

Debuggers:

  T32
    Some scripts to work with Lauterbach T32 is located at tools/t32. You should be able to use it
    either on the simulator or on real HW. To use the scripts run "do start" in that tools/t32 directory.
   
   To view the context switching, check menu->Arccore->Os->xxx
   
   View the software calculated load in:
   - Perf_TaskTimers
   - Perf_IsrTimers
   - Perf_KernelTimers
   

  


