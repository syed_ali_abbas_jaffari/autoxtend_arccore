
/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Dio.h"
#include "Dio_Cfg.h"

const Dio_ChannelType DioChannelConfigData[] = { 
	DioConf_DioChannel_DioChannel_gp0_14,/*gpio0_14*/
	DioConf_DioChannel_DioChannel_gp0_15,/*gpio0_15*/
	DioConf_DioChannel_DioChannel_gp0_17,/*gpio0_17*/
	DioConf_DioChannel_LED1,/*gpio1_0*/
	DioConf_DioChannel_LED2,/*gpio1_16*/
	DIO_END_OF_LIST
};

const Dio_ConfigType DioConfigData = {
	.ChannelConfig = DioChannelConfigData,
	.GroupConfig = NULL,
	.PortConfig = NULL
};

