
/*
 * Generator version: 5.0.1
 * AUTOSAR version:   4.1.2
 */

#include "Port.h"

const ArcPort_PinConfig pinConfigs[] = {

	/* P8_0 */ {.PortPinId = 98, PIN_FUNCTION_REG_98_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 1, .flags.BITF.PIBC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },

        {.PortPinId = PORT_INVALID_REG}
};

const Port_ConfigType PortConfigData =
{
  .pinConfig = pinConfigs,
};

