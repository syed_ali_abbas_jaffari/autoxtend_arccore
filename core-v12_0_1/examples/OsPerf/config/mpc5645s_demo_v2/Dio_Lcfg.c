
/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Dio.h"
#include "Dio_Cfg.h"

const Dio_ChannelType DioChannelConfigData[] = { 
	DioConf_DioChannel_DioChannel_GPIO_E0,
	DioConf_DioChannel_DioChannel_GPIO_E1,
	DioConf_DioChannel_DioChannel_SpiCs,
	DioConf_DioChannel_LED_GREEN,
	DioConf_DioChannel_LED_RED,
	DIO_END_OF_LIST
};

const Dio_PortType DioPortConfigData[] = { 
	DioConf_DioPort_PORT_C,
	DioConf_DioPort_PORT_E,
	DioConf_DioPort_PORT_F,
	DioConf_DioPort_PORT_K,
	DIO_END_OF_LIST
};

const Dio_ChannelGroupType DioGroupConfigData[] = {
	{
	  .port = DioConf_DioPort_PORT_E,
	  .mask = 0x3u,
	  .offset = 0u /* Calculated from mask */
	},
	{ 
	  .port = DIO_END_OF_LIST, 
	  .mask = 0u,
	  .offset = 0u
	}
};

const Dio_ConfigType DioConfigData = {
	.ChannelConfig = DioChannelConfigData,
	.GroupConfig = DioGroupConfigData,
	.PortConfig = DioPortConfigData
};

