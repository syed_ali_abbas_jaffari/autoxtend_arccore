
/*
 * Generator version: 5.0.1
 * AUTOSAR version:   4.1.2
 */

#include "Port.h"

const ArcPort_PinConfig pinConfigs[] = {

	/* AP0_0 */ {.PortPinId = 44, PIN_FUNCTION_REG_44_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_0 */ {.PortPinId = 27, PIN_FUNCTION_REG_27_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_3 */ {.PortPinId = 29, PIN_FUNCTION_REG_29_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },

        {.PortPinId = PORT_INVALID_REG}
};

const Port_ConfigType PortConfigData =
{
  .pinConfig = pinConfigs,
};

