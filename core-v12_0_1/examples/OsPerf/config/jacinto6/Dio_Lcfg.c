
/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Dio.h"
#include "Dio_Cfg.h"

const Dio_ChannelType DioChannelConfigData[] = { 
	DioConf_DioChannel_DioChannel_gp8_7,/*gpio8_7*/
	DioConf_DioChannel_DioChannel_gp8_8,/*gpio8_8*/
	DioConf_DioChannel_DioChannel_gp8_9,/*gpio8_9*/
	DioConf_DioChannel_LED1,/*gpio2_7*/
	DioConf_DioChannel_LED2,/*gpio2_2*/
	DIO_END_OF_LIST
};

const Dio_ConfigType DioConfigData = {
	.ChannelConfig = DioChannelConfigData,
	.GroupConfig = NULL,
	.PortConfig = NULL
};

