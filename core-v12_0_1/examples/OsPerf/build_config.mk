
# Version of build system
REQUIRED_BUILD_SYSTEM_VERSION=1.0.0

# Get configuration makefiles
-include ../config/*.mk
-include ../config/$(board_name)/*.mk

MOD_USE += SCHM DET
CFG += TIMER
#CFG += TIMER_TIMERS
CFG += TIMER_SCTM
#CFG += SIMULATOR
CFG += OS_PERF

# Project settings

SELECT_CONSOLE = RAMLOG

SELECT_OPT = OPT_DEBUG 

