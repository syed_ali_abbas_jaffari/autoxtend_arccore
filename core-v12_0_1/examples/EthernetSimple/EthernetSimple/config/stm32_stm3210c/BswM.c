
/*
 * Generator version: 3.3.0
 * AUTOSAR version:   4.0.3
 */

/* general requiremnets */
/* @req BswM0001 */
/* @req BswM0008 */

/* Mode Arbitration */
/* @req BswM0009 */
/* @req BswM0035 */
/* @req BswM0010 */
/* @req BswM0012 */
/* @req BswM0117 */
/* @req BswM0147 */

/* Immediate and Deferred Operation */
/* @req BswM0061 */
/* @req BswM0013 */
/* @req BswM0014 */

/* Arbitration Behavior after Initialization */
/* @req BswM0064 */

/* mode control */
/* @req BswM0015 */
/* @req BswM0019 */
/* @req BswM0067 */
/* @req BswM0037 */
/* @req BswM0062 */

/* Triggered and Conditional action lists */
/* @req BswM0011 */
/* @req BswM0023 */
/* @req BswM0115 */
/* @req BswM0116 */

/* Available Actions */
/* @req BswM0038 */
/* @req BswM0039 */
/* @req BswM0040 */
/* @req BswM0054 */

/* @req BswM0066 */

/* @req BswM0020 */
/* @req BswM0021 */
/* @req BswM9999 */


#include "BswM.h"
#include "BswM_Internal.h"
#include "MemMap.h"
//#include "SchM_BswM.h"
#if defined(USE_COMM)
#include "ComM.h"
#endif
#if defined(USE_PDUR)
#include "PduR.h"
#endif
#if defined(USE_NVM)
#include "NvM.h"
#endif
#if defined(USE_CANSM)
#include "CanSM.h"
#include "CanSM_BswM.h"
#endif
#if defined(USE_SD)
#include "SD.h"
#endif
#if defined(USE_FRSM)
#include "FrSM.h"
#endif
#if defined(USE_LINSM)
#include "LinSM.h"
#endif
#if defined(USE_ETHSM)
#include "EthSM.h"
#endif
#if defined(USE_COM)
#include "Com.h"
#endif
#if defined(USE_LINIF)
#include "LinIf.h"
#endif
#include "EcuM.h"
#if defined(USE_DCM)
#include "Dcm.h"
#endif
#if defined(USE_DET)
#include "Det.h"
#endif
#if defined(USE_DEM)
#include "Dem.h"
#endif
#if defined(USE_NM)
#include "Nm.h"
#endif

#if defined(USE_RTE)
#include "Rte_BswM.h"
#endif
#include "SchM_BswM.h"

#define IS_VALID_COMM_MODE(_x)	((COMM_FULL_COMMUNICATION == (_x)) || (COMM_SILENT_COMMUNICATION == (_x)) || (COMM_NO_COMMUNICATION == (_x)))


	

const BswM_ConfigType BswM_Config;

BswM_InternalType BswM_Internal = {
		.InitStatus = BSWM_UNINIT,
};

#ifdef HOST_TEST
BswM_InternalType* readinternal_status(void);
BswM_InternalType* readinternal_status(void){

	return (&BswM_Internal);
}
#endif

/* @req BswM0128 */
static Com_IpduGroupVector BswM_ComIpduGroupVector;
static boolean BswM_ComIpduInitialize;
static boolean BswM_PduGroupSwitchActionPerformed;


/*=============================== Mode types ================================*/


static ComM_ModeType BswComMIndicationMode = BSWM_UNDEFINED_REQUEST_VALUE; /*lint !e64 BSWM_UNDEFINED_REQUEST_VALUE is a generic define so supressing type check */
static uint8 SwcStartCommunicationMode = BSWM_UNDEFINED_REQUEST_VALUE; /*lint !e64 BSWM_UNDEFINED_REQUEST_VALUE is a generic define so supressing type check */


/*================================== Rules ==================================*/
static BswM_RuleState BswM_RuleStates[] = {
	BSWM_FALSE,
	BSWM_FALSE,
};


static void EnablePduGroupsRule( void ) {
	/* @req BswM0065 */
	if ((BswComMIndicationMode == COMM_FULL_COMMUNICATION)) {
		if (BswM_RuleStates[ENABLEPDUGROUPSRULE] != BSWM_TRUE) {
			EnablePdusActions();
		}
		BswM_RuleStates[ENABLEPDUGROUPSRULE] = BSWM_TRUE;
	} else {
		BswM_RuleStates[ENABLEPDUGROUPSRULE] = BSWM_FALSE;
	}

}

static void StartCommunicationRule( void ) {
	/* @req BswM0065 */
	if ((SwcStartCommunicationMode != COMM_NO_COMMUNICATION)) {
		if (BswM_RuleStates[STARTCOMMUNICATIONRULE] != BSWM_TRUE) {
			StartCommuncationActions();
		}
		BswM_RuleStates[STARTCOMMUNICATIONRULE] = BSWM_TRUE;
	} else {
		BswM_RuleStates[STARTCOMMUNICATIONRULE] = BSWM_FALSE;
	}

}


/*============================== Action lists ===============================*/
/* @req BswM0017 */
/* @req BswM0018 */

static void EnablePdusActions( void ) {

	/* @req BswM0055 */
	Com_SetIpduGroup(BswM_ComIpduGroupVector, ComConf_ComIPduGroup_ComIPduGroupRx, TRUE);
	Com_SetIpduGroup(BswM_ComIpduGroupVector, ComConf_ComIPduGroup_ComIPduGroupTx, TRUE);
	BswM_ComIpduInitialize = FALSE;
	BswM_PduGroupSwitchActionPerformed = TRUE;
}

static void StartCommuncationActions( void ) {

	/* @req BswM0055 */
	(void)	    ComM_RequestComMode(ComMConf_ComMUser_ComMUser, COMM_FULL_COMMUNICATION);
}

/*======================= Delayed request processing ========================*/

#define BSWM_DELAYED_REQUESTS_ARRAY_SIZE                      0
#define BSWM_IMMEDIATE_REQUESTS_EVALUATED_RULES_ARRAY_SIZE    0




static boolean BswM_RequestProcessingOngoing = false;



static inline boolean BswM_AtemptToProcessRequest ( void ) {
	boolean retVal = (BswM_RequestProcessingOngoing == TRUE) ? FALSE : TRUE;
	if (BswM_RequestProcessingOngoing == false) {
		BswM_RequestProcessingOngoing = true;
	}
	return retVal;
}

static inline void BswM_EndRequestProcessing ( void ) {
	BswM_RequestProcessingOngoing = false;
}


/*=========================== Immediate functions ===========================*/


/*============================== Main function ==============================*/
/* @req BswM0053 */
void BswM_MainFunction( void ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_MAINFUNCTION); /* @req BswM0076 */

	static boolean firstMainFunctionExecution = TRUE;
	/* The Com module is initialzed after the BswM so this must be done in the main funciton
	 * and not in BswMInit()
	 */
	if (TRUE == firstMainFunctionExecution) {
		Com_ClearIpduGroupVector(BswM_ComIpduGroupVector);
		firstMainFunctionExecution = FALSE;
	}

	boolean requestCanBeProcessed;	

	SchM_Enter_BswM_EA_0();
	requestCanBeProcessed = BswM_AtemptToProcessRequest();
	SchM_Exit_BswM_EA_0();
	
	/* @req BswM0059 */
	/* @req BswM0075 */
	/* @req BswM0060 */
	/* @req BswM0016 */
    Rte_Read_modeRequestPort_SwcStartCommunication_requestedMode(&SwcStartCommunicationMode);



	EnablePduGroupsRule();
	StartCommunicationRule();
	/* @req BswM0129 */
	if (TRUE == BswM_PduGroupSwitchActionPerformed) {
		Com_IpduGroupControl(BswM_ComIpduGroupVector, BswM_ComIpduInitialize);
		BswM_PduGroupSwitchActionPerformed = FALSE;
	}
	
	if (TRUE == requestCanBeProcessed) {
#if BSWM_DELAYED_REQUESTS_ARRAY_SIZE > 0
		BswM_ProcessDelayedRequests();
#endif
		BswM_EndRequestProcessing();
	}
}


/*================================== APIs ===================================*/

/* !req BswM0002 */
void BswM_Init( const BswM_ConfigType* ConfigPtr ) {
    (void)ConfigPtr; /*lint !e920 Avoid compiler warning (variable not used) */
	BswM_Internal.InitStatus = BSWM_INIT;

	BswM_ComIpduInitialize = FALSE;
	BswM_PduGroupSwitchActionPerformed = FALSE;
}

/* !req BswM0119 */
void BswM_Deinit( void ) {
	BswM_Internal.InitStatus = BSWM_UNINIT;
}


#if ( BSWM_VERSION_INFO_API == STD_ON )
void BswM_GetVersionInfo( Std_VersionInfoType* VersionInfo ) {
	BSWM_VALIDATE_POINTER_NORV(VersionInfo, BSWM_SERVICEID_GETVERSIONINFO);
	/* @req BswM0004 */
	/* @req BswM0136 */
	STD_GET_VERSION_INFO(VersionInfo,BSWM);
}
#endif /* BSWM_VERSION_INFO_API */


/* @req BswM0047 */
void BswM_ComM_CurrentMode( NetworkHandleType Network,
                            ComM_ModeType RequestedMode ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_COMMCURRENTMODE); /* @req BswM0078 */
	BSWM_VALIDATE_REQUESTMODE_NORV(IS_VALID_COMM_MODE(RequestedMode), BSWM_SERVICEID_COMMCURRENTMODE); /* @req BswM0091 */

	switch (Network) {
	case ComMConf_ComMChannel_ComMChannel:
		BswComMIndicationMode = RequestedMode;
		break;
	default:
		break;
	}
}

/* @req BswM0148 */
void BswM_ComM_CurrentPNCMode( PNCHandleType PNC,
                               ComM_PncModeType CurrentPncMode ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_COMMCURRENTPNCMODE); /* @req BswM0149 */

	(void)PNC;
	(void)CurrentPncMode;
}

/* @req BswM0056 */
void BswM_EcuM_CurrentState( EcuM_StateType CurrentState ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ECUMCURRENTSTATE); /* @req BswM0084 */
	/* !req BswM0103 */

	(void)CurrentState;
}

/* @req BswM0131 */
void BswM_EcuM_CurrentWakeup( EcuM_WakeupSourceType source,
                              EcuM_WakeupStatusType state ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ECUMCURRENTWAKEUP); /* @req BswM0132 */
	/* !req BswM0133 */

	(void)source;
	(void)state;
}

/* !req BswM0050 */
void BswM_EthSM_CurrentState( NetworkHandleType Network,
                              EthSM_NetworkModeStateType CurrentState ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ETHSMCURRENTSTATE); /* @req BswM0081 */
	/* !req BswM0097 */
	(void)Network;
	(void)CurrentState;
}



