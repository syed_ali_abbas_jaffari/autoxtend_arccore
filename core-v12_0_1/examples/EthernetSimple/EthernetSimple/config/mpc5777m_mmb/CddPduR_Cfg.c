
/*
 * Generator version: 4.0.0
 * AUTOSAR version:   4.0.3
 */

#include "CddPduR.h"
#include "PduR.h"
#include "PduR_PbCfg.h"

PduIdType CddPduR_PduMap[CDDPDUR_MAX_N_IPDUS] = {
	PDUR_PDU_ID_DLT_RX
,	PDUR_PDU_ID_DLT_TX
,	PDUR_PDU_ID_TEST_RX
,	PDUR_PDU_ID_TEST_TX
};

