/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_BswM.h"

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define BswM_START_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>
const Rte_CDS_BswM BswM_bswm = {
    ._dummy = 0
};
#define BswM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>

#define BswM_START_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>
const Rte_Instance Rte_Inst_BswM = &BswM_bswm;

#define BswM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>

/** === Runnables =================================================================================
 */
#define BswM_START_SEC_CODE
#include <BswM_MemMap.h>

/** ------ bswm -----------------------------------------------------------------------
 */
void Rte_bswm_BswMRunnable(void) {

    /* PRE */

    /* MAIN */

    BswM_MainFunction();

    /* POST */

}
#define BswM_STOP_SEC_CODE
#include <BswM_MemMap.h>

