/*
 * Generator version: 4.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef CDD_PDUR_CFG_H
#define CDD_PDUR_CFG_H

/* CddPduR IPDU IDs	*/
#define CDDPDUR_PDU_ID_DLT_RX  0 
#define CDDPDUR_PDU_ID_CDDPDURIPDU_DLT_RX  CDDPDUR_PDU_ID_DLT_RX

/* CddPduR IPDU IDs	*/
#define CDDPDUR_PDU_ID_DLT_TX  1 
#define CDDPDUR_PDU_ID_CDDPDURIPDU_DLT_TX  CDDPDUR_PDU_ID_DLT_TX

/* CddPduR IPDU IDs	*/
#define CDDPDUR_PDU_ID_TEST_RX  2 
#define CDDPDUR_PDU_ID_CDDPDURIPDU_TEST_RX  CDDPDUR_PDU_ID_TEST_RX

/* CddPduR IPDU IDs	*/
#define CDDPDUR_PDU_ID_TEST_TX  3 
#define CDDPDUR_PDU_ID_CDDPDURIPDU_TEST_TX  CDDPDUR_PDU_ID_TEST_TX


#define CDDPDUR_MAX_N_IPDUS 4	

extern PduIdType CddPduR_PduMap[CDDPDUR_MAX_N_IPDUS];

#endif






