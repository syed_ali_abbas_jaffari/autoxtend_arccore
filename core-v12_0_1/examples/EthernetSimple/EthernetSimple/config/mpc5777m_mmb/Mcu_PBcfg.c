/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Mcu.h"



const struct Mcu_Hw_ClockSettings Mcu_Hw_ClockSettingConfig[] = {
	{
      .pll0_PREDIV    = 5, /* PLL0_PREDIV */
	  .pll0_MFD    = 60,    /* PLL_0MFD */  
      .pll0_RFDPHI    = 3,  /* PLL0_RFDPHI */ 
      .pll1_MFD  = 3,    /* PLL1_MFD */
	  .pll1_RFDPHI  = 30,  /* PLL1_RFDPHI */
	  .pll1fm_MODEN  = false,  /* PLL1FM_MODEN */
	  .pll1fm_MODPRD  = 0,  /* PLL1FM_MODPRD */
	  .pll1fm_INCSTP  = 0,  /* PLL1FM_INCSTP */
	 },
};

const Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] = {
   {
      .McuClockReferencePointFrequency = 40000000UL,
      .Mcu_Hw_ClockSettings = &Mcu_Hw_ClockSettingConfig[0],
      
   }
};

const Mcu_ConfigType McuConfigData[] = {
   {
      .McuNumberOfMcuModes = 3u,
      .McuRamSectors = 0u,
      .McuClockSettings = 1u,
      .McuDefaultClockSettings = McuConf_McuClockSettingConfig_McuClockSettingConfig, 
      .McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],
#if defined (USE_DEM)
      .McuClockFailure = DEM_EVENT_ID_NULL,
#endif
      .McuRamSectorSettingConfig = NULL
   }
};

