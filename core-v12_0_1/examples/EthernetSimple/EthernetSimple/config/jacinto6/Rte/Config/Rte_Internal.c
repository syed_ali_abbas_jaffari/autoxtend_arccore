#include <Rte_Internal.h>
#include <Rte_Calprms.h>
#include <Rte_Assert.h>
#include <Rte_Fifo.h>
#include <Com.h>
#include <Os.h>
#include <Ioc.h>

/** --- EXTERNALS --------------------------------------------------------------------------- */
extern Std_ReturnType Rte_comM_RequestComMode(/*IN*/ComM_UserHandleType portDefArg1, /*IN*/ComM_ModeType ComMode);
extern Std_ReturnType Rte_comM_GetMaxComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_comM_GetRequestedComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_comM_GetCurrentComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_det_ReportError(/*IN*/uint16 portDefArg1, /*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId);
extern Std_ReturnType Rte_dlt_Dlt_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/
        constUint8Ptr log_data, /*IN*/uint16 log_data_length);
extern Std_ReturnType Rte_dlt_Dlt_SendTraceMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageTraceInfoType * trace_info, /*IN*/
        constUint8Ptr trace_data, /*IN*/uint16 trace_data_length);
extern Std_ReturnType Rte_dlt_Dlt_RegisterContext(/*IN*/Dlt_SessionIDType session_id, /*IN*/const uint8 * app_id, /*IN*/const uint8 * context_id, /*IN*/
        constUint8Ptr app_description, /*IN*/uint8 len_app_description, /*IN*/constUint8Ptr context_description, /*IN*/uint8 len_context_description);
extern Std_ReturnType Rte_ecuM_RequestRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_ReleaseRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_RequestPOSTRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_ReleasePOSTRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_SelectShutdownTarget(/*IN*/EcuM_StateType target, /*IN*/uint8 mode);
extern Std_ReturnType Rte_ecuM_GetShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode);
extern Std_ReturnType Rte_ecuM_GetLastShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode);
extern Std_ReturnType Rte_ecuM_SelectBootTarget(/*IN*/EcuM_BootTargetType target);
extern Std_ReturnType Rte_ecuM_GetBootTarget(/*OUT*/EcuM_BootTargetType * target);
extern Std_ReturnType Rte_ioHwAb_DigitalWrite(/*IN*/IoHwAb_SignalType_ portDefArg1, /*IN*/DigitalLevel Level);
extern Std_ReturnType Rte_ioHwAb_AnalogRead(/*IN*/IoHwAb_SignalType_ portDefArg1, /*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality);

/** --- RTE INTERNAL DATA ------------------------------------------------------------------- */
#define Rte_START_SEC_VAR_INIT_UNSPECIFIED
#include <Rte_MemMap.h>
boolean RteInitialized = FALSE;
#define Rte_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Rte_MemMap.h>

/** === BswM Data ===============================================================
 */
#define BswM_START_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>

ComMModeEnum Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode;
#define BswM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>

/** === ComM Data ===============================================================
 */

#define ComM_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <ComM_MemMap.h>
ComM_ModeMachinesType ComM_ModeMachines;
#define ComM_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <ComM_MemMap.h>

/** === Det Data ===============================================================
 */

/** === Dlt Data ===============================================================
 */

/** === EcuM Data ===============================================================
 */

#define EcuM_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <EcuM_MemMap.h>
EcuM_ModeMachinesType EcuM_ModeMachines;
#define EcuM_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <EcuM_MemMap.h>

/** === IoHwAb Data ===============================================================
 */

/** === SwcReaderType Data ===============================================================
 */

/** === SwcWriterType Data ===============================================================
 */
#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

sint32 Rte_Buffer_SwcWriter_AdcIn_data2;
#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

uint32 Rte_Buffer_SwcWriter_InputPort_data1;
#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

/** --- SERVER ACTIVATIONS ------------------------------------------------------------------ */

/** --- FUNCTIONS --------------------------------------------------------------------------- */
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === BswM ======================================================================= */
/** --- bswm -------------------------------------------------------------------- */

/** ------ modeRequestPort_SwcStartCommunication */
Std_ReturnType Rte_Read_BswM_bswm_modeRequestPort_SwcStartCommunication_requestedMode(/*OUT*/ComMModeEnum * value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Receiver (SwcWriter_ComMControl_to_bswm_modeRequestPort_SwcStartCommunication) */
    {
        SYS_CALL_SuspendOSInterrupts();
        *value = Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode;
        SYS_CALL_ResumeOSInterrupts();
    }

    return retVal;
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === ComM ======================================================================= */
/** --- comM -------------------------------------------------------------------- */

/** ------ UM_ComMUser */
Std_ReturnType Rte_Switch_ComM_comM_UM_ComMUser_currentMode(/*IN*/uint8 mode) {
    if (ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode == RTE_TRANSITION_ComM_comM_UM_ComMUser_currentMode) {
        {
            SYS_CALL_SuspendOSInterrupts();
            ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode = mode;
            SYS_CALL_ResumeOSInterrupts();
        }
        // Activate runnables
        // No runnables to activate
        SYS_CALL_SuspendOSInterrupts();
        ComM_ModeMachines.comM.UM_ComMUser_currentMode.currentMode = ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode;
        ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode = RTE_TRANSITION_ComM_comM_UM_ComMUser_currentMode;
        SYS_CALL_ResumeOSInterrupts();

        return RTE_E_OK;
    } else {
        return RTE_E_LIMIT;
    }
}

/** ------ UR_ComMUser */
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_RequestComMode(/*IN*/ComM_ModeType ComMode) {
    return Rte_comM_RequestComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetMaxComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetMaxComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetRequestedComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetRequestedComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetCurrentComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetCurrentComMode(0, ComMode);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === Det ======================================================================= */
/** --- det -------------------------------------------------------------------- */

/** ------ DS_DetPortReader */
Std_ReturnType Rte_Call_Det_det_DS_DetPortReader_ReportError(/*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId) {
    return Rte_det_ReportError(4096, InstanceId, ApiId, ErrorId);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === Dlt ======================================================================= */
/** --- dlt -------------------------------------------------------------------- */

/** ------ Dlt_service */
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/
        constUint8Ptr log_data, /*IN*/uint16 log_data_length) {
    return Rte_dlt_Dlt_SendLogMessage(session_id, log_info, log_data, log_data_length);
}
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_SendTraceMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageTraceInfoType * trace_info, /*IN*/
        constUint8Ptr trace_data, /*IN*/uint16 trace_data_length) {
    return Rte_dlt_Dlt_SendTraceMessage(session_id, trace_info, trace_data, trace_data_length);
}
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_RegisterContext(/*IN*/Dlt_SessionIDType session_id, /*IN*/const uint8 * app_id, /*IN*/
        const uint8 * context_id, /*IN*/constUint8Ptr app_description, /*IN*/uint8 len_app_description, /*IN*/constUint8Ptr context_description, /*IN*/
        uint8 len_context_description) {
    return Rte_dlt_Dlt_RegisterContext(session_id, app_id, context_id, app_description, len_app_description, context_description,
            len_context_description);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === EcuM ======================================================================= */
/** --- ecuM -------------------------------------------------------------------- */

/** ------ SR_EthernetUser */
Std_ReturnType Rte_Call_EcuM_ecuM_SR_EthernetUser_RequestRUN(void) {
    return Rte_ecuM_RequestRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_EthernetUser_ReleaseRUN(void) {
    return Rte_ecuM_ReleaseRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_EthernetUser_RequestPOSTRUN(void) {
    return Rte_ecuM_RequestPOSTRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_EthernetUser_ReleasePOSTRUN(void) {
    return Rte_ecuM_ReleasePOSTRUN(0);
}

/** ------ bootTarget */
Std_ReturnType Rte_Call_EcuM_ecuM_bootTarget_SelectBootTarget(/*IN*/EcuM_BootTargetType target) {
    return Rte_ecuM_SelectBootTarget(target);
}
Std_ReturnType Rte_Call_EcuM_ecuM_bootTarget_GetBootTarget(/*OUT*/EcuM_BootTargetType * target) {
    return Rte_ecuM_GetBootTarget(target);
}

/** ------ currentMode */
Std_ReturnType Rte_Switch_EcuM_ecuM_currentMode_currentMode(/*IN*/uint8 mode) {
    if (EcuM_ModeMachines.ecuM.currentMode_currentMode.nextMode == RTE_TRANSITION_EcuM_ecuM_currentMode_currentMode) {
        {
            SYS_CALL_SuspendOSInterrupts();
            EcuM_ModeMachines.ecuM.currentMode_currentMode.nextMode = mode;
            SYS_CALL_ResumeOSInterrupts();
        }
        // Activate runnables
        SYS_CALL_SetEvent(TASK_ID_OsRteTask, EVENT_MASK_ModeSwitchOsEvent);

        return RTE_E_OK;
    } else {
        return RTE_E_LIMIT;
    }
}

/** ------ shutdownTarget */
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_SelectShutdownTarget(/*IN*/EcuM_StateType target, /*IN*/uint8 mode) {
    return Rte_ecuM_SelectShutdownTarget(target, mode);
}
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_GetShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode) {
    return Rte_ecuM_GetShutdownTarget(target, mode);
}
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_GetLastShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode) {
    return Rte_ecuM_GetLastShutdownTarget(target, mode);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === IoHwAb ======================================================================= */
/** --- ioHwAb -------------------------------------------------------------------- */

/** ------ Analog_AnalogSignal */
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Analog_AnalogSignal_Read(/*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality) {
    return Rte_ioHwAb_AnalogRead(0, Value, Quality);
}

/** ------ Digital_DigitalSignal_LED1 */
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED1_Write(/*IN*/DigitalLevel Level) {
    return Rte_ioHwAb_DigitalWrite(0, Level);
}

/** ------ Digital_DigitalSignal_LED2 */
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED2_Write(/*IN*/DigitalLevel Level) {
    return Rte_ioHwAb_DigitalWrite(1, Level);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === SwcReaderType ======================================================================= */
/** --- SwcReader -------------------------------------------------------------------- */

/** ------ AdcResult */

Std_ReturnType Rte_Write_SwcReaderType_SwcReader_AdcResult_data2(/*IN*/sint32 value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Sender (SR4) */
    {
        SYS_CALL_AtomicCopy32(Rte_Buffer_SwcWriter_AdcIn_data2, value);

    }

    return retVal;
}

/** ------ AnalogReader */
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_AnalogReader_Read(/*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality) {
    return Rte_Call_IoHwAb_ioHwAb_Analog_AnalogSignal_Read(Value, Quality);
}

/** ------ Blinker */
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Blinker_Write(/*IN*/DigitalLevel Level) {
    return Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED1_Write(Level);
}

/** ------ Det */
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Det_ReportError(/*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId) {
    return Rte_Call_Det_det_DS_DetPortReader_ReportError(InstanceId, ApiId, ErrorId);
}

/** ------ Dlt */
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Dlt_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/
        constUint8Ptr log_data, /*IN*/uint16 log_data_length) {
    return Rte_Call_Dlt_dlt_Dlt_service_SendLogMessage(session_id, log_info, log_data, log_data_length);
}

/** ------ Mode */

/** ------ ReceiverPort */
Std_ReturnType Rte_Read_SwcReaderType_SwcReader_ReceiverPort_data1(/*OUT*/uint32 * value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Receiver (Rx01dataISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */

    retVal |= Com_ReceiveSignal(ComConf_ComSignal_ComSignal_RxUDP, value);

    return retVal;
}

/** ------ ResultPort */

Std_ReturnType Rte_Write_SwcReaderType_SwcReader_ResultPort_data1(/*IN*/uint32 value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Sender (SR3) */
    {
        SYS_CALL_AtomicCopy32(Rte_Buffer_SwcWriter_InputPort_data1, value);

    }

    return retVal;
}

/** ------ RunControl */
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_RunControl_RequestRUN(void) {
    return Rte_Call_EcuM_ecuM_SR_EthernetUser_RequestRUN();
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

/** === SwcWriterType ======================================================================= */
/** --- SwcWriter -------------------------------------------------------------------- */

/** ------ AdcIn */
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_AdcIn_data2(/*OUT*/sint32 * value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Receiver (SR4) */
    {
        SYS_CALL_AtomicCopy32(*value, Rte_Buffer_SwcWriter_AdcIn_data2);
    }

    return retVal;
}

/** ------ Blinker */
Std_ReturnType Rte_Call_SwcWriterType_SwcWriter_Blinker_Write(/*IN*/DigitalLevel Level) {
    return Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED2_Write(Level);
}

/** ------ ComMControl */

Std_ReturnType Rte_Write_SwcWriterType_SwcWriter_ComMControl_requestedMode(/*IN*/ComMModeEnum value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Sender (SwcWriter_ComMControl_to_bswm_modeRequestPort_SwcStartCommunication) */
    {
        SYS_CALL_SuspendOSInterrupts();
        Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode = value;
        SYS_CALL_ResumeOSInterrupts();

    }

    return retVal;
}

/** ------ InputPort */
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_InputPort_data1(/*OUT*/uint32 * value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Receiver (SR3) */
    {
        SYS_CALL_AtomicCopy32(*value, Rte_Buffer_SwcWriter_InputPort_data1);
    }

    return retVal;
}

/** ------ Mode */

/** ------ SenderPort */

Std_ReturnType Rte_Write_SwcWriterType_SwcWriter_SenderPort_data2(/*IN*/sint32 value) {
    Std_ReturnType retVal = RTE_E_OK;

    /* --- Sender (Tx01dataISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */

    retVal |= Com_SendSignal(ComConf_ComSignal_ComSignal_TxUDP, &value);

    return retVal;
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>

#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>
void Rte_Internal_Init_Buffers(void) {
    // Init communication buffers

    // Init mode machine queues

}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
