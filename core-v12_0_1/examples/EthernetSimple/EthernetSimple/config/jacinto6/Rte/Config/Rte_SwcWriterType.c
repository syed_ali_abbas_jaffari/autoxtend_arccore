/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_SwcWriterType.h"

/** === Runnable Prototypes =======================================================================
 */

/** ------ SwcWriter -----------------------------------------------------------------------
 */
void Rte_SwcWriter_SwcWriterRunnable(void);
void Rte_SwcWriter_Init(void);

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */
#define SwcWriterType_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

struct {
    struct {
        struct {
            Rte_DE_sint32 data2;
        } AdcIn;
        struct {
            Rte_DE_uint32 data1;
        } InputPort;
        struct {
            Rte_DE_sint32 data2;
        } SenderPort;

    } SwcWriterRunnable;
    struct {
        struct {
            Rte_DE_ComMModeEnum requestedMode;
        } ComMControl;

    } Init;
} ImplDE_SwcWriter;
#define SwcWriterType_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
const Rte_CDS_SwcWriterType SwcWriterType_SwcWriter = {
    .SwcWriterRunnable_InputPort_data1 = &ImplDE_SwcWriter.SwcWriterRunnable.InputPort.data1,
    .SwcWriterRunnable_AdcIn_data2 = &ImplDE_SwcWriter.SwcWriterRunnable.AdcIn.data2,
    .SwcWriterRunnable_SenderPort_data2 = &ImplDE_SwcWriter.SwcWriterRunnable.SenderPort.data2,
    .Init_ComMControl_requestedMode = &ImplDE_SwcWriter.Init.ComMControl.requestedMode
};
#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
const Rte_Instance Rte_Inst_SwcWriterType = &SwcWriterType_SwcWriter;

#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

/** === Runnables =================================================================================
 */
#define SwcWriterType_START_SEC_CODE
#include <SwcWriterType_MemMap.h>

/** ------ SwcWriter -----------------------------------------------------------------------
 */
void Rte_SwcWriter_SwcWriterRunnable(void) {

    /* PRE */
    Rte_Read_SwcWriterType_SwcWriter_InputPort_data1(&ImplDE_SwcWriter.SwcWriterRunnable.InputPort.data1.value);

    Rte_Read_SwcWriterType_SwcWriter_AdcIn_data2(&ImplDE_SwcWriter.SwcWriterRunnable.AdcIn.data2.value);

    /* MAIN */

    swcWriterRunnable();

    /* POST */
    Rte_Write_SwcWriterType_SwcWriter_SenderPort_data2(ImplDE_SwcWriter.SwcWriterRunnable.SenderPort.data2.value);

}
void Rte_SwcWriter_Init(void) {

    /* PRE */

    /* MAIN */

    SwcWriterInit();

    /* POST */
    Rte_Write_SwcWriterType_SwcWriter_ComMControl_requestedMode(ImplDE_SwcWriter.Init.ComMControl.requestedMode.value);

}
#define SwcWriterType_STOP_SEC_CODE
#include <SwcWriterType_MemMap.h>

