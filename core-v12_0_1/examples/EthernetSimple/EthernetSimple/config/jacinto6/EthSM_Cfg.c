
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.2.2
 */

#include "EthSM.h"
#if ETHSM_DUMMY_MODE != STD_ON
#include "EthIf.h"
#include "EthIf_PBcfg.h"
#endif

static const EthSM_NetworkConfigType EthSMNetworks[] = {
	{
		.EthIfControllerId  		    = 0,	/* No reference, assigned by default */
		.ComMNetworkHandle 				= ComMConf_ComMChannel_ComMChannel,        		
#if defined(USE_DEM) 
		.ETHSM_E_LINK_DOWN              = DEM_EVENT_ID_NULL,
#endif
		.EthTrcvAvailable		        = STD_OFF
	},
};


static const EthSM_ConfigSetType EthSMConfigSet = {
	.NofNetworks =              1,
	.Networks =                 EthSMNetworks,
};
/* @req 4.2.2/SWS_EthSM_00061 */
/* @req 4.2.2/SWS_EthSM_00081 */
const EthSM_ConfigType EthSMConfig = {	
	.ConfigSet =                &EthSMConfigSet,
};

