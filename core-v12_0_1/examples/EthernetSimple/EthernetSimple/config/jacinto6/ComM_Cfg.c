/*
 * Generator version: 2.2.1
 * AUTOSAR version:   4.0.3
 */

#include "ComM.h"

#if defined(USE_CANSM)
#include "CanSM.h"
#endif

#if defined(USE_NM)
#include "Nm.h" 
#endif


#if defined(USE_LINSM)
#include "LinSM.h"
#endif

#if defined(USE_ETHSM)
#include "EthSM.h"
#endif

const ComM_ChannelType ComM_Channels[COMM_CHANNEL_COUNT] = {
   {
      .BusType = COMM_BUS_TYPE_ETH,
      .ComMChannelId = ComMConf_ComMChannel_ComMChannel,
      .NmVariant = COMM_NM_VARIANT_NONE,
      .MainFunctionPeriod = 10,
      .LightTimeout = 10000,
      .PncGatewayType		= COMM_GATEWAY_TYPE_NONE
   }
};


const ComM_ChannelType* ComM_ComMUser_Channels[] = {
	&ComM_Channels[0] 
};


 


const ComM_UserType ComM_Users[COMM_USER_COUNT] = {
   { 
   	  /** @req ComM995 */
      .ChannelList 		= ComM_ComMUser_Channels,
      .ChannelCount 	= 1,
      .PncChnlList 		= NULL, 
      .PncChnlCount		= 0,
   }
};
const ComM_ConfigType ComM_Config = {
   .Channels 		= ComM_Channels,
   .Users 			= ComM_Users,
   .ComMPncConfig	= NULL
 };
 
  
 
 
extern void ComM_MainFunction(NetworkHandleType Channel);
 
/* @req COMM818 */
void ComM_MainFunction_ComMChannel(void) {
   ComM_MainFunction(ComMConf_ComMChannel_ComMChannel);
}
      


/** @req ComM986 */
/** @req ComM971 */

