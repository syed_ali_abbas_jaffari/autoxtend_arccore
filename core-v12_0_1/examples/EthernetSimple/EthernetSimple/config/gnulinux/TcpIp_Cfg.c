
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.2.2
 */

#include "TcpIp.h"
#if defined(USE_ETHIF)
#include "EthIf.h"
#include "EthIf_PBcfg.h"
#else
#include "ethernetif.h"
#endif




const TcpIp_AddressAssignmentType TcpIp_AddrAssignList_TcpIpLocalAddr[] = {
	{
		.AssignmentLifetime = TCPIP_FORGET,
		.AssignmentMethod = TCPIP_STATIC,
		.AssignmentPriority = 1,
		.AssignmentTrigger = TCPIP_AUTOMATIC,
	},
};
TcpIp_StaticIpAddressConfigType TcpIp_StaticIpAddressConfig_TcpIpLocalAddr = {
    .ArcValid = TRUE,
	.StaticIpAddress = IP4_ADDR_CHR_TO_UINT32(192,168,0,10),		
	.DefaultRouter = IP4_ADDR_CHR_TO_UINT32(192,168,0,1),
	.Netmask = 24,		
};
const TcpIp_AddressAssignmentType TcpIp_AddrAssignList_TcpIpLocalAddrMult[] = {
	{
		.AssignmentLifetime = TCPIP_FORGET,
		.AssignmentMethod = TCPIP_STATIC,
		.AssignmentPriority = 3,
		.AssignmentTrigger = TCPIP_MANUAL,
	},
};

TcpIp_StaticIpAddressConfigType TcpIp_StaticIpAddressConfig_TcpIpLocalAddrMult = {
	.ArcValid = FALSE,
	.StaticIpAddress = 0,		
	.DefaultRouter = 0,
	.Netmask = 0,
};
const TcpIp_AddressAssignmentType TcpIp_AddrAssignList_TcpIpLocalAddrMultAuto[] = {
	{
		.AssignmentLifetime = TCPIP_FORGET,
		.AssignmentMethod = TCPIP_STATIC,
		.AssignmentPriority = 2,
		.AssignmentTrigger = TCPIP_AUTOMATIC,
	},
};
TcpIp_StaticIpAddressConfigType TcpIp_StaticIpAddressConfig_TcpIpLocalAddrMultAuto = {
    .ArcValid = TRUE,
	.StaticIpAddress = IP4_ADDR_CHR_TO_UINT32(225,0,0,1),		
	.DefaultRouter = 0,
	.Netmask = 24,
};

const TcpIp_LocalAddrType TcpIp_LocalAddrList[] = {
	{
		// Name : "TcpIpLocalAddr"
     	.AddressId = 1u,
     	.AddressType = TCPIP_UNICAST,
     	.Domain = TCPIP_AF_INET,
		.TcpIpCtrlRef = TcpIpConf_TcpIpCtrl_TcpIpCtrl,
	 	.AddressAssignment = TcpIp_AddrAssignList_TcpIpLocalAddr,
	 	.StaticIpAddrConfig = &TcpIp_StaticIpAddressConfig_TcpIpLocalAddr,
	},
	{
		// Name : "TcpIpLocalAddrMult"
     	.AddressId = 2u,
     	.AddressType = TCPIP_MULTICAST,
     	.Domain = TCPIP_AF_INET,
		.TcpIpCtrlRef = TcpIpConf_TcpIpCtrl_TcpIpCtrl,
	 	.AddressAssignment = TcpIp_AddrAssignList_TcpIpLocalAddrMult,
	 	.StaticIpAddrConfig = &TcpIp_StaticIpAddressConfig_TcpIpLocalAddrMult,
	},
	{
		// Name : "TcpIpLocalAddrMultAuto"
     	.AddressId = 3u,
     	.AddressType = TCPIP_MULTICAST,
     	.Domain = TCPIP_AF_INET,
		.TcpIpCtrlRef = TcpIpConf_TcpIpCtrl_TcpIpCtrl,
	 	.AddressAssignment = TcpIp_AddrAssignList_TcpIpLocalAddrMultAuto,
	 	.StaticIpAddrConfig = &TcpIp_StaticIpAddressConfig_TcpIpLocalAddrMultAuto,
	},
};



const TcpIp_TcpIpCtrlType TcpIp_CtrlList[] = {
	{
		// Name : "TcpIpCtrl"
	
		.FramePrioDefault = 0,
	
		.EthIfCtrlRef = 0,
#if defined(USE_DEM)
		/** @req 4.2.2/SWS_TCPIP_00043 */	
		/** @req 4.2.2/SWS_TCPIP_00207 */
		/** @req 4.2.2/SWS_TCPIP_00208 */
		/** @req 4.2.2/SWS_TCPIP_00209 */
		/** @req 4.2.2/SWS_TCPIP_00210 */
		.CtrlDemEventParameterRefs = 	{
								.ConnRefused = DEM_EVENT_ID_NULL,
								.HostUnreach = DEM_EVENT_ID_NULL,
								.PacketToBig = DEM_EVENT_ID_NULL,
								.Timedout = DEM_EVENT_ID_NULL,
							},
#endif

	},
};


const TcpIp_SocketOwnerType TcpIpSocketOwnerList[] = {
	{
		// Name : "SoAd"
		.SocketOwnerCopyTxDataFncPtr = SoAd_CopyTxData,
		.SocketOwnerLocalIpAddrAssignmentChgFncPtr = SoAd_LocalIpAddrAssignmentChg,
		.SocketOwnerRxIndicationFncPtr = SoAd_RxIndication,
		.SocketOwnerTcpAcceptedFncPtr = SoAd_TcpAccepted,
		.SocketOwnerTcpConnectedFncPtr = SoAd_TcpConnected,
		.SocketOwnerTcpIpEventFncPtr = SoAd_TcpIpEvent,
		.SocketOwnerTxConfirmationFncPtr = SoAd_TxConfirmation,
		.SocketOwnerUpperLayerType = TCPIP_SOAD,
	},
};

const TcpIp_ConfigType TcpIp_Config = {
	.Config = {
		.CtrlList = TcpIp_CtrlList,
		.LocalAddrList = TcpIp_LocalAddrList,
		.PhysAddrConfig = NULL,
		.SocketOwnerConfig = {
			.SocketOwnerList = TcpIpSocketOwnerList
		}
	}
};

