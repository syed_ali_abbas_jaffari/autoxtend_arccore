
#include <Rte_Internal.h>
#include <Rte_Fifo.h>
#include <Rte_Calprms.h>

extern ComM_ModeMachinesType ComM_ModeMachines;
extern Dcm_ModeMachinesType Dcm_ModeMachines;
extern EcuM_ModeMachinesType EcuM_ModeMachines;

extern boolean RteInitialized;

/** === Lifecycle API =============================================================================
 */
Std_ReturnType Rte_Start(void) {
    // Initialize calibration parameters
    Rte_Init_Calprms();

    // Initialize buffers
    Rte_Internal_Init_Buffers();

    // Initialize port status

    // Initialize update flags

    // Initialize mode machines
    ComM_ModeMachines.comM.UM_ComMUser_currentMode.currentMode = 1;
    ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode = 3;
    ComM_ModeMachines.comM.UM_ComMUser_currentMode.previousMode = 3;
    ComM_ModeMachines.comM.UM_ComMUser_currentMode.transitionCompleted = false;
    Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.currentMode = 5;
    Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.nextMode = 7;
    Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.previousMode = 7;
    Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.transitionCompleted = false;
    Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.currentMode = 1;
    Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.nextMode = 2;
    Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.previousMode = 2;
    Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.transitionCompleted = false;
    Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.currentMode = 1;
    Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.nextMode = 2;
    Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.previousMode = 2;
    Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.transitionCompleted = false;
    Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.currentMode = 0;
    Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.nextMode = 3;
    Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.previousMode = 3;
    Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.transitionCompleted = false;
    EcuM_ModeMachines.ecuM.currentMode_currentMode.currentMode = 4;
    EcuM_ModeMachines.ecuM.currentMode_currentMode.nextMode = 6;
    EcuM_ModeMachines.ecuM.currentMode_currentMode.previousMode = 6;
    EcuM_ModeMachines.ecuM.currentMode_currentMode.transitionCompleted = false;

    RteInitialized = true;
    return RTE_E_OK;
}

Std_ReturnType Rte_Stop(void) {
    RteInitialized = false;
    return RTE_E_OK;
}

