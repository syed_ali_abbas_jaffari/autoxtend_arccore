
MOD_USE += RTE

obj-y += Rte.o
obj-y += Rte_Main.o
obj-y += Rte_Calprms.o
obj-y += Rte_Internal.o
obj-y += Rte_Callbacks.o
obj-y += Rte_Fifo.o
obj-y += Rte_BswM.o
obj-y += Rte_ComM.o
obj-y += Rte_Dcm.o
obj-y += Rte_Det.o
obj-y += Rte_Dlt.o
obj-y += Rte_EcuM.o
obj-y += Rte_IoHwAb.o
obj-y += Rte_SwcReaderType.o
obj-y += Rte_SwcWriterType.o
