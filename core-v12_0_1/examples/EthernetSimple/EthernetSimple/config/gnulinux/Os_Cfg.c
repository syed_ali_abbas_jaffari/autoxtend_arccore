
/*
 * Generator version: 1.0.1
 * AUTOSAR version:   4.0.3
 */

#include "os_i.h"


// ###############################    EXTERNAL REFERENCES    #############################
 
/* Application externals */




/* Interrupt externals */

// Set the os tick frequency
OsTickType OsTickFreq = 1000;


// ###############################    DEBUG OUTPUT     #############################
uint32 os_dbg_mask =  D_TASK | D_ALARM;                     


// ###############################    APPLICATIONS     #############################
GEN_APPLICATION_HEAD = {

	GEN_APPLICATION(
				/* id           */ APPLICATION_ID_OsApplication,
				/* name         */ "OsApplication",
				/* trusted      */ true,
				/* core         */ 0,
				/* StartupHook  */ NULL,
				/* ShutdownHook */ NULL,
				/* ErrorHook    */ NULL,
				/* rstrtTaskId  */ 0 // NOT CONFIGURABLE IN TOOLS (OsTasks.indexOf(app.Os RestartTask.value))
				)

};


 


// #################################    COUNTERS     ###############################

GEN_COUNTER_HEAD = {
	GEN_COUNTER(
				/* id          */		COUNTER_ID_OsCounter,
				/* name        */		"OsCounter",
				/* counterType */		COUNTER_TYPE_HARD,
				/* counterUnit */		COUNTER_UNIT_NANO,
				/* maxAllowed  */		0xffff,
				/*             */		1,
				/* minCycle    */		1,
				/*             */		0,
				/* owningApp   */		APPLICATION_ID_OsApplication,
				/* accAppMask..*/       (1 << APPLICATION_ID_OsApplication)
						                | (1 << APPLICATION_ID_OsApplication) 
 ) 
};




CounterType Os_Arc_OsTickCounter = COUNTER_ID_OsCounter;




// ##################################    ALARMS     ################################

GEN_ALARM_AUTOSTART(
				ALARM_ID_OsAlarmBswService,
				ALARM_AUTOSTART_RELATIVE,
				5,
				5,
				OSDEFAULTAPPMODE );

GEN_ALARM_AUTOSTART(
				ALARM_ID_OsAlarm100ms,
				ALARM_AUTOSTART_RELATIVE,
				100,
				100,
				OSDEFAULTAPPMODE );

GEN_ALARM_AUTOSTART(
				ALARM_ID_OsAlarm1000ms,
				ALARM_AUTOSTART_RELATIVE,
				1000,
				1000,
				OSDEFAULTAPPMODE );





GEN_ALARM_HEAD = {

	GEN_ALARM(	ALARM_ID_OsAlarmLWIPMain,
				"OsAlarmLWIPMain",
				COUNTER_ID_OsCounter,
				NULL,
				ALARM_ACTION_SETEVENT,
				TASK_ID_MainLWIPTask,
				EVENT_MASK_LWIP_MAIN,
				0,
				APPLICATION_ID_OsApplication, /* Application owner */
				(( 1 << APPLICATION_ID_OsApplication ) 
				| (1 << APPLICATION_ID_OsApplication) 
				) /* Accessing application mask */
			)
,
	GEN_ALARM(	ALARM_ID_OsAlarmBswService,
				"OsAlarmBswServic",
				COUNTER_ID_OsCounter,
				GEN_ALARM_AUTOSTART_NAME(ALARM_ID_OsAlarmBswService),
				ALARM_ACTION_ACTIVATETASK,
				TASK_ID_OsBswServiceTask,
				0,
				0,
				APPLICATION_ID_OsApplication, /* Application owner */
				(( 1 << APPLICATION_ID_OsApplication ) 
				| (1 << APPLICATION_ID_OsApplication) 
				) /* Accessing application mask */
			)
,
	GEN_ALARM(	ALARM_ID_OsAlarm100ms,
				"OsAlarm100ms",
				COUNTER_ID_OsCounter,
				GEN_ALARM_AUTOSTART_NAME(ALARM_ID_OsAlarm100ms),
				ALARM_ACTION_SETEVENT,
				TASK_ID_OsRteTask,
				EVENT_MASK_OsRteEvent1,
				0,
				APPLICATION_ID_OsApplication, /* Application owner */
				(( 1 << APPLICATION_ID_OsApplication ) 
				| (1 << APPLICATION_ID_OsApplication) 
				) /* Accessing application mask */
			)
,
	GEN_ALARM(	ALARM_ID_OsAlarm1000ms,
				"OsAlarm1000ms",
				COUNTER_ID_OsCounter,
				GEN_ALARM_AUTOSTART_NAME(ALARM_ID_OsAlarm1000ms),
				ALARM_ACTION_SETEVENT,
				TASK_ID_OsRteTask,
				EVENT_MASK_OsRteEvent2,
				0,
				APPLICATION_ID_OsApplication, /* Application owner */
				(( 1 << APPLICATION_ID_OsApplication ) 
				| (1 << APPLICATION_ID_OsApplication) 
				) /* Accessing application mask */
			)
,
	GEN_ALARM(	ALARM_ID_OsAlarm1msTimer,
				"OsAlarm1msTimer",
				COUNTER_ID_OsCounter,
				NULL,
				ALARM_ACTION_SETEVENT,
				TASK_ID_tcpip_task,
				EVENT_MASK_OsEvent1,
				0,
				APPLICATION_ID_OsApplication, /* Application owner */
				(( 1 << APPLICATION_ID_OsApplication ) 
				| (1 << APPLICATION_ID_OsApplication) 
				) /* Accessing application mask */
			)

};

 
// ################################    RESOURCES     ###############################


// ##############################    STACKS (TASKS)     ############################

DECLARE_STACK(OsIdle, OS_OSIDLE_STACK_SIZE);


DECLARE_STACK(MainLWIPTask,  2048);
DECLARE_STACK(OsBswServiceTask,  2048);
DECLARE_STACK(OsRteTask,  2048);
DECLARE_STACK(OsStartupTask,  1024);
DECLARE_STACK(tcpip_task,  3072);


// ##################################    TASKS     #################################


// Linker symbols defined for Non-Trusted Applications

GEN_TASK_HEAD = {
	
	{
	.pid = TASK_ID_OsIdle,
	.name = "OsIdle",
	.entry = OsIdle,
	.prio = 0,
	.scheduling = FULL,
	.autostart = TRUE,
	.proc_type = PROC_BASIC,
	.stack = {
		.size = sizeof stack_OsIdle,
		.top = stack_OsIdle,
	},
	.resourceAccess = 0,
	.activationLimit = 1,
	.applOwnerId = OS_CORE_0_MAIN_APPLICATION,
	.accessingApplMask = (1u << OS_CORE_0_MAIN_APPLICATION),
	},
	



{
	.pid = TASK_ID_MainLWIPTask,
	.name = "MainLWIPTask",
	.entry = MainLWIPTask,
	.prio = 3,
	.scheduling = FULL,
	.proc_type = PROC_EXTENDED,
	.stack = {
		.size = sizeof stack_MainLWIPTask,
		.top = stack_MainLWIPTask,
	},
	.autostart = TRUE,
	.resourceAccess = 0 , 
	.activationLimit = 1,
	.eventMask = 0 | EVENT_MASK_LWIP_MAIN ,
	
	.applOwnerId = APPLICATION_ID_OsApplication,
	.accessingApplMask = (1u <<APPLICATION_ID_OsApplication)
                      | (1u << APPLICATION_ID_OsApplication) ,
},
		

{
	.pid = TASK_ID_OsBswServiceTask,
	.name = "OsBswServiceTask",
	.entry = OsBswServiceTask,
	.prio = 5,
	.scheduling = FULL,

	.proc_type = PROC_BASIC,
	.stack = {
		.size = sizeof stack_OsBswServiceTask,
		.top = stack_OsBswServiceTask,
	},
	.autostart = TRUE,
	.resourceAccess = 0 , 
	.activationLimit = 1,
	.eventMask = 0 ,
	
	.applOwnerId = APPLICATION_ID_OsApplication,
	.accessingApplMask = (1u <<APPLICATION_ID_OsApplication)
                      | (1u << APPLICATION_ID_OsApplication) ,
},
		

{
	.pid = TASK_ID_OsRteTask,
	.name = "OsRteTask",
	.entry = OsRteTask,
	.prio = 2,
	.scheduling = FULL,
	.proc_type = PROC_EXTENDED,
	.stack = {
		.size = sizeof stack_OsRteTask,
		.top = stack_OsRteTask,
	},
	.autostart = TRUE,
	.resourceAccess = 0 , 
	.activationLimit = 1,
	.eventMask = 0 | EVENT_MASK_OsRteEvent1 | EVENT_MASK_OsRteEvent2 | EVENT_MASK_ModeSwitchOsEvent ,
	
	.applOwnerId = APPLICATION_ID_OsApplication,
	.accessingApplMask = (1u <<APPLICATION_ID_OsApplication)
                      | (1u << APPLICATION_ID_OsApplication) ,
},
		

{
	.pid = TASK_ID_OsStartupTask,
	.name = "OsStartupTask",
	.entry = OsStartupTask,
	.prio = 4,
	.scheduling = FULL,
	.proc_type = PROC_EXTENDED,
	.stack = {
		.size = sizeof stack_OsStartupTask,
		.top = stack_OsStartupTask,
	},
	.autostart = TRUE,
	.resourceAccess = 0 , 
	.activationLimit = 1,
	.eventMask = 0 | EVENT_MASK_OsEvent1 | EVENT_MASK_OsEvent10 | EVENT_MASK_OsEvent11 | EVENT_MASK_OsEvent12 | EVENT_MASK_OsEvent13 | EVENT_MASK_OsEvent14 | EVENT_MASK_OsEvent15 | EVENT_MASK_OsEvent16 | EVENT_MASK_OsEvent17 | EVENT_MASK_OsEvent18 | EVENT_MASK_OsEvent19 | EVENT_MASK_OsEvent2 | EVENT_MASK_OsEvent20 | EVENT_MASK_OsEvent21 | EVENT_MASK_OsEvent22 | EVENT_MASK_OsEvent23 | EVENT_MASK_OsEvent24 | EVENT_MASK_OsEvent25 | EVENT_MASK_OsEvent3 | EVENT_MASK_OsEvent4 | EVENT_MASK_OsEvent5 | EVENT_MASK_OsEvent6 | EVENT_MASK_OsEvent7 | EVENT_MASK_OsEvent8 | EVENT_MASK_OsEvent9 ,
	
	.applOwnerId = APPLICATION_ID_OsApplication,
	.accessingApplMask = (1u <<APPLICATION_ID_OsApplication)
                      | (1u << APPLICATION_ID_OsApplication) ,
},
		

{
	.pid = TASK_ID_tcpip_task,
	.name = "tcpip_task",
	.entry = tcpip_task,
	.prio = 4,
	.scheduling = FULL,
	.proc_type = PROC_EXTENDED,
	.stack = {
		.size = sizeof stack_tcpip_task,
		.top = stack_tcpip_task,
	},
	.autostart = TRUE,
	.resourceAccess = 0 , 
	.activationLimit = 1,
	.eventMask = 0 | EVENT_MASK_OsEvent1 | EVENT_MASK_OsEvent10 | EVENT_MASK_OsEvent11 | EVENT_MASK_OsEvent12 | EVENT_MASK_OsEvent13 | EVENT_MASK_OsEvent14 | EVENT_MASK_OsEvent15 | EVENT_MASK_OsEvent16 | EVENT_MASK_OsEvent17 | EVENT_MASK_OsEvent18 | EVENT_MASK_OsEvent19 | EVENT_MASK_OsEvent2 | EVENT_MASK_OsEvent20 | EVENT_MASK_OsEvent21 | EVENT_MASK_OsEvent22 | EVENT_MASK_OsEvent23 | EVENT_MASK_OsEvent24 | EVENT_MASK_OsEvent25 | EVENT_MASK_OsEvent3 | EVENT_MASK_OsEvent4 | EVENT_MASK_OsEvent5 | EVENT_MASK_OsEvent6 | EVENT_MASK_OsEvent7 | EVENT_MASK_OsEvent8 | EVENT_MASK_OsEvent9 | EVENT_MASK_START_TCPIP ,
	
	.applOwnerId = APPLICATION_ID_OsApplication,
	.accessingApplMask = (1u <<APPLICATION_ID_OsApplication)
,
},
		

};

// ##################################    HOOKS     #################################
GEN_HOOKS( 
	StartupHook, 
	NULL,
	ShutdownHook, 
 	ErrorHook,
 	NULL,
 	NULL 
);

// ##################################    ISRS     ##################################



GEN_ISR_MAP = {
  0
};

// ############################    SCHEDULE TABLES     #############################


 
 // ############################    SPINLOCKS     ##################################



