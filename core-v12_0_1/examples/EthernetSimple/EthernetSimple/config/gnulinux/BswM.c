
/*
 * Generator version: 3.2.0
 * AUTOSAR version:   4.0.3
 */


#include "BswM.h"
#include "BswM_Internal.h"
#include "MemMap.h"
//#include "SchM_BswM.h"
#if defined(USE_COMM)
#include "ComM.h"
#endif
#if defined(USE_PDUR)
#include "PduR.h"
#endif
#if defined(USE_NVM)
#include "NvM.h"
#endif
#if defined(USE_CANSM)
#include "CanSM.h"
#include "CanSM_BswM.h"
#endif
#if defined(USE_SD)
#include "SD.h"
#endif
#if defined(USE_FRSM)
#include "FrSM.h"
#endif
#if defined(USE_LINSM)
#include "LinSM.h"
#endif
#if defined(USE_ETHSM)
#include "EthSM.h"
#endif
#if defined(USE_COM)
#include "Com.h"
#endif
#if defined(USE_LINIF)
#include "LinIf.h"
#endif
#include "EcuM.h"
#if defined(USE_DCM)
#include "Dcm.h"
#endif
#if defined(USE_DET)
#include "Det.h"
#endif
#if defined(USE_DEM)
#include "Dem.h"
#endif
#if defined(USE_NM)
#include "Nm.h"
#endif

#include "Rte_BswM.h"
#include "SchM_BswM.h"

#define IS_VALID_COMM_MODE(_x)	((COMM_FULL_COMMUNICATION == (_x)) || (COMM_SILENT_COMMUNICATION == (_x)) || (COMM_NO_COMMUNICATION == (_x)))


	

const BswM_ConfigType BswM_Config;

BswM_InternalType BswM_Internal = {
		.InitStatus = BSWM_UNINIT,
};

static Com_IpduGroupVector BswM_ComIpduGroupVector;
static boolean BswM_ComIpduInitialize;
static boolean BswM_PduGroupSwitchActionPerformed;


/*=============================== Mode types ================================*/


static ComM_ModeType BswComMIndicationMode = BSWM_UNDEFINED_REQUEST_VALUE;
static uint8 SwcStartCommunicationMode = BSWM_UNDEFINED_REQUEST_VALUE;


/*================================== Rules ==================================*/

static BswM_RuleState BswM_RuleStates[] = {
	BSWM_FALSE,
	BSWM_FALSE,
};


static void EnablePduGroupsRule( void ) {
	if ((BswComMIndicationMode == COMM_FULL_COMMUNICATION)) {
		if (BswM_RuleStates[ENABLEPDUGROUPSRULE] != BSWM_TRUE) {
			EnablePdusActions();
		}
		BswM_RuleStates[ENABLEPDUGROUPSRULE] = BSWM_TRUE;
	} else {
		BswM_RuleStates[ENABLEPDUGROUPSRULE] = BSWM_FALSE;
	}

}

static void StartCommunicationRule( void ) {
	if ((SwcStartCommunicationMode != COMM_NO_COMMUNICATION)) {
		if (BswM_RuleStates[STARTCOMMUNICATIONRULE] != BSWM_TRUE) {
			StartCommuncationActions();
		}
		BswM_RuleStates[STARTCOMMUNICATIONRULE] = BSWM_TRUE;
	} else {
		BswM_RuleStates[STARTCOMMUNICATIONRULE] = BSWM_FALSE;
	}

}


/*============================== Action lists ===============================*/

static void EnablePdusActions( void ) {
	Com_SetIpduGroup(BswM_ComIpduGroupVector, ComConf_ComIPduGroup_ComIPduGroupRx, TRUE);
	Com_SetIpduGroup(BswM_ComIpduGroupVector, ComConf_ComIPduGroup_ComIPduGroupTx, TRUE);
	BswM_ComIpduInitialize = FALSE;
	BswM_PduGroupSwitchActionPerformed = TRUE;
}

static void StartCommuncationActions( void ) {
    ComM_RequestComMode(ComMConf_ComMUser_ComMUser, COMM_FULL_COMMUNICATION);
}

/*======================= Delayed request processing ========================*/

#define BSWM_DELAYED_REQUESTS_ARRAY_SIZE                      0
#define BSWM_IMMEDIATE_REQUESTS_EVALUATED_RULES_ARRAY_SIZE    0




static boolean BswM_RequestProcessingOngoing = false;



static inline boolean BswM_AtemptToProcessRequest ( void ) {
	boolean currentState = BswM_RequestProcessingOngoing;
	if (BswM_RequestProcessingOngoing == false) {
		BswM_RequestProcessingOngoing = true;
	}
	return !currentState;
}

static inline void BswM_EndRequestProcessing ( void ) {
	BswM_RequestProcessingOngoing = false;
}


/*=========================== Immediate functions ===========================*/


/*============================== Main function ==============================*/

void BswM_MainFunction( void ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_MAINFUNCTION);

	static boolean firstMainFunctionExecution = TRUE;
	/* The Com module is initialzed after the BswM so this must be done in the main funciton
	 * and not in BswMInit()
	 */
	if (firstMainFunctionExecution) {
		Com_ClearIpduGroupVector(BswM_ComIpduGroupVector);
		firstMainFunctionExecution = FALSE;
	}

	boolean requestCanBeProcessed;	

	SchM_Enter_BswM_EA_0();
	requestCanBeProcessed = BswM_AtemptToProcessRequest();
	SchM_Exit_BswM_EA_0();

    Rte_Read_modeRequestPort_SwcStartCommunication_requestedMode(&SwcStartCommunicationMode);



	EnablePduGroupsRule();
	StartCommunicationRule();

	if (BswM_PduGroupSwitchActionPerformed) {
		Com_IpduGroupControl(BswM_ComIpduGroupVector, BswM_ComIpduInitialize);
		BswM_PduGroupSwitchActionPerformed = FALSE;
	}

	if (requestCanBeProcessed) {
#if BSWM_DELAYED_REQUESTS_ARRAY_SIZE > 0
		BswM_ProcessDelayedRequests();
#endif
		BswM_EndRequestProcessing();
	}
}


/*================================== APIs ===================================*/

/* !req BswM0002 */
void BswM_Init( const BswM_ConfigType* ConfigPtr ) {
    (void)ConfigPtr; /*lint !e920 Avoid compiler warning (variable not used) */
	BswM_Internal.InitStatus = BSWM_INIT;

	BswM_ComIpduInitialize = FALSE;
	BswM_PduGroupSwitchActionPerformed = FALSE;
}

/* !req BswM0119 */
void BswM_Deinit( void ) {
	BswM_Internal.InitStatus = BSWM_UNINIT;
}


#if ( BSWM_VERSION_INFO_API == STD_ON )
void BswM_GetVersionInfo( Std_VersionInfoType* VersionInfo ) {
	BSWM_VALIDATE_POINTER_NORV(VersionInfo, BSWM_SERVICEID_GETVERSIONINFO);

	STD_GET_VERSION_INFO(VersionInfo,BSWM);
}
#endif /* BSWM_VERSION_INFO_API */


/* @req BswM0047 */
void BswM_ComM_CurrentMode( NetworkHandleType Network,
                            ComM_ModeType RequestedMode ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_COMMCURRENTMODE); /* @req BswM00078 */
	BSWM_VALIDATE_REQUESTMODE_NORV(IS_VALID_COMM_MODE(RequestedMode), BSWM_SERVICEID_COMMCURRENTMODE); /* @req BswM00091 */

	switch (Network) {
	case ComMConf_ComMChannel_ComMChannel:
		BswComMIndicationMode = RequestedMode;
		break;
	default:
		break;
	}
}

/* @req BswM00148 */
void BswM_ComM_CurrentPNCMode( PNCHandleType PNC,
                               ComM_PncModeType CurrentPncMode ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_COMMCURRENTPNCMODE); /* @req BswM00149 */

	(void)PNC;
	(void)CurrentPncMode;
}

/* @req BswM0056 */
void BswM_EcuM_CurrentState( EcuM_StateType CurrentState ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ECUMCURRENTSTATE); /* @req BswM00084 */
	/* !req BswM00103 */

	(void)CurrentState;
}

/* @req BswM00131 */
void BswM_EcuM_CurrentWakeup( EcuM_WakeupSourceType source,
                              EcuM_WakeupStatusType state ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ECUMCURRENTWAKEUP); /* @req BswM00132 */
	/* !req BswM00133 */

	(void)source;
	(void)state;
}

/* !req BswM00050 */
void BswM_EthSM_CurrentState( NetworkHandleType Network,
                              EthSM_NetworkModeStateType CurrentState ) {
	BSWM_VALIDATE_INIT_NORV(BSWM_SERVICEID_ETHSMCURRENTSTATE); /* @req BswM00081 */
	/* !req BswM00097 */
	(void)Network;
	(void)CurrentState;
}



