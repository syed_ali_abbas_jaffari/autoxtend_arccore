/*
 * Generator version: 3.2.0
 * AUTOSAR version:   4.0.3
 */

#ifndef BSWM_INTERNAL_H_
#define BSWM_INTERNAL_H_

typedef enum {
	BSWM_INIT,
	BSWM_UNINIT
} BswM_InitStatusType;

typedef struct {
	BswM_InitStatusType InitStatus;
} BswM_InternalType;

#if (BSWM_DEV_ERROR_DETECT == STD_ON)
#define BSWM_DET_REPORTERROR(serviceId, errorId)			\
	(void)Det_ReportError(MODULE_ID_BSWM, 0, serviceId, errorId)

#define BSWM_VALIDATE(expression, serviceId, errorId, ret)	\
	if (!(expression)) {									\
		BSWM_DET_REPORTERROR(serviceId, errorId);			\
		return ret;									\
	}

#define BSWM_VALIDATE_NORV(expression, serviceId, errorId)	\
	if (!(expression)) {									\
		BSWM_DET_REPORTERROR(serviceId, errorId);			\
		return;									\
	}

#else
#define BSWM_DET_REPORTERROR(...)
#define BSWM_VALIDATE(...)
#define BSWM_VALIDATE_NORV(...)
#endif

#define BSWM_VALIDATE_INIT(serviceID)					\
		BSWM_VALIDATE((BswM_Internal.InitStatus == BSWM_INIT), serviceID, BSWM_E_NO_INIT, 0, E_NOT_OK)

#define BSWM_VALIDATE_INIT_NORV(serviceID)					\
		BSWM_VALIDATE_NORV((BswM_Internal.InitStatus == BSWM_INIT), serviceID, BSWM_E_NO_INIT)

#define BSWM_VALIDATE_REQUESTMODE(expression, serviceID)					\
		BSWM_VALIDATE(expression, serviceID, BSWM_E_REQ_MODE_OUT_OF_RANGE, E_NOT_OK)

#define BSWM_VALIDATE_REQUESTMODE_NORV(expression, serviceID)					\
		BSWM_VALIDATE_NORV(expression, serviceID, BSWM_E_REQ_MODE_OUT_OF_RANGE)

#define BSWM_VALIDATE_POINTER_NORV(pointer, serviceID)					\
		BSWM_VALIDATE_NORV((pointer != NULL), serviceID, BSWM_E_PARAM_POINTER)

/*================================== Rules ==================================*/
typedef enum
{
	BSWM_UNDEFINED,
	BSWM_TRUE,
	BSWM_FALSE,
} BswM_RuleState;


#define ENABLEPDUGROUPSRULE 0
#define STARTCOMMUNICATIONRULE 1

static void EnablePduGroupsRule( void );
static void StartCommunicationRule( void );


/*============================== Action lists ===============================*/
static void EnablePdusActions( void );
static void StartCommuncationActions( void );

/*============================ Immediate requests ===========================*/
#define BSWM_UNDEFINED_REQUEST_VALUE 0xFF

#if 0
/* Not used */
static void ProcessQueuedRequests( void );
#endif


#endif /* BSWM_INTERNAL_H_ */

