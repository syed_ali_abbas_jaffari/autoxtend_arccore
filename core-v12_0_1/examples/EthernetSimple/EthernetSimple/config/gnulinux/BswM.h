/*
 * Generator version: 3.2.0
 * AUTOSAR version:   4.0.3
 */

#ifndef BSWM_H_
#define BSWM_H_


#include "Std_Types.h"
#include "ComStack_Types.h"
#include "EcuM_Types.h"
#if defined(USE_CANSM)
#include "BswM_CanSM.h"
#endif




#define BSWM_VENDOR_ID  VENDOR_ID_ARCCORE
#define BSWM_MODULE_ID  MODULE_ID_BSWM

#define BSWM_AR_RELEASE_MAJOR_VERSION	4
#define BSWM_AR_RELEASE_MINOR_VERSION	1
#define BSWM_AR_RELEASE_PATCH_VERSION	3

#define BSWM_AR_MAJOR_VERSION BSWM_AR_RELEASE_MAJOR_VERSION
#define BSWM_AR_MINOR_VERSION BSWM_AR_RELEASE_MINOR_VERSION
#define BSWM_AR_PATCH_VERSION BSWM_AR_RELEASE_PATCH_VERSION

#define BSWM_SW_MAJOR_VERSION	1
#define BSWM_SW_MINOR_VERSION	0
#define BSWM_SW_PATCH_VERSION	0

typedef uint8 BswM_UserType;
typedef uint8 BswM_ModeType;

typedef uint8 BswM_ConfigType;

#define BSWM_DEV_ERROR_DETECT           STD_ON
#define BSWM_VERSION_INFO_API           STD_ON

#define BSWM_E_NO_INIT                  0x01u
#define BSWM_E_NULL_POINTER             0x02u
#define BSWM_E_PARAM_INVALID            0x03u
#define BSWM_E_REQ_USER_OUT_OF_RANGE    0x04u
#define BSWM_E_REQ_MODE_OUT_OF_RANGE    0x05u
#define BSWM_E_PARAM_CONFIG             0x06u
#define BSWM_E_PARAM_POINTER            0x07u

#define BSWM_SERVICEID_INIT                  0x00u
#define BSWM_SERVICEID_GETVERSIONINFO        0x01u
#define BSWM_SERVICEID_REQUESTMODE           0x02u
#define BSWM_SERVICEID_MAINFUNCTION          0x03u
#define BSWM_SERVICEID_DEINIT                0x04u
#define BSWM_SERVICEID_CANSMCURRENTSTATE     0x05u
#define BSWM_SERVICEID_LINSMCURRENTSTATE     0x09u
#define BSWM_SERVICEID_LINSMCURRENTSCHEDULE  0x0Au
#define BSWM_SERVICEID_FRSMCURRENTSTATE      0x0Cu
#define BSWM_SERVICEID_ETHSMCURRENTSTATE     0x0Du
#define BSWM_SERVICEID_COMMCURRENTMODE       0x0Eu
#define BSWM_SERVICEID_ECUMCURRENTSTATE      0x0Fu
#define BSWM_SERVICEID_ECUMCURRENTWAKEUP     0x10u
#define BSWM_SERVICEID_COMMCURRENTPNCMODE    0x15u

#define BSWM_SERVICEID_SDCLIENTSERVICECURRENTSTATE 	 	    0x1Fu
#define BSWM_SERVICEID_SDEVENTHANDLERCURRENTSTATE 	 	    0x20u
#define BSWM_SERVICEID_SDCONSUMEDEVENTGROUPCURRENTSTATE	 	0x21u



#if defined(USE_DEM)
#define BSWM_REPORT_ERROR_STATUS(eventID, error) Dem_ReportErrorStatus(eventID, error);
#else
#define BSWM_REPORT_ERROR_STATUS(eventID, error)
#endif

/*================================ BswM API =================================*/

void BswM_Init( const BswM_ConfigType* ConfigPtr );
void BswM_Deinit( void );


#if ( BSWM_VERSION_INFO_API == STD_ON )

/* @req SWS_BswM_00003 */
void BswM_GetVersionInfo( Std_VersionInfoType* VersionInfo );
#endif /* BSWM_VERSION_INFO_API */


void BswM_ComM_CurrentMode( NetworkHandleType Network, ComM_ModeType RequestedMode );
void BswM_ComM_CurrentPNCMode( PNCHandleType PNC, ComM_PncModeType CurrentPncMode );

void BswM_EcuM_CurrentState( EcuM_StateType CurrentState );
void BswM_EcuM_CurrentWakeup( EcuM_WakeupSourceType source, EcuM_WakeupStatusType state );

extern const BswM_ConfigType  BswM_Config;

#endif  /* BSWM_H_ */

