/*
 * Generator version: 8.1.0
 * AUTOSAR version:   4.0.3
 */

#include "Std_Types.h"
#if defined(USE_PDUR)
#include "PduR.h"
#include "PduR_PbCfg.h"
#endif
#if defined(USE_NVM)
#include "NvM.h"
#endif
#if defined(USE_COMM)
#include "ComM.h"
#endif
#include "Dcm.h"
#include "Dcm_Lcfg.h"
#ifndef DCM_NOT_SERVICE_COMPONENT
#include "Rte_Dcm.h"
#endif

#define DCM_SESSION_EOL_INDEX 3
#define DCM_DID_LIST_EOL_INDEX 3
#define DCM_OBDM_0_TID_INDEX   0  

extern uint32 DspRoutineInfoReadUnsigned(uint8 *data, uint16 bitOffset, uint8 size, boolean changeEndianess);
extern sint32 DspRoutineInfoRead(uint8 *data, uint16 bitOffset, uint8 size, boolean changeEndianess);
extern void DspRoutineInfoWrite(uint32 val, uint8 *data, uint16 bitOffset, uint8 size, boolean changeEndianess);



const Dcm_DspSecurityRowType DspSecurityList[] = {
	{
		.DspSecurityLevel = 0,
		.DspSecurityADRSize = 0,
		.DspSecuritySeedSize = 4,
		.DspSecurityKeySize = 4,
		.DspSecurityDelayTimeOnBoot =5000,
		.DspSecurityNumAttDelay = 5,		
		.DspSecurityDelayTime = 10000,			
		.DspSecurityNumAttLock = 0,			/* Value is not configurable */
		.GetSeed = {.getSeedWithoutRecord = Rte_Call_SecurityAccess_SecurityLevel_0_GetSeed},
		.CompareKey = Rte_Call_SecurityAccess_SecurityLevel_0_CompareKey,
		.Arc_EOL = FALSE
	},

	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DspSecurityType DspSecurity = {
	.DspSecurityRow = DspSecurityList
};

const Dcm_DspSecurityRowType * const ServiceTable_ReadDataByIdentifier_0x22_SecurityList[] = {
	&DspSecurityList[0],
	&DspSecurityList[DCM_SECURITY_EOL_INDEX]
};
const Dcm_DspSecurityRowType * const ServiceTable_SessionControl_0x10_SecurityList[] = {
	&DspSecurityList[0],
	&DspSecurityList[DCM_SECURITY_EOL_INDEX]
};
const Dcm_DspSecurityRowType * const ServiceTable_TesterPresent_0x3E_SecurityList[] = {
	&DspSecurityList[0],
	&DspSecurityList[DCM_SECURITY_EOL_INDEX]
};


const Dcm_DspSessionRowType DspSessionList[] = {
	{
		.DspSessionLevel = 1,
		.ArcDspRteSessionLevelName = RTE_MODE_DcmDiagnosticSessionControl_DcmConf_DcmDspSessionRow_Default_Session,
		.DspSessionP2ServerMax = 50,
		.DspSessionP2StarServerMax = 5000,
		.DspSessionForBoot = DCM_NO_BOOT,
		.Arc_EOL = FALSE,
	},
	{
		.DspSessionLevel = 3,
		.ArcDspRteSessionLevelName = RTE_MODE_DcmDiagnosticSessionControl_DcmConf_DcmDspSessionRow_Extended_Session,
		.DspSessionP2ServerMax = 50,
		.DspSessionP2StarServerMax = 5000,
		.DspSessionForBoot = DCM_NO_BOOT,
		.Arc_EOL = FALSE,
	},
	{
		.DspSessionLevel = 5,
		.ArcDspRteSessionLevelName = RTE_MODE_DcmDiagnosticSessionControl_DcmConf_DcmDspSessionRow_OBD_Session,
		.DspSessionP2ServerMax = 50,
		.DspSessionP2StarServerMax = 5000,
		.DspSessionForBoot = DCM_NO_BOOT,
		.Arc_EOL = FALSE,
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DspSessionType DspSession = {
	.DspSessionRow = DspSessionList,
};

const Dcm_DspSessionRowType * const ServiceTable_ReadDataByIdentifier_0x22_SessionList[] = {
	&DspSessionList[0],
	&DspSessionList[DCM_SESSION_EOL_INDEX]
};
const Dcm_DspSessionRowType * const ServiceTable_SessionControl_0x10_SessionList[] = {
	&DspSessionList[0],
	&DspSessionList[1],
	&DspSessionList[DCM_SESSION_EOL_INDEX]
};
const Dcm_DspSessionRowType * const ServiceTable_TesterPresent_0x3E_SessionList[] = {
	&DspSessionList[1],
	&DspSessionList[DCM_SESSION_EOL_INDEX]
};


const Dcm_DspSessionRowType * const DidInfo_DcmDspDidInfo_read_sessionRefList[] = {
	&DspSessionList[0],
	&DspSessionList[DCM_SESSION_EOL_INDEX]
};

const Dcm_DspSecurityRowType * const DidInfo_DcmDspDidInfo_read_securityRefList[] = {
	&DspSecurityList[DCM_SECURITY_EOL_INDEX]
};

const Dcm_DspDidReadType didRead_DcmDspDidInfo = {
	.DspDidReadSessionRef = DidInfo_DcmDspDidInfo_read_sessionRefList,
	.DspDidReadSecurityLevelRef = DidInfo_DcmDspDidInfo_read_securityRefList
};

const Dcm_DspSessionRowType * const DidInfo_DcmDspDidInfo_write_sessionRefList[] = {
	&DspSessionList[DCM_SESSION_EOL_INDEX]
};

const Dcm_DspSecurityRowType * const DidInfo_DcmDspDidInfo_write_securityRefList[] = {
	&DspSecurityList[DCM_SECURITY_EOL_INDEX]
};

const Dcm_DspDidWriteType didWrite_DcmDspDidInfo = {
	.DspDidWriteSessionRef = DidInfo_DcmDspDidInfo_write_sessionRefList,
	.DspDidWriteSecurityLevelRef = DidInfo_DcmDspDidInfo_write_securityRefList
};


const Dcm_DspDidInfoType DspDidInfoList[] = {
		{
				.DspDidDynamicllyDefined = FALSE,
				.DspDidAccess = {
						.DspDidRead = &didRead_DcmDspDidInfo,
						.DspDidWrite = NULL,
						.DspDidControl = NULL,
				}
		},
};

extern const Dcm_DspPidType DspPidList[];

const Dcm_DspPidType DspPidList[] = {
	{
	    .Arc_EOL = TRUE
	}
};


const Dcm_DspTestResultObdmidType DspTestResultObdmidList[] = {
    {
        .Arc_EOL = TRUE
    }
};

const Dcm_DspTestResultByObdmidType DspObmidTestResult = {
		.DspTestResultObdmidTid = DspTestResultObdmidList		
};

const Dcm_DspVehInfoType DspVehInfoList[] = {
	{
        .Arc_EOL = TRUE
	}
};

extern const Dcm_DspDidType DspDidList[];

const Dcm_DspDidType * const DID_0x0101_refList[] = {
	&DspDidList[DCM_DID_LIST_EOL_INDEX],
};
const Dcm_DspDidType * const DID_0x0102_refList[] = {
	&DspDidList[DCM_DID_LIST_EOL_INDEX],
};
const Dcm_DspDidType * const DID_0x0103_refList[] = {
	&DspDidList[DCM_DID_LIST_EOL_INDEX],
};

const Dcm_DspDataInfoType DspDataInfo_DataInfo_FixedLength = {
	.DspDidFixedLength = TRUE,
	.DspDidScalingInfoSize = 0
};


const Dcm_DspDataType DspData_DID_0x0101_Data = {
    .DspDataInfoRef = &DspDataInfo_DataInfo_FixedLength,
    .DspDataReadDataLengthFnc = NULL,
	.DspDataConditionCheckReadFnc = DID_0x010X_ConditionCheck,
	.DspDataReadDataFnc = {.SynchDataReadFnc = DID_0x0101_Read},
	.DspDataWriteDataFnc = {.FixLenDataWriteFnc = NULL},
	.DspDataGetScalingInfoFnc = NULL,
	.DspDataReturnControlToEcuFnc = {.FuncReturnControlToECUFnc = NULL},
	.DspDataResetToDefaultFnc = {.FuncResetToDefaultFnc = NULL},
	.DspDataFreezeCurrentStateFnc = {.FuncFreezeCurrentStateFnc = NULL},
	.DspDataShortTermAdjustmentFnc = {.FuncShortTermAdjustmentFnc = NULL},
	.DspDataUsePort = DATA_PORT_SYNCH,
    .DspDataSize = 4,
};

const Dcm_DspDataType DspData_DID_0x0102_Data = {
    .DspDataInfoRef = &DspDataInfo_DataInfo_FixedLength,
    .DspDataReadDataLengthFnc = NULL,
	.DspDataConditionCheckReadFnc = DID_0x010X_ConditionCheck,
	.DspDataReadDataFnc = {.SynchDataReadFnc = DID_0x0102_Read},
	.DspDataWriteDataFnc = {.FixLenDataWriteFnc = NULL},
	.DspDataGetScalingInfoFnc = NULL,
	.DspDataReturnControlToEcuFnc = {.FuncReturnControlToECUFnc = NULL},
	.DspDataResetToDefaultFnc = {.FuncResetToDefaultFnc = NULL},
	.DspDataFreezeCurrentStateFnc = {.FuncFreezeCurrentStateFnc = NULL},
	.DspDataShortTermAdjustmentFnc = {.FuncShortTermAdjustmentFnc = NULL},
	.DspDataUsePort = DATA_PORT_SYNCH,
    .DspDataSize = 4,
};

const Dcm_DspDataType DspData_DID_0x0103_Data = {
    .DspDataInfoRef = &DspDataInfo_DataInfo_FixedLength,
    .DspDataReadDataLengthFnc = NULL,
	.DspDataConditionCheckReadFnc = DID_0x010X_ConditionCheck,
	.DspDataReadDataFnc = {.SynchDataReadFnc = DID_0x0103_Read},
	.DspDataWriteDataFnc = {.FixLenDataWriteFnc = NULL},
	.DspDataGetScalingInfoFnc = NULL,
	.DspDataReturnControlToEcuFnc = {.FuncReturnControlToECUFnc = NULL},
	.DspDataResetToDefaultFnc = {.FuncResetToDefaultFnc = NULL},
	.DspDataFreezeCurrentStateFnc = {.FuncFreezeCurrentStateFnc = NULL},
	.DspDataShortTermAdjustmentFnc = {.FuncShortTermAdjustmentFnc = NULL},
	.DspDataUsePort = DATA_PORT_SYNCH,
    .DspDataSize = 4,
};


const Dcm_DspSignalType DspDid_DID_0x0101_SignalList[] = {
	{
		.DspSignalDataRef = &DspData_DID_0x0101_Data,
		.DspSignalsPosition = 0
	},
};

const Dcm_DspSignalType DspDid_DID_0x0102_SignalList[] = {
	{
		.DspSignalDataRef = &DspData_DID_0x0102_Data,
		.DspSignalsPosition = 0
	},
};

const Dcm_DspSignalType DspDid_DID_0x0103_SignalList[] = {
	{
		.DspSignalDataRef = &DspData_DID_0x0103_Data,
		.DspSignalsPosition = 0
	},
};


const Dcm_DspDidType DspDidList[] = { 
	{	/* DID_0x0101 */
		.DspDidIdentifier = 0x101,
		.DspDidInfoRef = &DspDidInfoList[0],
		.DspDidRef = DID_0x0101_refList,
		.DspSignalRef = DspDid_DID_0x0101_SignalList,
		.DspNofSignals = 1,
		.DspDidDataSize = 4,
		.DspDidDataScalingInfoSize = 0,
		.DspDidRoeActivateFnc = NULL,
		.DspDidRoeEventId = 0,	
		.Arc_EOL = FALSE
	},
	{	/* DID_0x0102 */
		.DspDidIdentifier = 0x102,
		.DspDidInfoRef = &DspDidInfoList[0],
		.DspDidRef = DID_0x0102_refList,
		.DspSignalRef = DspDid_DID_0x0102_SignalList,
		.DspNofSignals = 1,
		.DspDidDataSize = 4,
		.DspDidDataScalingInfoSize = 0,
		.DspDidRoeActivateFnc = NULL,
		.DspDidRoeEventId = 0,	
		.Arc_EOL = FALSE
	},
	{	/* DID_0x0103 */
		.DspDidIdentifier = 0x103,
		.DspDidInfoRef = &DspDidInfoList[0],
		.DspDidRef = DID_0x0103_refList,
		.DspSignalRef = DspDid_DID_0x0103_SignalList,
		.DspNofSignals = 1,
		.DspDidDataSize = 4,
		.DspDidDataScalingInfoSize = 0,
		.DspDidRoeActivateFnc = NULL,
		.DspDidRoeEventId = 0,	
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};



const Dcm_DspRoutineType  DspRoutineList[] = {
	{
		.Arc_EOL = TRUE
	}
};



const Dcm_DspMemoryIdInfo DspMemoryIdInfoList[] = {
	{
			.Arc_EOL = TRUE
	}
};

const Dcm_DspMemoryType DspMemory = {
	.DcmDspUseMemoryId = FALSE,
	.DspMemoryIdInfo = DspMemoryIdInfoList,
};

const Dcm_DspType Dsp = {
	.DspDid = DspDidList,
	.DspDidInfo = DspDidInfoList,
	.DspEcuReset = NULL,
	.DspPid = DspPidList,
	.DspReadDTC = NULL,
	.DspRequestControl = NULL,
	.DspRoutine = DspRoutineList,
	.DspRoutineInfo = NULL,
	.DspSecurity = &DspSecurity,
	.DspSession = &DspSession,
	.DspTestResultByObdmid = &DspObmidTestResult,
	.DspVehInfo = DspVehInfoList,
	.DspMemory = NULL,
};

/************************************************************************
 *									DSD									*
 ************************************************************************/

const Dcm_DsdSubServiceType DcmDsdServiceTable_UDS_ReadDataByIdentifier_0x22_SubServices[] = {
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DsdSubServiceType DcmDsdServiceTable_UDS_SessionControl_0x10_SubServices[] = {
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DsdServiceType DcmDsdServiceTable_UDS_serviceList[] = {
	{
		.DsdSidTabServiceId = 0x22,
		.DsdSidTabSubfuncAvail = TRUE,
		.DsdSidTabSecurityLevelRef = ServiceTable_ReadDataByIdentifier_0x22_SecurityList,
		.DsdSidTabSessionLevelRef = ServiceTable_ReadDataByIdentifier_0x22_SessionList,
		.DspSidTabFnc = NULL,
		.ArcDcmDsdSidConditionCheckFnc = NULL,
		.DsdSubServiceList = NULL,
		.Arc_EOL = FALSE
	},
	{
		.DsdSidTabServiceId = 0x10,
		.DsdSidTabSubfuncAvail = TRUE,
		.DsdSidTabSecurityLevelRef = ServiceTable_SessionControl_0x10_SecurityList,
		.DsdSidTabSessionLevelRef = ServiceTable_SessionControl_0x10_SessionList,
		.DspSidTabFnc = NULL,
		.ArcDcmDsdSidConditionCheckFnc = NULL,
		.DsdSubServiceList = NULL,
		.Arc_EOL = FALSE
	},
	{
		.DsdSidTabServiceId = 0x3e,
		.DsdSidTabSubfuncAvail = FALSE,
		.DsdSidTabSecurityLevelRef = ServiceTable_TesterPresent_0x3E_SecurityList,
		.DsdSidTabSessionLevelRef = ServiceTable_TesterPresent_0x3E_SessionList,
		.DspSidTabFnc = NULL,
		.ArcDcmDsdSidConditionCheckFnc = NULL,
		.DsdSubServiceList = NULL,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DsdServiceTableType DsdServiceTable[] = {	
	{
		.DsdSidTabId = 0,
		.DsdService = DcmDsdServiceTable_UDS_serviceList,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DsdType Dsd = {
	.DsdServiceTable = DsdServiceTable
};


/************************************************************************
 *									DSL									*
 ************************************************************************/

uint8 Buffer_Rx[500];
Dcm_DslBufferRuntimeType rxBufferParams_Buffer_Rx =
{
	.status = BUFFER_AVAILABLE
};
uint8 Buffer_Tx[1000];
Dcm_DslBufferRuntimeType rxBufferParams_Buffer_Tx =
{
	.status = BUFFER_AVAILABLE
};

Dcm_DslRunTimeProtocolParametersType dcmDslRuntimeVariables[1];

const Dcm_DslBufferType DcmDslBufferList[DCM_DSL_BUFFER_LIST_LENGTH] = {
	{
		.DslBufferID = 0,
		.DslBufferSize = 0,	/* Value is not configurable */
		.pduInfo = {
			.SduDataPtr = Buffer_Rx,
			.SduLength = 500,
		},
		.externalBufferRuntimeData = &rxBufferParams_Buffer_Rx
	},
	{
		.DslBufferID = 1,
		.DslBufferSize = 0,	/* Value is not configurable */
		.pduInfo = {
			.SduDataPtr = Buffer_Tx,
			.SduLength = 1000,
		},
		.externalBufferRuntimeData = &rxBufferParams_Buffer_Tx
	},
};

const Dcm_DslCallbackDCMRequestServiceType DCMRequestServiceList[] = {
	{
		.StartProtocol = Rte_Call_CallbackDCMRequestServices_DcmDslCallbackDCMRequestService_StartProtocol,
		.StopProtocol = Rte_Call_CallbackDCMRequestServices_DcmDslCallbackDCMRequestService_StopProtocol,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

extern const Dcm_DslMainConnectionType DslMainConnectionList[];

const Dcm_DslProtocolRxType DcmDslProtocolRxList[] = {
	{
		.DslMainConnectionParent = &DslMainConnectionList[0],
		.DslProtocolAddrType = DCM_PROTOCOL_PHYSICAL_ADDR_TYPE,
		.DcmDslProtocolRxPduId = DCM_PDU_ID_DCM_RX,
#if defined(USE_COMM)
		.DcmDslProtocolRxComMChannel = ComMConf_ComMChannel_ComMChannel,
		.ComMChannelInternalIndex = 0u,
#endif
		.Arc_EOL = FALSE
	},
	{
		.DslMainConnectionParent = &DslMainConnectionList[0],
		.DslProtocolAddrType = DCM_PROTOCOL_FUNCTIONAL_ADDR_TYPE,
		.DcmDslProtocolRxPduId = DCM_PDU_ID_DCM_RXFUNC,
#if defined(USE_COMM)
		.DcmDslProtocolRxComMChannel = ComMConf_ComMChannel_ComMChannel,
		.ComMChannelInternalIndex = 0u,
#endif
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DslProtocolTxType DcmDslProtocolTxList[] = {
	{
		.DslMainConnectionParent = &DslMainConnectionList[0],
		.DcmDslProtocolTxPduId = PDUR_PDU_ID_DCM_TX,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};
extern const Dcm_DslProtocolRowType DslProtocolRowList[];


const Dcm_DslPeriodicTransmissionType DslPeriodicConnectionList[] = {
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DslPeriodicTxType DcmDslProtocolPeriodicTxList[] = {
	{
		.Arc_EOL = TRUE
	}
};

extern const Dcm_DslConnectionType DcmDslProtocolRow_DOIP_ConnectionList[];

const Dcm_DslMainConnectionType DslMainConnectionList[] = {
	{
		/* DcmDslProtocolRow_DOIP.DcmDslConnection1.DcmDslMainConnection */
		.DslConnectionParent = &DcmDslProtocolRow_DOIP_ConnectionList[0],
		.DslPeriodicTransmissionConRef = NULL,
		.DslProtocolTx = &DcmDslProtocolTxList[0],
		.DslPhysicalProtocolRx = &DcmDslProtocolRxList[0],
		.DslRxTesterSourceAddress = 3
	},
};


const Dcm_DslConnectionType DcmDslProtocolRow_DOIP_ConnectionList[] = {
	{
		/* DcmDslConnection1.DcmDslMainConnection */
		.DslProtocolRow = &DslProtocolRowList[0],
		.DslMainConnection = &DslMainConnectionList[0],
		.DslPeriodicTransmission = NULL,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};



extern const Dcm_DslProtocolTimingRowType ProtocolTimingList[];

const Dcm_DslProtocolRowType DslProtocolRowList[]= {
	{
		/* DcmDslProtocolRow_DOIP */
		.DslRunTimeProtocolParameters = &dcmDslRuntimeVariables[0],
		.DslProtocolID = DCM_UDS_ON_IP,
		.DslProtocolIsParallelExecutab = FALSE, // not supported
		.DslProtocolPreemptTimeout = 1000,
		.DslProtocolPriority = 1,
		.DslProtocolTransType = DCM_PROTOCOL_TRANS_TYPE_1,
		.DslProtocolRxBufferID = &DcmDslBufferList[0],
		.DslProtocolSIDTable = &DsdServiceTable[0],
		.DslProtocolTxBufferID = &DcmDslBufferList[1],
		.DslProtocolTimeLimit = &ProtocolTimingList[0],
		.DslConnections = DcmDslProtocolRow_DOIP_ConnectionList,
		.DslSendRespPendOnTransToBoot = TRUE,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DslProtocolType DslProtocol = {
		.DslProtocolTxGlobalList = DcmDslProtocolTxList,
		.DslProtocolRxGlobalList = DcmDslProtocolRxList,
		.DslProtocolPeriodicTxGlobalList = DcmDslProtocolPeriodicTxList,
		.DslProtocolRowList = DslProtocolRowList
};

const Dcm_DslProtocolTimingRowType ProtocolTimingList[] = {
	{
		.TimStrP2ServerMin = 50,
		.TimStrP2ServerMax = 50,
		.TimStrS3Server = 5000, /* Value is not configurable*/
		.TimStrP2StarServerMax = 0,		/* Value is not configurable */
		.TimStrP2StarServerMin = 0,		/* Value is not configurable */
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DslProtocolTimingType ProtocolTiming = {
	.DslProtocolTimingRow = ProtocolTimingList
};

const Dcm_DslServiceRequestNotificationType ServiceRequestNotification[] = {
	{
		.Arc_EOL = TRUE
	}
};

const Dcm_DslDiagRespType DiagResp = {
		.DslDiagRespForceRespPendEn = TRUE, /* Value is not configurable */
		.DslDiagRespMaxNumRespPend = 2
};

const Dcm_DslType Dsl = {
		.DslBuffer = DcmDslBufferList,
		.DslCallbackDCMRequestService = DCMRequestServiceList,
		.DslDiagResp = &DiagResp,
		.DslProtocol = &DslProtocol,
		.DslProtocolTiming = &ProtocolTiming,
		.DslServiceRequestNotification = ServiceRequestNotification
};

const Dcm_ConfigType DCM_Config = {
		.Dsp = &Dsp,
		.Dsd = &Dsd,
		.Dsl = &Dsl
};

