/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#if defined(USE_PDUR)
#include "PduR.h"
#include "PduR_PbCfg.h"
#endif

#if defined(USE_CANTP)
#include "CanTp.h"
#include "CanTp_PBCfg.h"
#endif

#include "Com.h"
#include "Com_PbCfg.h"

#include "MemMap.h"

#if (COM_MAX_BUFFER_SIZE < 12)
#error Com: The configured ram buffer size is less than required! (12 bytes required)
#endif
#if (COM_MAX_N_IPDUS < 2)
#error Com: Configured maximum number of Pdus is less than the number of Pdus in configuration!
#endif
#if (COM_MAX_N_SUPPORTED_IPDU_COUNTERS < 0)
#error Com: Configured maximum number of Pdus with counter(ArcMaxNumberOfSupportedIPduCounters) is less than the number of Pdus configured with counter !
#endif
#if (COM_MAX_N_SIGNALS < 2)
#error Com: Configured maximum number of signals is less than the number of signals in configuration!
#endif
#if (COM_MAX_N_GROUP_SIGNALS < 0)
#error Com: Configured maximum number of groupsignals is less than the number of groupsignals in configuration!
#endif
#if (COM_MAX_N_SUPPORTED_GWSOURCE_DESCRIPTIONS < 0)
#error Com: Configured maximum number of gateway source description is less than the number of ComGwSourceDescription in configuration!
#endif


/*
 * Signal init values.
 */
SECTION_POSTBUILD_DATA const uint32 Com_SignalInitValue_ComSignal_TxUDP = 0;
SECTION_POSTBUILD_DATA const uint32 Com_SignalInitValue_ComSignal_RxUDP = 0;


/*
 * Group signal definitions
 */
SECTION_POSTBUILD_DATA const ComGroupSignal_type ComGroupSignal[] = {
	{
		.Com_Arc_EOL = 1
	}
};


/*
 * SignalGroup GroupSignals lists.
 */



/*
 * Signal group masks.
 */


/*
 * Signal definitions
 */
 
SECTION_POSTBUILD_DATA const ComSignal_type ComSignal[] = {

	{ // ComSignal_TxUDP
		.ComHandleId 				= ComConf_ComSignal_ComSignal_TxUDP,
		.ComIPduHandleId 			= ComConf_ComIPdu_IPDU_UDP6661_TX,
		.ComFirstTimeoutFactor 		= 0,		
		.ComNotification 			= COM_NO_FUNCTION_CALLOUT,		
		.ComTimeoutFactor 			= 0,
		.ComTimeoutNotification 	= COM_NO_FUNCTION_CALLOUT,
		.ComErrorNotification 		= COM_NO_FUNCTION_CALLOUT,
		.ComTransferProperty 		= COM_TRIGGERED,
		.ComUpdateBitPosition 		= 0,
		.ComSignalArcUseUpdateBit 	= FALSE,
		.ComSignalInitValue 		= &Com_SignalInitValue_ComSignal_TxUDP,
		.ComBitPosition 			= 0,
		.ComBitSize 				= 32,
		.ComSignalEndianess 		= COM_OPAQUE,
		.ComSignalType 				= COM_UINT32,
		.Com_Arc_IsSignalGroup 		= 0,
		.ComGroupSignal 			= NULL,
		.ComRxDataTimeoutAction 	= COM_TIMEOUT_DATA_ACTION_NONE,
		.ComSigGwRoutingReq			= FALSE,
		.Com_Arc_EOL 				= 0
	},

	{ // ComSignal_RxUDP
		.ComHandleId 				= ComConf_ComSignal_ComSignal_RxUDP,
		.ComIPduHandleId 			= ComConf_ComIPdu_IPDU_UDP6661_RX,
		// @req COM292			
		.ComFirstTimeoutFactor 		= 0,
		.ComNotification 			= COM_NO_FUNCTION_CALLOUT,		
		.ComTimeoutFactor 			= 0,
		.ComTimeoutNotification 	= COM_NO_FUNCTION_CALLOUT,
		.ComErrorNotification 		= COM_NO_FUNCTION_CALLOUT,
		.ComTransferProperty 		= COM_PENDING,
		.ComUpdateBitPosition 		= 0,
		.ComSignalArcUseUpdateBit 	= FALSE,
		.ComSignalInitValue 		= &Com_SignalInitValue_ComSignal_RxUDP,
		.ComBitPosition 			= 0,
		.ComBitSize 				= 32,
		.ComSignalEndianess 		= COM_OPAQUE,
		.ComSignalType 				= COM_UINT32,
		.Com_Arc_IsSignalGroup 		= 0,
		.ComGroupSignal 			= NULL,
		.ComRxDataTimeoutAction 	= COM_TIMEOUT_DATA_ACTION_NONE,
		.ComSigGwRoutingReq			= FALSE,
		.Com_Arc_EOL 				= 0
	},

	{
		.Com_Arc_EOL				= 1
	}
};

/*
 * Gateway source signal description definitions
 */
SECTION_POSTBUILD_DATA const ComGwSrcDesc_type ComGwSourceDescs[] = {
{
	.Com_Arc_EOL				= 1
}
};


/*
 * Gateway destination signal description definitions
 */
SECTION_POSTBUILD_DATA const ComGwDestnDesc_type ComGwDestinationDescs[] = {
{
	.Com_Arc_EOL				= 1
}
};


/*
 * Gateway Destination signal references
 */


/*
 * Gateway mappings
 */
SECTION_POSTBUILD_DATA const ComGwMapping_type ComGwMapping[] = {
{
	.Com_Arc_EOL				= 1
}
};


/*
 * I-PDU group definitions
 */
SECTION_POSTBUILD_DATA const ComIPduGroup_type ComIPduGroup[] = {

{
	.ComIPduGroupHandleId 		= ComConf_ComIPduGroup_ComIPduGroupRx,
	.Com_Arc_EOL 				= 0
},

{
	.ComIPduGroupHandleId 		= ComConf_ComIPduGroup_ComIPduGroupTx,
	.Com_Arc_EOL 				= 0
},

{
	.Com_Arc_EOL				= 1
}
};


/* 
 * IPdu signal lists. 
 */

SECTION_POSTBUILD_DATA const ComSignal_type * const ComIPduSignalRefs_IPDU_UDP6661_RX[] = {
	&ComSignal[ComConf_ComSignal_ComSignal_RxUDP],
	NULL
};

SECTION_POSTBUILD_DATA const ComSignal_type * const ComIPduSignalRefs_IPDU_UDP6661_TX[] = {
	&ComSignal[ComConf_ComSignal_ComSignal_TxUDP],
	NULL
};


/*
 * IPdu Counter list
 */
 
/*
 * I-PDU group ref lists
 */
SECTION_POSTBUILD_DATA const ComIPduGroup_type ComIpduGroupRefs_IPDU_UDP6661_RX[] = {
	{
		.ComIPduGroupHandleId 		= ComConf_ComIPduGroup_ComIPduGroupRx,
		.Com_Arc_EOL 				= 0
	},
	{
		.Com_Arc_EOL				= 1
	}
};
SECTION_POSTBUILD_DATA const ComIPduGroup_type ComIpduGroupRefs_IPDU_UDP6661_TX[] = {
	{
		.ComIPduGroupHandleId 		= ComConf_ComIPduGroup_ComIPduGroupTx,
		.Com_Arc_EOL 				= 0
	},
	{
		.Com_Arc_EOL				= 1
	}
};

/*
 * Gateway signal description handle
 */
	

/*
 * I-PDU definitions
 */
SECTION_POSTBUILD_DATA const ComIPdu_type ComIPdu[] = {	


	{ // IPDU_UDP6661_RX
		.ArcIPduOutgoingId			= PDUR_REVERSE_PDU_ID_PDU_UDP6661_RX,
		.ComRxIPduCallout			= COM_NO_FUNCTION_CALLOUT,
		.ComTxIPduCallout			= COM_NO_FUNCTION_CALLOUT,
		.ComIPduSignalProcessing 	= COM_DEFERRED,
		.ComIPduSize				= 4,
		.ComIPduDirection			= COM_RECEIVE,
		.ComIPduGroupRefs			= ComIpduGroupRefs_IPDU_UDP6661_RX,
		.ComTxIPdu = {
			.ComTxIPduMinimumDelayFactor	= 0,
			.ComTxIPduUnusedAreasDefault	= 0,
			.ComTxModeTrue = {
				.ComTxModeMode						= COM_NONE,
				.ComTxModeNumberOfRepetitions		= 0,
				.ComTxModeRepetitionPeriodFactor	= 0,
				.ComTxModeTimeOffsetFactor			= 0,
				.ComTxModeTimePeriodFactor			= 0,
			},
		},
		.ComIPduSignalRef			= ComIPduSignalRefs_IPDU_UDP6661_RX,
		.ComIPduDynSignalRef		= NULL,
		.ComIpduCounterRef			= NULL,
		.ComIPduGwMapSigDescHandle	= NULL,
		.ComIPduGwRoutingReq		= FALSE,
		.Com_Arc_EOL				= 0
	},


	{ // IPDU_UDP6661_TX
		.ArcIPduOutgoingId			= PDUR_PDU_ID_PDU_UDP6661_TX,
		.ComRxIPduCallout			= COM_NO_FUNCTION_CALLOUT,
		.ComTxIPduCallout			= COM_NO_FUNCTION_CALLOUT,
		.ComIPduSignalProcessing 	= COM_IMMEDIATE,
		.ComIPduSize				= 4,
		.ComIPduDirection			= COM_SEND,
		.ComIPduGroupRefs			= ComIpduGroupRefs_IPDU_UDP6661_TX,
		.ComTxIPdu = {
			.ComTxIPduMinimumDelayFactor	= 0,
			.ComTxIPduUnusedAreasDefault	= 0,
			.ComTxModeTrue = {
				.ComTxModeMode						= COM_DIRECT,
				.ComTxModeNumberOfRepetitions		= 0,
				.ComTxModeRepetitionPeriodFactor	= 0,
				.ComTxModeTimeOffsetFactor			= 0,
				.ComTxModeTimePeriodFactor			= 0,
			},
		},
		.ComIPduSignalRef			= ComIPduSignalRefs_IPDU_UDP6661_TX,
		.ComIPduDynSignalRef		= NULL,
		.ComIpduCounterRef			= NULL,
		.ComIPduGwMapSigDescHandle	= NULL,
		.ComIPduGwRoutingReq		= FALSE,
		.Com_Arc_EOL				= 0
	},

  
  




	{
		.Com_Arc_EOL				= 1
	}
};

SECTION_POSTBUILD_DATA const Com_ConfigType ComConfiguration = {
	.ComConfigurationId 			= 1,
	.ComNofIPdus					= 2,
	.ComNofSignals					= 2,
	.ComNofGroupSignals				= 0,
	.ComIPdu 						= ComIPdu,
	.ComIPduGroup 					= ComIPduGroup,
	.ComSignal						= ComSignal,
	.ComGroupSignal					= ComGroupSignal,
	.ComGwMappingRef				= ComGwMapping,
	.ComGwSrcDesc					= ComGwSourceDescs,
	.ComGwDestnDesc					= ComGwDestinationDescs
};


