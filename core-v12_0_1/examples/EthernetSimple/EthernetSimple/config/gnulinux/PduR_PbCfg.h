/*
 * Generator version: 2.2.0
 * AUTOSAR version:   4.0.3
 */

#ifndef PDUR_PBCFG_H
#define PDUR_PBCFG_H

#include "PduR.h"

#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 2)) )
#error PduR: Configuration file expected BSW module version to be 2.2.*
#endif

#if !(((PDUR_AR_RELEASE_MAJOR_VERSION == 4) && (PDUR_AR_RELEASE_MINOR_VERSION == 0)) )
#error PduR: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#include "PduR_Types.h"


extern const PduR_PBConfigType PduR_Config;

// Symbolic names for PduR Src Pdus
#define PduRConf_Pdu_PDU_UDP6661_RX 0u 
#define PduRConf_Pdu_PDU_UDP6661_TX 1u 
#define PduRConf_Pdu_Dlt_Rx 2u 
#define PduRConf_Pdu_Dlt_Tx 3u 
#define PduRConf_Pdu_Test_Rx 4u 
#define PduRConf_Pdu_Test_Tx 5u 


//  PduR Polite Defines.
#define PDUR_PDU_ID_PDU_UDP6661_RX					0
#define PDUR_REVERSE_PDU_ID_PDU_UDP6661_RX			0

#define PDUR_PDU_ID_PDU_UDP6661_TX					1
#define PDUR_REVERSE_PDU_ID_PDU_UDP6661_TX			1

#define PDUR_PDU_ID_DLT_RX					2
#define PDUR_REVERSE_PDU_ID_DLT_RX			2

#define PDUR_PDU_ID_DLT_TX					3
#define PDUR_REVERSE_PDU_ID_DLT_TX			3

#define PDUR_PDU_ID_TEST_RX					4
#define PDUR_REVERSE_PDU_ID_TEST_RX			4

#define PDUR_PDU_ID_TEST_TX					5
#define PDUR_REVERSE_PDU_ID_TEST_TX			5



#endif

