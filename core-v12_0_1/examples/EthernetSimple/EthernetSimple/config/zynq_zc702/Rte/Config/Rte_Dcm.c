/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_Dcm.h"

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define Dcm_START_SEC_VAR_INIT_UNSPECIFIED
#include <Dcm_MemMap.h>
const Rte_CDS_Dcm Dcm_dcm = {
    ._dummy = 0
};
#define Dcm_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Dcm_MemMap.h>

#define Dcm_START_SEC_VAR_INIT_UNSPECIFIED
#include <Dcm_MemMap.h>
const Rte_Instance Rte_Inst_Dcm = &Dcm_dcm;

#define Dcm_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Dcm_MemMap.h>

/** === Runnables =================================================================================
 */
#define Dcm_START_SEC_CODE
#include <Dcm_MemMap.h>

/** ------ dcm -----------------------------------------------------------------------
 */
Std_ReturnType Rte_dcm_Dcm_GetSecurityLevel(/*OUT*/Dcm_SecLevelType * SecLevel) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = Dcm_GetSecurityLevel(SecLevel);

    /* POST */

    return retVal;
}
Std_ReturnType Rte_dcm_Dcm_GetSesCtrlType(/*OUT*/Dcm_SesCtrlType * SesCtrlType) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = Dcm_GetSesCtrlType(SesCtrlType);

    /* POST */

    return retVal;
}
Std_ReturnType Rte_dcm_Dcm_GetActiveProtocol(/*OUT*/Dcm_ProtocolType * ActiveProtocol) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = Dcm_GetActiveProtocol(ActiveProtocol);

    /* POST */

    return retVal;
}
Std_ReturnType Rte_dcm_Dcm_ResetToDefaultSession(void) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = Dcm_ResetToDefaultSession();

    /* POST */

    return retVal;
}
void Rte_dcm_Dcm_MainFunction(void) {

    /* PRE */

    /* MAIN */

    Dcm_MainFunction();

    /* POST */

}
#define Dcm_STOP_SEC_CODE
#include <Dcm_MemMap.h>

