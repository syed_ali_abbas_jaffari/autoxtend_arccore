
/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef IOHWAB_DCM_H_
#define IOHWAB_DCM_H_

#include "Dcm_Types.h"

/* Digital signal: DigitalSignal_LED1 */
Std_ReturnType IoHwAb_Dcm_DigitalSignal_LED1(uint8 action, uint8 *value);
Std_ReturnType IoHwAb_Dcm_Read_DigitalSignal_LED1(uint8 *value);

/* Digital signal: DigitalSignal_LED2 */
Std_ReturnType IoHwAb_Dcm_DigitalSignal_LED2(uint8 action, uint8 *value);
Std_ReturnType IoHwAb_Dcm_Read_DigitalSignal_LED2(uint8 *value);


/* Analog signal: AnalogSignal */
Std_ReturnType IoHwAb_Dcm_AnalogSignal(uint8 action, uint8 *value);
Std_ReturnType IoHwAb_Dcm_Read_AnalogSignal(uint8 *value);



#endif /* IOHWAB_DCM_H_ */

