/*
 * Generator version: 1.1.0
 * AUTOSAR version:   4.2.2
 */
#ifndef ETHIF_PBCFG_H_
#define ETHIF_PBCFG_H_

#if (ETHIF_MAX_FRAME_OWNER_CFG < 2)
#error "The number of configured EthIfFrameOwnerConfigs containers must be less than ArcEthIfMaxFrameOwnerConfig"
#endif

/* EthIf controller IDs */
#define EthIfConf_EthIfController_EthIfController  0u

/* EthIf Switch IDs */


/* Rx indication handles */
#define ETHIF_RXHANDLE_TCPIP_RXINDICATION		0U
#define ETHIF_RXHANDLE_TCPIP_RXINDICATION		1U



/* @req 4.2.2/SWS_EthIf_00105 */ /* @req 4.2.2/SWS_EthIf_00104 */
/** Rx Confirmation call back function for UL**/
extern void TcpIp_RxIndication ( uint8 CtrlIdx, Eth_FrameType FrameType, boolean IsBroadcast, const uint8* PhysAddrPtr, uint8* DataPtr, uint16 LenByte );
extern void TcpIp_RxIndication ( uint8 CtrlIdx, Eth_FrameType FrameType, boolean IsBroadcast, const uint8* PhysAddrPtr, uint8* DataPtr, uint16 LenByte );



#endif /* ETHIF_PBCFG_H_ */
