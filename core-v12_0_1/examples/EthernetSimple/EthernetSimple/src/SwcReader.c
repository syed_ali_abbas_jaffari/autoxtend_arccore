/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2014, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/
#include "Rte_SwcReaderType.h"

#define DUTY_100_PERCENT 1000
static AnalogValue maxValue = 0;
static AnalogValue minValue = 0;
static DigitalLevel state = 0;

// use later
static uint32 toPwmDuty(AnalogValue value){
    AnalogValue range;
    if(value > maxValue) {
        maxValue = value;
    }
    if(value < minValue) {
        minValue = value;
    }
    range = maxValue - minValue;
    uint32 duty = DUTY_100_PERCENT;
    if(0 != range) {
        duty = (DUTY_100_PERCENT*(((value - minValue)*1000)/range))/1000;
    }
    return duty;
}

void swcReaderRunnable() {
    AnalogValue value;
    SignalQuality quality;

    static Rte_DE_uint32 rxData;

    if (rxData.value != Rte_IRead_SwcReaderRunnable_ReceiverPort_data1()){
    	// send received data to SenderPort
    	rxData.value = Rte_IRead_SwcReaderRunnable_ReceiverPort_data1();
    	Rte_IWrite_SwcReaderRunnable_ResultPort_data1(rxData.value);
    	state = !state;
    }

    Rte_Call_AnalogReader_Read(&value, &quality);
    
    if(value > (1 << 16)) {
        // value is invalid, report error
        Rte_Call_Det_ReportError(0,0,0);
    }
    else{
      // Update ADC value
    	Rte_IWrite_SwcReaderRunnable_AdcResult_data2(toPwmDuty(value));
    }
    Rte_Call_Blinker_Write(state);

#if defined(USE_DLT)
	char data[]= "    SwcReader calling!";
    /* Data to be sent, always starting with 4 bytes message id */
	data[0]=0x1u;
	data[1]=0x2u;
	data[2]=0x3u;
	data[3]=0x4u;
	const Dlt_MessageLogInfoType log_info = {
		.app_id = {'H','E','J','0'},
		.context_id = {'S','T','D','0'},
		.arg_count = 0,
		.options = (uint8)0x00u,
		.log_level = DLT_LOG_INFO,
	};

	/* Example on how to use DLT from application */
    Rte_Call_Dlt_SendLogMessage(0x1234u, &log_info, (uint8 *)data, 22);
#endif
}

void swcReaderInit(void) {
    // perform a run request
    Rte_Call_RunControl_RequestRUN();
}

