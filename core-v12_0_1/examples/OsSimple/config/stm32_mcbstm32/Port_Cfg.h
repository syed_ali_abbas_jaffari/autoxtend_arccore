
#ifndef PORT_CFG_H_
#define PORT_CFG_H_

#include "Std_Types.h"
#include "Port_ConfigTypes.h"
#include "Port.h"

#if !(((PORT_SW_MAJOR_VERSION == 5) && (PORT_SW_MINOR_VERSION == 0)) )
#error Port: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((PORT_AR_RELEASE_MAJOR_VERSION == 4) && (PORT_AR_RELEASE_MINOR_VERSION == 1)) )
#error Port: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#define PORT_VERSION_INFO_API		STD_ON
#define PORT_DEV_ERROR_DETECT		STD_ON
#define PORT_SET_PIN_DIRECTION_API		STD_ON
/** Allow Pin mode changes during runtime (not avail on this CPU) */
#define PORT_SET_PIN_MODE_API		STD_OFF
#define PORT_POSTBUILD_VARIANT STD_OFF

#define PortConf_PortPin_PortPin_B3	((Port_PinType)19)
#define Port_PortPin_B3 PortConf_PortPin_PortPin_B3
#define PortConf_PortPin_PortPin_B4	((Port_PinType)20)
#define Port_PortPin_B4 PortConf_PortPin_PortPin_B4
/** Instance of the top level configuration container */
extern const Port_ConfigType PortConfigData;

/* Configuration Set Handles */
#define PortConfigSet (PortConfigData)
#define Can_PortConfigSet (PortConfigData)

#endif /* PORT_CFG_H_ */
