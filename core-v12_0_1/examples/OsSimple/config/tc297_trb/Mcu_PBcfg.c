/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Mcu.h"



const struct Mcu_Hw_ClockSettings Mcu_Hw_ClockSettingConfig[] = {
};

const Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] = {
   {
      .McuClockReferencePointFrequency = 8000000UL,
		
	  .Mcu_Hw_ClockSettings = &Mcu_Hw_ClockSettingConfig[0],	
	}

};

const Mcu_ConfigType McuConfigData[] = {
   {
      .McuNumberOfMcuModes = 2u,
      .McuRamSectors = 0u,
      .McuClockSettings = 1u,
      .McuDefaultClockSettings = McuConf_McuClockSettingConfig_McuClockSettingConfig, 
      .McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],
#if defined (USE_DEM)
      .McuClockFailure = 0,
#endif
      .McuRamSectorSettingConfig = NULL
   }
};

