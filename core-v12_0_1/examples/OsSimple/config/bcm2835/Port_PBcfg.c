
/*
 * Generator version: 5.0.1
 * AUTOSAR version:   4.1.2
 */

#include "Port.h"

#define PIN_CONTROL_REG(nr, mode) PIN_CONTROL_REG_ ## nr ## _## mode

/*lint -save -e835 A zero has been given as argument to operator */
static const ArcPort_ConfigType PortConfig[] = {
	{4, PIN_CONTROL_REG(4, ARM_JTAG_TDI)},
	//{14, ENABLE_PULL_UP | RISING_EDGE_DETECT_ENABLE | PIN_CONTROL_REG(14, INPUT)},
	{18, PIN_CONTROL_REG(18, OUTPUT)},
	{22, PIN_CONTROL_REG(22, ARM_JTAG_TRST)},
	{22, GPFN_ALT0},
	{24, PIN_CONTROL_REG(24, ARM_JTAG_TDO)},
	{25, PIN_CONTROL_REG(25, ARM_JTAG_TCK)},
	{27, PIN_CONTROL_REG(27, ARM_JTAG_TMS)},
	{PORT_INVALID_REG, 0},
};


static const ArcPort_OutConfigType GpioOutConfig[] = {
	{18, 1},
	{PORT_INVALID_REG, 0},
};
/*lint -restore*/

const Port_ConfigType PortConfigData =
{
	.PortConfig = & PortConfig[0],
	.OutConfig  = & GpioOutConfig[0]
};




