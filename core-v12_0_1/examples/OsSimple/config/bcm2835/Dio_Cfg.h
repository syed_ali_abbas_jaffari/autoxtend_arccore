
#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#define DIO_VERSION_INFO_API    STD_OFF
#define DIO_DEV_ERROR_DETECT    STD_OFF

#define DIO_END_OF_LIST  (0xFFu)

// Physical ports for BCM2835 (GPIO)
typedef enum
{
  DIO_BANK_0 = 0,
  DIO_BANK_1 = 1,
} Dio_PortTypesType;

// Channels
#define DioConf_DioChannel_LED1 18
#define Dio_LED1 DioConf_DioChannel_LED1

// Ports
#define DioConf_DioPort_LED (DIO_BANK_0)
#define Dio_LED DioConf_DioPort_LED

// Configuration Set Handles
#define DioConfig (DioConfigData)
#define Dio_DioConfig (DioConfigData)

#endif // DIO_CFG_H_
