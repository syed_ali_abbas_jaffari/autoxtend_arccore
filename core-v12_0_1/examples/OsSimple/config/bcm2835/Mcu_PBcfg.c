#ifndef MCU_CFG_C_
#define MCU_CFG_C_

#include "Mcu.h"

Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] =
{
  {
    .McuClockReferencePointFrequency = 250000000UL,
    .Pll1    = 0,	// Not used
    .Pll2    = 0,	// Not used
    .Pll3    = 0,	// Not used
  },
};

const Mcu_ConfigType McuConfigData[] = {
  {
	.McuRamSectors = 0,
	.McuClockSettings = 1,
	.McuDefaultClockSettings = 0,
	.McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],
	.McuRamSectorSettingConfig = NULL,
  }
};

#endif // MCU_CFG_C_
