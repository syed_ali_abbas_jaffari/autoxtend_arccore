
/*
 * Generator version: 5.0.1
 * AUTOSAR version:   4.1.2
 */

#include "PortDefs.h"
#include "bcm2835.h"

#if !(((PORT_SW_MAJOR_VERSION == 5) && (PORT_SW_MINOR_VERSION == 0)) )
#error Port: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((PORT_AR_RELEASE_MAJOR_VERSION == 4) && (PORT_AR_RELEASE_MINOR_VERSION == 1)) )
#error Port: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#ifndef PORT_CFG_H_
#define PORT_CFG_H_

#define	PORT_VERSION_INFO_API			STD_ON
#define	PORT_DEV_ERROR_DETECT			STD_ON
#define PORT_SET_PIN_MODE_API			STD_ON
#define PORT_SET_PIN_DIRECTION_API		STD_ON

#define PORT_INVALID_REG	255

//#define PIN_CONTROL_REG(nr, mode) PIN_CONTROL_REG_ ## nr ## _## mode

typedef struct {
	uint8_t gpioPin;
	uint16_t config;				// bits 0:2 -> functionSelect (i.e. input, output, or ALT0-5)
									// bit 3 -> rising edge detect enable
									// bit 4 -> falling edge detect enable
									// bit 5 -> high level detect enable
									// bit 6 -> low level detect enable
									// bit 7 -> async. rising edge detect enable (bits 3 and 7 can not be high at the same time. When set async detection will be enabled.)
									// bit 8 -> async. falling edge detect enable (bits 4 and 8 can not be high at the same time. When set async detection will be enabled.)
									// bit 9:10 -> pull-up/down
									// bits 11:15 -> Unused
}   ArcPort_ConfigType;				// Defines the GPIO pin settings

typedef struct {
	uint8_t gpioPin;
	uint8_t level;
} ArcPort_OutConfigType;			// Defines the initial level (i.e. High/low) written on the GPIO pin

typedef struct
{
    const ArcPort_ConfigType *PortConfig;
    const ArcPort_OutConfigType *OutConfig;
} Port_ConfigType;							// Defines the complete data structure

extern const Port_ConfigType PortConfigData;

/* Configuration Set Handles */
#define PortConfigSet (PortConfigData)
#define Port_PortConfigSet (PortConfigData)


#endif /* PORT_CFG_H_ */
