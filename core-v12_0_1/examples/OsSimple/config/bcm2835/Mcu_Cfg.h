
#if !(((MCU_SW_MAJOR_VERSION == 2) && (MCU_SW_MINOR_VERSION == 0)) )
#error Mcu: Configuration file expected BSW module version to be 2.0.*
#endif

#if !(((MCU_AR_RELEASE_MAJOR_VERSION == 4) && (MCU_AR_RELEASE_MINOR_VERSION == 1)) )
#error Mcu: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#ifndef MCU_CFG_H_
#define MCU_CFG_H_

#define MCU_DEV_ERROR_DETECT                STD_OFF
#define MCU_INIT_CLOCK                      STD_ON

/* Mcu Modes (Symbolic name) */
//#define McuConf_McuModeSettingConf_NORMAL (Mcu_ModeType)0u 
#define McuConf_McuModeSettingConf_RUN (Mcu_ModeType)1u 
//#define McuConf_McuModeSettingConf_SLEEP (Mcu_ModeType)2u 

#endif /* MCU_CFG_H_ */

