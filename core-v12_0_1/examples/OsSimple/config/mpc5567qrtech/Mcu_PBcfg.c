/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Mcu.h"




const Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] = {
   {
      .McuClockReferencePointFrequency = 16000000UL,
      .Pll1    = 1,
      .Pll2    = 12,
      .Pll3    = 0,
   }
};

const Mcu_ConfigType McuConfigData[] = {
   {
      .McuNumberOfMcuModes = 3u,
      .McuRamSectors = 0u,
      .McuClockSettings = 1u,
      .McuDefaultClockSettings = McuConf_McuClockSettingConfig_Clock, 
      .McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],
#if defined(USE_DEM)
      .McuClockFailure = DEM_EVENT_ID_NULL,
#endif      
      .McuRamSectorSettingConfig = NULL
   }
};

