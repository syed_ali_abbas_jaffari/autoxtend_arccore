
# Version of build system
REQUIRED_BUILD_SYSTEM_VERSION=1.0.0

# Get configuration makefiles
-include ../config/$(BOARDDIR)/Rte/Config/Rte.mk
-include ../config/Rte/Config/Rte.mk

-include ../config/$(BOARDDIR)/*.mk
-include ../config/*.mk

ifeq ($(ARCH),aurix)
SELECT_CORE = 0
endif

# Project settings
ifeq ($(BOARDDIR),bcm2835)
SELECT_CONSOLE = TTY_SERIAL_BCM2835
endif

#ifeq ($(BOARDDIR),stm32_stm3210c)
#SELECT_CONSOLE = TTY_SERIAL_STM32F103
#endif

SELECT_OPT = OPT_DEBUG

