
# Version of build system
REQUIRED_BUILD_SYSTEM_VERSION=1.0.0

# Get configuration makefiles
-include ../config/$(BOARDDIR)/*.mk

# Project settings

ifeq ($(BOARDDIR),mpc5604b_xpc560b)
SELECT_CONSOLE = RAMLOG
endif

ifeq ($(BOARDDIR),bcm2835)
SELECT_CONSOLE = TTY_SERIAL_BCM2835
endif

SELECT_OPT = OPT_DEBUG 

