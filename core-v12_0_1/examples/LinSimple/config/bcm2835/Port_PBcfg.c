#include "Port.h"

#define PIN_CONTROL_REG(nr, mode) PIN_CONTROL_REG_ ## nr ## _## mode

// lint -save -e835 A zero has been given as argument to operator
static const ArcPort_ConfigType PortConfig[] = {
	{32, 0 | PIN_CONTROL_REG(32, UART_TXD0)},
	{15, 0 | PIN_CONTROL_REG(15, UART_RXD0)},
	{40, 0 | PIN_CONTROL_REG(40, UART_TXD1)},
	{41, 0 | PIN_CONTROL_REG(41, UART_RXD1)},
	{22, 0 | PIN_CONTROL_REG(22, OUTPUT)},
	{PORT_INVALID_REG, 0},
};

static const ArcPort_OutConfigType GpioOutConfig[] = {
	{32, 0},
	{15, 0},
	{40, 0},
	{41, 0},
	{22, 0},
	{PORT_INVALID_REG, 0},
};

const Port_ConfigType PortConfigData =
{
	.PortConfig = & PortConfig[0],
	.OutConfig  = & GpioOutConfig[0]
};

