
#ifndef PORT_CFG_H_
#define PORT_CFG_H_

#include "PortDefs.h"
#include "bcm2835.h"

#define PORT_VERSION_INFO_API		STD_OFF
#define PORT_DEV_ERROR_DETECT		STD_OFF
#define PORT_SET_PIN_MODE_API		STD_OFF
#define PORT_SET_PIN_DIRECTION_API		STD_OFF

#define PORT_INVALID_REG		255

typedef struct {
	uint8_t gpioPin;
	uint16_t config;		// bits 0:2 -> functionSelect (i.e. input, output, or ALT0-5)
							// bit 3 -> rising edge detect enable
					// bit 4 -> falling edge detect enable
							// bit 5 -> high level detect enable
					// bit 6 -> low level detect enable
							// bit 7 -> async. rising edge detect enable (bits 3 and 7 can not be high at the same time.
							// When set async detection will be enabled.)
							// bit 8 -> async. falling edge detect enable (bits 4 and 8 can not be high at the same time.
							// When set async detection will be enabled.)
							// bit 9:10 -> pull-up/down
					// bits 11:15 -> Unused
} ArcPort_ConfigType;					// Defines the GPIO pin settings

typedef struct {
	uint8_t gpioPin;
	uint8_t level;
} ArcPort_OutConfigType;					// Defines the initial level (i.e. High/low) written on the GPIO pin

typedef struct {
	const ArcPort_ConfigType *PortConfig;
	const ArcPort_OutConfigType *OutConfig;
} Port_ConfigType;					// Defines the complete data structure

#define PortConf_PortPin_LIN0TX	((Port_PinType)32)
#define PortConf_PortPin_LIN0RX	((Port_PinType)15)
#define PortConf_PortPin_LIN1TX	((Port_PinType)40)
#define PortConf_PortPin_LIN1RX	((Port_PinType)41)
#define PortConf_PortPin_GPIO22	((Port_PinType)22)

extern const Port_ConfigType PortConfigData;

// Configuration Set Handles
#define PortConfigSet (PortConfigData)
#define Port_PortConfigSet (PortConfigData)

#endif // PORT_CFG_H_
