
#ifndef MCU_CFG_C_
#define MCU_CFG_C_

#include "Mcu.h"

Mcu_ClockSettingConfigType Mcu_ClockSettingConfigData[] =
{
	{
		.McuClockReferencePointFrequency = 8000000UL,
		.Pll1	= 0,	// Not used
		.Pll2	= 0,	// Not used
		.Pll3	= 0,	// Not used
	},
};

const Mcu_ConfigType McuConfigData[] = {
	{
		.McuRamSectors = 0u,
		.McuClockSettings = 1u,
		.McuDefaultClockSettings = McuConf_LinClockSettings_LinClockSettings,
		.McuClockSettingConfig = &Mcu_ClockSettingConfigData[0],
		.McuRamSectorSettingConfig = NULL,
	}
};

#endif // MCU_CFG_C_
