
#ifndef MCU_CFG_H_
#define MCU_CFG_H_

#define MCU_DEV_ERROR_DETECT STD_OFF
#define MCU_INIT_CLOCK STD_ON

// Mcu Modes (Symbolic name)
#define McuConf_McuModeSettingConf_RUN (Mcu_ModeType)0u
#define McuConf_McuModeSettingConf_NORMAL (Mcu_ModeType)1u

// Mcu Clock Types (Symbolic name)
#define McuConf_LinClockSettings_LinClockSettings (Mcu_ClockType)0u

#endif // MCU_CFG_H_
