/*
 * Generator version: 2.4.0
 * AUTOSAR version:   4.0.3
 */

/* File: LinSM_Lcfg.c
 * Link time configurable parameters.
 */

#include "LinSM_ConfigTypes.h" /* Configuration types */
#include "LinSM.h"      /* symbolic indexes */
#include "LinIf.h"

/*===========[LinSMSchedules]=================================================*/

/* LinSMSchedule objects assigned to channel LinSMChannel1 */
const LinSM_ScheduleType LinSMSchedules_LinSMChannel1[1] =
{
	{
		.LinSMScheduleIndex = LinIfConf_LinIfScheduleTable_SCHEDULE_01
	},
};

/*===========[LinSMChannels]==================================================*/
const LinSM_ChannelType LinSMChannels[LINSM_CHANNEL_CNT] =
{
	/* LinSMChannel1 */
	{
		.LinSMConfirmationTimeout    = 800,
		.LinSMSleepSupport           = FALSE,
		.LinSMTransceiverPassiveMode = FALSE,
		.LinSMComMNetworkHandleRef   = 0,/*ComMConf_ComMChannel_ComMChannel1 */
		.LinSMSchedule_Cnt           = 1,
		.LinSMSchedules              = LinSMSchedules_LinSMChannel1,
	},
};

/*===========[LinSM configuration]============================================*/
const LinSM_ConfigType LinSM_Config =
{
	.LinSMChannels      = LinSMChannels,
	.LinSMChannels_Size = LINSM_CHANNEL_CNT
};

