/*
 * Generator version: 2.1.1
 * AUTOSAR version:   4.1.2
 */

/* File: Lin_Lcfg.c */

/** @tagSettings DEFAULT_ARCHITECTURE=BCM2835 */
/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */


#include "Mcu.h"
#include "Lin.h"


/*===========[Lin channels configuration data]================================*/

/* This table stores the configuration data of every setup LinChannel.
 * It is indexed by Lin channel identifier.
 */
/** @req SWS_Lin_00011 */
const Lin_ChannelConfigType LinChannelConfigData[LIN_CONTROLLER_CNT] =
{
    /* LinChannel1 */
    [LinConf_LinChannel_LinChannel1] =
    {
        .LinChannelBaudRate         = 19200,
        .LinChannelWakeUpSupport    = FALSE, //Not supported
        .LinChannelEcuMWakeUpSource = 0, //not used
        /*lint -e{641} */
        .LinChannelId				= 0,
    },
};

const uint8 Lin_HwId2Channel[1] = {
	[0] = LinConf_LinChannel_LinChannel1,
};

const Lin_ConfigType Lin_Config =
{
	.LinChannelConfig				=	LinChannelConfigData,
	.Lin_HwId2ChannelMap			=	Lin_HwId2Channel
};

