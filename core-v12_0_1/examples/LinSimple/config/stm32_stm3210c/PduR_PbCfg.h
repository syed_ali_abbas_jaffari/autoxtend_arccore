/*
 * Generator version: 2.2.0
 * AUTOSAR version:   4.0.3
 */

#ifndef PDUR_PBCFG_H
#define PDUR_PBCFG_H

#include "PduR.h"

#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 2)) )
#error PduR: Configuration file expected BSW module version to be 2.2.*
#endif

#if !(((PDUR_AR_RELEASE_MAJOR_VERSION == 4) && (PDUR_AR_RELEASE_MINOR_VERSION == 0)) )
#error PduR: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#include "PduR_Types.h"


extern const PduR_PBConfigType PduR_Config;

// Symbolic names for PduR Src Pdus
#define PduRConf_Pdu_LAMPS_RX 0u 
#define PduRConf_Pdu_TCM_RX 1u 
#define PduRConf_Pdu_ENG_TX 2u 


//  PduR Polite Defines.
#define PDUR_PDU_ID_LAMPS_RX					0
#define PDUR_REVERSE_PDU_ID_LAMPS_RX			0

#define PDUR_PDU_ID_TCM_RX					1
#define PDUR_REVERSE_PDU_ID_TCM_RX			1

#define PDUR_PDU_ID_ENG_TX					2
#define PDUR_REVERSE_PDU_ID_ENG_TX			2



#endif

