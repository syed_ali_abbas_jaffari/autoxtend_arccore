/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2014, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "Rte_SwcReaderType.h"

#define DUTY_100_PERCENT 1000
static AnalogValue maxValue = 0;
static AnalogValue minValue = 0;

static uint32 toPwmDuty(AnalogValue value)
{
    AnalogValue range;
    if(value > maxValue) {
        maxValue = value;
    }
    if(value < minValue) {
        minValue = value;
    }
    range = maxValue - minValue;
    uint32 duty = DUTY_100_PERCENT;
    if(0 != range) {
        duty = (DUTY_100_PERCENT*(((value - minValue)*1000)/range))/1000;
    }
    return duty;
}

static TestCmdType oldCmd = TESTER_CMD_NONE;
static uint8 kickWdg = 0;
void swcReaderRunnable() {


    AnalogValue value;
    SignalQuality quality;

    // call the synchronous operation with received data
#if defined(CFG_MPC5604B)
    /* Led is active low (mpc5604b_xpc560b) */
    Rte_Call_Blinker_Write(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1() > 0 ? STD_ON : STD_OFF );
#else
    /* Led active high (STM32, TMS570 and others) */
    Rte_Call_Blinker_Write(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1() > 0 ? STD_OFF : STD_ON );
#endif
    // send received data to SenderPort
    Rte_IWrite_SwcReaderRunnable_ResultPort_data1(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1());

    Rte_Call_AnalogReader_Read(&value, &quality);
    if(value > (1 << 16)) {
        // value is invalid, report error
        Rte_Call_Det_ReportError(0,0,0);
    }
    Rte_IWrite_SwcReaderRunnable_AdcResult_data1(value);
    Rte_Call_PWMWriter_Set(toPwmDuty(value), &quality);

    // Check to see if we get a command
    TestCmdType cmd = Rte_IRead_SwcReaderRunnable_ReceiverPort_cmd();
    if (cmd != TESTER_CMD_NONE) {
    	if (cmd != oldCmd) {
    		oldCmd = cmd;
    		if (cmd == TESTER_CMD_SHUTDOWN) {
    			Rte_Call_RunControl_ReleaseRUN();
    		}
    		if (cmd == TESTER_CMD_ENABLE_WDG) {
    			Rte_IWrite_SwcReaderRunnable_WdgM_StateReq_requestedMode(SUPERVISION_OK);
    			kickWdg = 1;
    		}
    		if (cmd == TESTER_CMD_DISABLE_WDG) {
    			Rte_IWrite_SwcReaderRunnable_WdgM_StateReq_requestedMode(SUPERVISION_DEACTIVATED);
    			kickWdg = 0;
    		}
    		if (cmd == TESTER_CMD_STOP_WDG_KICK) {
    			kickWdg = 0;
    		}
    	}
    }

    if (kickWdg) {
    	Rte_Call_WdgM_AliveSup_UpdateAliveCounter();
    }


#if defined(USE_DLT)
    /* Trigger a Det_ReportError in order to write to Dlt */
    if(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1() == 0xdededede){
    	Rte_Call_Det_ReportError(1,2,3);
    }
    /* Trigger a DEM error in order to write to Dlt */
    if(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1() == 0xbabababa){
    	Rte_Call_Dem_TestEvent_SetEventStatus(DEM_EVENT_STATUS_FAILED);
    }

    /* Trigger a write to Dlt */
    if(Rte_IRead_SwcReaderRunnable_ReceiverPort_data1() == 0xadadadad){
		char data[]= "    SwcReader calling!";
		/* Data to be sent, always starting with 4 bytes message id */
		data[0]=0xadu;
		data[1]=0xadu;
		data[2]=0xadu;
		data[3]=0xadu;
		const Dlt_MessageLogInfoType log_info = {
			.app_id = {'H','E','J','0'},
			.context_id = {'S','T','D','0'},
			.arg_count = 0,
			.options = (uint8)0x00u,
			.log_level = DLT_LOG_INFO,
		};

		/* Example on how to use DLT from application */
		Rte_Call_Dlt_SendLogMessage(0x1234u, &log_info, (uint8 *)data, 22);
    }
#endif

}

void swcReaderInit(void) {
    // perform a run request
    Rte_Call_RunControl_RequestRUN();
}
