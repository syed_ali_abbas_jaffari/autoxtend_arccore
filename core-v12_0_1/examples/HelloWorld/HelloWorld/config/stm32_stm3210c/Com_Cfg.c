/*
 * Generator version: 1.1.0
 * AUTOSAR version:   4.0.3
 */

#include "Com.h"

// Notifications
const ComNotificationCalloutType ComNotificationCallouts [] = { 
	Rte_COMCbk_SwcMemRx_Request,
	NULL
};


// Rx callouts
const ComRxIPduCalloutType ComRxIPduCallouts[] = {
	NULL
};

// Tx callouts
const ComTxIPduCalloutType ComTxIPduCallouts[] = {
	NULL
};

// Trigger transmit callouts
const ComTxIPduCalloutType ComTriggerTransmitIPduCallouts[] = {
	NULL
};
