
/*
 * Generator version: 5.11.0
 * AUTOSAR version:   4.0.3
 */

#include "Dem.h"
#if (DEM_USE_NVM == STD_ON)
#include "NvM.h"
#endif

#if defined(USE_RTE)
#include "Rte_Dem.h"
#endif

#include "CalibrationData.h"
// Included header files containing callback declarations	
#include "Dem_Callbacks.h"

// #########################################  INFO  #########################################
// The following data read callbacks should be declared in the included header files:

// Std_ReturnType ReadTest(uint8 *Data);
// ##########################################################################################
// Rte functions
// Std_ReturnType Rte_Call_CBInitEvt_TestEvent_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason);

#define DEM_PIDANDDID_LIST_EOL_INDEX 	1
#define DEM_FF_LIST_EOL_INDEX 			1

#define DEM_PID_LIST_EOL_INDEX			0
#define DEM_DID_LIST_EOL_INDEX			1

#if (DEM_MAX_NUMBER_EVENT_PRI_MEM < 1) && (DEM_EVENT_DISPLACEMENT_SUPPORT == STD_OFF)
#warning Dem: The number of events with event destination primary memory is greater than the configured maximum \
number of events which can be stored in primary memory (Dem Max Number Event Entry Primary)! \
If changing Dem Max Number Event Entry Primary also make sure that the Fee block is big enough (if Fee used).
#endif

#if (DEM_MAX_NUMBER_EVENT_SEC_MEM < 0) && (DEM_EVENT_DISPLACEMENT_SUPPORT == STD_OFF)
#warning Dem: The number of events with event destination secondary memory is greater than the configured maximum \
number of events which can be stored in seconday memory (Dem Max Number Event Entry Secondary)! \
If changing Dem Max Number Event Entry Secondary also make sure that the Fee block is big enough (if Fee used).
#endif

#define Dem_START_SEC_CALIB_UNSPECIFIED
#include "Dem_MemMap.h"
/**
 * @a2l_characteristic TestEvent_DTC_DTC
 * @desc calib param for DTC TestEvent_DTC
 * @struct_size 2
 * @field_name DTC
 * @min 1
 * @max 16777214
 * @type uint32
 * @field_name DTCUsed
 * @min 0
 * @max 1
 * @type uint8
 */
#if !defined(HOST_TEST)
ARC_DECLARE_CALIB(const Arc_Dem_DTC, TestEvent_DTC_DTC) = {
#else
Arc_Dem_DTC TestEvent_DTC_DTC = {
#endif /* HOST_TEST */
	.DTC = 3,
	.DTCUsed = TRUE,
};
#define Dem_STOP_SEC_CALIB_UNSPECIFIED
#include "Dem_MemMap.h"

const Dem_DTCClassType DtcClassList[] = {
	{	//TestEvent_DTC
		.DTCRef = &TestEvent_DTC_DTC,
		.DTCKind = DEM_DTC_KIND_ALL_DTCS,
		.DTCSeverity = DEM_SEVERITY_NO_SEVERITY,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dem_ExtendedDataRecordClassType ExtendedDataRecordClassList[] = {
	{	//DemExtendedDataRecordClass
		.RecordNumber = 1,
		.DataSize = 1,
		.UpdateRule = DEM_UPDATE_RECORD_YES,
		.CallbackGetExtDataRecord = ReadTest,
		.InternalDataElement = DEM_NO_ELEMENT,
	},
};

const Dem_ExtendedDataClassType DemExtendedDataClass = {
	.ExtendedDataRecordClassRef = {
		&ExtendedDataRecordClassList[0],		
		NULL,
	}
};

/* Counter based predebounce */

/* Internal monitor */



const Dem_PidOrDidType DemDidList[] = {
	{
		.PidOrDidSize = 1,
		.DidIdentifier = 1,
		.DidReadFnc = ReadTest,
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};

const Dem_PidOrDidType DemPidList[] = {
	{
		.Arc_EOL = TRUE
	}
};

const Dem_PidOrDidType * const DemFreezeFrameClass_didRefList[] = {
		&DemDidList[0],
		&DemDidList[DEM_DID_LIST_EOL_INDEX]
};

const Dem_PidOrDidType * const OBDFreezeFrame_pidRefList[] = {
		&DemPidList[DEM_PID_LIST_EOL_INDEX]
};

const Dem_FreezeFrameClassType FreezeFrameClassList[] = {
	{	//DemFreezeFrameClass
		.FFKind = DEM_FREEZE_FRAME_NON_OBD,
		.FFIdClassRef = DemFreezeFrameClass_didRefList
	},
};



const Arc_FailureCycleCounterThreshold NoFailure_FailureCycleCounter = {
	.Threshold = 0,
};


const Dem_EventClassType CAN_E_TIMEOUT_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_NOT_USED,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_IGNITION,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};

const Dem_EventClassType TestEvent_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_PRIMARY_MEMORY,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_ACTIVE,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};

const Dem_EventClassType FLS_E_WRITE_FAILED_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_NOT_USED,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_ACTIVE,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};

const Dem_EventClassType FLS_E_READ_FAILED_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_NOT_USED,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_ACTIVE,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};

const Dem_EventClassType FLS_E_ERASE_FAILED_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_NOT_USED,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_ACTIVE,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};

const Dem_EventClassType FLS_E_COMPARE_FAILED_DemEventClass = {
	.ConsiderPtoStatus = FALSE,
	.EventDestination = DEM_DTC_ORIGIN_NOT_USED,
	.IndicatorAttribute = NULL,
	.EventPriority = 1,
	.FFPrestorageSupported = FALSE, /* Value is not configurable */
	.OperationCycleRef = DEM_ACTIVE,
	.PreDebounceAlgorithmClass = NULL,
	.AgingAllowed = FALSE,
	.AgingCycleCounterThreshold = 0,
	.AgingCycleRef = DEM_ACTIVE, /* Not used */
	.FailureCycleRef= DEM_OPERATION_CYCLE_ID_ENDMARK,
	.FailureCycleCounterThresholdRef = &NoFailure_FailureCycleCounter,
	.EnableConditionGroupRef = NULL
};


const Dem_EventParameterType EventParameterList[] = {
	{
		.EventID = DemConf_DemEventParameter_CAN_E_TIMEOUT,
		.EventKind = DEM_EVENT_KIND_BSW,
		.EventClass = &CAN_E_TIMEOUT_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = NULL,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = NULL,
		.Arc_EOL = FALSE
	},
	{
		.EventID = DemConf_DemEventParameter_FLS_E_WRITE_FAILED,
		.EventKind = DEM_EVENT_KIND_BSW,
		.EventClass = &FLS_E_WRITE_FAILED_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = NULL,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = NULL,
		.Arc_EOL = FALSE
	},
	{
		.EventID = DemConf_DemEventParameter_FLS_E_READ_FAILED,
		.EventKind = DEM_EVENT_KIND_BSW,
		.EventClass = &FLS_E_READ_FAILED_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = NULL,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = NULL,
		.Arc_EOL = FALSE
	},
	{
		.EventID = DemConf_DemEventParameter_FLS_E_ERASE_FAILED,
		.EventKind = DEM_EVENT_KIND_BSW,
		.EventClass = &FLS_E_ERASE_FAILED_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = NULL,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = NULL,
		.Arc_EOL = FALSE
	},
	{
		.EventID = DemConf_DemEventParameter_FLS_E_COMPARE_FAILED,
		.EventKind = DEM_EVENT_KIND_BSW,
		.EventClass = &FLS_E_COMPARE_FAILED_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = NULL,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = NULL,
		.Arc_EOL = FALSE
	},
	{
		.EventID = DemConf_DemEventParameter_TestEvent,
		.EventKind = DEM_EVENT_KIND_SWC,
		.EventClass = &TestEvent_DemEventClass,
		.ExtendedDataClassRef = NULL,
		.FreezeFrameClassRef = NULL,
		.MaxNumberFreezeFrameRecords = 0,
		.FreezeFrameRecNumClassRef = NULL,
		.CallbackInitMforE = Rte_Call_CBInitEvt_TestEvent_InitMonitorForEvent,
		.CallbackEventStatusChanged = NULL,
		.CallbackClearEventAllowed = NULL,
		.CallbackEventDataChanged = NULL,
		.DTCClassRef = &DtcClassList[0],
		.Arc_EOL = FALSE
	},
	{
		.Arc_EOL = TRUE
	}
};


const Dem_ConfigSetType DEM_ConfigSet = {
		.EventParameter = EventParameterList,
		.DTCClass = DtcClassList,
		.GlobalOBDFreezeFrameClassRef = NULL,
		.Indicators = NULL,
#if (DEM_ENABLE_CONDITION_SUPPORT == STD_ON)
		.EnableCondition = DemEnableConditionList
#endif
};


const Dem_ConfigType DEM_Config = {
	.ConfigSet = &DEM_ConfigSet,
};

