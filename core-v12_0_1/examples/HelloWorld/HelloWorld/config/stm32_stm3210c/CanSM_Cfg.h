
/*
 * Generator version: 3.2.0
 * AUTOSAR version:   4.0.3
 */

#if !(((CANSM_SW_MAJOR_VERSION == 3) && (CANSM_SW_MINOR_VERSION == 2)) )
#error CanSM: Configuration file expected BSW module version to be 3.2.*
#endif

#if !(((CANSM_AR_RELEASE_MAJOR_VERSION == 4) && (CANSM_AR_RELEASE_MINOR_VERSION == 0)) )
#error CanSM: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef CANSM_CFG_H
#define CANSM_CFG_H

/* @req CANSM010 */
#define CANSM_DEV_ERROR_DETECT		STD_ON
#define CANSM_VERSION_INFO_API		STD_OFF
#define CANSM_CAN_TRCV_SUPPORT		STD_OFF

#define NOF_CANSM_CANIF_CONTROLLERS	1
#define CANSM_NETWORK_COUNT 1

/* CanSM module configuration. */
extern const CanSM_ConfigType CanSM_Config;

#endif /*CANSM_CFG_H*/

