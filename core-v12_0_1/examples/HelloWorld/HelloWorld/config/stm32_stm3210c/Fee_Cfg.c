/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.0.3 */

#include "Fee.h"
#include "NvM_Cbk.h"

const Fee_BlockConfigType BlockConfigList[] = {
	{
		/* SwcMem_Block1 */
		.BlockNumber = 			1,
		.BlockSize = 			(8 + FEE_VIRTUAL_PAGE_SIZE  - 1) & ~ (FEE_VIRTUAL_PAGE_SIZE - 1),/* @req FEE005 */
		.ImmediateData = 		FALSE, /* Not supported */
		.NumberOfWriteCycles = 	1,

	}
,	{
		/* SwcMem_Block2 */
		.BlockNumber = 			2,
		.BlockSize = 			(8 + FEE_VIRTUAL_PAGE_SIZE  - 1) & ~ (FEE_VIRTUAL_PAGE_SIZE - 1),/* @req FEE005 */
		.ImmediateData = 		FALSE, /* Not supported */
		.NumberOfWriteCycles = 	1,

	}

};

const Fee_ConfigType Fee_Config = {
		.General = {
		#if (FEE_USE_JOB_NOTIFICATIONS == STD_ON)
				.NvmJobEndCallbackNotificationCallback = NvM_JobEndNotification,
				.NvmJobErrorCallbackNotificationCallback = NvM_JobErrorNotification		
		#else
				.NvmJobEndCallbackNotificationCallback = NULL,
				.NvmJobErrorCallbackNotificationCallback = NULL
		#endif
		},
		.BlockConfig = BlockConfigList,
};

