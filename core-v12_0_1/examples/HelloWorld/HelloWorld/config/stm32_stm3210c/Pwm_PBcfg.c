
/*
 * Generator version: 2.1.0
 * AUTOSAR version:   4.1.2
 */
#include "Pwm.h"

/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here.
 */

const Pwm_ConfigType PwmConfig = {
	.Channels = {
		
		PWM_CHANNEL_CONFIG(
							 PwmConf_PwmChannel_Channel_LED_3,
							// Period in ticks. Target: 0.001 Actual: 0.001
							36000,
							// Duty cycle (0 ~> 0%, 0x8000 ~> 100%)
							16384,
							// Local prescaler zero base register value
							1,
							// Polarity
							PWM_HIGH),
	},
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	.ChannelClass={	
		PWM_FIXED_PERIOD,
	},
#endif
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	.NotificationHandlers = {
		// Notification routine for Pwm_Channel_LED_3
		NULL,
	}
#endif
};

