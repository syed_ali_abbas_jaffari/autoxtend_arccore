
/*
 * Generator version: 5.0.1
 * AUTOSAR version:   4.1.2
 */

#include "Port.h"

const ArcPort_PinConfig pinConfigs[] = {

	/* CAN0RX_INTP0 */ {.PortPinId = 118, PIN_FUNCTION_REG_118_PORT_PIN_MODE_CAN0_CAN0RX_INTP0_PORT_PIN_IN, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* CAN0TX */ {.PortPinId = 119, PIN_FUNCTION_REG_119_PORT_PIN_MODE_CAN0_CAN0TX_PORT_PIN_OUT, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* CSIH0CSS2 */ {.PortPinId = 102, PIN_FUNCTION_REG_102_PORT_PIN_MODE_CSIH0_CSIH0CSS2_PORT_PIN_OUT, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* CSIH0SC */ {.PortPinId = 42, PIN_FUNCTION_REG_42_PORT_PIN_MODE_CSIH0_CSIH0SC_PORT_PIN_OUT, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* CSIH0SI */ {.PortPinId = 41, PIN_FUNCTION_REG_41_PORT_PIN_MODE_CSIH0_CSIH0SI_PORT_PIN_IN, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* CSIH0SO */ {.PortPinId = 43, PIN_FUNCTION_REG_43_PORT_PIN_MODE_CSIH0_CSIH0SO_PORT_PIN_OUT, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_0 */ {.PortPinId = 98, PIN_FUNCTION_REG_98_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_1 */ {.PortPinId = 99, PIN_FUNCTION_REG_99_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_2 */ {.PortPinId = 100, PIN_FUNCTION_REG_100_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 1, .flags.BITF.PIBC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* P8_3 */ {.PortPinId = 101, PIN_FUNCTION_REG_101_PORT_PIN_MODE_DIO, .flags.BITF.P = 0, .flags.BITF.PM = 0, .flags.BITF.PBDC = 1,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },
	/* TAUD0O5 */ {.PortPinId = 120, PIN_FUNCTION_REG_120_PORT_PIN_MODE_TAUD0_TAUD0O5_PORT_PIN_OUT, .flags.BITF.P = 0,  .flags.BITF.PDSC = 0, .flags.BITF.PU = 0, .flags.BITF.PD = 0, .flags.BITF.PODC = 0  },

        {.PortPinId = PORT_INVALID_REG}
};

const Port_ConfigType PortConfigData =
{
  .pinConfig = pinConfigs,
};

