

#ifndef CAN_CFG_H_
#define CAN_CFG_H_

#if !(((CAN_SW_MAJOR_VERSION == 5) && (CAN_SW_MINOR_VERSION == 0)) )
#error Can: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((CAN_AR_MAJOR_VERSION == 4) && (CAN_AR_MINOR_VERSION == 1)) )
#error Can: Configuration file expected AUTOSAAR version to be 4.1.*
#endif

// Number of controller configs
#define CAN_ARC_CTRL_CONFIG_CNT			1

#define CAN_DEV_ERROR_DETECT			STD_ON
#define CAN_VERSION_INFO_API			STD_OFF
#define CAN_MULTIPLEXED_TRANSMISSION	STD_OFF  // Not supported
#define CAN_WAKEUP_SUPPORT				STD_OFF  // Not supported
#define CAN_HW_TRANSMIT_CANCELLATION	STD_OFF
#define ARC_CAN_ERROR_POLLING			STD_OFF
#define CAN_USE_WRITE_POLLING			STD_OFF
#define CAN_USE_READ_POLLING			STD_OFF
#define CAN_USE_BUSOFF_POLLING			STD_OFF
#define CAN_USE_WAKEUP_POLLING			STD_OFF  // Not supported
#define CAN_POSTBUILD_VARIANT  			STD_OFF

#define CAN_ARC_LOG_UNIT_CNT			1U
#define CAN_ARC_HW_UNIT_HIGH			0U
#define CAN_ARC_HW_UNIT_CNT				(CAN_ARC_HW_UNIT_HIGH + 1U) 

#define USE_RSCAN_CH0 STD_ON
#define USE_RSCAN_CH1 STD_OFF
#define USE_RSCAN_CH2 STD_OFF
#define USE_RSCAN_CH3 STD_OFF
#define USE_RSCAN_CH4 STD_OFF
#define USE_RSCAN_CH5 STD_OFF
#define USE_RSCAN_CH6 STD_OFF
#define USE_RSCAN_CH7 STD_OFF

#define NUM_RSCAN_UNITS 2
#define RSCAN0_IN_USE 1
#define RSCAN1_IN_USE 0
#define NUM_RSCAN_IN_USE  (RSCAN0_IN_USE + RSCAN1_IN_USE)

/* _CTRL_ is actually is actually the channel on this HW */
#define CAN_ARC_CTRL_CNT			3 /* IMPROVEMENT */
//#define CAN_ARC_CTRL_CONFIG_CNT		3 /* IMPROVEMENT */
#define CAN_ARC_CTRL_MAX			3 /* IMPROVEMENT */

#define NO_CANIF_CONTROLLER 0xFFU

#define RSCAN_CH0   0U
#define RSCAN_CH1   1U
#define	RSCAN_CH2   2U
#define	RSCAN_CH3   3U
#define	RSCAN_CH4   4U
#define	RSCAN_CH5   5U
#define	RSCAN_CH6   6U
#define	RSCAN_CH7   7U

#define CanConf_CanController_CanController0	(uint8)0
#define Can_CanController0	CanConf_CanController_CanController0

#define CAN_ARC_CANHARDWAREOBJECT_CTRL_0_TX_0 (Can_HwHandleType)0
#define NUM_OF_HTHS (Can_HwHandleType)1

#define CAN_ARC_CANHARDWAREOBJECT_CTRL_0_RX_0 (Can_HwHandleType)1

#define CanConf_CanHardwareObject_Ctrl_0_Rx_0 (Can_HwHandleType)0
#define Can_Ctrl_0_Rx_0 CanConf_CanHardwareObject_Ctrl_0_Rx_0
#define CanConf_CanHardwareObject_Ctrl_0_Tx_0 (Can_HwHandleType)1
#define Can_Ctrl_0_Tx_0 CanConf_CanHardwareObject_Ctrl_0_Tx_0

#define NUM_OF_HOHS 2

/* Configuration Set Handles */
#define CanConfigSet (CanConfigData)
#define Can_CanConfigSet (CanConfigData)


/* CanControllerBaudRateConfigID */
#define CanConf_CanControllerBaudrateConfig_CanControllerBaudrateConfig 0


#endif /*CAN_CFG_H_*/
