/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.3
 */

#if !(((SPI_SW_MAJOR_VERSION == 2) && (SPI_SW_MINOR_VERSION == 0)) )
#error Spi: Configuration file expected BSW module version to be 2.0.X
#endif

#if !(((SPI_AR_RELEASE_MAJOR_VERSION == 4) && (SPI_AR_RELEASE_MINOR_VERSION == 1)) )
#error Spi: Configuration file expected AUTOSAR version to be 4.1.*
#endif


#ifndef SPI_CFG_H
#define SPI_CFG_H

/*
 * General configuration
 */
 #define SPI_CONFIG_VERSION      		1
 
#define SPI_CANCEL_API						STD_OFF

// Selects the SPI Handler/Driver Channel Buffers usage allowed and delivered.
// LEVEL 0 - Only Internal buffers
// LEVEL 1 - Only external buffers
// LEVEL 2 - Both internal/external buffers
#define SPI_CHANNEL_BUFFERS_ALLOWED			1

#define SPI_DEV_ERROR_DETECT 				STD_ON
#define SPI_HW_STATUS_API					STD_ON
#define SPI_INTERRUPTIBLE_SEQ_ALLOWED		STD_OFF
#define SPI_SUPPORT_CONCURRENT_SYNC_TRANSMIT STD_OFF
// LEVEL 0 - Simple sync
// LEVEL 1 - Basic async
// LEVEL 2 - Enhanced mode
#define SPI_LEVEL_DELIVERED					2

#define SPI_VERSION_INFO_API				STD_ON
#define SPI_CONTROLLER_CNT                  1
#define SPI_ARC_CTRL_MAX             		1
#define SPI_ARC_CTRL_CONFIG_CNT             1
#define SPI_EXT_DEVICES_CNT                 1
#define SPI_POSTBUILD_VARIANT  				STD_OFF
#define SPI_MAIN_FUNCTION_PERIOD_MS			0

#define SPI_CHANNELS_CONFIGURED				((1uL<<CSIB0))  	  			 


// External devices
typedef enum {
	SPI_device_1
} Spi_ExternalDeviceTypeType;

// Channels
#define SpiConf_SpiChannel_CH_ADDR   0
#define Spi_CH_ADDR   SpiConf_SpiChannel_CH_ADDR
#define SpiConf_SpiChannel_CH_CMD   1
#define Spi_CH_CMD   SpiConf_SpiChannel_CH_CMD
#define SpiConf_SpiChannel_CH_DATA   2
#define Spi_CH_DATA   SpiConf_SpiChannel_CH_DATA
#define SpiConf_SpiChannel_CH_WREN   3
#define Spi_CH_WREN   SpiConf_SpiChannel_CH_WREN

// Jobs
#define SpiConf_SpiJob_JOB_WREN   0
#define Spi_JOB_WREN   SpiConf_SpiJob_JOB_WREN
#define SpiConf_SpiJob_JOB_DATA   1
#define Spi_JOB_DATA   SpiConf_SpiJob_JOB_DATA
#define SpiConf_SpiJob_JOB_CMD2   2
#define Spi_JOB_CMD2   SpiConf_SpiJob_JOB_CMD2
#define SpiConf_SpiJob_JOB_CMD   3
#define Spi_JOB_CMD   SpiConf_SpiJob_JOB_CMD


// Sequences
#define SpiConf_SpiSequence_SEQ_CMD   0
#define Spi_SEQ_CMD   SpiConf_SpiSequence_SEQ_CMD
#define SpiConf_SpiSequence_SEQ_CMD2   1
#define Spi_SEQ_CMD2   SpiConf_SpiSequence_SEQ_CMD2
#define SpiConf_SpiSequence_SEQ_READ   2
#define Spi_SEQ_READ   SpiConf_SpiSequence_SEQ_READ
#define SpiConf_SpiSequence_SEQ_WRITE   3
#define Spi_SEQ_WRITE   SpiConf_SpiSequence_SEQ_WRITE

#define SPI_MAX_JOB             4				
#define SPI_MAX_CHANNEL			4
#define SPI_MAX_SEQUENCE		4

#define SPI_USE_HW_UNIT_0       STD_ON
#define SPI_USE_HW_UNIT_1   	STD_OFF
#define SPI_USE_HW_UNIT_2       STD_OFF
#define SPI_USE_HW_UNIT_3   	STD_OFF

#define SPI_USE_ARC_HW_STRUCT

typedef struct {
    uint32 SpiArcDAfter;
    uint32 SpiArcDBtwn;
    uint32 SpiArcDNss;
}SpiZynqDelay0RegType;


typedef struct {
    const SpiZynqDelay0RegType *SpiHwDelayReg;
    uint8 SpiHwExternalDeviceIdx[SPI_MAX_JOB];
}Spi_Hw_DeviceConfigType;


/* Configuration Set Handles */
#define SpiDriver (SpiConfigData)
#define Spi_SpiDriver (SpiConfigData)
		
#endif /* SPI_CFG_H_ */

