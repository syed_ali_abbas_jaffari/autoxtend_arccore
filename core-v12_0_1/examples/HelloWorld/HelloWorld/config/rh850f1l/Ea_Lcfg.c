
/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#include "Ea.h"
#include "NvM_Cbk.h"


/*
	ITEM NAME:		<Ea_GeneralData>
	SCOPE:			<Ea Module>
	DESCRIPTION:
		configuration for Ea_General.  
*/
const Ea_GeneralType Ea_GeneralData = {
	.EaIndex = 0,
	.EaNvmJobEndNotification = NULL_PTR,
	.EaNvmJobErrorNotification = NULL_PTR,
};

/*
	ITEM NAME:		<Ea_BlockConfigData>
	SCOPE:			<Ea Module>
	DESCRIPTION:
		configuration for Ea_BlockConfig.
		This container mapped EEPROM with block of EA Module
*/
const Ea_BlockConfigType Ea_BlockConfigData[] = 
{ 
	{
		.EaBlockNumber = 1,
		.EaBlockSize = 8,
		.EaImmediateData = FALSE,
		.EaDeviceIndex = 2, 
		.EaBlockEOL = FALSE
	}, 
	{
		.EaBlockNumber = 2,
		.EaBlockSize = 8,
		.EaImmediateData = FALSE,
		.EaDeviceIndex = 2, 
		.EaBlockEOL = TRUE
	}, 
};

