/*
 * Generator version: 2.1.0
 * AUTOSAR version:   4.1.3
 */

#include "Pwm.h"

/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here, if any.
 */

const Pwm_ConfigType PwmConfig = {
    .Channels = {
        {
            .channel   = PwmConf_PwmChannel_Channel_LED_3,        
            .idleState = PWM_LOW, 
            .frequency = 1000, // Hz
            .duty      = 16384,
            .prescaler = PWM_CH_PRESALER_CK3,
            .polarity  = PWM_HIGH,
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON        
            .class     = PWM_FIXED_PERIOD,
#endif            
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
            .notification = NULL,
#endif
        },
    },
};

