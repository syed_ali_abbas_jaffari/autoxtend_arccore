
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.3
 */

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */
/** @tagSettings DEFAULT_ARCHITECTURE=RH850F1H */ 

#include "Wdg.h"

const Wdg_ModeConfigType WdgModeConfig = {
	.Wdg_DefaultMode = WDGIF_OFF_MODE,
	.WdgSettingsFast = 9, 
	.WdgSettingsSlow = 17476 
};

#if defined(USE_DEM) || defined(CFG_WDG_USE_DEM)
const Wdg_DEMEventIdRefsType Wdg_DEMEventIdRefs = {
	.WdgSetMode = DEM_EVENT_ID_NULL
};
#endif	

const Wdg_ConfigType WdgConfig = {
	.Wdg_ModeConfig = &WdgModeConfig,
	.WdgInitialTimeout = 1,
	.WdgMaxTimeout = 2,
#if defined(USE_DEM) || defined(CFG_WDG_USE_DEM)
	.Wdg_DEMEventIdRefs = &Wdg_DEMEventIdRefs
#endif
};


