
/*
 * Generator version: 5.11.0
 * AUTOSAR version:   4.0.3
 */

#if !(((DEM_SW_MAJOR_VERSION == 5) && (DEM_SW_MINOR_VERSION == 11)) )
#error Dem: Configuration file expected BSW module version to be 5.11.*
#endif

#if !(((DEM_AR_RELEASE_MAJOR_VERSION == 4) && (DEM_AR_RELEASE_MINOR_VERSION == 0)) )
#error DEM: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef DEMINTERRID_H_
#define DEMINTERRID_H_

#define DEM_EVENT_ID_NULL (Dem_EventIdType)0
#define DEM_EVENT_ID_BSW_START (Dem_EventIdType)1
#define DemConf_DemEventParameter_CAN_E_TIMEOUT	1
#define DemConf_DemEventParameter_FLS_E_WRITE_FAILED	2
#define DemConf_DemEventParameter_FLS_E_READ_FAILED	3
#define DemConf_DemEventParameter_FLS_E_ERASE_FAILED	4
#define DemConf_DemEventParameter_FLS_E_COMPARE_FAILED	5
#define DEM_EVENT_ID_LAST_FOR_BSW (Dem_EventIdType)5
#endif /*DEMINTERRID_H_*/
