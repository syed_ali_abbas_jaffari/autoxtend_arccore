/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_SwcMemType.h"

/** === Runnable Prototypes =======================================================================
 */

/** ------ SwcMem -----------------------------------------------------------------------
 */
void Rte_SwcMem_Init(void);
void Rte_SwcMem_Main(void);
Std_ReturnType Rte_SwcMem_JobFinishedPIM1(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult);
Std_ReturnType Rte_SwcMem_JobFinishedPIM2(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult);

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */
#define SwcMemType_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <SwcMemType_MemMap.h>

struct {
    struct {
        struct {
            Rte_DE_NvmCmdType command;
            Rte_DE_uint8 blockId;
            Rte_DE_uint32 payload;
        } SwcMemRx;

    } Main;
} ImplDE_SwcMem;
#define SwcMemType_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <SwcMemType_MemMap.h>

/** === Per Instance Memories =====================================================================
 */
#define SwcMemType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>
Rte_PimType_SwcMemType_PIM1Type Rte_Pim_SwcMemType_SwcMem_PIM1;
#define SwcMemType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>

#define SwcMemType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>
Rte_PimType_SwcMemType_PIM2Type Rte_Pim_SwcMemType_SwcMem_PIM2;
#define SwcMemType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>

/** === Component Data Structure Instances ========================================================
 */
#define SwcMemType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>
const Rte_CDS_SwcMemType SwcMemType_SwcMem = {
    .Main_SwcMemRx_command = &ImplDE_SwcMem.Main.SwcMemRx.command,
    .Main_SwcMemRx_blockId = &ImplDE_SwcMem.Main.SwcMemRx.blockId,
    .Main_SwcMemRx_payload = &ImplDE_SwcMem.Main.SwcMemRx.payload,
    .Pim_PIM1 = &Rte_Pim_SwcMemType_SwcMem_PIM1,
    .Pim_PIM2 = &Rte_Pim_SwcMemType_SwcMem_PIM2
};
#define SwcMemType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>

#define SwcMemType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>
const Rte_Instance Rte_Inst_SwcMemType = &SwcMemType_SwcMem;

#define SwcMemType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcMemType_MemMap.h>

/** === Runnables =================================================================================
 */
#define SwcMemType_START_SEC_CODE
#include <SwcMemType_MemMap.h>

/** ------ SwcMem -----------------------------------------------------------------------
 */
void Rte_SwcMem_Init(void) {

    /* PRE */

    /* MAIN */

    SwcMem_Init();

    /* POST */

}
void Rte_SwcMem_Main(void) {

    /* PRE */
    Rte_Read_SwcMemType_SwcMem_SwcMemRx_command(&ImplDE_SwcMem.Main.SwcMemRx.command.value);

    Rte_Read_SwcMemType_SwcMem_SwcMemRx_blockId(&ImplDE_SwcMem.Main.SwcMemRx.blockId.value);

    Rte_Read_SwcMemType_SwcMem_SwcMemRx_payload(&ImplDE_SwcMem.Main.SwcMemRx.payload.value);

    /* MAIN */

    SwcMem_MainFunction();

    /* POST */

}
Std_ReturnType Rte_SwcMem_JobFinishedPIM1(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = SwcMem_JobFinishedPIM1(ServiceId, JobResult);

    /* POST */

    return retVal;
}
Std_ReturnType Rte_SwcMem_JobFinishedPIM2(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = SwcMem_JobFinishedPIM2(ServiceId, JobResult);

    /* POST */

    return retVal;
}
#define SwcMemType_STOP_SEC_CODE
#include <SwcMemType_MemMap.h>

