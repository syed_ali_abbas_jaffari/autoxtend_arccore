
#ifndef PWM_CFG_H_
#define PWM_CFG_H_

#if !(((PWM_SW_MAJOR_VERSION == 2) && (PWM_SW_MINOR_VERSION == 1)) )
#error Pwm: Configuration file expected BSW module version to be 2.1.*
#endif

#if !(((PWM_AR_RELEASE_MAJOR_VERSION == 4) && (PWM_AR_RELEASE_MINOR_VERSION == 1)) )
#error Pwm: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#include "Pwm_ConfigTypes.h"

/****************************************************************************
 * Global configuration options and defines
 */

// PWM003
#define PWM_DEV_ERROR_DETECT                STD_ON
#define PWM_NOTIFICATION_SUPPORTED          STD_ON

// PWM132
#define PWM_DUTYCYCLE_UPDATED_ENDPERIOD     STD_ON
#define PWM_PERIOD_UPDATED_ENDPERIOD        STD_ON

// Define what functions to enable.
#define PWM_GET_OUTPUT_STATE_API            STD_OFF //Not supported
#define PWM_SET_PERIOD_AND_DUTY_API         STD_OFF
#define PWM_DE_INIT_API                     STD_OFF
#define PWM_SET_DUTY_CYCLE_API              STD_ON
#define PWM_SET_OUTPUT_TO_IDLE_API          STD_OFF
#define PWM_VERSION_INFO_API                STD_OFF

#define PWM_POSTBUILD_VARIANT  				STD_OFF

// Legacy module support
#define PWM_STATICALLY_CONFIGURED 		STD_OFF

/* Not defined in Autosar */
#define PWM_ISR_PRIORITY	30

/*
 * Setting to ON freezes the current output state of a PWM channel when in
 * debug mode.
 */
#define PWM_FREEZE_ENABLE	STD_ON

#define PwmConf_PwmChannel_Channel_LED_3 PWM_CHANNEL_1 

#define PWM_NUMBER_OF_CHANNELS 1


typedef struct {
	Pwm_ChannelConfigurationType Channels[PWM_NUMBER_OF_CHANNELS];
#if (PWM_NOTIFICATION_SUPPORTED==STD_ON)
	Pwm_NotificationHandlerType NotificationHandlers[PWM_NUMBER_OF_CHANNELS];
#endif
	Pwm_NotificationHandlerType ArcNotificationHandlers[PWM_NUMBER_OF_CHANNELS];

#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	Pwm_ChannelClassType  ChannelClass[PWM_NUMBER_OF_CHANNELS];
#endif
} Pwm_ConfigType;


/* Configuration Set Handles */
#define PwmChannelConfigSet (&PwmConfig)
#define Pwm_PwmChannelConfigSet (&PwmConfig)


#endif /* PWM_CFG_H_ */

