

#if !(((DIO_SW_MAJOR_VERSION == 5) && (DIO_SW_MINOR_VERSION == 0)) )
#error Dio: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((DIO_AR_RELEASE_MAJOR_VERSION == 4) && (DIO_AR_RELEASE_MINOR_VERSION == 1)) )
#error Dio: Configuration file expected AUTOSAR version to be 4.1.*
#endif


#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#include "Port.h"

#define DIO_VERSION_INFO_API    STD_ON
#define DIO_DEV_ERROR_DETECT    STD_ON
#define DIO_POSTBUILD_VARIANT   STD_OFF

#define DIO_END_OF_LIST  (0xFFu)


//Some enum Dio_PortTypesType from Port
typedef enum
{
  DIO_PORT_A = 0,
  DIO_PORT_B = 1,
  DIO_PORT_C = 2,
  DIO_PORT_D = 3
} Dio_PortTypesType;

// Channels
#define DioConf_DioChannel_LED1 43
#define DioConf_DioChannel_LED2 21
#define DioConf_DioChannel_LED_D3 33
#define DioConf_DioChannel_Switch7 7

// Channel groups


// Ports
#define DioConf_DioPort_PORT_A (DIO_PORT_A)
#define DioConf_DioPort_PORT_NHET (DIO_PORT_C)

// Pointers for convenience.
// Channels	
extern const Dio_ChannelType DioChannelTypes[];

extern const Dio_PortType DioChannelToPortMap[];

// Channel group
extern const Dio_ChannelGroupType DioChannelConfigData[];

// Port
extern const Dio_PortType DioPortConfigData[];

#endif /*DIO_CFG_H_*/
