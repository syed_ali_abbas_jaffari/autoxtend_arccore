
#include "Pwm.h"

/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here.
 */
extern void NotificationPwmCh1(void);

const Pwm_ConfigType PwmConfig = {
	.Channels = {
		
		PWM_CHANNEL_CONFIG(
							PwmConf_PwmChannel_Channel_LED_3,
							// Desired period in seconds: 0.001
							1000, // Hz
							// Duty cycle (0 ~> 0%, 0x8000 ~> 100%)
							16384,
							// Local prescaler zero base register value
							0,
							// Polarity
							PWM_HIGH,
							// Idle state
							PWM_LOW,
							// Channel class
							PWM_FIXED_PERIOD,
							// Custom notification period
							0,
							// Deadband delay
							0),
							
	},
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	.ChannelClass={	
		PWM_FIXED_PERIOD,
	},
#endif
	.ArcNotificationHandlers = {
		// Custom notification routine for Pwm_Channel_LED_3
		NULL,
	},
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	.NotificationHandlers = {
		// Notification routine for Pwm_Channel_LED_3
		NotificationPwmCh1,
	}
#endif
};

