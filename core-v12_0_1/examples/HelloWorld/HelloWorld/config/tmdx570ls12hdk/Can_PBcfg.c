#include <stdlib.h>
#include "Can.h"
#include "CanIf_Cbk.h"


const Can_HardwareObjectType CanHardwareObjectConfig[] = {
	{
		.CanObjectId	=	CanConf_CanHardwareObject_Ctrl_0_Rx_0, 
		.CanHandleType	=	CAN_ARC_HANDLE_TYPE_BASIC,
		.CanIdType		=	CAN_ID_TYPE_STANDARD,
		.CanObjectType	=	CAN_OBJECT_TYPE_RECEIVE,
		.CanHwFilterCode	= 	0x2U,
		.CanHwFilterMask 	=	0x0U,
		.Can_Arc_NrMessageBoxes = 1,
		.ArcMailboxMask 	    = 	0x1ULL,
		.ArcCanControllerId	 	=	0,
		.Can_Arc_EOL	= 	0
	},
	{
		.CanObjectId	=	CanConf_CanHardwareObject_Ctrl_0_Tx_0, 
		.CanHandleType	=	CAN_ARC_HANDLE_TYPE_BASIC,
		.CanIdType		=	CAN_ID_TYPE_STANDARD,
		.CanObjectType	=	CAN_OBJECT_TYPE_TRANSMIT,
		.CanHwFilterCode	= 	0x0U,
		.CanHwFilterMask 	=	0x1fffffffU,
		.Can_Arc_NrMessageBoxes = 1,
		.ArcMailboxMask 	    = 	0x2ULL,
		.ArcCanControllerId	 	=	0,
		.Can_Arc_EOL	= 	1
	},
};




const uint8 Can_MailBoxToHrh_CanController0[]= {
	CanConf_CanHardwareObject_Ctrl_0_Rx_0,
	CanConf_CanHardwareObject_Ctrl_0_Tx_0,
};

	
const Can_ControllerBaudrateConfigType Can_SupportedBaudrates_CanController0[] =
{
	{
		.CanControllerBaudRate = 		500,
		.CanControllerPropSeg =			8,
		.CanControllerSeg1 =			5,
		.CanControllerSeg2 =			2,
		.CanControllerSyncJumpWidth = 	1,
	},
};


	
const Can_ControllerConfigType CanControllerConfigData[] =
{
	{
		.CanControllerActivation	=	TRUE,
		.CanControllerId	 		=	0,
		.CanBusOffProcessing 		=	CAN_ARC_PROCESS_TYPE_INTERRUPT,
		.CanRxProcessing 			=	CAN_ARC_PROCESS_TYPE_INTERRUPT,
		.CanTxProcessing			=	CAN_ARC_PROCESS_TYPE_INTERRUPT,
		.CanWakeupProcessing		=	CAN_ARC_PROCESS_TYPE_INTERRUPT,
		
 		.Can_Arc_Hoh 				=	CanHardwareObjectConfig,
    	
    	.Can_Arc_Fifo 				= 	0, 
		.CanControllerDefaultBaudrate = 500,
    	.CanControllerSupportedBaudrates = Can_SupportedBaudrates_CanController0,
    	.CanControllerSupportedBaudratesCount = 1,
    	.CanControllerPropSeg		=	8,
		.CanControllerSeg1			=	5,    	    	 	 	
		.CanControllerSeg2			=	2,
		.Can_Arc_MailBoxToHrh       =     Can_MailBoxToHrh_CanController0,
		.Can_Arc_Loopback           =   0,
  	},
};

const Can_CallbackType CanCallbackConfigData = {
    NULL,
    CanIf_RxIndication,
    CanIf_ControllerBusOff,
    CanIf_TxConfirmation,
    NULL, //CanIf_ControllerWakeup,
    NULL, 
    CanIf_ControllerModeIndication,
};

const Can_ConfigSetType CanConfigSetData =
{
	.CanController 	=	CanControllerConfigData,
	.CanCallbacks 	=	&CanCallbackConfigData,
	.ArcCanHardwareObjects = CanHardwareObjectConfig
};

const Can_ConfigType CanConfigData = {
	.CanConfigSetPtr =	&CanConfigSetData,
};
