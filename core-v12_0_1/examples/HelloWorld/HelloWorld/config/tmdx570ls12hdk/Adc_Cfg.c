

#include "Adc.h"
#include "Adc_Cfg.h"


Adc_GroupStatus AdcGroupStatus_AdcHwUnit[1];

const Adc_HWConfigurationType AdcHWUnitConfiguration_AdcHwUnit =
{
	.hwUnitId = 0,
	.adcPrescale = 8,
	.clockSource = ADC_SYSTEM_CLOCK,
};

const Adc_ChannelConfigurationType AdcChannelConfiguration_AdcHwUnit[] =
{
	{ 
		//.adcChannelConvTime = 0 
	},
};

const Adc_ChannelType Adc_AdcHwUnit_AdcGroup1_ChannelList[ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS] = 
{
	ADC_CH0
};

Adc_ValueGroupType Adc_AdcHwUnit_AdcGroup1_Buffer[ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS];
	
	

const Adc_GroupDefType AdcGroupConfiguration_AdcHwUnit[] =
{	
	{
     	.accessMode         = ADC_ACCESS_MODE_SINGLE,
     	.conversionMode     = ADC_CONV_MODE_ONESHOT,
     	.triggerSrc         = ADC_TRIGG_SRC_SW,			/* TRIGG_SRC_HW not supported */
     	.hwTriggerSignal    = ADC_HW_TRIG_FALLING_EDGE, /* TRIGG_SRC_HW supported */
     	.hwTriggerTimer     = ADC_NO_TIMER,				/* TRIGG_SRC_HW supported */ 
		.groupCallback 		= IoHwAb_AdcConversionComplete,
     	.streamBufferMode   = ADC_STREAM_BUFFER_LINEAR,
		 
		.streamNumSamples   = 1,
				
		.channelList 		= Adc_AdcHwUnit_AdcGroup1_ChannelList,
		.resultBuffer       = Adc_AdcHwUnit_AdcGroup1_Buffer,		
		.numberOfChannels   = ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS,
		.status             = &AdcGroupStatus_AdcHwUnit[0],
		.hwUnit				= 0,
		.groupId 			= 0 * 100 + 0,
		.arcAdcGroupSampTime = 4,
	},
};

const Adc_ConfigType AdcConfig[] =
{
	{
		.hwConfigPtr      = &AdcHWUnitConfiguration_AdcHwUnit,
		.channelConfigPtr = AdcChannelConfiguration_AdcHwUnit,
		.nbrOfChannels    = 1,
		.groupConfigPtr   = AdcGroupConfiguration_AdcHwUnit,
		.nbrOfGroups      = 1
	},
};
