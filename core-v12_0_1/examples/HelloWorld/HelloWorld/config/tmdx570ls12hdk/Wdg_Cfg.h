

#include "Wdg.h"
#include "Mcu.h"

#if !(((WDG_SW_MAJOR_VERSION == 2) && (WDG_SW_MINOR_VERSION == 0)) )
#error Wdg: Configuration file expected BSW module version to be 2.0.*
#endif

#if !(((WDG_AR_RELASE_MAJOR_VERSION == 4) && (WDG_AR_RELASE_MINOR_VERSION == 1)) )
#error Wdg: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#ifndef WDG_CFG_H_
#define WDG_CFG_H_

#define	WDG_VERSION_INFO_API			STD_OFF
#define	WDG_DEV_ERROR_DETECT			STD_ON
#define WDG_DISABLE_ALLOWED				STD_OFF //never allowed on this hardware

typedef struct {
	uint16 			period;
} Wdg_SettingsType;

typedef struct {
	WdgIf_ModeType 				Wdg_DefaultMode;
	const Wdg_SettingsType* 	WdgSettingsFast;
} Wdg_ModeConfigType;

typedef struct
{
	const Wdg_ModeConfigType *Wdg_ModeConfig;
	boolean Wdg_ResetEnable;
} Wdg_ConfigType;

 extern const Wdg_ConfigType WdgConfig;
 
#define WdgConf_WdgIndex_WdgIndex 

#endif /* WDG_CFG_H_ */
