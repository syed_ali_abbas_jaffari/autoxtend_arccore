

#include "Port.h"

#include <stdlib.h>


static const ArcPort_PadConfigType PortPadConfigData[] = {

	{   /* Name: N2HET1_27, signal: N2HET1_27, pin: 4, pad: A5 */
		.regBit = PIN_N2HET1_27_REG_BIT,
		.extraBit = PIN_N2HET1_27_MULTI_REG_BIT,
		.config    = (PORT_DIR_OUT | PORT_PULL_NONE  | PORT_PIN_LEVEL_LOW),
		.mode      = PORT_PIN_MODE_DIO,
		.regOffset = PIN_N2HET1_27_REG_OFFSET,
		.extraOffset = PIN_N2HET1_27_MULTI_REG_OFFSET
	},
	{   /* Name: GIOA_7, signal: GIOA_7, pin: 11, pad: A5 */
		.regBit = PIN_GIOA_7_REG_BIT,
		.extraBit = PORT_FUNC_INVALID,
		.config    = (PORT_DIR_IN | PORT_PULL_NONE  | PORT_PIN_LEVEL_LOW),
		.mode      = PORT_PIN_MODE_DIO,
		.regOffset = PIN_GIOA_7_REG_OFFSET,
		.extraOffset = PORT_FUNC_INVALID
	},
	{   /* Name: ETPWM1A, signal: ETPWM1A, pin: 14, pad: A5 */
		.regBit = PIN_ETPWM1A_REG_BIT,
		.extraBit = PORT_FUNC_INVALID,
		.config    = (PORT_DIR_OUT | PORT_PULL_NONE  | PORT_PIN_LEVEL_LOW),
		.mode      = PORT_PIN_MODE_PWM,
		.regOffset = PIN_ETPWM1A_REG_OFFSET,
		.extraOffset = PORT_FUNC_INVALID
	},
	{   /* Name: N2HET1_05, signal: N2HET1_05, pin: 31, pad: A5 */
		.regBit = PIN_N2HET1_05_REG_BIT,
		.extraBit = PIN_N2HET1_05_MULTI_REG_BIT,
		.config    = (PORT_DIR_OUT | PORT_PULL_NONE  | PORT_PIN_LEVEL_LOW),
		.mode      = PORT_PIN_MODE_DIO,
		.regOffset = PIN_N2HET1_05_REG_OFFSET,
		.extraOffset = PIN_N2HET1_05_MULTI_REG_OFFSET
	},
	{   /* Name: N2HET1_17, signal: N2HET1_17, pin: 130, pad: A5 */
		.regBit = PIN_N2HET1_17_REG_BIT,
		.extraBit = PIN_N2HET1_17_MULTI_REG_BIT,
		.config    = (PORT_DIR_OUT | PORT_PULL_NONE  | PORT_PIN_LEVEL_LOW),
		.mode      = PORT_PIN_MODE_DIO,
		.regOffset = PIN_N2HET1_17_REG_OFFSET,
		.extraOffset = PIN_N2HET1_17_MULTI_REG_OFFSET
	},

};



const Port_ConfigType PortConfigData =
{
  .padConfig = PortPadConfigData,
};


