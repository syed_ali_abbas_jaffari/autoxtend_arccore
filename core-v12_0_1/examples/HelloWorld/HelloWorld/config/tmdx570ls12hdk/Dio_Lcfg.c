

#include "Dio.h"
#include "Dio_Cfg.h"

const Dio_ChannelType DioChannelTypes[] = { 
	DioConf_DioChannel_Switch7,
	DioConf_DioChannel_LED2,
	DioConf_DioChannel_LED_D3,
	DioConf_DioChannel_LED1,
	DIO_END_OF_LIST
};

const Dio_PortType DioPortConfigData[] = { 
	DioConf_DioPort_PORT_A,
	DioConf_DioPort_PORT_NHET,
	DIO_END_OF_LIST
};

const Dio_ChannelGroupType DioChannelConfigData[] = {
	{ 
	  .port = DIO_END_OF_LIST, 
	  .mask = 0,
	  .offset = 0
	}
};

const Dio_ConfigType DioConfigData = {
    .ChannelConfig = DioChannelTypes,
    .GroupConfig = DioChannelConfigData,
    .PortConfig = DioPortConfigData
};

