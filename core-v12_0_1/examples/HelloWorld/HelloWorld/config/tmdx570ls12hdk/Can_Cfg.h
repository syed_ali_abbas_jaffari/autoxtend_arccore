

#ifndef CAN_CFG_H_
#define CAN_CFG_H_

#if !(((CAN_SW_MAJOR_VERSION == 5) && (CAN_SW_MINOR_VERSION == 0)) )
#error Can: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((CAN_AR_MAJOR_VERSION == 4) && (CAN_AR_MINOR_VERSION == 1)) )
#error Can: Configuration file expected AUTOSAAR version to be 4.1.*
#endif

// Number of controller configs
#define CAN_ARC_CTRL_CONFIG_CNT			1

#define CAN_DEV_ERROR_DETECT			STD_ON
#define CAN_VERSION_INFO_API			STD_ON
#define CAN_MULTIPLEXED_TRANSMISSION	STD_OFF  // Not supported
#define CAN_WAKEUP_SUPPORT				STD_OFF  // Not supported
#define CAN_HW_TRANSMIT_CANCELLATION	STD_OFF  // Not supported
#define ARC_CAN_ERROR_POLLING			STD_OFF  // Not supported
#define CAN_USE_WRITE_POLLING			STD_OFF  // Not supported
#define CAN_USE_READ_POLLING			STD_OFF  // Not supported
#define CAN_USE_BUSOFF_POLLING			STD_OFF  // Not supported
#define CAN_USE_WAKEUP_POLLING			STD_OFF  // Not supported
#define CAN_POSTBUILD_VARIANT  			STD_OFF

typedef enum {
	DCAN1 = 0,
	DCAN2 = 1,
	DCAN3 = 2
} CanControllerIdType;

#define CAN_CONTROLLER_CNT 1

#define CanConf_CanController_CanController0	(CanControllerIdType)0 //FLEXCAN_A


#define CanConf_CanHardwareObject_Ctrl_0_Rx_0 (Can_HwHandleType)0
#define CanConf_CanHardwareObject_Ctrl_0_Tx_0 (Can_HwHandleType)1

#define NUM_OF_HOHS (Can_HwHandleType)2

/* Configuration Set Handles */
#define CanConfigSet (&CanConfigData)
#define Can_CanConfigSet (&CanConfigData)

#endif /*CAN_CFG_H_*/

