

#if !(((PORT_SW_MAJOR_VERSION == 5) && (PORT_SW_MINOR_VERSION == 0)) )
#error Port: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((PORT_AR_RELEASE_MAJOR_VERSION == 4) && (PORT_AR_RELEASE_MINOR_VERSION == 1)) )
#error Port: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#ifndef PORT_CFG_H_
#define PORT_CFG_H_


#define	PORT_VERSION_INFO_API			STD_OFF
#define	PORT_DEV_ERROR_DETECT			STD_ON
#define PORT_SET_PIN_MODE_API			STD_OFF
#define PORT_SET_PIN_DIRECTION_API		STD_OFF
#define PORT_POSTBUILD_VARIANT 			STD_OFF

 
#define PORT_DIR_IN         (1 << 0)
#define PORT_DIR_OUT        (1 << 1)
#define PORT_ODE_ENABLE     (1 << 2)
#define PORT_PULL_NONE		(1 << 3)
#define PORT_PULL_UP        (1 << 4)
#define PORT_PULL_DOWN      (1 << 5)
#define PORT_PIN_LEVEL_HIGH (1 << 6)
#define PORT_PIN_LEVEL_LOW  (0 << 6)
#define PORT_PIN_N2HET2  	(1 << 7)

#define PORT_PIN_DIO_SPI1 		(0 << 8)
#define PORT_PIN_DIO_SPI2 		(1 << 8)
#define PORT_PIN_DIO_SPI3		(2 << 8)
#define PORT_PIN_DIO_SPI4 		(3 << 8)
#define PORT_PIN_DIO_SPI5 		(4 << 8)

#define PORT_FUNC0          0
#define PORT_FUNC1          1
#define PORT_FUNC2          2
#define PORT_FUNC3          3

#define PORT_FUNC_INVALID   (0xFF)



#define	PORT_GPDO_RESET		(0)
#define	PORT_GPDO_HIGH		(1)



#define PORT_PIN_MODE_DIO 0
#define PORT_PIN_MODE_DIO_SPI 1
#define PORT_PIN_MODE_PWM 2 


/* Mode specific function resolving */
#include "PortDefs.h"
#define PORT_INVALID_REG	((uint16)0xFFFF)


typedef struct {
	uint32 regBit; // Used as pin for GIO ports and as bit for PINMMR regs.
	uint32 extraBit; // Used for PINMMR bit for some multiplexed func/GIO pins 
	uint32 config;
	uint8 mode;
	uint8 regOffset; // Used as identifier of GIO port reg number and PINMMR reg number
	uint8 extraOffset; // Used for PINMMR register for some multiplexed func/GIO pins
} ArcPort_PadConfigType;


typedef struct
{
	const ArcPort_PadConfigType *padConfig;
} Port_ConfigType;




#define PortConf_PortPin_N2HET1_27 ((Port_PinType)0)
#define PortConf_PortPin_GIOA_7 ((Port_PinType)1)
#define PortConf_PortPin_ETPWM1A ((Port_PinType)2)
#define PortConf_PortPin_N2HET1_05 ((Port_PinType)3)
#define PortConf_PortPin_N2HET1_17 ((Port_PinType)4)

#define PORT_NR_PINS 5 

extern const Port_ConfigType PortConfigData;

/* Configuration Set Handles */
#define PortConfigSet (&PortConfigData)
#define Port_PortConfigSet (&PortConfigData)


#endif /* PORT_CFG_H_ */
