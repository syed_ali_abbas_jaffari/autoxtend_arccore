

#include "Wdg.h"

const Wdg_SettingsType WdgSettingsFast = {
	.period = 4, //in Hz
};

const Wdg_ModeConfigType WdgModeConfig = {
	.Wdg_DefaultMode = WDGIF_OFF_MODE,
	.WdgSettingsFast = &WdgSettingsFast,
};

const Wdg_ConfigType WdgConfig = {
	.Wdg_ModeConfig = &WdgModeConfig,
	
};


