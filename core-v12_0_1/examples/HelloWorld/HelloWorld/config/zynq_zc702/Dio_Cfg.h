
/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.1.2
 */

#ifndef DIO_CFG_H_
#define DIO_CFG_H_

#if !(((DIO_SW_MAJOR_VERSION == 5) && (DIO_SW_MINOR_VERSION == 0)) )
#error Dio: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((DIO_AR_RELEASE_MAJOR_VERSION == 4) && (DIO_AR_RELEASE_MINOR_VERSION == 1)) )
#error Dio: Configuration file expected AUTOSAR version to be 4.1.*
#endif


#define DIO_VERSION_INFO_API    STD_OFF
#define DIO_DEV_ERROR_DETECT    STD_ON
#define DIO_POSTBUILD_VARIANT   STD_OFF
#define DIO_AVAILABLE_CHANNELS 	0x900UL //bit coded message representing individual channels supported

#define DIO_END_OF_LIST  (0xFFFFFFFFu)


// Physical ports for Zynq (GPIO)
typedef enum
{
  DIO_BANK_0 = 0,
  DIO_BANK_1 = 1,
  DIO_BANK_2 = 2,
  DIO_BANK_3 = 3
} Dio_PortTypesType;

// Channels
#define DioConf_DioChannel_LED1 8
#define Dio_LED1 DioConf_DioChannel_LED1
#define DioConf_DioChannel_LED2 11
#define Dio_LED2 DioConf_DioChannel_LED2

// Channel groups


// Ports
#define DioConf_DioPort_LED_PORT (DIO_BANK_0)
#define Dio_LED_PORT DioConf_DioPort_LED_PORT

/* Configuration Set Handles */
#define DioConfig (DioConfigData)
#define Dio_DioConfig (DioConfigData)

#endif /*DIO_CFG_H_*/
