/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

/*lint -save -e785 Misra 2012 9.3  Too few initializers */
/*lint -save -e957 Misra 2004 8.1  Some function defined without a prototype in scope */

#include "Spi.h"

// Callbacks
 


// External Devices 
const Spi_ExternalDeviceType SpiExternalConfigData[] =
{
 {
  .SpiBaudrate = 434027UL,
  .SpiCsIdentifier = 0,
   
  .SpiHwUnit = CSIB0,
  .SpiDataShiftEdge = SPI_EDGE_TRAILING,
  .SpiEnableCs   = 0, // NOT SUPPORTED IN TOOLS
  .SpiShiftClockIdleLevel = STD_HIGH,
  .SpiTimeClk2Cs = 0,   // ns
 }
};

const SpiZynqDelay0RegType SpiExternalDeviceDelayCfg[] =
{
    {
        .SpiArcDAfter  = 0, 
        .SpiArcDBtwn   = 0, 
        .SpiArcDNss    = 0,
    }
};

const Spi_Hw_DeviceConfigType SpiHwDeviceCfg = {
    .SpiHwDelayReg = SpiExternalDeviceDelayCfg,
    .SpiHwExternalDeviceIdx = {
    0,  
    0,  
    0,  
    0,  
    },
};

// Channels 
const Spi_ChannelConfigType SpiChannelConfigData[] =
{
{
  .SpiChannelId = SpiConf_SpiChannel_CH_ADDR,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 16,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_CMD,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_DATA,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_WREN,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 1,
  .SpiDefaultData = 6,
 }
,
 {
   .SpiChannelId = CH_NOT_VALID,
 }

};


// Jobs
const Spi_JobConfigType SpiJobConfigData[] =
{
   {
      .SpiJobId = SpiConf_SpiJob_JOB_WREN,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 3,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_WREN],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_DATA,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 2,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_ADDR],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_DATA],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_CMD2,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 0,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_DATA],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_CMD,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 0,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
};



// Sequences 
const Spi_SequenceConfigType SpiSequenceConfigData[] =
{
   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_CMD,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_CMD],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_CMD2,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_CMD2],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_READ,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_DATA],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_WRITE,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_WREN],
          &SpiJobConfigData[SpiConf_SpiJob_JOB_DATA],
          NULL          
      },
   }
};


uint32 Spi_GetChanneCnt(void ) { return sizeof(SpiChannelConfigData)/sizeof(SpiChannelConfigData[0]); }

uint32 Spi_GetExternalDeviceCnt(void ) { return sizeof(SpiExternalConfigData)/sizeof(SpiExternalConfigData[0]); }


const Spi_DriverType SpiConfigData =
{
  .SpiMaxChannel = SPI_MAX_CHANNEL,
  .SpiMaxJob = SPI_MAX_JOB,
  .SpiMaxSequence = SPI_MAX_SEQUENCE,
  .SpiChannelConfig = &SpiChannelConfigData[0],
  .SpiSequenceConfig = &SpiSequenceConfigData[0],
  .SpiJobConfig = &SpiJobConfigData[0],
  .SpiExternalDevice = &SpiExternalConfigData[0],
  .SpiHwDeviceConfig = &SpiHwDeviceCfg,
};

/*lint -restore*/
