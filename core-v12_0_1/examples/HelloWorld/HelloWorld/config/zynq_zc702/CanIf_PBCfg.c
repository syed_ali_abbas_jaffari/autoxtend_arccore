/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.0.3
 */

#include "CanIf.h"
#include "CanIf_PBCfg.h"

#if defined(USE_CANTP)
#include "CanTp.h"
#include "CanTp_Cbk.h"
#endif

#if defined(USE_J1939TP)
#include "J1939Tp.h"
#include "J1939Tp_Cbk.h"
#endif

#if defined(USE_CANNM)
#include "CanNm.h"
#include "CanNm_PBCfg.h"
#endif
#if defined(USE_PDUR)
#include "PduR.h"
#include "PduR_PbCfg.h"
#endif
#if defined(USE_XCP)
#include "Xcp.h"
#include "XcpOnCan_Cbk.h"
#endif

#include "MemMap.h"


// Data for init configuration CanIfInitConfiguration

SECTION_POSTBUILD_DATA const CanIf_HthConfigType CanIfHthConfigData_CanIfInitHohCfg[] =
{
	{ 
    	.CanIfHthType 				= CANIF_HANDLE_TYPE_BASIC,
    	.CanIfCanControllerIdRef 	= CanIfConf_CanIfCtrlCfg_CanIfCtrlCfg,
    	.CanIfHthIdSymRef 			= CanConf_CanHardwareObject_Ctrl_0_Tx_0,
    	.CanIf_Arc_EOL 				= TRUE,
	},
};

SECTION_POSTBUILD_DATA const CanIf_HrhConfigType CanIfHrhConfigData_CanIfInitHohCfg[] =
{
	{
    	.CanIfHrhType 				= CANIF_HANDLE_TYPE_BASIC,
    	.CanIfSoftwareFilterHrh 	= TRUE,
    	.CanIfCanControllerHrhIdRef = CanIfConf_CanIfCtrlCfg_CanIfCtrlCfg,
    	.CanIfHrhIdSymRef 			= CanConf_CanHardwareObject_Ctrl_0_Rx_0,
    	.CanIf_Arc_EOL				= TRUE,
  	},
};

SECTION_POSTBUILD_DATA const CanIf_InitHohConfigType CanIfHohConfigData[] = { 
	{
		.CanIfHrhConfig 	= CanIfHrhConfigData_CanIfInitHohCfg,
	    .CanIfHthConfig 	= CanIfHthConfigData_CanIfInitHohCfg,
    	.CanIf_Arc_EOL 		= TRUE,
	},
};

SECTION_POSTBUILD_DATA const CanIf_TxBufferConfigType CanIfBufferCfgData[] = {
	{
		.CanIfBufferSize = 0,
		.CanIfBufferHthRef = &CanIfHthConfigData_CanIfInitHohCfg[0],
		.CanIf_Arc_BufferId = 0
	},
};

SECTION_POSTBUILD_DATA const CanIf_TxPduConfigType CanIfTxPduConfigData[] = {
	{
		.CanIfTxPduId 				= PDUR_REVERSE_PDU_ID_ECU01,
    	.CanIfCanTxPduIdCanId 		= 1,
    	.CanIfCanTxPduIdDlc 		= 8,
    	.CanIfCanTxPduType 			= CANIF_PDU_TYPE_STATIC,
    	.CanIfTxPduPnFilterEnable	= STD_OFF,
#if ( CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadTxPduNotifyStatus = FALSE,
#endif
    	.CanIfTxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserTxConfirmation 	= PDUR_CALLOUT,
    	/* [CanIfBufferCfg] */
    	.CanIfTxPduBufferRef		= &CanIfBufferCfgData[0],
    	.PduIdRef 					= NULL,
	},
	{
		.CanIfTxPduId 				= CANTP_PDU_ID_DIAGPHYSICALTX,
    	.CanIfCanTxPduIdCanId 		= 1587,
    	.CanIfCanTxPduIdDlc 		= 8,
    	.CanIfCanTxPduType 			= CANIF_PDU_TYPE_STATIC,
    	.CanIfTxPduPnFilterEnable	= STD_OFF,
#if ( CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadTxPduNotifyStatus = FALSE,
#endif
    	.CanIfTxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserTxConfirmation 	= CANTP_CALLOUT,
    	/* [CanIfBufferCfg] */
    	.CanIfTxPduBufferRef		= &CanIfBufferCfgData[0],
    	.PduIdRef 					= NULL,
	},
	{
		.CanIfTxPduId 				= PDUR_REVERSE_PDU_ID_DIAGUUDT,
    	.CanIfCanTxPduIdCanId 		= 1696,
    	.CanIfCanTxPduIdDlc 		= 8,
    	.CanIfCanTxPduType 			= CANIF_PDU_TYPE_STATIC,
    	.CanIfTxPduPnFilterEnable	= STD_OFF,
#if ( CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadTxPduNotifyStatus = FALSE,
#endif
    	.CanIfTxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserTxConfirmation 	= PDUR_CALLOUT,
    	/* [CanIfBufferCfg] */
    	.CanIfTxPduBufferRef		= &CanIfBufferCfgData[0],
    	.PduIdRef 					= NULL,
	},
	{
		.CanIfTxPduId 				= XCP_PDU_ID_XCP_TX_PDU,
    	.CanIfCanTxPduIdCanId 		= 1001,
    	.CanIfCanTxPduIdDlc 		= 8,
    	.CanIfCanTxPduType 			= CANIF_PDU_TYPE_STATIC,
    	.CanIfTxPduPnFilterEnable	= STD_OFF,
#if ( CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadTxPduNotifyStatus = FALSE,
#endif
    	.CanIfTxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserTxConfirmation 	= XCP_CALLOUT,
    	/* [CanIfBufferCfg] */
    	.CanIfTxPduBufferRef		= &CanIfBufferCfgData[0],
    	.PduIdRef 					= NULL,
	},
	{
		.CanIfTxPduId 				= PDUR_REVERSE_PDU_ID_SWCMEM_TX_CANIF,
    	.CanIfCanTxPduIdCanId 		= 16,
    	.CanIfCanTxPduIdDlc 		= 8,
    	.CanIfCanTxPduType 			= CANIF_PDU_TYPE_STATIC,
    	.CanIfTxPduPnFilterEnable	= STD_OFF,
#if ( CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadTxPduNotifyStatus = FALSE,
#endif
    	.CanIfTxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserTxConfirmation 	= PDUR_CALLOUT,
    	/* [CanIfBufferCfg] */
    	.CanIfTxPduBufferRef		= &CanIfBufferCfgData[0],
    	.PduIdRef 					= NULL,
	},
};

SECTION_POSTBUILD_DATA const CanIf_RxPduConfigType CanIfRxPduConfigData[] = {
	{
		.CanIfCanRxPduId 			= PDUR_PDU_ID_TESTER01,
    	.CanIfCanRxPduLowerCanId 	= 2,
    	.CanIfCanRxPduUpperCanId 	= 2,
    	.CanIfCanRxPduDlc 			= 8,
#if ( CANIF_PUBLIC_READRXPDU_DATA_API == STD_ON )    
    	.CanIfReadRxPduData 		= FALSE,
#endif    
#if ( CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadRxPduNotifyStatus = FALSE, 
#endif
		.CanIfCanRxPduHrhRef		= &CanIfHrhConfigData_CanIfInitHohCfg[0],
    	.CanIfRxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserRxIndication 		= PDUR_CALLOUT,
    	.CanIfCanRxPduCanIdMask 	= 0x7FF,
    	.PduIdRef	 				= NULL,
	},
	{
		.CanIfCanRxPduId 			= CANTP_PDU_ID_DIAGFUNCTIONALRX,
    	.CanIfCanRxPduLowerCanId 	= 2047,
    	.CanIfCanRxPduUpperCanId 	= 2047,
    	.CanIfCanRxPduDlc 			= 8,
#if ( CANIF_PUBLIC_READRXPDU_DATA_API == STD_ON )    
    	.CanIfReadRxPduData 		= FALSE,
#endif    
#if ( CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadRxPduNotifyStatus = FALSE, 
#endif
		.CanIfCanRxPduHrhRef		= &CanIfHrhConfigData_CanIfInitHohCfg[0],
    	.CanIfRxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserRxIndication 		= CANTP_CALLOUT,
    	.CanIfCanRxPduCanIdMask 	= 0x7FF,
    	.PduIdRef	 				= NULL,
	},
	{
		.CanIfCanRxPduId 			= CANTP_PDU_ID_DIAGPHYSICALRX,
    	.CanIfCanRxPduLowerCanId 	= 1843,
    	.CanIfCanRxPduUpperCanId 	= 1843,
    	.CanIfCanRxPduDlc 			= 8,
#if ( CANIF_PUBLIC_READRXPDU_DATA_API == STD_ON )    
    	.CanIfReadRxPduData 		= FALSE,
#endif    
#if ( CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadRxPduNotifyStatus = FALSE, 
#endif
		.CanIfCanRxPduHrhRef		= &CanIfHrhConfigData_CanIfInitHohCfg[0],
    	.CanIfRxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserRxIndication 		= CANTP_CALLOUT,
    	.CanIfCanRxPduCanIdMask 	= 0x7FF,
    	.PduIdRef	 				= NULL,
	},
	{
		.CanIfCanRxPduId 			= XCP_PDU_ID_XCP_RX_PDU,
    	.CanIfCanRxPduLowerCanId 	= 1000,
    	.CanIfCanRxPduUpperCanId 	= 1000,
    	.CanIfCanRxPduDlc 			= 8,
#if ( CANIF_PUBLIC_READRXPDU_DATA_API == STD_ON )    
    	.CanIfReadRxPduData 		= FALSE,
#endif    
#if ( CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadRxPduNotifyStatus = FALSE, 
#endif
		.CanIfCanRxPduHrhRef		= &CanIfHrhConfigData_CanIfInitHohCfg[0],
    	.CanIfRxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserRxIndication 		= XCP_CALLOUT,
    	.CanIfCanRxPduCanIdMask 	= 0x7FF,
    	.PduIdRef	 				= NULL,
	},
	{
		.CanIfCanRxPduId 			= PDUR_PDU_ID_SWCMEM_RX_CANIF,
    	.CanIfCanRxPduLowerCanId 	= 17,
    	.CanIfCanRxPduUpperCanId 	= 17,
    	.CanIfCanRxPduDlc 			= 8,
#if ( CANIF_PUBLIC_READRXPDU_DATA_API == STD_ON )    
    	.CanIfReadRxPduData 		= FALSE,
#endif    
#if ( CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API == STD_ON )
    	.CanIfReadRxPduNotifyStatus = FALSE, 
#endif
		.CanIfCanRxPduHrhRef		= &CanIfHrhConfigData_CanIfInitHohCfg[0],
    	.CanIfRxPduIdCanIdType 		= CANIF_CAN_ID_TYPE_11,
    	.CanIfUserRxIndication 		= PDUR_CALLOUT,
    	.CanIfCanRxPduCanIdMask 	= 0x7FF,
    	.PduIdRef	 				= NULL,
	},
};

SECTION_POSTBUILD_DATA const CanIf_TxBufferConfigType *const CanIfCtrlCfg_BufferList[] = {
	/* CanIfBufferCfg */
	&CanIfBufferCfgData[0],
};


SECTION_POSTBUILD_DATA const CanIf_Arc_ChannelConfigType CanIf_Arc_ChannelConfig[CANIF_CHANNEL_CNT] = { 
	{
		/* CanIfCtrlCfg */
		.CanControllerId 		= CanConf_CanController_CanController0,
		.NofTxBuffers 			= 1,
		.TxBufferRefList 		= CanIfCtrlCfg_BufferList,
		.CanIfCtrlWakeUpSupport = STD_OFF,
		.CanIfCtrlWakeUpSrc		= 0,
		.CanIfCtrlPnFilterSet	= STD_OFF,
	},
};


// This container contains the init parameters of the CAN
// Multiplicity 1.
SECTION_POSTBUILD_DATA const CanIf_InitConfigType CanIfInitConfig =
{
	.CanIfConfigSet 					= 0, // Not used  
	.CanIfNumberOfCanRxPduIds 			= 5,
	.CanIfNumberOfCanTXPduIds 			= 5,
	.CanIfNumberOfDynamicCanTXPduIds	= 0, // Not used
	.CanIfNumberOfTxBuffers				= 1,

	// Containers
	.CanIfBufferCfgPtr					= CanIfBufferCfgData,
	.CanIfHohConfigPtr 					= CanIfHohConfigData,
	.CanIfRxPduConfigPtr 				= CanIfRxPduConfigData,
	.CanIfTxPduConfigPtr 				= CanIfTxPduConfigData,
};

// This container includes all necessary configuration sub-containers
// according the CAN Interface configuration structure.
SECTION_POSTBUILD_DATA const CanIf_ConfigType CanIf_Config =
{
	
	.InitConfig 					= &CanIfInitConfig,
	.CanIfTransceiverConfig 		= NULL,
	.Arc_ChannelConfig			 	= CanIf_Arc_ChannelConfig,  
};




