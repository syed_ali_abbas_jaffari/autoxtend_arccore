/*
 * Generator version: 2.1.0
 * AUTOSAR version:   4.1.2
 */

#include "Pwm.h"

/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here, if any.
 */
extern void TestChannel1_Notification(void);

const Pwm_ConfigType PwmConfig = {
	.Channels = {
		{
			.channel   = PwmConf_PwmChannel_Channel_LED_3,
			.frequency = 1000, // Hz
			.duty      = 16384,
			.prescaler = PWM_CHANNEL_PRESCALER_0,
			.polarity  = PWM_HIGH,
		},
	},
	.IdleState = {
		PWM_LOW,
	},

#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	.ChannelClass={	
		PWM_FIXED_PERIOD,
	},
#endif	
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	.NotificationHandlers = {
		// Notification routine for Pwm_Channel_LED_3
		TestChannel1_Notification,
	}
#endif
};

