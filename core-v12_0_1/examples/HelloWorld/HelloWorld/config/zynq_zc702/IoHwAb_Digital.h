
/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef IOHWAB_DIGITAL_H_
#define IOHWAB_DIGITAL_H_

#include "Std_Types.h"

#define IOHWAB_SIGNAL_DIGITALSIGNAL_LED1 ((IoHwAb_SignalType)0)
#define IOHWAB_SIGNAL_DIGITALSIGNAL_LED2 ((IoHwAb_SignalType)1)

Std_ReturnType IoHwAb_Digital_Write(IoHwAb_SignalType signal, IoHwAb_LevelType newValue);


#endif /* IOHWAB_DIGITAL_H_ */


