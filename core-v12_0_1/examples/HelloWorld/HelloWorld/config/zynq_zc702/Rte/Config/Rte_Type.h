/**
 * RTE Types Header File
 *
 * @req SWS_Rte_01161
 * @req SWS_Rte_01160
 */

#ifndef RTE_TYPE_H_
#define RTE_TYPE_H_

/** --- Includes ----------------------------------------------------------------------------------
 * @req SWS_Rte_01163
 */
#include <Rte.h>
#include <Rte_Type_Workarounds.h>

/* @req SWS_Rte_08732 */
typedef struct {
    uint16 clientId;
    uint16 sequenceCounter;
} Rte_Cs_TransactionHandleType;

typedef uint32 Rte_PimType_SwcMemType_PIM1Type;
typedef uint32 Rte_PimType_SwcMemType_PIM2Type;
typedef uint8 EcuM_UserType;
typedef uint8 EcuM_StateType;
typedef uint8 EcuM_BootTargetType;
typedef uint8 Dcm_OpStatusType;
typedef uint8 Dcm_NegativeResponseCodeType;
typedef uint8 Dcm_ConfirmationStatusType;
typedef uint8 Dcm_ProtocolType;
typedef uint8 Dcm_RoeStateType;
typedef uint8 Dcm_SecLevelType;
typedef uint8 Dcm_SesCtrlType;
typedef uint8 Dlt_ReturnType;
typedef uint8 Dlt_MessageLogLevelType;
typedef uint8 Dlt_MessageTraceType;
typedef uint32 Dlt_SessionIDType;
typedef uint32 Dlt_MessageIDType;
typedef uint8 Dlt_MessageOptionsType;
typedef uint16 Dlt_MessageArgumentCount;
typedef uint8 Dlt_ApplicationIDType[4];
typedef uint8 Dlt_ContextIDType[4];
typedef uint8 NvM_RequestResultType;
typedef uint16 NvM_BlockIdType;
typedef uint16 WdgM_CheckpointIdType;
typedef uint16 WdgM_SupervisedEntityIdType;
typedef uint8 WdgM_LocalStatusType;
typedef uint8 WdgM_GlobalStatusType;
typedef uint8 WdgM_ModeType;
typedef uint8 Dem_EventStatusType;
typedef uint8 Dem_EventStatusExtendedType;
typedef uint8 Dem_DTCFormatType;
typedef uint8 Dem_OperationCycleStateType;
typedef uint8 Dem_DTCOriginType;
typedef uint8 Dem_ReturnClearDTCType;
typedef uint8 Dem_IndicatorStatusType;
typedef uint8 Dem_InitMonitorReasonType;
typedef uint16 Dem_EventIdType;
typedef uint8 Dem_OperationCycleIdType;
typedef uint8 Dem_DTCStatusMaskType;
typedef uint8 ComM_UserHandleType;
typedef uint8 ComM_InhibitionStatusType;
typedef uint8 ComM_ModeType;
typedef uint8 StbM_TimeBaseStatusType;
typedef uint16 StbM_SynchronizedTimeBaseType;
typedef uint32 IoHwAb_SignalType_;
typedef uint8 SignalQuality;
typedef uint8 DigitalLevel;
typedef sint32 AnalogValue;
typedef uint32 DutyCycle;
typedef uint32 Frequency;
typedef const void * ConstVoidPtr;
typedef void * VoidPtr;
typedef uint16 * uint16Ptr;
typedef const uint32 * constUint32Ptr;
typedef const uint8 * constUint8Ptr;
typedef uint32 * uint32Ptr;
typedef uint8 * uint8Ptr;
typedef const uint16 * constUint16Ptr;
typedef uint8 NvmCmdType;
typedef uint8 NvmResType;
typedef uint8 TestCmdType;
typedef uint8 Dem_MaxDataValueType[1];
typedef uint8 ComMModeEnum;
typedef uint8 WdgMModeEnum;
typedef uint8 DTRStatusType;
typedef uint8 SecurityLevel_0SeedType[4];
typedef uint8 SecurityLevel_0KeyType[4];

typedef struct {
    Dlt_MessageArgumentCount arg_count;
    Dlt_MessageLogLevelType log_level;
    Dlt_MessageOptionsType options;
    Dlt_ContextIDType context_id;
    Dlt_ApplicationIDType app_id;
} Dlt_MessageLogInfoType;

typedef struct {
    Dlt_MessageTraceType trace_info;
    Dlt_MessageOptionsType options;
    Dlt_ContextIDType context;
    Dlt_ApplicationIDType app_id;
} Dlt_MessageTraceInfoType;

typedef struct {
    StbM_TimeBaseStatusType timeBaseStatus;
    uint32 nanoseconds;
    uint32 seconds;
    uint16 secondsHi;
} StbM_TimeStampType;

typedef struct {
    uint8 userDataLength;
    uint8 userByte0;
    uint8 userByte1;
    uint8 userByte2;
} StbM_UserDataType;

typedef struct {
    StbM_TimeBaseStatusType timeBaseStatus;
    uint32 nanoseconds;
    uint64 seconds;
} StbM_TimeStampExtendedType;

#endif /* RTE_TYPE_H_ */
