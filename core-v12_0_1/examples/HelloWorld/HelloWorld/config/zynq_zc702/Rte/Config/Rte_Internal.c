#include <Rte_Internal.h>
#include <Rte_Calprms.h>
#include <Rte_Assert.h>
#include <Rte_Fifo.h>
#include <Com.h>
#include <Os.h>
#include <Ioc.h>

// --- EXTERNALS ---
extern Std_ReturnType Rte_ioHwAb_DigitalWrite(/*IN*/IoHwAb_SignalType_ portDefArg1, /*IN*/DigitalLevel Level);
extern Std_ReturnType Rte_ioHwAb_AnalogRead(/*IN*/IoHwAb_SignalType_ portDefArg1, /*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality);
extern Std_ReturnType Rte_ioHwAb_PwmSetDuty(/*IN*/IoHwAb_SignalType_ portDefArg1, /*IN*/DutyCycle DutyValue, /*OUT*/SignalQuality * Quality);
extern Std_ReturnType Rte_det_ReportError(/*IN*/uint16 portDefArg1, /*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId);
extern Std_ReturnType Rte_ecuM_RequestRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_ReleaseRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_RequestPOSTRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_ReleasePOSTRUN(/*IN*/EcuM_UserType portDefArg1);
extern Std_ReturnType Rte_ecuM_SelectShutdownTarget(/*IN*/EcuM_StateType target, /*IN*/uint8 mode);
extern Std_ReturnType Rte_ecuM_GetShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode);
extern Std_ReturnType Rte_ecuM_GetLastShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode);
extern Std_ReturnType Rte_ecuM_SelectBootTarget(/*IN*/EcuM_BootTargetType target);
extern Std_ReturnType Rte_ecuM_GetBootTarget(/*OUT*/EcuM_BootTargetType * target);
extern Std_ReturnType Rte_dem_SetEventStatus(/*IN*/Dem_EventIdType portDefArg1, /*IN*/Dem_EventStatusType EventStatus);
extern Std_ReturnType Rte_dem_ResetEventStatus(/*IN*/Dem_EventIdType portDefArg1);
extern Std_ReturnType Rte_dem_SetEventDisabled(/*IN*/Dem_EventIdType portDefArg1);
extern Std_ReturnType Rte_dem_PrestoreFreezeFrame(/*IN*/Dem_EventIdType portDefArg1);
extern Std_ReturnType Rte_dem_ClearPrestoredFreezeFrame(/*IN*/Dem_EventIdType portDefArg1);
extern Std_ReturnType Rte_dem_GetEventStatus(/*IN*/Dem_EventIdType EventId, /*OUT*/Dem_EventStatusExtendedType * EventStatusExtended);
extern Std_ReturnType Rte_dem_GetEventFailed(/*IN*/Dem_EventIdType EventId, /*OUT*/boolean * EventFailed);
extern Std_ReturnType Rte_dem_GetEventTested(/*IN*/Dem_EventIdType EventId, /*OUT*/boolean * EventTested);
extern Std_ReturnType Rte_dem_GetDTCOfEvent(/*IN*/Dem_EventIdType EventId, /*IN*/Dem_DTCFormatType DTCFormat, /*OUT*/uint32 * DTCOfEvent);
extern Std_ReturnType Rte_dem_GetFaultDetectionCounter(/*IN*/Dem_EventIdType EventId, /*OUT*/sint8 * FaultDetectionCounter);
extern Std_ReturnType Rte_dem_GetEventFreezeFrameData(/*IN*/Dem_EventIdType EventId, /*IN*/uint8 RecordNumber, /*IN*/boolean ReportTotalRecord, /*IN*/uint16 DataId, /*OUT*/uint8 * DestBuffer);
extern Std_ReturnType Rte_dem_GetEventExtendedDataRecord(/*IN*/Dem_EventIdType EventId, /*IN*/uint8 RecordNumber, /*OUT*/uint8 * DestBuffer);
extern Std_ReturnType Rte_dem_SetOperationCycleState(/*IN*/Dem_OperationCycleIdType portDefArg1, /*IN*/Dem_OperationCycleStateType CycleState);
extern Std_ReturnType Rte_dem_ClearDTC(/*IN*/uint32 DTC, /*IN*/Dem_DTCFormatType DTCFormat, /*IN*/Dem_DTCOriginType DTCOrigin);
extern Std_ReturnType Rte_dlt_Dlt_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/constUint8Ptr log_data, /*IN*/uint16 log_data_length);
extern Std_ReturnType Rte_dlt_Dlt_SendTraceMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageTraceInfoType * trace_info, /*IN*/constUint8Ptr trace_data, /*IN*/uint16 trace_data_length);
extern Std_ReturnType Rte_dlt_Dlt_RegisterContext(/*IN*/Dlt_SessionIDType session_id, /*IN*/const uint8 * app_id, /*IN*/const uint8 * context_id, /*IN*/constUint8Ptr app_description, /*IN*/uint8 len_app_description, /*IN*/constUint8Ptr context_description, /*IN*/uint8 len_context_description);
extern Std_ReturnType Rte_dcm_Dcm_GetSecurityLevel(/*OUT*/Dcm_SecLevelType * SecLevel);
extern Std_ReturnType Rte_dcm_Dcm_GetSesCtrlType(/*OUT*/Dcm_SesCtrlType * SesCtrlType);
extern Std_ReturnType Rte_dcm_Dcm_GetActiveProtocol(/*OUT*/Dcm_ProtocolType * ActiveProtocol);
extern Std_ReturnType Rte_dcm_Dcm_ResetToDefaultSession(void);
extern Std_ReturnType Rte_nvm_GetErrorStatus(/*IN*/NvM_BlockIdType portDefArg1, /*OUT*/NvM_RequestResultType * RequestResultPtr);
extern Std_ReturnType Rte_nvm_SetDataIndex(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/uint8 DataIndex);
extern Std_ReturnType Rte_nvm_GetDataIndex(/*IN*/NvM_BlockIdType portDefArg1, /*OUT*/uint8 * DataIndexPtr);
extern Std_ReturnType Rte_nvm_SetRamBlockStatus(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/boolean BlockChanged);
extern Std_ReturnType Rte_nvm_ReadBlock(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/VoidPtr DstPtr);
extern Std_ReturnType Rte_nvm_WriteBlock(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/ConstVoidPtr SrcPtr);
extern Std_ReturnType Rte_nvm_RestoreBlockDefaults(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/VoidPtr DstPtr);
extern Std_ReturnType Rte_nvm_EraseNvBlock(/*IN*/NvM_BlockIdType portDefArg1);
extern Std_ReturnType Rte_nvm_InvalidateNvBlock(/*IN*/NvM_BlockIdType portDefArg1);
extern Std_ReturnType Rte_nvm_SetBlockProtection(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/boolean ProtectionEnabled);
extern Std_ReturnType Rte_wdgm_UpdateAliveCounter(/*IN*/WdgM_SupervisedEntityIdType portDefArg1);
extern Std_ReturnType Rte_wdgm_CheckpointReached(/*IN*/WdgM_SupervisedEntityIdType portDefArg1, /*IN*/WdgM_CheckpointIdType CheckpointID);
extern Std_ReturnType Rte_comM_RequestComMode(/*IN*/ComM_UserHandleType portDefArg1, /*IN*/ComM_ModeType ComMode);
extern Std_ReturnType Rte_comM_GetMaxComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_comM_GetRequestedComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_comM_GetCurrentComMode(/*IN*/ComM_UserHandleType portDefArg1, /*OUT*/ComM_ModeType * ComMode);
extern Std_ReturnType Rte_SwcMem_JobFinishedPIM1(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult);
extern Std_ReturnType Rte_SwcMem_JobFinishedPIM2(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult);

// --- RTE INTERNAL DATA ---
#define Rte_START_SEC_VAR_INIT_UNSPECIFIED
#include <Rte_MemMap.h>
boolean RteInitialized = FALSE;
#define Rte_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Rte_MemMap.h>

// === IoHwAb Data ===============================================================

// === Det Data ===============================================================

// === EcuM Data ===============================================================
#define EcuM_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <EcuM_MemMap.h>
EcuM_ModeMachinesType EcuM_ModeMachines;
#define EcuM_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <EcuM_MemMap.h>

// === Dem Data ===============================================================

// === BswM Data ===============================================================
#define BswM_START_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>
ComMModeEnum Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode;
#define BswM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>
#define BswM_START_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>
WdgMModeEnum Rte_Buffer_bswm_modeRequestPort_WdgMMode_requestedMode;
#define BswM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <BswM_MemMap.h>

// === Dlt Data ===============================================================

// === Dcm Data ===============================================================
#define Dcm_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <Dcm_MemMap.h>
Dcm_ModeMachinesType Dcm_ModeMachines;
#define Dcm_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <Dcm_MemMap.h>

// === NvM Data ===============================================================

// === WdgM Data ===============================================================
#define WdgM_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <WdgM_MemMap.h>
WdgM_ModeMachinesType WdgM_ModeMachines;
#define WdgM_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <WdgM_MemMap.h>

// === ComM Data ===============================================================
#define ComM_START_SEC_VAR_CLEARED_UNSPECIFIED
#include <ComM_MemMap.h>
ComM_ModeMachinesType ComM_ModeMachines;
#define ComM_STOP_SEC_VAR_CLEARED_UNSPECIFIED
#include <ComM_MemMap.h>

// === SwcWriterType Data ===============================================================
#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
uint32 Rte_Buffer_SwcWriter_InputPort_data1;
#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
#define SwcWriterType_START_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>
TestCmdType Rte_Buffer_SwcWriter_InputPort_cmd;
#define SwcWriterType_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <SwcWriterType_MemMap.h>

// === SwcMemType Data ===============================================================

// === SwcReaderType Data ===============================================================

// --- SERVER ACTIVATIONS ------------------------------------------------------------------

// --- FUNCTIONS ---------------------------------------------------------------------------
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === IoHwAb =======================================================================
// --- ioHwAb --------------------------------------------------------------------

// ------ Digital_DigitalSignal_LED1
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED1_Write(/*IN*/DigitalLevel Level) {
    return Rte_ioHwAb_DigitalWrite(0, Level);
}
// ------ Digital_DigitalSignal_LED2
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED2_Write(/*IN*/DigitalLevel Level) {
    return Rte_ioHwAb_DigitalWrite(1, Level);
}
// ------ Analog_AnalogSignal
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Analog_AnalogSignal_Read(/*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality) {
    return Rte_ioHwAb_AnalogRead(0, Value, Quality);
}
// ------ Pwm_PwmSignal_LED3Duty
Std_ReturnType Rte_Call_IoHwAb_ioHwAb_Pwm_PwmSignal_LED3Duty_Set(/*IN*/DutyCycle DutyValue, /*OUT*/SignalQuality * Quality) {
    return Rte_ioHwAb_PwmSetDuty(0, DutyValue, Quality);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === Det =======================================================================
// --- det --------------------------------------------------------------------

// ------ DS_DetPortReader
Std_ReturnType Rte_Call_Det_det_DS_DetPortReader_ReportError(/*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId) {
    return Rte_det_ReportError(4096, InstanceId, ApiId, ErrorId);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === EcuM =======================================================================
// --- ecuM --------------------------------------------------------------------

// ------ SR_HelloWorldUser
Std_ReturnType Rte_Call_EcuM_ecuM_SR_HelloWorldUser_RequestRUN(void) {
    return Rte_ecuM_RequestRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_HelloWorldUser_ReleaseRUN(void) {
    return Rte_ecuM_ReleaseRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_HelloWorldUser_RequestPOSTRUN(void) {
    return Rte_ecuM_RequestPOSTRUN(0);
}
Std_ReturnType Rte_Call_EcuM_ecuM_SR_HelloWorldUser_ReleasePOSTRUN(void) {
    return Rte_ecuM_ReleasePOSTRUN(0);
}
// ------ currentMode
Std_ReturnType Rte_Switch_EcuM_ecuM_currentMode_currentMode(/*IN*/uint8 mode) {
	if (EcuM_ModeMachines.ecuM.currentMode_currentMode.nextMode == RTE_TRANSITION_EcuM_ecuM_currentMode_currentMode) {
		{
			SYS_CALL_SuspendOSInterrupts();
			EcuM_ModeMachines.ecuM.currentMode_currentMode.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		SYS_CALL_SetEvent(TASK_ID_OsRteTask, EVENT_MASK_ModeSwitchOsEvent);
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ shutdownTarget
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_SelectShutdownTarget(/*IN*/EcuM_StateType target, /*IN*/uint8 mode) {
    return Rte_ecuM_SelectShutdownTarget(target, mode);
}
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_GetShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode) {
    return Rte_ecuM_GetShutdownTarget(target, mode);
}
Std_ReturnType Rte_Call_EcuM_ecuM_shutdownTarget_GetLastShutdownTarget(/*OUT*/EcuM_StateType * target, /*OUT*/uint8 * mode) {
    return Rte_ecuM_GetLastShutdownTarget(target, mode);
}
// ------ bootTarget
Std_ReturnType Rte_Call_EcuM_ecuM_bootTarget_SelectBootTarget(/*IN*/EcuM_BootTargetType target) {
    return Rte_ecuM_SelectBootTarget(target);
}
Std_ReturnType Rte_Call_EcuM_ecuM_bootTarget_GetBootTarget(/*OUT*/EcuM_BootTargetType * target) {
    return Rte_ecuM_GetBootTarget(target);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === Dem =======================================================================
// --- dem --------------------------------------------------------------------

// ------ Event_TestEvent
Std_ReturnType Rte_Call_Dem_dem_Event_TestEvent_SetEventStatus(/*IN*/Dem_EventStatusType EventStatus) {
    return Rte_dem_SetEventStatus(6, EventStatus);
}
Std_ReturnType Rte_Call_Dem_dem_Event_TestEvent_ResetEventStatus(void) {
    return Rte_dem_ResetEventStatus(6);
}
Std_ReturnType Rte_Call_Dem_dem_Event_TestEvent_SetEventDisabled(void) {
    return Rte_dem_SetEventDisabled(6);
}
Std_ReturnType Rte_Call_Dem_dem_Event_TestEvent_PrestoreFreezeFrame(void) {
    return Rte_dem_PrestoreFreezeFrame(6);
}
Std_ReturnType Rte_Call_Dem_dem_Event_TestEvent_ClearPrestoredFreezeFrame(void) {
    return Rte_dem_ClearPrestoredFreezeFrame(6);
}
// ------ EvtInfo_TestEvent
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetEventStatus(/*OUT*/Dem_EventStatusExtendedType * EventStatusExtended) {
    return Rte_dem_GetEventStatus(6, EventStatusExtended);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetEventFailed(/*OUT*/boolean * EventFailed) {
    return Rte_dem_GetEventFailed(6, EventFailed);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetEventTested(/*OUT*/boolean * EventTested) {
    return Rte_dem_GetEventTested(6, EventTested);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetDTCOfEvent(/*IN*/Dem_DTCFormatType DTCFormat, /*OUT*/uint32 * DTCOfEvent) {
    return Rte_dem_GetDTCOfEvent(6, DTCFormat, DTCOfEvent);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetFaultDetectionCounter(/*OUT*/sint8 * FaultDetectionCounter) {
    return Rte_dem_GetFaultDetectionCounter(6, FaultDetectionCounter);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetEventFreezeFrameData(/*IN*/uint8 RecordNumber, /*IN*/boolean ReportTotalRecord, /*IN*/uint16 DataId, /*OUT*/uint8 * DestBuffer) {
    return Rte_dem_GetEventFreezeFrameData(6, RecordNumber, ReportTotalRecord, DataId, DestBuffer);
}
Std_ReturnType Rte_Call_Dem_dem_EvtInfo_TestEvent_GetEventExtendedDataRecord(/*IN*/uint8 RecordNumber, /*OUT*/uint8 * DestBuffer) {
    return Rte_dem_GetEventExtendedDataRecord(6, RecordNumber, DestBuffer);
}
// ------ GeneralEvtInfo
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetEventStatus(/*IN*/Dem_EventIdType EventId, /*OUT*/Dem_EventStatusExtendedType * EventStatusExtended) {
    return Rte_dem_GetEventStatus(EventId, EventStatusExtended);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetEventFailed(/*IN*/Dem_EventIdType EventId, /*OUT*/boolean * EventFailed) {
    return Rte_dem_GetEventFailed(EventId, EventFailed);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetEventTested(/*IN*/Dem_EventIdType EventId, /*OUT*/boolean * EventTested) {
    return Rte_dem_GetEventTested(EventId, EventTested);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetDTCOfEvent(/*IN*/Dem_EventIdType EventId, /*IN*/Dem_DTCFormatType DTCFormat, /*OUT*/uint32 * DTCOfEvent) {
    return Rte_dem_GetDTCOfEvent(EventId, DTCFormat, DTCOfEvent);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetFaultDetectionCounter(/*IN*/Dem_EventIdType EventId, /*OUT*/sint8 * FaultDetectionCounter) {
    return Rte_dem_GetFaultDetectionCounter(EventId, FaultDetectionCounter);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetEventFreezeFrameData(/*IN*/Dem_EventIdType EventId, /*IN*/uint8 RecordNumber, /*IN*/boolean ReportTotalRecord, /*IN*/uint16 DataId, /*OUT*/uint8 * DestBuffer) {
    return Rte_dem_GetEventFreezeFrameData(EventId, RecordNumber, ReportTotalRecord, DataId, DestBuffer);
}
Std_ReturnType Rte_Call_Dem_dem_GeneralEvtInfo_GetEventExtendedDataRecord(/*IN*/Dem_EventIdType EventId, /*IN*/uint8 RecordNumber, /*OUT*/uint8 * DestBuffer) {
    return Rte_dem_GetEventExtendedDataRecord(EventId, RecordNumber, DestBuffer);
}
// ------ OpCycle_DemOperationCycle
Std_ReturnType Rte_Call_Dem_dem_OpCycle_DemOperationCycle_SetOperationCycleState(/*IN*/Dem_OperationCycleStateType CycleState) {
    return Rte_dem_SetOperationCycleState(2, CycleState);
}
// ------ Dcm
Std_ReturnType Rte_Call_Dem_dem_Dcm_ClearDTC(/*IN*/uint32 DTC, /*IN*/Dem_DTCFormatType DTCFormat, /*IN*/Dem_DTCOriginType DTCOrigin) {
    return Rte_dem_ClearDTC(DTC, DTCFormat, DTCOrigin);
}
// ------ CBInitEvt_TestEvent
Std_ReturnType Rte_Call_Dem_dem_CBInitEvt_TestEvent_InitMonitorForEvent(/*IN*/Dem_InitMonitorReasonType InitMonitorReason) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === BswM =======================================================================
// --- bswm --------------------------------------------------------------------

// ------ modeRequestPort_SwcStartCommunication
Std_ReturnType Rte_Read_BswM_bswm_modeRequestPort_SwcStartCommunication_requestedMode(/*OUT*/ComMModeEnum * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (SwcWriter_ComMControl_to_bswm_modeRequestPort_SwcStartCommunication) */
	{
		SYS_CALL_SuspendOSInterrupts();
		*value = Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}
// ------ modeRequestPort_WdgMMode
Std_ReturnType Rte_Read_BswM_bswm_modeRequestPort_WdgMMode_requestedMode(/*OUT*/WdgMModeEnum * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (SwcReader_WdgM_StateReq_to_bswm_modeRequestPort_WdgMMode) */
	{
		SYS_CALL_SuspendOSInterrupts();
		*value = Rte_Buffer_bswm_modeRequestPort_WdgMMode_requestedMode;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === Dlt =======================================================================
// --- dlt --------------------------------------------------------------------

// ------ Dlt_service
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/constUint8Ptr log_data, /*IN*/uint16 log_data_length) {
    return Rte_dlt_Dlt_SendLogMessage(session_id, log_info, log_data, log_data_length);
}
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_SendTraceMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageTraceInfoType * trace_info, /*IN*/constUint8Ptr trace_data, /*IN*/uint16 trace_data_length) {
    return Rte_dlt_Dlt_SendTraceMessage(session_id, trace_info, trace_data, trace_data_length);
}
Std_ReturnType Rte_Call_Dlt_dlt_Dlt_service_RegisterContext(/*IN*/Dlt_SessionIDType session_id, /*IN*/const uint8 * app_id, /*IN*/const uint8 * context_id, /*IN*/constUint8Ptr app_description, /*IN*/uint8 len_app_description, /*IN*/constUint8Ptr context_description, /*IN*/uint8 len_context_description) {
    return Rte_dlt_Dlt_RegisterContext(session_id, app_id, context_id, app_description, len_app_description, context_description, len_context_description);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === Dcm =======================================================================
// --- dcm --------------------------------------------------------------------

// ------ SecurityAccess_SecurityLevel_0
Std_ReturnType Rte_Call_Dcm_dcm_SecurityAccess_SecurityLevel_0_GetSeed(/*IN*/Dcm_OpStatusType OpStatus, /*OUT*/uint8 * Seed, /*OUT*/Dcm_NegativeResponseCodeType * ErrorCode) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
Std_ReturnType Rte_Call_Dcm_dcm_SecurityAccess_SecurityLevel_0_CompareKey(/*IN*/const uint8 * Key, /*IN*/Dcm_OpStatusType OpStatus) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
// ------ CallbackDCMRequestServices_DcmDslCallbackDCMRequestService
Std_ReturnType Rte_Call_Dcm_dcm_CallbackDCMRequestServices_DcmDslCallbackDCMRequestService_StartProtocol(/*IN*/Dcm_ProtocolType ProtocolID) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
Std_ReturnType Rte_Call_Dcm_dcm_CallbackDCMRequestServices_DcmDslCallbackDCMRequestService_StopProtocol(/*IN*/Dcm_ProtocolType ProtocolID) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
// ------ DcmEcuReset
Std_ReturnType Rte_Switch_Dcm_dcm_DcmEcuReset_DcmEcuReset(/*IN*/uint8 mode) {
	if (Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.nextMode == RTE_TRANSITION_Dcm_dcm_DcmEcuReset_DcmEcuReset) {
		{
			SYS_CALL_SuspendOSInterrupts();
			Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.currentMode = Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.nextMode;
		Dcm_ModeMachines.dcm.DcmEcuReset_DcmEcuReset.nextMode = RTE_TRANSITION_Dcm_dcm_DcmEcuReset_DcmEcuReset;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ DcmRapidPowerShutDown
Std_ReturnType Rte_Switch_Dcm_dcm_DcmRapidPowerShutDown_DcmRapidPowerShutDown(/*IN*/uint8 mode) {
	if (Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.nextMode == RTE_TRANSITION_Dcm_dcm_DcmRapidPowerShutDown_DcmRapidPowerShutDown) {
		{
			SYS_CALL_SuspendOSInterrupts();
			Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.currentMode = Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.nextMode;
		Dcm_ModeMachines.dcm.DcmRapidPowerShutDown_DcmRapidPowerShutDown.nextMode = RTE_TRANSITION_Dcm_dcm_DcmRapidPowerShutDown_DcmRapidPowerShutDown;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ DcmCommunicationControl_ComMChannel
Std_ReturnType Rte_Switch_Dcm_dcm_DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel(/*IN*/uint8 mode) {
	if (Dcm_ModeMachines.dcm.DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel.nextMode == RTE_TRANSITION_Dcm_dcm_DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel) {
		{
			SYS_CALL_SuspendOSInterrupts();
			Dcm_ModeMachines.dcm.DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		Dcm_ModeMachines.dcm.DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel.currentMode = Dcm_ModeMachines.dcm.DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel.nextMode;
		Dcm_ModeMachines.dcm.DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel.nextMode = RTE_TRANSITION_Dcm_dcm_DcmCommunicationControl_ComMChannel_DcmCommunicationControl_ComMChannel;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ DcmControlDTCSetting
Std_ReturnType Rte_Switch_Dcm_dcm_DcmControlDTCSetting_DcmControlDTCSetting(/*IN*/uint8 mode) {
	if (Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.nextMode == RTE_TRANSITION_Dcm_dcm_DcmControlDTCSetting_DcmControlDTCSetting) {
		{
			SYS_CALL_SuspendOSInterrupts();
			Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.currentMode = Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.nextMode;
		Dcm_ModeMachines.dcm.DcmControlDTCSetting_DcmControlDTCSetting.nextMode = RTE_TRANSITION_Dcm_dcm_DcmControlDTCSetting_DcmControlDTCSetting;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ DcmDiagnosticSessionControl
Std_ReturnType Rte_Switch_Dcm_dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(/*IN*/uint8 mode) {
	if (Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.nextMode == RTE_TRANSITION_Dcm_dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl) {
		{
			SYS_CALL_SuspendOSInterrupts();
			Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.currentMode = Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.nextMode;
		Dcm_ModeMachines.dcm.DcmDiagnosticSessionControl_DcmDiagnosticSessionControl.nextMode = RTE_TRANSITION_Dcm_dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}
// ------ DcmIf
Std_ReturnType Rte_Call_Dcm_dcm_DcmIf_ClearDTC(/*IN*/uint32 DTC, /*IN*/Dem_DTCFormatType DTCFormat, /*IN*/Dem_DTCOriginType DTCOrigin) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
// ------ DCMServices
Std_ReturnType Rte_Call_Dcm_dcm_DCMServices_GetSecurityLevel(/*OUT*/Dcm_SecLevelType * SecLevel) {
    return Rte_dcm_Dcm_GetSecurityLevel(SecLevel);
}
Std_ReturnType Rte_Call_Dcm_dcm_DCMServices_GetSesCtrlType(/*OUT*/Dcm_SesCtrlType * SesCtrlType) {
    return Rte_dcm_Dcm_GetSesCtrlType(SesCtrlType);
}
Std_ReturnType Rte_Call_Dcm_dcm_DCMServices_GetActiveProtocol(/*OUT*/Dcm_ProtocolType * ActiveProtocol) {
    return Rte_dcm_Dcm_GetActiveProtocol(ActiveProtocol);
}
Std_ReturnType Rte_Call_Dcm_dcm_DCMServices_ResetToDefaultSession(void) {
    return Rte_dcm_Dcm_ResetToDefaultSession();
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === NvM =======================================================================
// --- nvm --------------------------------------------------------------------

// ------ PS_SwcMem_Block1
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_GetErrorStatus(/*OUT*/NvM_RequestResultType * RequestResultPtr) {
    return Rte_nvm_GetErrorStatus(2, RequestResultPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_SetDataIndex(/*IN*/uint8 DataIndex) {
    return Rte_nvm_SetDataIndex(2, DataIndex);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_GetDataIndex(/*OUT*/uint8 * DataIndexPtr) {
    return Rte_nvm_GetDataIndex(2, DataIndexPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_SetRamBlockStatus(/*IN*/boolean BlockChanged) {
    return Rte_nvm_SetRamBlockStatus(2, BlockChanged);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_ReadBlock(/*IN*/VoidPtr DstPtr) {
    return Rte_nvm_ReadBlock(2, DstPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_WriteBlock(/*IN*/ConstVoidPtr SrcPtr) {
    return Rte_nvm_WriteBlock(2, SrcPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_RestoreBlockDefaults(/*IN*/VoidPtr DstPtr) {
    return Rte_nvm_RestoreBlockDefaults(2, DstPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_EraseNvBlock(void) {
    return Rte_nvm_EraseNvBlock(2);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block1_InvalidateNvBlock(void) {
    return Rte_nvm_InvalidateNvBlock(2);
}
// ------ PNJobFinished_SwcMem_Block1
Std_ReturnType Rte_Call_NvM_nvm_PNJobFinished_SwcMem_Block1_JobFinished(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    return Rte_Call_SwcMemType_SwcMem_JobFinishedPIM1_JobFinished(ServiceId, JobResult);
}
// ------ PNInitBlock_SwcMem_Block1
Std_ReturnType Rte_Call_NvM_nvm_PNInitBlock_SwcMem_Block1_InitBlock(void) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
// ------ PAdmin_SwcMem_Block1
Std_ReturnType Rte_Call_NvM_nvm_PAdmin_SwcMem_Block1_SetBlockProtection(/*IN*/boolean ProtectionEnabled) {
    return Rte_nvm_SetBlockProtection(2, ProtectionEnabled);
}
// ------ PS_SwcMem_Block2
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_GetErrorStatus(/*OUT*/NvM_RequestResultType * RequestResultPtr) {
    return Rte_nvm_GetErrorStatus(3, RequestResultPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_SetDataIndex(/*IN*/uint8 DataIndex) {
    return Rte_nvm_SetDataIndex(3, DataIndex);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_GetDataIndex(/*OUT*/uint8 * DataIndexPtr) {
    return Rte_nvm_GetDataIndex(3, DataIndexPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_SetRamBlockStatus(/*IN*/boolean BlockChanged) {
    return Rte_nvm_SetRamBlockStatus(3, BlockChanged);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_ReadBlock(/*IN*/VoidPtr DstPtr) {
    return Rte_nvm_ReadBlock(3, DstPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_WriteBlock(/*IN*/ConstVoidPtr SrcPtr) {
    return Rte_nvm_WriteBlock(3, SrcPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_RestoreBlockDefaults(/*IN*/VoidPtr DstPtr) {
    return Rte_nvm_RestoreBlockDefaults(3, DstPtr);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_EraseNvBlock(void) {
    return Rte_nvm_EraseNvBlock(3);
}
Std_ReturnType Rte_Call_NvM_nvm_PS_SwcMem_Block2_InvalidateNvBlock(void) {
    return Rte_nvm_InvalidateNvBlock(3);
}
// ------ PNJobFinished_SwcMem_Block2
Std_ReturnType Rte_Call_NvM_nvm_PNJobFinished_SwcMem_Block2_JobFinished(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    return Rte_Call_SwcMemType_SwcMem_JobFinishedPIM2_JobFinished(ServiceId, JobResult);
}
// ------ PNInitBlock_SwcMem_Block2
Std_ReturnType Rte_Call_NvM_nvm_PNInitBlock_SwcMem_Block2_InitBlock(void) {
    /* --- Unconnected */
    return RTE_E_UNCONNECTED;
}
// ------ PAdmin_SwcMem_Block2
Std_ReturnType Rte_Call_NvM_nvm_PAdmin_SwcMem_Block2_SetBlockProtection(/*IN*/boolean ProtectionEnabled) {
    return Rte_nvm_SetBlockProtection(3, ProtectionEnabled);
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === WdgM =======================================================================
// --- wdgm --------------------------------------------------------------------

// ------ alive_Supervised100msTask
Std_ReturnType Rte_Call_WdgM_wdgm_alive_Supervised100msTask_UpdateAliveCounter(void) {
    return Rte_wdgm_UpdateAliveCounter(0);
}
Std_ReturnType Rte_Call_WdgM_wdgm_alive_Supervised100msTask_CheckpointReached(/*IN*/WdgM_CheckpointIdType CheckpointID) {
    return Rte_wdgm_CheckpointReached(0, CheckpointID);
}
// ------ mode_Supervised100msTask

// ------ globalMode

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === ComM =======================================================================
// --- comM --------------------------------------------------------------------

// ------ UR_ComMUser
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_RequestComMode(/*IN*/ComM_ModeType ComMode) {
    return Rte_comM_RequestComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetMaxComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetMaxComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetRequestedComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetRequestedComMode(0, ComMode);
}
Std_ReturnType Rte_Call_ComM_comM_UR_ComMUser_GetCurrentComMode(/*OUT*/ComM_ModeType * ComMode) {
    return Rte_comM_GetCurrentComMode(0, ComMode);
}
// ------ UM_ComMUser
Std_ReturnType Rte_Switch_ComM_comM_UM_ComMUser_currentMode(/*IN*/uint8 mode) {
	if (ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode == RTE_TRANSITION_ComM_comM_UM_ComMUser_currentMode) {
		{
			SYS_CALL_SuspendOSInterrupts();
			ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode = mode;
			SYS_CALL_ResumeOSInterrupts();
		}
		// Activate runnables
		// No runnables to activate
		SYS_CALL_SuspendOSInterrupts();
		ComM_ModeMachines.comM.UM_ComMUser_currentMode.currentMode = ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode;
		ComM_ModeMachines.comM.UM_ComMUser_currentMode.nextMode = RTE_TRANSITION_ComM_comM_UM_ComMUser_currentMode;
		SYS_CALL_ResumeOSInterrupts();
		return RTE_E_OK;
	} else {
		return RTE_E_LIMIT;
	}
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === SwcWriterType =======================================================================
// --- SwcWriter --------------------------------------------------------------------

// ------ CommandPort
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_CommandPort_data1(/*OUT*/uint32 * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (Rx01data1ISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_rxsignal01, value);
	return retVal;
}
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_CommandPort_cmd(/*OUT*/TestCmdType * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (Rx01CmdIsig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_rxCmdSignal, value);
	return retVal;
}
// ------ InputPort
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_InputPort_data1(/*OUT*/uint32 * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (SR3) */
	{
		SYS_CALL_AtomicCopy32(*value, Rte_Buffer_SwcWriter_InputPort_data1);
	}
	return retVal;
}
Std_ReturnType Rte_Read_SwcWriterType_SwcWriter_InputPort_cmd(/*OUT*/TestCmdType * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (SR3) */
	{
		SYS_CALL_SuspendOSInterrupts();
		*value = Rte_Buffer_SwcWriter_InputPort_cmd;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}
// ------ SenderPort
Std_ReturnType Rte_Write_SwcWriterType_SwcWriter_SenderPort_data1(/*IN*/uint32 value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (Tx01data1ISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_SendSignal(ComConf_ComSignal_txsignal01, &value);
	return retVal;
}
Std_ReturnType Rte_Write_SwcWriterType_SwcWriter_SenderPort_cmd(/*IN*/TestCmdType value) {
	Std_ReturnType retVal = RTE_E_OK;
	// --- Unconnected
	return retVal;
}
// ------ Blinker
Std_ReturnType Rte_Call_SwcWriterType_SwcWriter_Blinker_Write(/*IN*/DigitalLevel Level) {
    return Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED2_Write(Level);
}
// ------ ComMControl
Std_ReturnType Rte_Write_SwcWriterType_SwcWriter_ComMControl_requestedMode(/*IN*/ComMModeEnum value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (SwcWriter_ComMControl_to_bswm_modeRequestPort_SwcStartCommunication) */
	{
		SYS_CALL_SuspendOSInterrupts();
		Rte_Buffer_bswm_modeRequestPort_SwcStartCommunication_requestedMode = value;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}
// ------ Mode

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === SwcMemType =======================================================================
// --- SwcMem --------------------------------------------------------------------

// ------ SwcMemRx
Std_ReturnType Rte_Read_SwcMemType_SwcMem_SwcMemRx_command(/*OUT*/NvmCmdType * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (MemRxCmdISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_SwcMemRx_Request, value);
	return retVal;
}
Std_ReturnType Rte_Read_SwcMemType_SwcMem_SwcMemRx_blockId(/*OUT*/uint8 * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (MemRxBlockIdISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_SwcMemRx_BlockId, value);
	return retVal;
}
Std_ReturnType Rte_Read_SwcMemType_SwcMem_SwcMemRx_payload(/*OUT*/uint32 * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (MemRxPayloadISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_SwcMemRx_Data, value);
	return retVal;
}
// ------ SwcMemTx
Std_ReturnType Rte_Write_SwcMemType_SwcMem_SwcMemTx_result(/*IN*/NvmResType value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (MemTxResISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_SendSignal(ComConf_ComSignal_SwcMemTx_Result, &value);
	return retVal;
}
Std_ReturnType Rte_Write_SwcMemType_SwcMem_SwcMemTx_blockId(/*IN*/uint8 value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (MemTxBlockIdISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_SendSignal(ComConf_ComSignal_SwcMemTx_BlockId, &value);
	return retVal;
}
Std_ReturnType Rte_Write_SwcMemType_SwcMem_SwcMemTx_payload(/*IN*/uint32 value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (MemTxPayloadISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_SendSignal(ComConf_ComSignal_SwcMemTx_Data, &value);
	return retVal;
}
// ------ JobFinishedPIM1
Std_ReturnType Rte_Call_SwcMemType_SwcMem_JobFinishedPIM1_JobFinished(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    return Rte_SwcMem_JobFinishedPIM1(ServiceId, JobResult);
}
// ------ JobFinishedPIM2
Std_ReturnType Rte_Call_SwcMemType_SwcMem_JobFinishedPIM2_JobFinished(/*IN*/uint8 ServiceId, /*IN*/NvM_RequestResultType JobResult) {
    return Rte_SwcMem_JobFinishedPIM2(ServiceId, JobResult);
}
// ------ ServicePIM1
Std_ReturnType Rte_Call_SwcMemType_SwcMem_ServicePIM1_SetRamBlockStatus(/*IN*/boolean BlockChanged) {
    return Rte_Call_NvM_nvm_PS_SwcMem_Block1_SetRamBlockStatus(BlockChanged);
}
// ------ ServicePIM2
Std_ReturnType Rte_Call_SwcMemType_SwcMem_ServicePIM2_ReadBlock(/*IN*/VoidPtr DstPtr) {
    return Rte_Call_NvM_nvm_PS_SwcMem_Block2_ReadBlock(DstPtr);
}
Std_ReturnType Rte_Call_SwcMemType_SwcMem_ServicePIM2_WriteBlock(/*IN*/ConstVoidPtr SrcPtr) {
    return Rte_Call_NvM_nvm_PS_SwcMem_Block2_WriteBlock(SrcPtr);
}
// ------ Mode

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>

// === SwcReaderType =======================================================================
// --- SwcReader --------------------------------------------------------------------

// ------ ReceiverPort
Std_ReturnType Rte_Read_SwcReaderType_SwcReader_ReceiverPort_data1(/*OUT*/uint32 * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (Rx01data1ISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_rxsignal01, value);
	return retVal;
}
Std_ReturnType Rte_Read_SwcReaderType_SwcReader_ReceiverPort_cmd(/*OUT*/TestCmdType * value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Receiver (Rx01CmdIsig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_ReceiveSignal(ComConf_ComSignal_rxCmdSignal, value);
	return retVal;
}
// ------ Blinker
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Blinker_Write(/*IN*/DigitalLevel Level) {
    return Rte_Call_IoHwAb_ioHwAb_Digital_DigitalSignal_LED1_Write(Level);
}
// ------ AnalogReader
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_AnalogReader_Read(/*OUT*/AnalogValue * Value, /*OUT*/SignalQuality * Quality) {
    return Rte_Call_IoHwAb_ioHwAb_Analog_AnalogSignal_Read(Value, Quality);
}
// ------ ResultPort
Std_ReturnType Rte_Write_SwcReaderType_SwcReader_ResultPort_data1(/*IN*/uint32 value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (SR3) */
	{
		SYS_CALL_AtomicCopy32(Rte_Buffer_SwcWriter_InputPort_data1, value);
	}
	return retVal;
}
Std_ReturnType Rte_Write_SwcReaderType_SwcReader_ResultPort_cmd(/*IN*/TestCmdType value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (SR3) */
	{
		SYS_CALL_SuspendOSInterrupts();
		Rte_Buffer_SwcWriter_InputPort_cmd = value;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}
// ------ AdcResult
Std_ReturnType Rte_Write_SwcReaderType_SwcReader_AdcResult_data1(/*IN*/sint32 value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (TxAdcdata1ISig) @req SWS_Rte_04505, @req SWS_Rte_06023 */
	retVal |= Com_SendSignal(ComConf_ComSignal_AdcValue, &value);
	return retVal;
}
// ------ PWMWriter
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_PWMWriter_Set(/*IN*/DutyCycle DutyValue, /*OUT*/SignalQuality * Quality) {
    return Rte_Call_IoHwAb_ioHwAb_Pwm_PwmSignal_LED3Duty_Set(DutyValue, Quality);
}
// ------ Mode

// ------ RunControl
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_RunControl_ReleaseRUN(void) {
    return Rte_Call_EcuM_ecuM_SR_HelloWorldUser_ReleaseRUN();
}
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_RunControl_RequestRUN(void) {
    return Rte_Call_EcuM_ecuM_SR_HelloWorldUser_RequestRUN();
}
// ------ Det
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Det_ReportError(/*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId) {
    return Rte_Call_Det_det_DS_DetPortReader_ReportError(InstanceId, ApiId, ErrorId);
}
// ------ Dlt
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Dlt_SendLogMessage(/*IN*/Dlt_SessionIDType session_id, /*IN*/const Dlt_MessageLogInfoType * log_info, /*IN*/constUint8Ptr log_data, /*IN*/uint16 log_data_length) {
    return Rte_Call_Dlt_dlt_Dlt_service_SendLogMessage(session_id, log_info, log_data, log_data_length);
}
// ------ Dem_TestEvent
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_Dem_TestEvent_SetEventStatus(/*IN*/Dem_EventStatusType EventStatus) {
    return Rte_Call_Dem_dem_Event_TestEvent_SetEventStatus(EventStatus);
}
// ------ WdgM_AliveSup
Std_ReturnType Rte_Call_SwcReaderType_SwcReader_WdgM_AliveSup_UpdateAliveCounter(void) {
    return Rte_Call_WdgM_wdgm_alive_Supervised100msTask_UpdateAliveCounter();
}
// ------ WdgM_LocalMode

// ------ WdgM_StateReq
Std_ReturnType Rte_Write_SwcReaderType_SwcReader_WdgM_StateReq_requestedMode(/*IN*/WdgMModeEnum value) {
	Std_ReturnType retVal = RTE_E_OK;
	/* --- Sender (SwcReader_WdgM_StateReq_to_bswm_modeRequestPort_WdgMMode) */
	{
		SYS_CALL_SuspendOSInterrupts();
		Rte_Buffer_bswm_modeRequestPort_WdgMMode_requestedMode = value;
		SYS_CALL_ResumeOSInterrupts();
	}
	return retVal;
}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>

#define Rte_START_SEC_CODE
#include <Rte_MemMap.h>
void Rte_Internal_Init_Buffers(void) {
	// Init communication buffers

	// Init mode machine queues

}

#define Rte_STOP_SEC_CODE
#include <Rte_MemMap.h>
