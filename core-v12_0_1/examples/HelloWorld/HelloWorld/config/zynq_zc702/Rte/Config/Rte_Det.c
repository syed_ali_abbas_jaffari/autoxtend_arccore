/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_Det.h"

/** === Runnable Prototypes =======================================================================
 */

/** ------ det -----------------------------------------------------------------------
 */
Std_ReturnType Rte_det_ReportError(/*IN*/uint16 portDefArg1, /*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId);

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define Det_START_SEC_VAR_INIT_UNSPECIFIED
#include <Det_MemMap.h>
const Rte_CDS_Det Det_det = {
	._dummy = 0
};
#define Det_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Det_MemMap.h>

#define Det_START_SEC_VAR_INIT_UNSPECIFIED
#include <Det_MemMap.h>
const Rte_Instance Rte_Inst_Det = &Det_det;

#define Det_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Det_MemMap.h>

/** === Runnables =================================================================================
 */
#define Det_START_SEC_CODE
#include <Det_MemMap.h>

/** ------ det -----------------------------------------------------------------------
 */
Std_ReturnType Rte_det_ReportError(/*IN*/uint16 portDefArg1, /*IN*/uint8 InstanceId, /*IN*/uint8 ApiId, /*IN*/uint8 ErrorId) {
	Std_ReturnType retVal = RTE_E_OK;

	/* PRE */
	/* MAIN */

	retVal = Det_ReportError(portDefArg1, InstanceId, ApiId, ErrorId);

	/* POST */
	return retVal;
}
#define Det_STOP_SEC_CODE
#include <Det_MemMap.h>

