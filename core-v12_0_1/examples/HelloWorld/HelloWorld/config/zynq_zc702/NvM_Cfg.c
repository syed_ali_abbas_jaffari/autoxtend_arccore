/*
 * Generator version: 1.2.1
 * AUTOSAR version:   4.0.3
 */

#include "NvM.h"
#ifndef NVM_NOT_SERVICE_COMPONENT
#include "Rte_NvM.h"
#endif

#define DEVICE_ID_FEE 1u
#define DEVICE_ID_EA 0u

extern uint8 Rte_Pim_SwcMemType_SwcMem_PIM1[];


#ifdef USE_RTE
extern Std_ReturnType Rte_Call_PNInitBlock_SwcMem_Block1_InitBlock(void);
#endif
#ifdef USE_RTE 
extern Std_ReturnType Rte_Call_PNJobFinished_SwcMem_Block1_JobFinished(uint8 ServiceId, NvM_RequestResultType JobResult);
#endif



#ifdef USE_RTE
extern Std_ReturnType Rte_Call_PNInitBlock_SwcMem_Block2_InitBlock(void);
#endif
#ifdef USE_RTE 
extern Std_ReturnType Rte_Call_PNJobFinished_SwcMem_Block2_JobFinished(uint8 ServiceId, NvM_RequestResultType JobResult);
#endif






const NvM_BlockDescriptorType BlockDescriptorList[] = {
	{
	/* Configuration ID */
		.BlockCRCType = 					NVM_CRC16,
		.BlockJobPriority = 				0,
		.BlockManagementType = 				NVM_BLOCK_NATIVE,
		.BlockUseCrc = 						false,

		.BlockWriteProt = 					false,

		.CalcRamBlockCrc = 					false,
		.InitBlockCallback =				NULL,


		.NvBlockBaseNumber = 				0,
		.NvBlockLength = 					2,
		.NvBlockNum = 						1,
		.NvramDeviceId =					0,
		.RamBlockDataAddress =				NULL,

		.ResistantToChangesSw =				false,
		.RomBlockDataAdress =				NULL,
		.RomBlockNum =						0,


		.SingleBlockCallback =				NULL,

		.WriteBlockOnce =					false,



	},
	{
		.BlockCRCType = 					NVM_CRC32,
		.BlockJobPriority = 				0,
		.BlockManagementType = 				NVM_BLOCK_NATIVE,
		.BlockUseCrc = 						true,

		.BlockWriteProt = 					false,

		.CalcRamBlockCrc = 					true,
#ifdef USE_RTE
		.InitBlockCallback =				Rte_Call_PNInitBlock_SwcMem_Block1_InitBlock,
#else
		.InitBlockCallback =				NULL,
#endif		


		.NvBlockBaseNumber = 				1,
		.NvBlockLength = 					4,
		.NvBlockNum = 						1,
		.NvramDeviceId =					DEVICE_ID_FEE,
		.RamBlockDataAddress =				Rte_Pim_SwcMemType_SwcMem_PIM1,

		.ResistantToChangesSw =				FALSE, /* Not supported */
		.RomBlockDataAdress =				NULL,
		.RomBlockNum =						0,
		.SelectBlockForReadall = 			true,

#ifdef USE_RTE
		.SingleBlockCallback =				Rte_Call_PNJobFinished_SwcMem_Block1_JobFinished,
#else
		.SingleBlockCallback =				NULL,	
#endif		

		.WriteBlockOnce =					FALSE,



	},
	{
		.BlockCRCType = 					NVM_CRC32,
		.BlockJobPriority = 				0,
		.BlockManagementType = 				NVM_BLOCK_NATIVE,
		.BlockUseCrc = 						true,

		.BlockWriteProt = 					false,

		.CalcRamBlockCrc = 					false,
#ifdef USE_RTE
		.InitBlockCallback =				Rte_Call_PNInitBlock_SwcMem_Block2_InitBlock,
#else
		.InitBlockCallback =				NULL,
#endif		


		.NvBlockBaseNumber = 				1,
		.NvBlockLength = 					4,
		.NvBlockNum = 						1,
		.NvramDeviceId =					DEVICE_ID_EA,
		.RamBlockDataAddress =				NULL,

		.ResistantToChangesSw =				FALSE, /* Not supported */
		.RomBlockDataAdress =				NULL,
		.RomBlockNum =						0,
		.SelectBlockForReadall = 			FALSE,

#ifdef USE_RTE
		.SingleBlockCallback =				Rte_Call_PNJobFinished_SwcMem_Block2_JobFinished,
#else
		.SingleBlockCallback =				NULL,	
#endif		

		.WriteBlockOnce =					FALSE,



	}
};

const NvM_ConfigType NvM_Config = {
		.Common = {
				.MultiBlockCallback = NULL,
		},
#if defined(USE_DEM)
		.DemEvents = {
				.NvMIntegrityFailedDemEventId = DEM_EVENT_ID_NULL,
				.NvMReqFailedDemEventId = DEM_EVENT_ID_NULL,
		},
#endif
		.BlockDescriptor = BlockDescriptorList,
};

