
/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef IOHWAB_PWM_H_
#define IOHWAB_PWM_H_

#include "Std_Types.h"


#define IOHWAB_SIGNAL_PWMSIGNAL_LED3 ((IoHwAb_SignalType)0)



Std_ReturnType IoHwAb_Pwm_Set_Duty(IoHwAb_SignalType signal, IoHwAb_DutyType duty, IoHwAb_StatusType *status);



#endif /* IOHWAB_PWM_H_ */

