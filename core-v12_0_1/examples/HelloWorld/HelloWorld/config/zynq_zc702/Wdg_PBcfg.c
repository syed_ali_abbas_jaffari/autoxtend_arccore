
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */
/** @tagSettings DEFAULT_ARCHITECTURE=ZYNQ */ 

#include "Wdg.h"

const Wdg_ModeConfigType WdgModeConfig = {
	.Wdg_DefaultMode = WDGIF_SLOW_MODE,
	.WdgSettingsFast = 10, // in ms 
	.WdgSettingsSlow = 1000 // in ms 
};

const Wdg_ConfigType WdgConfig = {
	.Wdg_ModeConfig = &WdgModeConfig,
	.Wdg_ResetEnable = STD_ON,
	.Wdg_Notification = NULL
};


