/*
 * Generator version: 2.2.0
 * AUTOSAR version:   4.0.3
 */

#ifndef PDUR_PBCFG_H
#define PDUR_PBCFG_H

#include "PduR.h"

#if !(((PDUR_SW_MAJOR_VERSION == 2) && (PDUR_SW_MINOR_VERSION == 2)) )
#error PduR: Configuration file expected BSW module version to be 2.2.*
#endif

#if !(((PDUR_AR_RELEASE_MAJOR_VERSION == 4) && (PDUR_AR_RELEASE_MINOR_VERSION == 0)) )
#error PduR: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#include "PduR_Types.h"


extern const PduR_PBConfigType PduR_Config;

// Symbolic names for PduR Src Pdus
#define PduRConf_Pdu_Tester01 0u 
#define PduRConf_Pdu_Ecu01 1u 
#define PduRConf_Pdu_DiagPhysicalRx 2u 
#define PduRConf_Pdu_DiagPhysicalTx 3u 
#define PduRConf_Pdu_DiagFunctionalRx 4u 
#define PduRConf_Pdu_DiagUUDT 5u 
#define PduRConf_Pdu_SwcMem_Tx_Com 6u 
#define PduRConf_Pdu_SwcMem_Rx_CanIf 7u 


//  PduR Polite Defines.
#define PDUR_PDU_ID_TESTER01					0
#define PDUR_REVERSE_PDU_ID_TESTER01			0

#define PDUR_PDU_ID_ECU01					1
#define PDUR_REVERSE_PDU_ID_ECU01			1

#define PDUR_PDU_ID_DIAGPHYSICALRX					2
#define PDUR_REVERSE_PDU_ID_DIAGPHYSICALRX			2

#define PDUR_PDU_ID_DIAGPHYSICALTX					3
#define PDUR_REVERSE_PDU_ID_DIAGPHYSICALTX			3

#define PDUR_PDU_ID_DIAGFUNCTIONALRX					4
#define PDUR_REVERSE_PDU_ID_DIAGFUNCTIONALRX			4

#define PDUR_PDU_ID_DIAGUUDT					5
#define PDUR_REVERSE_PDU_ID_DIAGUUDT			5

#define PDUR_PDU_ID_SWCMEM_TX_COM					6
#define PDUR_REVERSE_PDU_ID_SWCMEM_TX_CANIF			6

#define PDUR_PDU_ID_SWCMEM_RX_CANIF					7
#define PDUR_REVERSE_PDU_ID_SWCMEM_RX_COM			7



#endif

