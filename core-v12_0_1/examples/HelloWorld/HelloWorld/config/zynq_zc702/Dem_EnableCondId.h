
/*
 * Generator version: 5.11.0
 * AUTOSAR version:   4.0.3
 */

#if !(((DEM_SW_MAJOR_VERSION == 5) && (DEM_SW_MINOR_VERSION == 11)) )
#error Dem: Configuration file expected BSW module version to be 5.11.*
#endif

#if !(((DEM_AR_RELEASE_MAJOR_VERSION == 4) && (DEM_AR_RELEASE_MINOR_VERSION == 0)) )
#error DEM: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef DEMENABBLECONDIDID_H_
#define DEMENABBLECONDIDID_H_

enum {
	DEM_ENABLE_CONDITION_EOL 
};


#endif /*DEMENABBLECONDIDID_H_*/
