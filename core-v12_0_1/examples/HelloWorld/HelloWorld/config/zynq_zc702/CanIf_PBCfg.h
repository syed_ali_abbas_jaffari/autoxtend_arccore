/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef CANIF_PBCFG_H_
#define CANIF_PBCFG_H_

#if !(((CANIF_SW_MAJOR_VERSION == 5) && (CANIF_SW_MINOR_VERSION == 0)) )
#error CanIf: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((CANIF_AR_RELEASE_MAJOR_VERSION == 4) && (CANIF_AR_RELEASE_MINOR_VERSION == 0)) )
#error CanIf: Configuration file expected AUTOSAR version to be 4.0.*
#endif




//Number of tx l-pdus
#define CANIF_NUM_TX_LPDU	5

#define CANIF_PDU_ID_TESTER01		0
#define CANIF_PDU_ID_DIAGFUNCTIONALRX		1
#define CANIF_PDU_ID_DIAGPHYSICALRX		2
#define CANIF_PDU_ID_XCP_RX_PDU		3
#define CANIF_PDU_ID_SWCMEM_RX_CANIF		4

#define CANIF_PDU_ID_ECU01		0
#define CANIF_PDU_ID_DIAGPHYSICALTX		1
#define CANIF_PDU_ID_DIAGUUDT		2
#define CANIF_PDU_ID_XCP_TX_PDU		3
#define CANIF_PDU_ID_SWCMEM_TX_CANIF		4

#endif
