/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef FEE_CFG_H_
#define FEE_CFG_H_

#include "MemIf_Types.h"
#include "Fee_ConfigTypes.h"

#define FEE_DEV_ERROR_DETECT		STD_ON
#define FEE_INDEX					0
#define FEE_POLLING_MODE			STD_ON
#define FEE_SET_MODE_SUPPORTED		STD_OFF /* Not supported  */
#define FEE_VERSION_INFO_API		STD_OFF
#define FEE_VIRTUAL_PAGE_SIZE		256
#define FEE_USE_JOB_NOTIFICATIONS	STD_ON

#define FEE_NUM_OF_BLOCKS			1

#define FeeConf_FeeBlockConfiguration_SwcMem_Block1	1




#define FEE_MAX_NUM_SETS			1

#endif /* FEE_CFG_H_ */ 

