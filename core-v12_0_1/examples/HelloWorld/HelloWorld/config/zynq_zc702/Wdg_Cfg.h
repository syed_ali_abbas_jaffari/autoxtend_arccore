
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */
/** @tagSettings DEFAULT_ARCHITECTURE=ZYNQ */ 

#include "Wdg.h"
#include "Mcu.h"

#if !(((WDG_SW_MAJOR_VERSION == 2) && (WDG_SW_MINOR_VERSION == 0)) )
#error Wdg: Configuration file expected BSW module version to be 2.0.*
#endif

#if !(((WDG_AR_RELASE_MAJOR_VERSION == 4) && (WDG_AR_RELASE_MINOR_VERSION == 1)) )
#error Wdg: Configuration file expected AUTOSAR version to be 4.1.*
#endif

#ifndef WDG_CFG_H_
#define WDG_CFG_H_

#define	WDG_VERSION_INFO_API			STD_OFF
#define	WDG_DEV_ERROR_DETECT			STD_ON

/* @req SWS_Wdg_00171 */
typedef struct {
	WdgIf_ModeType Wdg_DefaultMode;
	uint16		   WdgSettingsFast;
	uint16		   WdgSettingsSlow;
}Wdg_ModeConfigType;

typedef struct
{
	const Wdg_ModeConfigType *Wdg_ModeConfig;
	boolean Wdg_ResetEnable;
	void (*Wdg_Notification) (void);
}Wdg_ConfigType;

extern const Wdg_ConfigType WdgConfig;
 

#define WdgConf_WdgIndex_WdgIndex 

/* Configuration Set Handles */
#define WdgSettingsConfig (WdgConfig)
#define Wdg_WdgSettingsConfig (WdgConfig)

#endif /* WDG_CFG_H_ */
