/*
 * Generator version: 2.2.1
 * AUTOSAR version:   4.0.3
 */

#if !(((COMM_SW_MAJOR_VERSION == 2) && (COMM_SW_MINOR_VERSION == 2)) )
#error ComM: Configuration file expected BSW module version to be 2.2.*
#endif

#if !(((COMM_AR_RELEASE_MAJOR_VERSION == 4) && (COMM_AR_RELEASE_MINOR_VERSION == 0)) )
#error ComM: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef COMM_CFG_H_
#define COMM_CFG_H_ 

#define COMM_DEV_ERROR_DETECT               STD_ON
#define COMM_VERSION_INFO_API               STD_ON
#define COMM_MODE_LIMITATION_ENABLED        STD_OFF
#define COMM_NO_COM                         STD_OFF
#define COMM_RESET_AFTER_FORCING_NO_COMM    STD_OFF      // Not supported
#define COMM_SYNCHRONOUS_WAKE_UP            STD_OFF      // Not supported
#define COMM_PNC_SUPPORT					STD_OFF
#define	COMM_PNC_NUM						0u
#define COMM_PNC_COMSIGNAL_VECTOR_LEN		0
#define COMM_T_MIN_FULL_COM_MODE_DURATION   5000
#define COMM_T_PNC_PREPARE_SLEEP			0
#define COMM_PNC_GATEWAY_ENABLED			STD_OFF
  
#define COMM_CHANNEL_COUNT 					1  
#define COMM_USER_COUNT      				1
 

#define COMM_NETWORK_HANDLE_ComMChannel 0u
#define ComMConf_ComMChannel_ComMChannel 0u

#define INVALID_NETWORK_HANDLE 0xFFu
#define INVALID_SIGNAL_HANDLE 0xFFFFu
#define COMM_USER_HANDLE_ComMUser ((ComM_UserHandleType)0)
#define ComMConf_ComMUser_ComMUser 0u

void ComM_MainFunction_ComMChannel(void);



/* Call back function declaration */

/* Call back function declaration */
/** @req ComM986 */
/** @req ComM971 */


/* ComM module configuration. */
extern const ComM_ConfigType ComM_Config;

#endif /* COMM_CFG_H_ */

