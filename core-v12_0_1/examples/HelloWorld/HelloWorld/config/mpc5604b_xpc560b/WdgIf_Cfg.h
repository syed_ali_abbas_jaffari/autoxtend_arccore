/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef WDGIF_CFG_H_
#define WDGIF_CFG_H_

#include "Std_Types.h"
#include "WdgIf_Types.h"

#if !(((WDGIF_SW_MAJOR_VERSION == 1) && (WDGIF_SW_MINOR_VERSION == 0)) )
#error WdgIf: Configuration file expected BSW module version to be 1.0.*
#endif

#if !(((WDGIF_AR_RELEASE_MAJOR_VERSION == 4) && (WDGIF_AR_RELEASE_MINOR_VERSION == 0)) )
#error WdgIf: Configuration file expected AUTOSAR version to be 4.0.*
#endif


#define WDGIF_DEV_ERROR_DETECT 		STD_ON
#define WDGIF_VERSION_INFO_API 		STD_ON

typedef struct
{
	uint8 WdgIf_DeviceIndex;
	const Wdg_GeneralType *WdgRef;
}WdgIf_DeviceType;

typedef struct
{
	uint8 WdgIf_NumberOfDevices;
}WdgIf_GeneralType;

typedef struct
{
	const WdgIf_GeneralType		*WdgIf_General;
	const WdgIf_DeviceType		*WdgIf_Device;
}WdgIf_ConfigType;

extern const WdgIf_DeviceType WdgIfDevices[];
extern const WdgIf_ConfigType WdgIfConfig;

#define WDGIF_DEVICES 1

#endif /*WDGIF_CFG_H_*/

