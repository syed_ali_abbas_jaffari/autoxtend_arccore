/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_WdgM.h"

/** === Inter-Runnable Variable Buffers ===========================================================
 */

/** === Inter-Runnable Variable Functions =========================================================
 */

/** === Implicit Buffer Instances =================================================================
 */

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define WdgM_START_SEC_VAR_INIT_UNSPECIFIED
#include <WdgM_MemMap.h>
const Rte_CDS_WdgM WdgM_wdgm = {
    .mode_Supervised100msTask = {
        ._dummy = 0 // SWS_Rte_07138 requires the existence of a PDS in the CDS. Even if it is empty.
    }
};
#define WdgM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <WdgM_MemMap.h>

#define WdgM_START_SEC_VAR_INIT_UNSPECIFIED
#include <WdgM_MemMap.h>
const Rte_Instance Rte_Inst_WdgM = &WdgM_wdgm;

#define WdgM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <WdgM_MemMap.h>

/** === Runnables =================================================================================
 */
#define WdgM_START_SEC_CODE
#include <WdgM_MemMap.h>

/** ------ wdgm -----------------------------------------------------------------------
 */
Std_ReturnType Rte_wdgm_UpdateAliveCounter(/*IN*/WdgM_SupervisedEntityIdType portDefArg1) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = WdgM_UpdateAliveCounter(portDefArg1);

    /* POST */

    return retVal;
}
Std_ReturnType Rte_wdgm_CheckpointReached(/*IN*/WdgM_SupervisedEntityIdType portDefArg1, /*IN*/WdgM_CheckpointIdType CheckpointID) {
    Std_ReturnType retVal = RTE_E_OK;

    /* PRE */

    /* MAIN */

    retVal = WdgM_CheckpointReached(portDefArg1, CheckpointID);

    /* POST */

    return retVal;
}
#define WdgM_STOP_SEC_CODE
#include <WdgM_MemMap.h>

