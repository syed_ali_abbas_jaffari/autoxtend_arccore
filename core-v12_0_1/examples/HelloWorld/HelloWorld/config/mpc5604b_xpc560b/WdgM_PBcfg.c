/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.0.3
 */

#include "WdgM.h"


const uint16 Supervised100msTask_CheckpointIds[] = {
	WdgMConf_WdgMCheckpoint_Supervised100msCheckpoint,
};



/******* Alive supervisions *******************************************/
const WdgM_AliveSupervision WdgM_AliveSupervision_WatchdogOn_Supervised100msTask[] =
{
	{
		.CheckpointId = WdgMConf_WdgMCheckpoint_Supervised100msCheckpoint, 
		.ExpectedAliveIndications = 4,
		.MinMargin = 1,
		.MaxMargin = 1,
		.SupervisionReferenceCycle = 40,
	},
};




/******* Deadline supervisions *******************************************/


/* Triggers */
const WdgM_Trigger WdgM_Triggers_WatchdogOn[] =
{
	{
		.TriggerConditionValue = 1,
		.WatchdogMode = WDGIF_SLOW_MODE,
		.WatchdogId = 0,
	},
};

const WdgM_Trigger WdgM_Triggers_WatchdogOff[] =
{
	{
		.TriggerConditionValue = 1,
		.WatchdogMode = WDGIF_OFF_MODE,
		.WatchdogId = 0,
	},
};




const WdgM_SupervisedEntityConfiguration WatchdogOn_ModeConfiguration[] =
{
 
   /* Supervised100msTask */
   {
      .SupervisedEntityId = WdgMConf_WdgMSupervisedEntity_Supervised100msTask,
      .CheckInternalLogic = FALSE,
      .AliveSupervisions = WdgM_AliveSupervision_WatchdogOn_Supervised100msTask,
      .Length_AliveSupervisions = 1,
      .DeadlineSupervisions = NULL,
      .Length_DeadlineSupervisions = 0,
      .FailedAliveSupervisionReferenceCycleTol = 0,
   },
};



const WdgM_Mode Modes[] =
{
	{
		.Id = WdgMConf_WdgMMode_WatchdogOn,
		.ExpiredSupervisionCycleTol = 1,
		.SupervisionCycle = 10,
		.SEConfigurations = WatchdogOn_ModeConfiguration,
		.Length_SEConfigurations = sizeof(WatchdogOn_ModeConfiguration)/sizeof(WdgM_SupervisedEntityConfiguration),
		.Triggers = WdgM_Triggers_WatchdogOn,
		.Length_Triggers = sizeof(WdgM_Triggers_WatchdogOn)/sizeof(WdgM_Trigger)
	},
	{
		.Id = WdgMConf_WdgMMode_WatchdogOff,
		.ExpiredSupervisionCycleTol = 0,
		.SupervisionCycle = 10,
		.SEConfigurations = NULL,
		.Length_SEConfigurations = 0,
		.Triggers = WdgM_Triggers_WatchdogOff,
		.Length_Triggers = sizeof(WdgM_Triggers_WatchdogOff)/sizeof(WdgM_Trigger)
	},
};


const WdgM_SupervisedEntity SEs[] =
{
	{
		.Id                                     = WdgMConf_WdgMSupervisedEntity_Supervised100msTask,
		.CheckpointIds 							= Supervised100msTask_CheckpointIds,
		.Length_CheckpointIds 					= 1,
		.Transitions 							= NULL,
		.Length_Transitions 					= 0,
		.StartCheckpointIds 					= NULL,
		.Length_StartCheckpointIds 				= 0,
		.FinalCheckpointIds 					= NULL,
		.Length_FinalCheckpointIds 				= 0,
		.isOsApplicationRefSet 					= FALSE,
		.OsApplicationRef 						= 0
	},
};	




/******* Watchdogs *******************************************/
const WdgM_Watchdog WdgMWatchdog_Configuration[] =
{
	{
		.WatchdogId = 0,
		.WatchdogDeviceId = 0,	
	},
};

const uint16 callerIDs[] = { 1 };

const WdgM_ConfigType WdgMConfig =
{
	.General = {	
		.CallerIds = 	{
							.allowedCallerIds = callerIDs,
							.Length_allowedCallerIds = sizeof(callerIDs)/sizeof(uint16),
						},
		.SupervisedEntities = SEs,
		.Length_SupervisedEntities = sizeof(SEs)/sizeof(WdgM_SupervisedEntity),
		.Watchdogs = WdgMWatchdog_Configuration,
		.Length_Watchdogs = sizeof(WdgMWatchdog_Configuration)/sizeof(WdgM_Watchdog),
	},
	.ConfigSet = {
#if defined(USE_DEM)	
		.DemEventIdRefs = 	{
								.ImproperCaller = DEM_EVENT_ID_NULL,
								.Monitoring = DEM_EVENT_ID_NULL,
								.SetMode = DEM_EVENT_ID_NULL,
							},
#endif
		.initialModeId = WdgMConf_WdgMMode_WatchdogOff,
		.Modes = Modes,
		.Length_Modes = sizeof(Modes)/sizeof(WdgM_Mode)	
	}
};


/*
 *	RUNTIME-DATA
 *
*/


/******* Alive supervisions *******************************************/
WdgM_internal_AliveSupervision AliveSupervisionRuntime_WatchdogOn_Supervised100msTask[] =
{
	{
		.AliveCounter				= 0,
		.SupervisionCycleCounter	= 0,
		.wasEvaluated				= FALSE,
	},
};



/******* Deadline supervisions *******************************************/



WdgM_internal_SupervisedEntityConfig RunTimeModeData_WatchdogOn[] =
{
		{
				.AliveSupervisions = AliveSupervisionRuntime_WatchdogOn_Supervised100msTask,
				.Length_AliveSupervisions = 1,
				.DeadlineSupervisions = NULL,
				.Length_DeadlineSupervisions = 0,
		},
};




/* MODES */

/* not for each SE a configuration is neccessary (same within applies for the config!) BUT in order to avoid expensive searching
 * an emptry SE configuration may be inserted when no alive and no deadline BUT external Supervision or internal Supervision is configured!!!!!!
 * in other words when ".CheckInternalLogic == TRUE" within the mode-dependent SEConfig
 */


WdgM_internal_Mode runtime_Modes[] =
{
	{
		.SE_Configs = RunTimeModeData_WatchdogOn,
		.Length_SEConfigs = sizeof(RunTimeModeData_WatchdogOn)/sizeof(WdgM_internal_SupervisedEntityConfig),
		.ExpiredSupervisionCycleCounter = 0,
	},	
	{
		.SE_Configs = NULL,
		.Length_SEConfigs = 0,
		.ExpiredSupervisionCycleCounter = 0,
	},	
};


WdgM_internal_SupervisedEntity runtime_SEs[] = 
{
	{
		.LocalState = 							WDGM_LOCAL_STATUS_OK,
		.SubstateAlive = 						WDGM_SUBSTATE_CORRECT,
		.SubstateDeadline = 					WDGM_SUBSTATE_CORRECT,
		.SubstateLogical = 						WDGM_SUBSTATE_CORRECT,
		.IsInternalGraphActive = 				FALSE,
		.PreviousCheckpointId_Deadline = 		0,
		.PreviousCheckpointId_internalLogic = 	0,
		.FailedAliveCyclesCounter = 			0,
	},
};	


/* INTERNAL RUNTIME DATA */
WdgM_internal_RuntimeData WdgM_runtimeData =
{
	.SEs = 				runtime_SEs,
	.Length_SEs = 		sizeof(runtime_SEs)/sizeof(WdgM_internal_SupervisedEntity),
	.Modes = 			runtime_Modes,
	.Length_Modes = 	sizeof(runtime_Modes)/sizeof(WdgM_internal_Mode),
};
