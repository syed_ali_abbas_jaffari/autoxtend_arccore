/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#include "WdgIf.h"
#include "Wdg.h"



extern void  Wdg_SetTriggerCondition(uint16 Cond);
extern Std_ReturnType Wdg_SetMode(WdgIf_ModeType Mode);

const Wdg_GeneralType Wdg =
{
	.Wdg_Index = 0, 
	.Wdg_TriggerLocationPtr = Wdg_SetTriggerCondition,
	.Wdg_SetModeLocationPtr = Wdg_SetMode,
};


const WdgIf_DeviceType WdgIfDevices[] =
{
	{
		.WdgRef = &Wdg,
	}

};


const WdgIf_GeneralType WdgIfGeneral =
{
  .WdgIf_NumberOfDevices = sizeof(WdgIfDevices)/sizeof(WdgIfDevices[0]),
};

const WdgIf_ConfigType WdgIfConfig =
{
  .WdgIf_General = &WdgIfGeneral,
  .WdgIf_Device = &WdgIfDevices[0],
};

