/*
 * Generator version: 1.1.0
 * AUTOSAR version:   4.0.3
 */

#ifndef COM_PBCFG_H
#define COM_PBCFG_H

#if !(((COM_SW_MAJOR_VERSION == 1) && (COM_SW_MINOR_VERSION == 1)) )
#error Com: Configuration file expected BSW module version to be 1.1.*
#endif

#if !(((COM_AR_RELEASE_MAJOR_VERSION == 4) && (COM_AR_RELEASE_MINOR_VERSION == 0)) )
#error Com: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#include "Com_Types.h"

// COM IPDU IDs
#define ComConf_ComIPdu_Tester01					0
#define ComConf_ComIPdu_Ecu01					1
#define ComConf_ComIPdu_SwcMemRx					2
#define ComConf_ComIPdu_SwcMemTx					3

// COM PDU GROUP IDs
#define ComConf_ComIPduGroup_ComIPduGroupRx			0
#define ComConf_ComIPduGroup_ComIPduGroupTx			1

// COM SIGNAL IDs
#define ComConf_ComSignal_rxsignal01			0
#define ComConf_ComSignal_txsignal01			1
#define ComConf_ComSignal_AdcValue			2
#define ComConf_ComSignal_SwcMemRx_BlockId			3
#define ComConf_ComSignal_SwcMemRx_Request			4
#define ComConf_ComSignal_SwcMemRx_Data			5
#define ComConf_ComSignal_SwcMemTx_BlockId			6
#define ComConf_ComSignal_SwcMemTx_Result			7
#define ComConf_ComSignal_SwcMemTx_Data			8
#define ComConf_ComSignal_rxCmdSignal			9

// COM GROUP SIGNAL IDs

// COM GATEWAY SOURCE SIGNAL DESCRIPTION IDs

// COM GATEWAY DESTINATION SIGNAL DESCRIPTION IDs

// COM GATEWAY MAPPING IDs

#endif /* COM_PBCFG_H */
