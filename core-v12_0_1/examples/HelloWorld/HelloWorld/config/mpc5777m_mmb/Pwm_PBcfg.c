/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Pwm.h"


/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here.
 */

const Pwm_ConfigType PwmConfig = {
	.Channels = {
		{	// channel Channel_LED_3
			.frequency = 1000, // Hz
			.duty      = 9830,
			.index	   = 0,
			.prescaler = PWM_CHANNEL_PRESCALER_DIV_128,
			.polarity  = PWM_HIGH,
		},
		{	// channel flexpwm0_sub0_X
			.frequency = 1000, // Hz
			.duty      = 3276,
			.index	   = 2,
			.prescaler = PWM_CHANNEL_PRESCALER_DIV_128,
			.polarity  = PWM_HIGH,
		},
		{	// channel flexpwm1_sub1_B
			.frequency = 100, // Hz
			.duty      = 4915,
			.index	   = 16,
			.prescaler = PWM_CHANNEL_PRESCALER_DIV_128,
			.polarity  = PWM_HIGH,
		},
	},
	.IdleState = {
		PWM_LOW, // idle state for Channel_LED_3
		PWM_LOW, // idle state for flexpwm0_sub0_X
		PWM_HIGH, // idle state for flexpwm1_sub1_B
	},
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	.ChannelClass={	
		PWM_VARIABLE_PERIOD,  // channel class for Channel_LED_3
		PWM_FIXED_PERIOD_SHIFTED,  // channel class for flexpwm0_sub0_X
		PWM_FIXED_PERIOD,  // channel class for flexpwm1_sub1_B
	},
#endif	
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	.NotificationHandlers = {
		// Notification routine for Pwm_Channel_LED_3
		NULL,
		// Notification routine for Pwm_flexpwm0_sub0_X
		NULL,
		// Notification routine for Pwm_flexpwm1_sub1_B
		NULL,
	}
#endif
};

