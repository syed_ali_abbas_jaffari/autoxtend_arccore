
/*
 * Generator version: 5.11.0
 * AUTOSAR version:   4.0.3
 */

#if !(((DEM_SW_MAJOR_VERSION == 5) && (DEM_SW_MINOR_VERSION == 11)) )
#error Dem: Configuration file expected BSW module version to be 5.11.*
#endif

#if !(((DEM_AR_RELEASE_MAJOR_VERSION == 4) && (DEM_AR_RELEASE_MINOR_VERSION == 0)) )
#error DEM: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef DEMINTEVTID_H_
#define DEMINTEVTID_H_

#define DEM_EVENT_ID_SWC_START (Dem_EventIdType)6
#define DemConf_DemEventParameter_TestEvent (Dem_EventIdType)6
#define DEM_EVENT_ID_LAST_FOR_SWC 	(Dem_EventIdType)6
#define DEM_EVENT_ID_LAST_VALID_ID	DEM_EVENT_ID_LAST_FOR_SWC

#endif /*DEMINTEVTID_H_*/
