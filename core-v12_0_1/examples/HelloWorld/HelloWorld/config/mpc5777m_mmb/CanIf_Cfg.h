/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.0.3
 */

#if !(((CANIF_SW_MAJOR_VERSION == 5) && (CANIF_SW_MINOR_VERSION == 0)) )
#error CanIf: Configuration file expected BSW module version to be 5.0.*
#endif

/* @req 4.0.3/CANIF021 */
#if !(((CANIF_AR_RELEASE_MAJOR_VERSION == 4) && (CANIF_AR_RELEASE_MINOR_VERSION == 0)) )
#error CanIf: Configuration file expected AUTOSAR version to be 4.0.*
#endif

#ifndef CANIF_CFG_H_
#define CANIF_CFG_H_

/* @req 4.0.3/CANIF377 */
#include "Can.h"

#define CANIF_PUBLIC_CANCEL_TRANSMIT_SUPPORT			STD_OFF  // Not supported
#define CANIF_PUBLIC_CHANGE_BAUDRATE_SUPPORT			STD_OFF  // Not supported
#define CANIF_PUBLIC_DEV_ERROR_DETECT					STD_ON
#define CANIF_PUBLIC_MULTIPLE_DRV_SUPPORT				STD_OFF  // Not supported
#define CANIF_PUBLIC_PN_SUPPORT							STD_OFF
#define CANIF_PUBLIC_READRXPDU_DATA_API					STD_OFF  // Not supported
#define CANIF_PUBLIC_READRXPDU_NOTIFY_STATUS_API		STD_OFF  // Not supported
#define CANIF_PUBLIC_READTXPDU_NOTIFY_STATUS_API		STD_OFF  // Not supported
#define CANIF_PUBLIC_SETDYNAMICTXID_API					STD_OFF  // Not supported
#define CANIF_PUBLIC_TX_BUFFERING						STD_OFF
#define CANIF_PUBLIC_TXCONFIRM_POLLING_SUPPORT			STD_OFF  // Not supported
#define CANIF_PUBLIC_VERSION_INFO_API					STD_OFF
#define CANIF_PUBLIC_WAKEUP_CHECK_VALID_BY_NM			STD_OFF  // Not supported
#define CANIF_PUBLIC_WAKEUP_CHECK_VALIDATION_SUPPORT	STD_OFF

#define CANIF_PRIVATE_DLC_CHECK							STD_ON


#define CANIF_CTRL_WAKEUP_SUPPORT						STD_OFF  // Not supported 
#define CANIF_TRCV_WAKEUP_SUPPORT						STD_OFF

#define CANIF_CTRLDRV_TX_CANCELLATION					STD_OFF

// ArcCore
#define CANIF_ARC_RUNTIME_PDU_CONFIGURATION				STD_OFF  // Not supported
#define CANIF_ARC_TRANSCEIVER_API						STD_OFF

//Software filter
#define CANIF_PRIVATE_SOFTWARE_FILTER_TYPE_MASK

typedef uint8 CanIf_Arc_ChannelIdType;
#define CanIfConf_CanIfCtrlCfg_CanIfCtrlCfg	(CanIf_Arc_ChannelIdType)0
#define CANIF_CHANNEL_CNT (CanIf_Arc_ChannelIdType)1


#define CANIF_TRANSCEIVER_CHANNEL_CNT	(uint8)

#define	CANNM_CALLOUT    0u
#define	CANTP_CALLOUT    1u
#define	J1939TP_CALLOUT  2u
#define	PDUR_CALLOUT     3u
#define	XCP_CALLOUT      4u



#include "CanIf_ConfigTypes.h"

#endif
