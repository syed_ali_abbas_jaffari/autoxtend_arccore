
/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef IOHWAB_ANALOG_H_
#define IOHWAB_ANALOG_H_


/* @req ARCIOHWAB005 */

#include "Std_Types.h"

typedef IoHwAb_AnalogValueType IoHwAb_AmpereType;
typedef IoHwAb_AnalogValueType IoHwAb_VoltType;
typedef IoHwAb_AnalogValueType IoHwAb_OhmType;

#define IOHWAB_SIGNAL_ANALOGSIGNAL ((IoHwAb_SignalType)0)


Std_ReturnType IoHwAb_Analog_Read(IoHwAb_SignalType signal, IoHwAb_AnalogValueType *value, IoHwAb_StatusType *status);


#endif /* IOHWAB_ANALOG_H_ */

