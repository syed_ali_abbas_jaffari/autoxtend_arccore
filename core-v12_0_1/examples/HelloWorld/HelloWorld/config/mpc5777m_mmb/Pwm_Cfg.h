
/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#ifndef PWM_CFG_H_
#define PWM_CFG_H_

#if !(((PWM_SW_MAJOR_VERSION == 2) && (PWM_SW_MINOR_VERSION == 1)) )
#error Pwm: Configuration file expected BSW module version to be 2.1.*
#endif

#if !(((PWM_AR_RELEASE_MAJOR_VERSION == 4) && (PWM_AR_RELEASE_MINOR_VERSION == 1)) )
#error Pwm: Configuration file expected AUTOSAR version to be 4.1.*
#endif


/****************************************************************************
 * Global configuration options and defines
 */
 
// PWM003
#define PWM_DEV_ERROR_DETECT                STD_OFF
#define PWM_NOTIFICATION_SUPPORTED          STD_ON

// PWM132. Currently only ON is supported.
#define PWM_DUTYCYCLE_UPDATED_ENDPERIOD     STD_ON
#define PWM_PERIOD_UPDATED_ENDPERIOD        STD_ON


// Define what functions to enable.
#define PWM_GET_OUTPUT_STATE_API            STD_ON
#define PWM_SET_PERIOD_AND_DUTY_API         STD_ON
#define PWM_DE_INIT_API                     STD_ON
#define PWM_SET_DUTY_CYCLE_API              STD_ON
#define PWM_SET_OUTPUT_TO_IDLE_API          STD_ON
#define PWM_VERSION_INFO_API                STD_ON
#define PWM_POSTBUILD_VARIANT  				STD_OFF

/****************************************************************************
 * Not defined in AUTOSAR.
 */
#define PWM_ISR_PRIORITY	4
#define PWM_MAX_CHANNEL		48
/*
 * Setting to ON freezes the current output state of a PWM channel when in
 * debug mode.
 */
 #define PWM_FREEZE_ENABLE	STD_ON


/* Pwm_ChannelType == channel id */
typedef uint8 Pwm_ChannelType;


/* Channels - Symbolic Names */
#define  PwmConf_PwmChannel_Channel_LED_3 (Pwm_ChannelType)0
#define  Pwm_Channel_LED_3 PwmConf_PwmChannel_Channel_LED_3
#define  PwmConf_PwmChannel_flexpwm0_sub0_X (Pwm_ChannelType)1
#define  Pwm_flexpwm0_sub0_X PwmConf_PwmChannel_flexpwm0_sub0_X
#define  PwmConf_PwmChannel_flexpwm1_sub1_B (Pwm_ChannelType)2
#define  Pwm_flexpwm1_sub1_B PwmConf_PwmChannel_flexpwm1_sub1_B
#define PWM_NUMBER_OF_CHANNELS 3 
 


/*
 * PWM070: All time units used within the API services of the PWM module shall
 * be of the unit ticks.
 */
typedef uint16 Pwm_PeriodType;

typedef enum {
	PWM_CHANNEL_PRESCALER_DIV_1=0,
	PWM_CHANNEL_PRESCALER_DIV_2,
	PWM_CHANNEL_PRESCALER_DIV_4,
	PWM_CHANNEL_PRESCALER_DIV_8,
	PWM_CHANNEL_PRESCALER_DIV_16,
	PWM_CHANNEL_PRESCALER_DIV_32,
	PWM_CHANNEL_PRESCALER_DIV_64,
	PWM_CHANNEL_PRESCALER_DIV_128,
	PWM_CHANNEL_PRESCALER_DIV_AUTO,
} Pwm_ChannelPrescalerType;



typedef struct {
	uint32  					frequency;
	uint16						duty;
	uint8						index;
	Pwm_OutputStateType			polarity;
	Pwm_ChannelPrescalerType	prescaler;
} Pwm_ChannelConfigurationType;


typedef struct {
	Pwm_ChannelConfigurationType Channels[PWM_NUMBER_OF_CHANNELS];
	Pwm_OutputStateType IdleState[PWM_NUMBER_OF_CHANNELS];
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
		Pwm_ChannelClassType  ChannelClass[PWM_NUMBER_OF_CHANNELS];
#endif
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	Pwm_NotificationHandlerType NotificationHandlers[PWM_NUMBER_OF_CHANNELS];
#endif
} Pwm_ConfigType;

#endif /* PWM_CFG_H_ */

