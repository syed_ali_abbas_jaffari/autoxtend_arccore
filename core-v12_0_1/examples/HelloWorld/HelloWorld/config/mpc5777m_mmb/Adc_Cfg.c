/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Adc.h"
#include "Adc_Cfg.h"

const Adc_HWConfigurationType AdcHWUnitConfiguration_ADC1 =
{
  .hwUnitId = ADC_CTRL_1,
  .adcPrescale = ADC_SYSTEM_CLOCK_DIVIDE_FACTOR_2,
  .clockSource = ADC_SYSTEM_CLOCK,
};

const Adc_ChannelType Adc_ADC1_AdcGroup2_ChannelList[ADC_NBR_OF_ADC1_ADCGROUP2_CHANNELS] =
{
ADC_CH11,

};


Adc_GroupStatus AdcGroupStatus_ADC1[ADC_NBR_OF_ADC1_GROUPS]=
{
	{ // ADC_ADC1_ADCGROUP2
		.notifictionEnable = 0,
		.resultBufferPtr = NULL,
		.groupStatus = ADC_IDLE,
		.currSampleCount = 0,
		.currResultBufPtr = NULL,
	},

};	

const Adc_GroupDefType AdcGroupConfiguration_ADC1[] =
{
   { 
   	 .adcChannelConvTime= {
   	 							ADC_INPLATCH_0,
   	 							ADC_INPCPM_1,
   	 							2
   	 					   },
     .accessMode        = ADC_ACCESS_MODE_STREAMING,
     .conversionMode    = ADC_CONV_MODE_CONTINUOUS,
     .triggerSrc        = ADC_TRIGG_SRC_SW,
     .hwTriggerSignal   = ADC_HW_TRIG_FALLING_EDGE,
     .hwTriggerTimer    = ADC_NO_TIMER,
/* @req SWS_Adc_00084 */     
     .groupCallback     = NULL,
     .streamBufferMode  = ADC_STREAM_BUFFER_LINEAR,
     .streamNumSamples  = 1,
     .channelList       = Adc_ADC1_AdcGroup2_ChannelList,
     .numberOfChannels  = (Adc_ChannelType)ADC_NBR_OF_ADC1_ADCGROUP2_CHANNELS,
     .status            = &AdcGroupStatus_ADC1[0]
   },

};
const Adc_HWConfigurationType AdcHWUnitConfiguration_AdcHwUnit =
{
  .hwUnitId = ADC_CTRL_B,
  .adcPrescale = ADC_SYSTEM_CLOCK_DIVIDE_FACTOR_2,
  .clockSource = ADC_SYSTEM_CLOCK,
};

const Adc_ChannelType Adc_AdcHwUnit_AdcGroup1_ChannelList[ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS] =
{
ADC_CH11,
ADC_CH0,
ADC_CH10,
ADC_CH15,

};


Adc_GroupStatus AdcGroupStatus_AdcHwUnit[ADC_NBR_OF_ADCHWUNIT_GROUPS]=
{
	{ // ADC_ADCHWUNIT_ADCGROUP1
		.notifictionEnable = 0,
		.resultBufferPtr = NULL,
		.groupStatus = ADC_IDLE,
		.currSampleCount = 0,
		.currResultBufPtr = NULL,
	},

};	

const Adc_GroupDefType AdcGroupConfiguration_AdcHwUnit[] =
{
   { 
   	 .adcChannelConvTime= {
   	 							ADC_INPLATCH_0,
   	 							ADC_INPCPM_1,
   	 							2
   	 					   },
     .accessMode        = ADC_ACCESS_MODE_STREAMING,
     .conversionMode    = ADC_CONV_MODE_CONTINUOUS,
     .triggerSrc        = ADC_TRIGG_SRC_SW,
     .hwTriggerSignal   = ADC_HW_TRIG_FALLING_EDGE,
     .hwTriggerTimer    = ADC_NO_TIMER,
/* @req SWS_Adc_00084 */     
     .groupCallback     = IoHwAb_AdcConversionComplete,
     .streamBufferMode  = ADC_STREAM_BUFFER_LINEAR,
     .streamNumSamples  = 1,
     .channelList       = Adc_AdcHwUnit_AdcGroup1_ChannelList,
     .numberOfChannels  = (Adc_ChannelType)ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS,
     .status            = &AdcGroupStatus_AdcHwUnit[0]
   },

};


const Adc_ConfigType AdcConfig[] =
{
  {
   .hwConfigPtr      = &AdcHWUnitConfiguration_ADC1,
   .groupConfigPtr   = AdcGroupConfiguration_ADC1,
   .nbrOfGroups      = ADC_NBR_OF_ADC1_GROUPS,
  },
  {
   .hwConfigPtr      = &AdcHWUnitConfiguration_AdcHwUnit,
   .groupConfigPtr   = AdcGroupConfiguration_AdcHwUnit,
   .nbrOfGroups      = ADC_NBR_OF_ADCHWUNIT_GROUPS,
  },

};
