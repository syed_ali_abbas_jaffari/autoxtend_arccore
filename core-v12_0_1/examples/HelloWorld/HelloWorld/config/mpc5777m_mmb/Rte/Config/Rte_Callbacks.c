
#include <Rte_Internal.h>
#include <Os.h>

void Rte_COMCbk_SwcMemRx_Request(void) {

    SYS_CALL_SetEvent(TASK_ID_OsRteTask, EVENT_MASK_SwcMemCmdReceived);
}

