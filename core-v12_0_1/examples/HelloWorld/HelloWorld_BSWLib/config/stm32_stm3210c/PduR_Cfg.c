/*
 * Generator version: 2.2.0
 * AUTOSAR version:   4.0.3
 */

#include "PduR.h"




PduRTpBufferInfo_type PduRTpBuffers[] = {

	{
		.pduInfoPtr = NULL,
		.status = PDUR_BUFFER_NOT_ALLOCATED_FROM_UP_MODULE,
		.bufferSize = 0
	}
};


PduRTpBufferInfo_type *PduRTpRouteBufferPtrs[] = {
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL, // NEED TO FIXE SOMETHING HERE... Also fix PduR_Config.NTpRouteBuffers
	NULL
};







const PduR_RamBufCfgType PduR_RamBufCfg = {
	.TpBuffers = PduRTpBuffers,
	.TpRouteBuffers = PduRTpRouteBufferPtrs,
	.TxBuffers = NULL,
	.NTpBuffers = 1, // Including 1 non-allocated buffer place holder for Upper module allocated buffer
	.NTpRouteBuffers = 50, 
	.NTxBuffers = 0
};


