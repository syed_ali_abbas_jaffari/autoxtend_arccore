/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.0.3
 */

#include "Dlt.h"

#if !(((DLT_SW_MAJOR_VERSION == 1) && (DLT_SW_MINOR_VERSION == 0)) )
#error Dlt: Configuration file expected BSW module version to be 1.1.*
#endif

#if !(((DLT_AR_MAJOR_VERSION == 4) && (DLT_AR_MINOR_VERSION == 0)) )
#error Dlt: Expected AUTOSAR version to be 4.0.*
#endif

const Dlt_ConfigType Dlt_Config = 
{
	.EcuId = "DLTT",
};
