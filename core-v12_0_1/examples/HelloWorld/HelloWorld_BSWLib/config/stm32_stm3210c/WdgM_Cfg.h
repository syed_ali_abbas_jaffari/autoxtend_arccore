/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.0.3
 */

#ifndef WDGM_CFG_H_
#define WDGM_CFG_H_

#if !(((WDGM_SW_MAJOR_VERSION == 2) && (WDGM_SW_MINOR_VERSION == 0)) )
#error WdgM: Configuration file expected BSW module version to be 2.0.*
#endif

#if !(((WDGM_AR_RELEASE_MAJOR_VERSION == 4) && (WDGM_AR_RELEASE_MINOR_VERSION == 0)) )
#error WdgM: Configuration file expected AUTOSAR version to be 4.0.*
#endif


#define WDGM_DEV_ERROR_DETECT 					STD_ON
#define WDGM_IMMEDIATE_RESET					STD_OFF
#define WDGM_OFF_MODE_ENABLED					STD_OFF
#define WDGM_VERSION_INFO_API					STD_ON
#define WDGM_DEFENSIVE_BEHAVIOR					STD_OFF
#define WDGM_DEM_ALIVE_SUPERVISION_REPORT		STD_OFF

/** Supervised entities */
#define WdgMConf_WdgMSupervisedEntity_Supervised100msTask (WdgM_SupervisedEntityIdType)0

/* @req WDGM178 */
#define WdgMConf_WdgMMode_WatchdogOn	(WdgM_ModeType)0
#define WdgMConf_WdgMMode_WatchdogOff	(WdgM_ModeType)1

#define	WdgMConf_WdgMCheckpoint_Supervised100msCheckpoint (WdgM_CheckpointIdType)0



#endif /*WDGM_CFG_H_*/

