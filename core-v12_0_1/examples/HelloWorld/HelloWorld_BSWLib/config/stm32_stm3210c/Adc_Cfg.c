/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Adc.h"
#include "Adc_Cfg.h"
#include "stm32f10x_adc.h"


Adc_GroupStatus AdcGroupStatus_AdcHwUnit[1];

const Adc_HWConfigurationType AdcHWUnitConfiguration_AdcHwUnit =
{
	.hwUnitId = 0,
	.adcPrescale = ADC_SYSTEM_CLOCK_DIVIDE_FACTOR_2,
	.clockSource = ADC_SYSTEM_CLOCK,
};

const Adc_ChannelConfigurationType AdcChannelConfiguration_AdcHwUnit[] =
{
	{ .adcChannelConvTime = ADC_SampleTime_239Cycles5 },
};

const Adc_ChannelType Adc_AdcHwUnit_AdcGroup1_ChannelList[ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS] = 
{
	ADC_CH14
};

Adc_ValueGroupType Adc_AdcHwUnit_AdcGroup1_Buffer[ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS];

const Adc_GroupDefType AdcGroupConfiguration_AdcHwUnit[] =
{	
	{
		.conversionMode 	= ADC_CONV_MODE_ONESHOT,
		.triggerSrc 		= ADC_TRIGG_SRC_SW,
		.groupCallback 		= IoHwAb_AdcConversionComplete,
		.channelList 		= Adc_AdcHwUnit_AdcGroup1_ChannelList,
		.resultBuffer       = Adc_AdcHwUnit_AdcGroup1_Buffer,		
		.numberOfChannels   = ADC_NBR_OF_ADCHWUNIT_ADCGROUP1_CHANNELS,
		.status             = &AdcGroupStatus_AdcHwUnit[0],
	},
};


const Adc_ConfigType AdcConfig[] =
{
	{
		.hwConfigPtr      = &AdcHWUnitConfiguration_AdcHwUnit,
		.channelConfigPtr = AdcChannelConfiguration_AdcHwUnit,
		.nbrOfChannels    = 1,
		.groupConfigPtr   = AdcGroupConfiguration_AdcHwUnit,
		.nbrOfGroups      = 1
	},
};
