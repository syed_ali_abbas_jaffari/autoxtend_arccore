/*
 * Generator version: 1.0.0
 * AUTOSAR version:   4.1.3
 */

#include "Xcp.h"
#include "Xcp_Callout_Stubs.h"
#include <string.h>

#define COUNTOF(a) (sizeof(a)/sizeof(*(a)))





/*XcpEventChannel Daq references*/ 
static Xcp_DaqListType* Xcp_Channels_0_daqlist[4];

static Xcp_EventChannelType Xcp_Channels[ ] = {
    {   /*XcpEventChannel*/
    	.XcpEventChannelNumber              = 0,
        .XcpEventChannelMaxDaqList          = COUNTOF(Xcp_Channels_0_daqlist),
        .XcpEventChannelTriggeredDaqListRef = Xcp_Channels_0_daqlist,
        .XcpEventChannelName                = "XcpEventChannel",
        .XcpEventChannelRate                = 1,
        .XcpEventChannelUnit                = XCP_TIMESTAMP_UNIT_10MS,
        .XcpEventChannelProperties          = XCP_EVENTCHANNEL_PROPERTY_ALL,
        .XcpEventChannelDaqCount            = 0,
    },
};

/* These parameters are necessary for the Dynamic part of the DAQ. */
static Xcp_DaqListType  XcpDynamicDaq[XCP_DAQ_COUNT];          
static Xcp_OdtType      XcpDynamicOdt[XCP_DAQ_COUNT * XCP_ODT_COUNT];      	
static Xcp_OdtEntryType XcpDynamicOdtEntry[XCP_DAQ_COUNT * XCP_ODT_COUNT * XCP_ODT_ENTRIES_COUNT]; 

/* This is the collection of the Runtime parameters*/
static Xcp_RunTimeType XcpRuntimeParameters = {

    /* Pointer to the statically allocated DAQ Lists*/
    .XcpDaqList       = NULL,
    /* Statically allocated DAQ Lists size*/  
    .XcpDaqListSize   = 0u
};

const Xcp_ConfigType XcpConfig = {

    .rt                 = &XcpRuntimeParameters
  , .XcpEventChannel    = Xcp_Channels 
  , .XcpMaxEventChannel = COUNTOF(Xcp_Channels)
  , .XcpInfo            = { .XcpMC2File = "XcpServer" }
  , .XcpMtaInit         = Xcp_Arc_MtaInit
  , .XcpBuildChecksum   = Xcp_Arc_BuildChecksum
  
#if(XCP_FEATURE_PROTECTION == STD_ON)
  , .XcpOriginalProtect = XCP_PROTECT_NONE
  , .XcpSeedFn          = Xcp_Arc_GetSeed
  , .XcpUnlockFn        = Xcp_Arc_Unlock
#endif /*XCP_FEATURE_PROTECTION == STD_ON*/

#if (XCP_FEATURE_CALPAG == STD_ON)
  , .XcpSetCalPage      				 = Xcp_Arc_SetCalPage
  , .XcpGetCalPage      				 = Xcp_Arc_GetCalPage
  , .XcpCopyCalPage     				 = Xcp_Arc_CopyCalPage
  , .XcpGetPagProcessorInfo 			 = Xcp_Arc_GetPagProcessorInfo
  , .XcpGetSegmentInfo_GetBasicAddress   = Xcp_Arc_GetSegmentInfo_GetBasicAddress
  , .XcpGetSegmentInfo_GetStandardInfo   = Xcp_Arc_GetSegmentInfo_GetStandardInfo
  , .XcpGetSegmentInfo_GetAddressMapping = Xcp_Arc_GetSegmentInfo_GetAddressMapping
  , .XcpGetPageInfo     				 = Xcp_Arc_GetPageInfo
  , .XcpSetSegmentMode  				 = Xcp_Arc_SetSegmentMode
  , .XcpGetSegmentMode  				 = Xcp_Arc_GetSegmentMode
#endif /*XCP_FEATURE_CALPAG == STD_ON*/

#if(XCP_FEATURE_PGM == STD_ON)
  , .XcpProgramClear_FunctionalAccess    = Xcp_Arc_ProgramClear_FunctionalAccess	
  , .XcpProgramClear_AbsoluteAccess      = Xcp_Arc_ProgramClear_AbsoluteAccess
  , .XcpProgramReset                     = Xcp_Arc_ProgramReset
  , .XcpGetPgmProcessorInfo              = Xcp_Arc_GetPgmProcessorInfo
  , .XcpGetSectorInfo_Mode0_1            = Xcp_Arc_GetSectorInfo_Mode0_1
  , .XcpGetSectorInfo_Mode2              = Xcp_Arc_GetSectorInfo_Mode2
  , .XcpProgramPrepare                   = Xcp_Arc_ProgramPrepare
  , .XcpProgramFormat                    = Xcp_Arc_ProgramFormat
  , .XcpProgramVerify                    = Xcp_Arc_ProgramVerify
  , .XcpProgram                          = Xcp_Arc_Program
#endif /*XCP_FEATURE_PGM == STD_ON*/

#if(XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC)
  , .ptrDynamicDaq      = XcpDynamicDaq          
  , .ptrDynamicOdt      = XcpDynamicOdt        	
  , .ptrDynamicOdtEntry = XcpDynamicOdtEntry
#endif /*XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC*/  
};


/*This wrapper function is intended to be called from periodic task, 
  which has the period: 10 millisecond. */
void Xcp_Arc_WrapperForEventChannel_XcpEventChannel(void) {
	Xcp_MainFunction_Channel(0);
}

