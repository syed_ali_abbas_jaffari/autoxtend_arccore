/*
 * Generator version: 2.1.0
 * AUTOSAR version:   4.1.2
 */

#include "Pwm.h"


/*
 * Notification routines are defined elsewhere but need to be linked from here,
 * so we define the routines as external here.
 */

const Pwm_ConfigType PwmConfig = {
	.Channels = {
		{
			.channel   = PwmConf_PwmChannel_Channel_LED_3,
			.frequency = 1000, // Hz
			.duty      = 16384,
			.mode      = PWM_MODE_BASIC,
			.clksrc	   = PWM_CLKSRC_COUNTER_BUS_A,
			.prescaler = PWM_CHANNEL_PRESCALER_AUTO,
			.polarity  = PWM_LOW,
		},
		{
			.channel   = PwmConf_PwmChannel_Channel_LED_4,
			.frequency = 1000, // Hz
			.duty      = 0,
			.mode      = PWM_MODE_COUNTER_BUS_PROVIDER_MCB_UP,
			.clksrc	   = PWM_CLKSRC_INTERNAL_COUNTER,
			.prescaler = PWM_CHANNEL_PRESCALER_AUTO,
			.polarity  = PWM_LOW,
		},
	},
	.IdleState = {
		PWM_HIGH,
		PWM_HIGH,
	},
#if PWM_SET_PERIOD_AND_DUTY_API==STD_ON
	.ChannelClass={	
		### ERROR: value is unset ###,
		### ERROR: value is unset ###,
	},
#endif	
#if PWM_NOTIFICATION_SUPPORTED==STD_ON
	.NotificationHandlers = {
		// Notification routine for Pwm_Channel_LED_3
		NULL,
		// Notification routine for Pwm_Channel_LED_4
		NULL,
	}
#endif
};

