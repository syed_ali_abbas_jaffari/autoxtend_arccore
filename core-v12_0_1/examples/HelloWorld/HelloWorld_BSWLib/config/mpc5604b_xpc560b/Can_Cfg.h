
/*
 * Generator version: 5.0.0
 * AUTOSAR version:   4.1.2
 */

#ifndef CAN_CFG_H_
#define CAN_CFG_H_

#if !(((CAN_SW_MAJOR_VERSION == 5) && (CAN_SW_MINOR_VERSION == 0)) )
#error Can: Configuration file expected BSW module version to be 5.0.*
#endif

#if !(((CAN_AR_MAJOR_VERSION == 4) && (CAN_AR_MINOR_VERSION == 1)) )
#error Can: Configuration file expected AUTOSAAR version to be 4.1.*
#endif

// Number of controller configs
#define CAN_ARC_CTRL_CONFIG_CNT			1

#define CAN_DEV_ERROR_DETECT			STD_ON
#define CAN_VERSION_INFO_API			STD_OFF
#define CAN_MULTIPLEXED_TRANSMISSION	STD_OFF  // Not supported
#define CAN_WAKEUP_SUPPORT				STD_OFF  // Not supported
#define CAN_HW_TRANSMIT_CANCELLATION	STD_OFF
#define ARC_CAN_ERROR_POLLING			STD_OFF
#define CAN_USE_WRITE_POLLING			STD_OFF
#define CAN_USE_READ_POLLING			STD_OFF
#define CAN_USE_BUSOFF_POLLING			STD_OFF
#define CAN_USE_WAKEUP_POLLING			STD_OFF  // Not supported
#define CAN_POSTBUILD_VARIANT  			STD_OFF


#define USE_CAN_CTRL_A STD_ON
#define USE_CAN_CTRL_B STD_OFF
#define USE_CAN_CTRL_C STD_OFF
#define USE_CAN_CTRL_D STD_OFF
#define USE_CAN_CTRL_E STD_OFF
#define USE_CAN_CTRL_F STD_OFF

#define NO_CANIF_CONTROLLER 0xFF

#define FLEXCAN_A  0u
#define CAN_CTRL_A 0u
#define FLEXCAN_B  1u
#define CAN_CTRL_B 1u
#define FLEXCAN_C  2u
#define CAN_CTRL_C 2u
#define FLEXCAN_D  3u
#define CAN_CTRL_D 3u
#define FLEXCAN_E  4u
#define CAN_CTRL_E 4u
#define FLEXCAN_F  5u
#define CAN_CTRL_F 5u
#define CAN_CONTROLLER_CNT 6u
typedef uint8 CanHwUnitIdType;


#define CanConf_CanController_CanController0	(uint8)0
#define Can_CanController0	CanConf_CanController_CanController0

#define CAN_ARC_CANHARDWAREOBJECT_CTRL_0_TX_0 (Can_HwHandleType)0
#define NUM_OF_HTHS (Can_HwHandleType)1

#define CAN_ARC_CANHARDWAREOBJECT_CTRL_0_RX_0 (Can_HwHandleType)1

#define CanConf_CanHardwareObject_Ctrl_0_Rx_0 (Can_HwHandleType)0
#define Can_Ctrl_0_Rx_0 CanConf_CanHardwareObject_Ctrl_0_Rx_0
#define CanConf_CanHardwareObject_Ctrl_0_Tx_0 (Can_HwHandleType)1
#define Can_Ctrl_0_Tx_0 CanConf_CanHardwareObject_Ctrl_0_Tx_0

#define NUM_OF_HOHS 2

/* Configuration Set Handles */
#define CanConfigSet (CanConfigData)
#define Can_CanConfigSet (CanConfigData)

#endif /*CAN_CFG_H_*/
