/*
 * Generator version: 2.0.0
 * AUTOSAR version:   4.1.2
 */

#include "Spi.h"
/*Lint exception */
/*lint -save -e785 Using only designated initializers */

/* Callbacks */
extern void Spi_device_1( uint32 in );



// External Devices 
const Spi_ExternalDeviceType SpiExternalConfigData[] =
{
 {
  .SpiBaudrate = 4000000UL,
  .SpiCsIdentifier = 1,
  .SpiCsCallback = Spi_device_1,
  .SpiCsPolarity = STD_LOW,
  .SpiHwUnit = CSIB0,
  .SpiDataShiftEdge = SPI_EDGE_LEADING,
  .SpiEnableCs = 0, // NOT SUPPORTED IN TOOLS
  .SpiShiftClockIdleLevel = STD_LOW,
  .SpiTimeClk2Cs = 6000,   // ns
  .SpiTimeCs2Clk = 6000    // ns
 }
};


// Channels 
const Spi_ChannelConfigType SpiChannelConfigData[] =
{
{
  .SpiChannelId = SpiConf_SpiChannel_CH_ADDR,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 16,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
  .SpiTransferStart = SPI_TRANSFER_START_MSB,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_CMD,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
  .SpiTransferStart = SPI_TRANSFER_START_MSB,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_DATA,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 64,
  .SpiDefaultData = 0,
  .SpiTransferStart = SPI_TRANSFER_START_MSB,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_WREN,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 8,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 1,
  .SpiDefaultData = 6,
  .SpiTransferStart = SPI_TRANSFER_START_MSB,
 }
,{
  .SpiChannelId = SpiConf_SpiChannel_CH_WATCHDOG,
  .SpiChannelType = SPI_EB,
  .SpiDataWidth = 16,
  .SpiIbNBuffers = 0, 
  .SpiEbMaxLength = 0,
  .SpiDefaultData = 23040,
  .SpiTransferStart = SPI_TRANSFER_START_MSB,
 }
,
 {
   .SpiChannelId = CH_NOT_VALID,
 }

};


// Jobs
const Spi_JobConfigType SpiJobConfigData[] =
{
   {
      .SpiJobId = SpiConf_SpiJob_JOB_WREN,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 3,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_WREN],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_DATA,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 2,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_ADDR],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_DATA],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_CMD2,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 0,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_DATA],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
,   {
      .SpiJobId = SpiConf_SpiJob_JOB_CMD,
      .SpiHwUnit = CSIB0,
      .SpiJobPriority = 0,
      .SpiJobEndNotification = NULL,
      .SpiChannelAssignment = {
         &SpiChannelConfigData[SpiConf_SpiChannel_CH_CMD],
         NULL
      },
      .SpiDeviceAssignment = &SpiExternalConfigData[SPI_device_1],
   }
};


// Sequences 
const Spi_SequenceConfigType SpiSequenceConfigData[] =
{
   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_CMD,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_CMD],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_CMD2,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_CMD2],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_READ,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_DATA],
          NULL          
      },
   }
,   {
      .SpiSequenceId = SpiConf_SpiSequence_SEQ_WRITE,
      .SpiInterruptibleSequence = FALSE,
      .SpiSeqEndNotification = NULL,
      .SpiJobAssignment = {
          &SpiJobConfigData[SpiConf_SpiJob_JOB_WREN],
          &SpiJobConfigData[SpiConf_SpiJob_JOB_DATA],
          NULL          
      },
   }
};

uint32 Spi_GetExternalDeviceCnt(void ) { return sizeof(SpiExternalConfigData)/sizeof(SpiExternalConfigData[0]); }


const Spi_DriverType SpiConfigData =
{
  .SpiMaxChannel = SPI_MAX_CHANNEL,
  .SpiMaxJob = SPI_MAX_JOB,
  .SpiMaxSequence = SPI_MAX_SEQUENCE,
  .SpiChannelConfig = &SpiChannelConfigData[0],
  .SpiSequenceConfig = &SpiSequenceConfigData[0],
  .SpiJobConfig = &SpiJobConfigData[0],
  .SpiExternalDevice = &SpiExternalConfigData[0],
};

//lint -restore

