/** === HEADER ====================================================================================
 */

#include <Rte.h>

#include <Os.h>
#if ((OS_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (OS_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Os version mismatch
#endif

#include <Com.h>
#if ((COM_AR_RELEASE_MAJOR_VERSION != RTE_AR_RELEASE_MAJOR_VERSION) || (COM_AR_RELEASE_MINOR_VERSION != RTE_AR_RELEASE_MINOR_VERSION))
#error Com version mismatch
#endif

#include <Rte_Hook.h>
#include <Rte_Internal.h>
#include <Rte_Calprms.h>

#include "Rte_NvM.h"

/** === Implicit Buffer Instances =================================================================
 */

/** === Per Instance Memories =====================================================================
 */

/** === Component Data Structure Instances ========================================================
 */
#define NvM_START_SEC_VAR_INIT_UNSPECIFIED
#include <NvM_MemMap.h>
const Rte_CDS_NvM NvM_nvm = {
    ._dummy = 0
};
#define NvM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <NvM_MemMap.h>

#define NvM_START_SEC_VAR_INIT_UNSPECIFIED
#include <NvM_MemMap.h>
const Rte_Instance Rte_Inst_NvM = &NvM_nvm;

#define NvM_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <NvM_MemMap.h>

/** === Runnables =================================================================================
 */
#define NvM_START_SEC_CODE
#include <NvM_MemMap.h>

/** ------ nvm -----------------------------------------------------------------------
 */

void Rte_nvm_NvM_MainFunction(void) {
    /* PRE */

    /* MAIN */

    NvM_MainFunction();

    /* POST */

}

Std_ReturnType Rte_nvm_GetErrorStatus(/*IN*/NvM_BlockIdType portDefArg1, /*OUT*/NvM_RequestResultType * RequestResultPtr) {
    /* PRE */

    /* MAIN */

    Std_ReturnType retVal = NvM_GetErrorStatus(portDefArg1, RequestResultPtr);

    /* POST */

    return retVal;
}

Std_ReturnType Rte_nvm_SetDataIndex(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/uint8 DataIndex) {
    /* PRE */

    /* MAIN */

    Std_ReturnType retVal = NvM_SetDataIndex(portDefArg1, DataIndex);

    /* POST */

    return retVal;
}

Std_ReturnType Rte_nvm_GetDataIndex(/*IN*/NvM_BlockIdType portDefArg1, /*OUT*/uint8 * DataIndexPtr) {
    /* PRE */

    /* MAIN */

    Std_ReturnType retVal = NvM_GetDataIndex(portDefArg1, DataIndexPtr);

    /* POST */

    return retVal;
}

Std_ReturnType Rte_nvm_SetRamBlockStatus(/*IN*/NvM_BlockIdType portDefArg1, /*IN*/boolean BlockChanged) {
    /* PRE */

    /* MAIN */

    Std_ReturnType retVal = NvM_SetRamBlockStatus(portDefArg1, BlockChanged);

    /* POST */

    return retVal;
}

#define NvM_STOP_SEC_CODE
#include <NvM_MemMap.h>

