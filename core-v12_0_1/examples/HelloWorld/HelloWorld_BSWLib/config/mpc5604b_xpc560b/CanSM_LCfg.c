
/*
 * Generator version: 3.2.0
 * AUTOSAR version:   4.0.3
 */

#include "CanSM.h"
#include "ComM.h"
#include "CanIf.h"

const CanSM_ControllerType Controllers_CanSMManagerNetwork[] = {
	{
		.CanIfControllerId = CanIfConf_CanIfCtrlCfg_CanIfCtrlCfg,
	},
};

/* Map ComM channel Id to the corresponding CanSM index */
const NetworkHandleType CanSMComMHandleToCanSMIndex[1] = {
	[0u] = 0u,/* ComMChannel -> CanSMManagerNetwork */
};


const CanSM_NetworkType Networks[] = {
	{
		.Controllers 					= Controllers_CanSMManagerNetwork,
		.ControllerCount 				= 1,
		.ComMNetworkHandle 				= 0u,/* ComMConf_ComMChannel_ComMChannel */
		.CanSMBorTimeTxEnsured 			= 20,//in num main calls
		.CanSMBorTimeL1 				= 10,//in num main calls
		.CanSMBorTimeL2 				= 10,//in num main calls
		.CanSMBorCounterL1ToL2 			= 10,
		.CanSMBorTxConfirmationPolling	= false,//NOT SUPPORTED IN TOOLS
#if defined(USE_DEM)
		.CanSMDemEventId 				= DEM_EVENT_ID_NULL,
#endif
		.CanTrcvAvailable				= STD_OFF,
		.CanIfTransceiverId				= 0,
		.CanSMTrcvPNEnabled				= STD_OFF,
	},
};

const CanSM_ConfigType CanSM_Config = {
	.CanSMModeRequestRepetitionMax 		= 254,
	.CanSMModeRequestRepetitionTime 	= 20,//in num main calls
	.Networks 							= Networks,
	.ChannelMap							= CanSMComMHandleToCanSMIndex,
	.ChannelMapSize   					= 1,
};

