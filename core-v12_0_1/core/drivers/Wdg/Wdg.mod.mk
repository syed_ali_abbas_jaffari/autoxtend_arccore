#Wdg
obj-$(USE_WDG) += Wdg.o
obj-$(USE_WDG) += Wdg_Lcfg.o
obj-$(USE_WDG)-$(CFG_TMS570) += Wdg_PBcfg.o
obj-$(USE_WDG)-$(CFG_ZYNQ) += Wdg_PBcfg.o
obj-$(USE_WDG)-$(CFG_RH850) += Wdg_PBcfg.o
obj-$(USE_WDG)-$(CFG_RH850) += Wdg_rh850f1h.o
inc-$(USE_WDG)-$(CFG_RH850) += $(ROOTDIR)/drivers/Wdg 
vpath-$(USE_WDG)-$(CFG_RH850) += $(ROOTDIR)/drivers/Wdg