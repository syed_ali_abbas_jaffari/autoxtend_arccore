/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/* ----------------------------[information]----------------------------------*/
/*
 *
 * Description:
 *   Implements serial port for debugging on BCM2835
 */

/* ----------------------------[includes]------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "Std_Types.h"
#include "MemMap.h"
#include "device_serial.h"
#include "sys/queue.h"
#include "bcm2835.h"

/* ----------------------------[private define]------------------------------*/

/* ----------------------------[private macro]-------------------------------*/

/* ----------------------------[private typedef]-----------------------------*/

/* ----------------------------[private function prototypes]-----------------*/

/* ----------------------------[private variables]---------------------------*/

/* ----------------------------[private functions]---------------------------*/
int SerialWrite(int dev_fd, const char *buf, size_t count) {
	uint8_t iter;
	for (iter = 0; iter < (uint8_t)count; iter++)
	{
		while((AUX_MU_LSR_REG & 0x20) == 0);
		AUX_MU_IO_REG = *(buf + iter);
	}

	return (count);
}

void bcm2835InitDbg() {
	AUX_ENABLES = 1;			// Enable UART

	AUX_MU_IIR_REG  = 0xC6;			// Enable receive interrupt, disable transmit interrupt,
	AUX_MU_IER_REG  = 0x00;			// No interrupts, no pending
	AUX_MU_LCR_REG  = 0x03; 		// No DLB Access, No Break, 8-bit mode
	AUX_MU_MCR_REG  = 0x00;			// UART1_RTS line is high
	AUX_MU_CNTL_REG = 0x00;			// Nothing. Just to make sure that the pins are in their correct mode before turning uart on
	AUX_MU_BAUD_REG = ((BCM2835_CLOCK_FREQ / (8 * (115200))) - 1);

	bcm2835_GpioFnSel(14, GPFN_ALT5);
	bcm2835_GpioFnSel(15, GPFN_ALT5);

	GPPUD = 0;
	bcm2835_Sleep(150);
	GPPUDCLK0 = (1<<14)|(1<<15);
	bcm2835_Sleep(150);
	GPPUDCLK0 = 0;
	AUX_MU_CNTL_REG = 0x03;			// No flow control, enable transmit and receive
}
/* ----------------------------[public functions]----------------------------*/
static int bcm2835_Read( uint8_t *buffer, size_t nbytes ) {
	return 0;
}

static int bcm2835_Open( const char *path, int oflag, int mode ) {

	bcm2835InitDbg();
	return 0;
}

static int bcm2835_Write( uint8_t *buffer, size_t nbytes) {
	return SerialWrite(0, (char *)buffer, nbytes);
}

DeviceSerialType BCM2835_DBG_Device = {
	.device.type = DEVICE_TYPE_CONSOLE,
	.name = "serial_bcm2835",
	.read = bcm2835_Read,
	.write = bcm2835_Write,
	.open = bcm2835_Open,
};

