/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/* ----------------------------[information]----------------------------------*/
/*
 *
 * Description:
 *   Implements serial port for debugging on stm32f103
 */

/* ----------------------------[includes]------------------------------------*/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "Std_Types.h"
#include "MemMap.h"
#include "device_serial.h"
#include "sys/queue.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"

/* ----------------------------[private define]------------------------------*/
// STM32FX Reference manual page 185
#define USART1_GPIO         GPIOA
#define USART1_CLK          RCC_APB2Periph_USART1
#define USART1_GPIO_CLK     RCC_APB2Periph_GPIOA
#define USART1_RxPin        GPIO_Pin_10
#define USART1_TxPin 				GPIO_Pin_9

/* ----------------------------[private macro]-------------------------------*/

/* ----------------------------[private typedef]-----------------------------*/

/* ----------------------------[private function prototypes]-----------------*/

/* ----------------------------[private variables]---------------------------*/

/* ----------------------------[private functions]---------------------------*/
int SerialWrite(int dev_fd, const char *buf, size_t count) {
	uint8_t iter;
	for (iter = 0; iter < (uint8_t)count; iter++)
	{
    /* Send one byte via USART1 */
    USART_SendData(USART1, *(buf + iter));
    
    /* Loop until USART1 DR register is empty */ 
    while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET){}
	}

	return (count);
}

void stm32f103InitDbg() {
  /* Enable GPIO clock */
  RCC_APB2PeriphClockCmd(USART1_GPIO_CLK, ENABLE);

  /* Enable USART1 Clock */
  RCC_APB2PeriphClockCmd(USART1_CLK, ENABLE); 
	GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure USART1 Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = USART1_RxPin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(USART1_GPIO, &GPIO_InitStructure);
  
  /* Configure USART1 Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = USART1_TxPin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(USART1_GPIO, &GPIO_InitStructure);

	USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 19200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  /* Configure USART1 */
  USART_Init(USART1, &USART_InitStructure);

  /* Enable the USART1 */
  USART_Cmd(USART1, ENABLE);
}
/* ----------------------------[public functions]----------------------------*/
static int stm32f103_Read( uint8_t *buffer, size_t nbytes ) {
	return 0;
}

static int stm32f103_Open( const char *path, int oflag, int mode ) {

	stm32f103InitDbg();
	return 0;
}

static int stm32f103_Write( uint8_t *buffer, size_t nbytes) {
	return SerialWrite(0, (char *)buffer, nbytes);
}

DeviceSerialType STM32F103_DBG_Device = {
	.device.type = DEVICE_TYPE_CONSOLE,
	.name = "serial_stm32f103",
	.read = stm32f103_Read,
	.write = stm32f103_Write,
	.open = stm32f103_Open,
};

