#kernel
# CPU specific
ifeq ($(CFG_PPC),y)
obj-$(USE_KERNEL) += mpc5xxx_handlers.o
obj-$(USE_KERNEL) += mpc5xxx_handlers_asm.o
ifeq ($(filter os_mpu_mpc5516.o os_mpu_mpc5643l.o os_mpu_spc56xl70.,$(obj-y)),)
obj-$(USE_KERNEL)-$(CFG_MPC5516) += os_mpu_mpc5516.o
obj-$(USE_KERNEL)-$(CFG_MPC5643L) += os_mpu_mpc5643l.o
obj-$(USE_KERNEL)-$(CFG_SPC56XL70) += os_mpu_spc56xl70.o
endif
ifeq ($(filter mpc5xxx_callout_stubs.o,$(obj-y)),)
obj-$(USE_KERNEL) += mpc5xxx_callout_stubs.o
endif
endif

ifeq ($(CFG_ARM_CM3)$(CFG_ARM_CM4),y)
obj-$(USE_KERNEL) += arm_cortex_mx_handlers.o
ifeq ($(filter arm_cortex_mx_callout_stubs.o,$(obj-y)),)
obj-$(USE_KERNEL) += arm_cortex_mx_callout_stubs.o
endif
endif

obj-$(USE_KERNEL)-$(CFG_TC2XX) += tc2xx_trap_asm.o
obj-$(USE_KERNEL)-$(CFG_TC2XX) += tc2xx_trap.o