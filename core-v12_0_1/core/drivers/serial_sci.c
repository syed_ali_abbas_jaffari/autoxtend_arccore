/*/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 * 
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with  
 * the terms contained in the written license agreement between you and ArcCore, 
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as 
 * published by the Free Software Foundation and appearing in the file 
 * LICENSE.GPL included in the packaging of this file or here 
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/*
 * DESCRIPTION
 *   Uses the linFlex module that contains a UART.
 *   The HW features 4 RX and 4 TX buffers
 */

/* ----------------------------[includes]------------------------------------*/
#include <stdint.h>
#include "device_serial.h"
#include "sys/queue.h"
#include "MemMap.h"
#include "mpc55xx.h"
#include "Mcu.h"

/* ----------------------------[private define]------------------------------*/

#define UARTCR_UART 			0x00000001
#define UARTCR_TDFL(_size)		((_size)<<(31-18))
#define UARTCR_RDFL(_size)		((_size)<<(31-21))
#define UARTCR_RXEN				(1<<(31-26))
#define UARTCR_TXEN				(1<<(31-27))
#define UARTCR_WL_8BIT			(1<<(31-30))

/* ----------------------------[private macro]-------------------------------*/
/* ----------------------------[private typedef]-----------------------------*/
/* ----------------------------[private function prototypes]-----------------*/
/* ----------------------------[private variables]---------------------------*/

DeviceSerialType SCI_Device;

/* ----------------------------[private functions]---------------------------*/

static void SCI_Open( const uint8_t *data, size_t nbytes );
void SCI_Write( const uint8_t *data, size_t nbytes );
static int SCI_Read( uint8_t *data, size_t nbytes );

/* ----------------------------[public functions]----------------------------*/


/* IMPROVMENT: change interface? */
void SCI_Init( uint32_t baud ) {
	uint32_t pClk;

	/* Set to init mode (NO, were are not initialized after this ) */
	LINFlexD_0.LINCR1.R = 1;

	/* Set to UART mode, FIFO=4 for both RX, TX */
	LINFlexD_0.UARTCR.R =   (1UL<<0U);          /* Enable UART mode */
	// 0x0233;* Enable UART mode */
	LINFlexD_0.UARTCR.R = ( (1UL<<(31U-11U)) |  /* */
	                        (1UL<<(31U-11U)) |  /* NEF - 1 */
	                        (1UL<<(31U-12U)) |  /* DTU_PCETX */
	                        (0UL<<(31U-13U)) |  /* SBUR       00- 1 stop bit*/
	                        (1UL<<(31U-18U)) |  /* TDFL_TFC - */
	                        (1UL<<(31U-26U)) |  /* 1 - Receiver enabled */
	                        (1UL<<(31U-27U)) |  /* 1 - Transmitter enabled */
	                        (0UL<<(31U-29U)) |  /* 0 - Parity disabled */
	                        (1UL<<(31U-30U)) |  /* 1 - word length = 8 */
	                        UARTCR_WL_8BIT);

	BDRL

	/**
	 * Baud = Fper / ( 16 *LFDIV )
	 *
	 */
#if 1
	// 57600
	LINFlexD_0.LINIBRR.B.DIV_M = 69;
	LINFlexD_0.LINFBRR.B.DIV_F = 7;
#else
	// 115200
	LINFlexD_0.LINIBRR.B.DIV_M = 34;
	LINFlexD_0.LINFBRR.B.DIV_F = 12;
#endif

	/* Set to normal mode */
	LINFlexD_0.LINCR1.R = 0;

	pClk = Mcu_Arc_GetPeripheralClock(PERIPHERAL_CLOCK_LIN_A);


	/* PB[2] = PCR[18] = TXD0 , Func 1 */
	/* PB[3] = PCR[19] = RXD0 , Func 1 */

	/* Move to board */
	SIU.PCR[18].R = 0x0400;         /* MPC56xxS: Configure port B2 as LIN0TX */
	SIU.PCR[19].R = 0x0503;         /* MPC56xxS: Configure port B3 as LIN0RX */

//	Device_RegisterSerial(&SCI_Device);

	// Send out nothing to skip "states" in code */
	WRITE8(0xffe40000UL+0x3b,' ');
}

/**
 * Blocking write to serial port
 */
void SCI_Write( const uint8_t *data, size_t nbytes ) {
	int i = 0;

	while(nbytes--) {
		// Check DTF
		while( (READ32(0xffe40000UL+0x14) & (1<<(31-30))) == 0 ) {};

		WRITE32( (0xffe40000UL+0x14), (1<<(31-30)));
		WRITE32((0xffe40000UL+0x38),data[i++]);
	}

}

static int SCI_Read( uint8_t *data, size_t nbytes ) {


	//if( UARTCR.B.RX)
	return 0;
}

