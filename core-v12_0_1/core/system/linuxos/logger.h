/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef _LOGGER_H
#define _LOGGER_H


// Use Syslog ?
#ifndef _WIN32
#define USE_SYSLOG
#endif
//


// Log priorities 
//
// If NOT using syslog then hard define them here.
// Taken from /usr/include/syslog.h
#ifdef USE_SYSLOG
#include <syslog.h>
#else

#define LOG_EMERG       0       /* system is unusable */
#define LOG_ALERT       1       /* action must be taken immediately */
#define LOG_CRIT        2       /* critical conditions */
#define LOG_ERR         3       /* error conditions */
#define LOG_WARNING     4       /* warning conditions */
#define LOG_NOTICE      5       /* normal but significant condition */
#define LOG_INFO        6       /* informational */
#define LOG_DEBUG       7       /* debug-level messages */

#endif
//

// Declare functions

void logger_open_close ( int open_close, char *progname );

void logger_set_prefix(char *prefix);

int logger_set_output ( int bitMask );

void logger ( int loglevel, char *format, ... );

char* logger_format_hex( char* s, int slength);

#endif
