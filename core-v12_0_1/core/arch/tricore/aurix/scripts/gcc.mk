
# prefered version
CC_VERSION=4.8.2
SELECT_CLIB?=CLIB_DNK
ifeq ($(SELECT_CLIB),CLIB_NEWLIB)
CFG_CLIB_NEWLIB=y
else
CFG_CLIB_DNK=y
endif
#SELECT_CLIB=CLIB_DNK


cflags-y += -DSELECT_CLIB=$(SELECT_CLIB)

# vfpv3-d16

# ARMv7, Thumb-2, little endian, soft-float.
# -mthumb 
 
#cflags-$(CFG_HW_FLOAT)	+= -mfloat-abi=hard -mfpu=vfpv3
#cflags-$(CFG_SOFT_FLOAT)+= -mfloat-abi=softfp -mfpu=vfpv3
#cflags-$(CFG_THUMB) += -mthumb 

#def-y += _ALLOW_KEYWORD_MACROS 

#inc-y += $(dir $(CROSS_COMPILE))../tricore/include/machine
#inc-y += $(dir $(CROSS_COMPILE))../lib/gcc/tricore/4.6.4/include
#inc-y += $(dir $(ROOTDIR))../vcc-asdm/ASDM_Application/integration
inc-y += $(dir $(CROSS_COMPILE))../tricore/include/machine
inc-y += $(dir $(CROSS_COMPILE))../lib/gcc/tricore/4.6.4/include
inc-y += $(dir $(ROOTDIR))../vcc-asdm/ASDM_Application/integration
inc-y += $(dir $(CROSS_COMPILE))../bsp/tricore/TriBoard-TC29xB/h
#cc_inc_path

libpath-y += -L$(dir $(CROSS_COMPILE))../tricore/lib/

#$(error $(apa-y))
#cflags-y 	+= -march=armv7-a 
#cflags-y 	+= -ggdb
#cflags-y 	+= -mtpcs-frame


#cflags-y += -mtc161
#cflags-y += -mcpu=tc29xx

cflags-y += -mtc161
cflags-y += -fdata-sections
cflags-y += -finline-is-always-inline
lib-$(CFG_CLIB_NEWLIB) 	+= -lgcc
lib-$(CFG_CLIB_NEWLIB) 	+= -lc
lib-$(CFG_CLIB_NEWLIB) 	+= -lm

def-$(CFG_CLIB_DNK) += _ALLOW_KEYWORD_MACROS

lib-$(CFG_CLIB_DNK) += -ldnk_c

lib-y   	+= -lgcc
asflags += -mtc161 
asflags += --gdwarf-2
#asflags-$(CFG_THUMB) +=  -mthumb
#asflags-$(CFG_HW_FLOAT)	+= -mfloat-abi=hard -mfpu=vfpv3

#ASFLAGS 	+= $(asflags-y) -march=armv7-a
ASFLAGS += $(asflags-y)

libpath-y += -L/C/DEVTOOLS/HIGHTEC/toolchains/tricore/v4.6.5.0/tricore/lib/tc161
LDFLAGS     += --allow-multiple-definition --cref --oformat=elf32-tricore --mcpu=tc161 --mem-holes --extmap="a"

