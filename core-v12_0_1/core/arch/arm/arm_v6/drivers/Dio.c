/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * Dio.c
 *
 *  Created on: May 17, 2016
 *     Author: Ali Syed
 *
 */

/** @tagSettings DEFAULT_ARCHITECTURE=BCM2835 */

/** @req SWS_Dio_00129  PRE-COMPILE is supported */
/** @req SWS_Dio_00128  A general-purpose digital IO pin represents a DIO channel */
/** @req SWS_Dio_00063 The Dio module shall adapt its configuration and usage to the microcontroller and ECU */
/** @req SWS_DIO_00065 Dio module shall detect errors and exceptions depending on its build version */

#include "Std_Types.h"
#include "Dio.h"
#include "bcm2835.h"
/**@req SWS_Dio_00194 */
#if defined(USE_DET)
#include "Det.h"
#endif

typedef struct {
	const Dio_ConfigType* Config;
	boolean InitRun;
}Dio_GlobalType;

static Dio_GlobalType DioGlobal = {.InitRun = FALSE, .Config = NULL};

#if ( DIO_DEV_ERROR_DETECT == STD_ON )
static uint8 Channel_Config_Contains(Dio_ChannelType channelId)
{
	const Dio_ChannelType* ch_ptr = DioGlobal.Config->ChannelConfig;
	uint8 rv = 0;
	while (DIO_END_OF_LIST != *ch_ptr) {
		if (*ch_ptr == channelId) {
			rv = 1;
			break;
		}
		ch_ptr++;
	}
	return rv;
}

static uint32 Port_Config_Contains(Dio_PortType portId)
{
  const Dio_PortType* port_ptr=DioGlobal.Config->PortConfig;
  uint32 rv = 0u;
  while (DIO_END_OF_LIST!=*port_ptr)
  {
    if (*port_ptr==portId)
    {
    	rv = 1u;
    	break;
    }
    port_ptr++;
  }
  return rv;
}

#define IS_VALID_CHANNEL(_x) (0 != Channel_Config_Contains(_x))

/** @req SWS_Dio_00075 */
#define VALIDATE_PORT_RV(_portId, _api, _rv)\
    if(0u == Port_Config_Contains(_portId)) {\
        (void)Det_ReportError(DIO_MODULE_ID,0,_api,DIO_E_PARAM_INVALID_PORT_ID ); \
        return _rv;\
    }
#define VALIDATE_PORT(_portId, _api)\
    if(0u == Port_Config_Contains(_portId)) {\
        (void)Det_ReportError(DIO_MODULE_ID,0,_api,DIO_E_PARAM_INVALID_PORT_ID ); \
        return;\
    }
#define VALIDATE(_exp,_api,_err ) \
        if( !(_exp) ) { \
          Det_ReportError(DIO_MODULE_ID,0,_api,_err); \
          return; \
        }
#define VALIDATE_RV(_exp,_api,_err, _rv ) \
        if( !(_exp) ) { \
          Det_ReportError(DIO_MODULE_ID,0,_api,_err); \
          return _rv; \
        }
#else
#define IS_VALID_CHANNEL(_x)
#define VALIDATE_PORT_RV(_portId, _api, _rv)
#define VALIDATE_PORT(_portId, _api)
#define VALIDATE(_exp,_api,_err )
#define VALIDATE_RV(_exp,_api,_err,_rv )
#endif

/**
 *
 * @param channelId
 * @return
 */

/* @req SWS_Dio_00012 */ /* @req SWS_Dio_00051*/
Dio_LevelType Dio_ReadChannel(Dio_ChannelType channelId)
{
	Dio_LevelType level;
	/** @req SWS_Dio_00118 */
	VALIDATE_RV( DioGlobal.InitRun, DIO_READCHANNEL_ID, DIO_E_UNINIT, (Dio_LevelType)0 );
	/** @req SWS_Dio_00074 */ /** @req SWS_Dio_00118 */
	VALIDATE_RV( IS_VALID_CHANNEL(channelId), DIO_READCHANNEL_ID, DIO_E_PARAM_INVALID_CHANNEL_ID, (Dio_LevelType)0 );
	/** @req SWS_Dio_00011 */
	if( bcm2835_ReadGpioPin(channelId) ) {
		level = STD_HIGH;
	} else {
		level = STD_LOW;
	}
	return level; /** @req SWS_Dio_00027 */
}

/**
 *
 * @param channelId
 * @param level
 */
/* @req SWS_Dio_00051 */
void Dio_WriteChannel(Dio_ChannelType channelId, Dio_LevelType level)
{   /** @req SWS_Dio_00119 */
	VALIDATE( DioGlobal.InitRun, DIO_WRITECHANNEL_ID, DIO_E_UNINIT );
	/** @req SWS_Dio_00074 */ /** @req SWS_Dio_00119 */
	VALIDATE( IS_VALID_CHANNEL(channelId), DIO_WRITECHANNEL_ID, DIO_E_PARAM_INVALID_CHANNEL_ID);
	/** @req SWS_Dio_00089 */ /** @req SWS_Dio_00006 */
	/**  !req SWS_Dio_00028 */ /**  !req SWS_Dio_00029 */
	if( level == STD_HIGH ) {
		bcm2835_SetPinInGpioReg(&GPSET0, channelId);
	} else if(level == STD_LOW ) {
		bcm2835_SetPinInGpioReg(&GPCLR0, channelId);
	}else{
		// do nothing
	}

	return;
}

/** @req SWS_Dio_00053 */
/** @req SWS_Dio_00031 */
/** @req SWS_Dio_00013 */
/** @!req SWS_Dio_00104 Undefined port pins are not set to 0 (for arch < 16bit) */
Dio_PortLevelType Dio_ReadPort(Dio_PortType portId)
{
	VALIDATE_RV( DioGlobal.InitRun, DIO_READPORT_ID, DIO_E_UNINIT, (Dio_PortLevelType)0 );
	VALIDATE_PORT_RV(portId, DIO_READPORT_ID, level);

	return (Dio_PortLevelType) bcm2835_ReadGpioPort(portId);
}

/** @req SWS_Dio_00034 */
/** @req SWS_Dio_00053 */
/** @req SWS_Dio_00007 */
/** @!req SWS_Dio_00004 No check if channels of port is set as input */
/** @!req SWS_Dio_00035 No check if channels of port is set as input */
/** @req SWS_Dio_00105 Note: No check for endianess */
void Dio_WritePort(Dio_PortType portId, Dio_PortLevelType level)
{
    VALIDATE( DioGlobal.InitRun, DIO_WRITEPORT_ID, DIO_E_UNINIT );
    VALIDATE_PORT(portId, DIO_WRITEPORT_ID);

	bcm2835_WriteGpioPort(portId, level);
}
/**
 *
 * @param ConfigPtr
 */
void Dio_Init(const Dio_ConfigType* ConfigPtr)
{
    /** @req SWS_Dio_00167 **/
    /** @req SWS_Dio_00166 **/
    VALIDATE( (NULL != ConfigPtr), DIO_INIT_ID, DIO_E_PARAM_CONFIG );
    DioGlobal.Config = ConfigPtr;
    DioGlobal.InitRun = TRUE;
}


#if (DIO_VERSION_INFO_API == STD_ON)
void Dio_GetVersionInfo(Std_VersionInfoType* versioninfo)
{
    VALIDATE( DioGlobal.InitRun, DIO_GETVERSIONINFO_ID, DIO_E_UNINIT );
    /** @req SWS_Dio_00189 **/
    VALIDATE( !(versioninfo == NULL), DIO_GETVERSIONINFO_ID, DIO_E_PARAM_POINTER );

    versioninfo->vendorID = DIO_VENDOR_ID;
    versioninfo->moduleID = DIO_MODULE_ID;
    versioninfo->sw_major_version = DIO_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = DIO_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = DIO_SW_PATCH_VERSION;

}

#endif

void Dio_Arc_Deinit(void)
{
    DioGlobal.Config = NULL;
    DioGlobal.InitRun = FALSE;
}

