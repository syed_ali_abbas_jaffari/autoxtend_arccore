/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * Lin.c
 *
 *  Created on: July 6, 2017
 *     Author: Ali Syed
 *
 *  Comment: LIN bus function mock (i.e. does not use LIN protocol). Designed for BCM2835 (Raspberry Pi)
 *  Frame is designed to be able to use with any uart device without lin stack
 *  Frame format: <PID>, <DataByte1>, <DataByte2>, <DataByte3>, <DataByte4>, <DataByte5>, <DataByte6>, <DataByte7>, <DataByte8>
 *  Note: 	- Number of data bytes are defined by the pdu data size.
 *  		- No break/sync field, no checksum, no slave to slave, no sleep, no wakeup, only 1 channel (i.e. mini uart0) supported (the variables are properly sized though for expansion)
 *  TODO: Check reception of PDUs
 */

#include "Lin.h"
#if defined(USE_DET)
#include "Det.h"
#endif
#include "SchM_Lin.h"
#include "isr.h"
#include "irq.h"
#include "bcm2835.h"

#define LIN_MAX_MSG_LENGTH 8
#define LIN_PRIO 3				// Smaller number has lower priority

/* Reset -> LIN_UNINIT: After reset, the Lin module shall set its state to LIN_UNINIT. */
static Lin_DriverStatusType Lin_DrivStat = LIN_UNINIT;
static Lin_StatusType Lin_ChanStat[LIN_CONTROLLER_CNT];
static const Lin_ConfigType* Lin_CfgPtr = NULL;
/* static buffers, holds one frame at a time */
static uint8 Lin_BufTxRx[LIN_CONTROLLER_CNT][LIN_MAX_MSG_LENGTH];
static uint8  Lin_TxRxSize[LIN_CONTROLLER_CNT];

/* Development error macros. */
#if ( LIN_DEV_ERROR_DETECT == STD_ON )
#define VALIDATE(_exp,_api,_err ) \
    if( !(_exp) ) { \
    (void)Det_ReportError(LIN_MODULE_ID,0,_api,_err); \
    return; \
    }

#define VALIDATE_W_RV(_exp,_api,_err,_rv ) \
    if( !(_exp) ) { \
    (void)Det_ReportError(LIN_MODULE_ID,0,_api,_err); \
    return (_rv); \
    }

#define VALIDATE_PTR_W_RV(_exp,_rv ) \
    if( !(_exp) ) { \
    return (_rv); \
    }
#else
#define VALIDATE(_exp,_api,_err )
#define VALIDATE_W_RV(_exp,_api,_err,_rv )
#define VALIDATE_PTR_W_RV(_exp,_rv )
#endif

/**
 * Handles the UART interrupt routine
 * The function will only be actived on reception
 * @param hwId
 */
void Lin_Interrupt(void) {
	//static uint8 speed = 0;
	bcm2835_SetPinInGpioReg(&GPSET0, 27);
	// Assuming that no SPI1 or SPI2 interrupt is enabled
    static uint8 byteNr[LIN_CONTROLLER_CNT] = {0};
    uint8 ch = Lin_CfgPtr->Lin_HwId2ChannelMap[0];

    byteNr[ch] = Lin_TxRxSize[ch];

    /* Determine if we are going to receive (to make sure) */
    if (LIN_RX_BUSY == Lin_ChanStat[ch]) {
        while (byteNr[ch] > 0) {
            while ( (AUX_MU_LSR_REG & 0x01) != 0x01 ) {}			// Wait until receiver holds a valid byte
            Lin_BufTxRx[ch][byteNr[ch]] = (uint8)AUX_MU_IO_REG;
            byteNr[ch]--;
        }
        Lin_ChanStat[ch] = LIN_RX_OK;
    }
	// Wait for the transmitter to be ready
	/*while ( !(AUX_MU_LSR_REG & 0x20) ) {}
	AUX_MU_IO_REG = 0x05;
    while ( !(AUX_MU_LSR_REG & 0x20) ) {}
    AUX_MU_IO_REG = Lin_BufTxRx[ch][1];
    while ( !(AUX_MU_LSR_REG & 0x20) ) {}
    AUX_MU_IO_REG = speed;
	speed++;
    while ( !(AUX_MU_LSR_REG & 0x20) ) {}
    AUX_MU_IO_REG = 0x07;*/
	bcm2835_SetPinInGpioReg(&GPCLR0, 27);
	/* Clear interrupt flags */
    AUX_MU_IIR_REG  = 0xC6;
}

/**
 * Initializes the module
 * @param Config
 */
void Lin_Init(const Lin_ConfigType* Config) {
    VALIDATE( (Lin_DrivStat == LIN_UNINIT), LIN_INIT_SERVICE_ID, LIN_E_STATE_TRANSITION );
    VALIDATE( (Config!=NULL), LIN_INIT_SERVICE_ID, LIN_E_INVALID_POINTER );

	bcm2835_GpioFnSel(18, GPFN_OUT);
	bcm2835_GpioFnSel(27, GPFN_OUT);
	bcm2835_SetPinInGpioReg(&GPSET0, 18);

    Lin_CfgPtr = Config;
    for (uint8 i = 0; i < LIN_CONTROLLER_CNT; i++) {
        Lin_TxRxSize[i] = 0;
    }

    /* Install the interrupt */
    ISR_INSTALL_ISR2("LinIsr", Lin_Interrupt, BCM2835_IRQ_AUX, LIN_PRIO, 0);
    for (uint8 Channel = 0; Channel < LIN_CONTROLLER_CNT; Channel++) {
        AUX_ENABLES = 1;			// Enable UART
        AUX_MU_IER_REG  = 0x00;			// Enable receive interrupt, disable transmit interrupt
        AUX_MU_CNTL_REG = 0x00;				// No CTS/RTS or flow control, disable TX and RX
        AUX_MU_LCR_REG  = 0x03; 		// No DLB Access, No Break, 8-bit mode
        AUX_MU_MCR_REG  = 0x00;			// UART1_RTS line is high
        AUX_MU_IIR_REG  = 0x00;			// clear any pending interrupt flags
        AUX_MU_IER_REG  = 0x05;			// Enable receive interrupt, disable transmit interrupt
        AUX_MU_IIR_REG  = 0xC6;			// Enable receive interrupt, disable transmit interrupt
		AUX_MU_BAUD_REG = (((uint32)BCM2835_CLOCK_FREQ / (8 * (uint32)Lin_CfgPtr->LinChannelConfig[Channel].LinChannelBaudRate)) - 1);
        bcm2835_GpioFnSel(14, GPFN_ALT5);
        bcm2835_GpioFnSel(15, GPFN_ALT5);

        GPPUD = 0;
        bcm2835_Sleep(150);
        GPPUDCLK0 = (1<<14)|(1<<15);
        bcm2835_Sleep(150);
        GPPUDCLK0 = 0;
		//IRQ_ENABLE1 = 1 << 29;
        AUX_MU_CNTL_REG = 0x03;				// No CTS/RTS or flow control, enable TX and RX
        Lin_ChanStat[Channel] = LIN_OPERATIONAL;
    }

    /* LIN_UNINIT -> LIN_INIT: The Lin module shall transition from LIN_UNINIT
     * to LIN_INIT when the function Lin_Init is called. */
    Lin_DrivStat = LIN_INIT;
}

/**
 * De-initializes the module
 */
void Lin_Arc_DeInit(void){
    VALIDATE( (Lin_DrivStat != LIN_UNINIT), LIN_ARC_DEINIT_SERVICE_ID, LIN_E_UNINIT);

    Lin_DrivStat = LIN_UNINIT;
    AUX_MU_CNTL_REG = 0x00;				// No CTS/RTS or flow control, disable TX and RX
}

/**
 * Sends the header on the bus and depending of direction it sets or listens to response data
 * @param Channel
 * @param PduInfoPtr
 * @return
 */
Std_ReturnType Lin_SendFrame(uint8 Channel, Lin_PduType* PduInfoPtr) /*lint -e{818} Follows AUTOSAR API spec */
{
	//static uint8 byteNr = 0;
	//static uint8 speed = 0;
    uint8 i;

    VALIDATE_W_RV( (Channel < LIN_CONTROLLER_CNT), LIN_SEND_FRAME_SERVICE_ID, LIN_E_INVALID_CHANNEL, E_NOT_OK);
    VALIDATE_W_RV( (PduInfoPtr != NULL), LIN_SEND_FRAME_SERVICE_ID, LIN_E_PARAM_POINTER, E_NOT_OK);

    SchM_Enter_Lin_EA_0();
    if( (Lin_ChanStat[Channel] == LIN_TX_BUSY) || (Lin_ChanStat[Channel] == LIN_RX_BUSY) ) {
        while( !(AUX_MU_LSR_REG & 0x20) ) {}  // Make sure that transmitter is idle
        Lin_ChanStat[Channel] = LIN_OPERATIONAL;
    }
    SchM_Exit_Lin_EA_0();

    /* Set data for PID transmission */
    AUX_MU_IO_REG = PduInfoPtr->Pid;

    if (PduInfoPtr->Drc == LIN_MASTER_RESPONSE) {
        // Send complete frame without involving interrupt
        Lin_ChanStat[Channel] = LIN_TX_BUSY;
        if (PduInfoPtr->DI > 0){
            for (i = 0; i < PduInfoPtr->DI; i++) {
                // Wait for the transmitter to be ready
                while ( !(AUX_MU_LSR_REG & 0x20) ) {}
                // Send data byte
                AUX_MU_IO_REG = PduInfoPtr->SduPtr[i];
            }
            Lin_ChanStat[Channel] = LIN_TX_OK;
        }
    } else {
        // Set variables to be used by interrupt handler
        Lin_ChanStat[Channel] = LIN_RX_BUSY;
        Lin_TxRxSize[Channel] = PduInfoPtr->DI;
		/*byteNr = Lin_TxRxSize[Channel];
	    while (byteNr > 0) {
	        while ( (AUX_MU_LSR_REG & 0x01) != 0x01 ) {}			// Wait until receiver holds a valid byte
	        Lin_BufTxRx[Channel][byteNr] = (uint8)AUX_MU_IO_REG;
	        byteNr--;
	    }
		// Wait for the transmitter to be ready
		while ( !(AUX_MU_LSR_REG & 0x20) ) {}
		AUX_MU_IO_REG = 0x05;
		while ( !(AUX_MU_LSR_REG & 0x20) ) {}
		AUX_MU_IO_REG = Lin_BufTxRx[Channel][1]; //(uint8)( (uint16)(Lin_BufTxRx[Channel][0] << 8 | Lin_BufTxRx[Channel][1]) >> 2);
		while ( !(AUX_MU_LSR_REG & 0x20) ) {}
		AUX_MU_IO_REG = speed;//(uint8)( (uint16)(Lin_BufTxRx[Channel][0] << 8 | Lin_BufTxRx[Channel][1]) >> 3);
		speed++;
		while ( !(AUX_MU_LSR_REG & 0x20) ) {}
		AUX_MU_IO_REG = 0x07;
	    Lin_ChanStat[Channel] = LIN_RX_OK;*/
    }
    return E_OK ;
}

/**
 * Sends the GoToSleep command on the bus and sets the internal module state to sleep pending.
 * @param Channel
 * @return
 */
Std_ReturnType Lin_GoToSleep(uint8 Channel)
{
    // Never sleep
    return E_OK;
}

/**
 * Sets the internal module state to sleep
 * @param Channel
 * @return
 */
Std_ReturnType Lin_GoToSleepInternal(uint8 Channel)
{
    // Never sleep
    return E_OK;
}

/**
 * Generates a wake up pulse and sets the channel state operational
 * @param Channel
 * @return
 */
Std_ReturnType Lin_Wakeup( uint8 Channel )
{
    // Since never sleep, never wakeup
    return E_OK;
}

/**
 * Sets the channel state to operational without generating a wakeup pulse.
 * @param Channel
 * @return
 */
Std_ReturnType   Lin_WakeupInternal(uint8 Channel) {
    // Since never sleep, never wakeup
    return E_OK;
}

/**
 * Checks if the wakeup source have caused the wakeup. If it has it will
 * call EcuM_SetWakeupEvent.
 * @param WakeupSource
 * @return
 */
Std_ReturnType Lin_CheckWakeup(uint8 Channel) {
    // Since never sleep, never wakeup
    return E_OK;
}

/**
 * Gets the status of the LIN driver.
 * @param Channel
 * @param Lin_SduPtr
 * @return
 */
Lin_StatusType Lin_GetStatus( uint8 Channel, uint8** Lin_SduPtr ){

    VALIDATE_W_RV( (Channel < LIN_CONTROLLER_CNT), LIN_GETSTATUS_SERVICE_ID, LIN_E_INVALID_CHANNEL, LIN_NOT_OK);
    VALIDATE_W_RV( (Lin_SduPtr!=NULL), LIN_GETSTATUS_SERVICE_ID, LIN_E_PARAM_POINTER, LIN_NOT_OK);

    SchM_Enter_Lin_EA_0();
    Lin_StatusType res = Lin_ChanStat[Channel];

    switch (res) {
    case LIN_RX_OK:
        *Lin_SduPtr = Lin_BufTxRx[Channel];
        Lin_ChanStat[Channel]=LIN_OPERATIONAL;
        break;
    case LIN_TX_OK:
        Lin_ChanStat[Channel]=LIN_OPERATIONAL;
        break;
    case LIN_RX_ERROR:
    case LIN_TX_ERROR:
    case LIN_RX_NO_RESPONSE:
    case LIN_TX_HEADER_ERROR:
    case LIN_CH_SLEEP_PENDING:
    default:
        break;
    }

    SchM_Exit_Lin_EA_0();
    return res;
}

#if (LIN_VERSION_INFO_API == STD_ON)
/**
 * Returns the version information of the module
 * @param versioninfo
 */
void Lin_GetVersionInfo(Std_VersionInfoType* versioninfo)
{
    VALIDATE( !(versioninfo == NULL), LIN_GETVERSIONINFO_SERVICE_ID, LIN_E_PARAM_POINTER );

    versioninfo->vendorID = LIN_VENDOR_ID;
    versioninfo->moduleID = LIN_MODULE_ID;
    versioninfo->sw_major_version = LIN_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = LIN_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = LIN_SW_PATCH_VERSION;
    return;
}

#endif
