/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * Port.c
 *
 *  Created on: May 17, 2016
 *     Author: Ali Syed
 *
 *  Comment: Designed for BCM2835 (Raspberry Pi)
 */

/* General Port module requirements */
/** @req SWS_Port_00004 */
/** @req SWS_Port_00079 All pins are changeable during runtime */
/** @req SWS_Port_00081 */
/** @req SWS_Port_00082 */
/** @req SWS_Port_00124 */
/** @req SWS_Port_00013 Symbolic names */
/** @req SWS_Port_00073 */

/** @req SWS_Port_00006 Implemented in the generator/tool */
/** @req SWS_Port_00207 Implemented in the generator/tool */
/** @req SWS_Port_00076 Implemented in the generator */
/** @req SWS_Port_00006 */
/** @req SWS_Port_00075 Atomic access */
/** @req SWS_Port_00129 */
/** @req SWS_Port_00087 Not PORT_E_MODE_UNCHANGEABLE and PORT_E_DIRECTION_UNCHANGEABLE*/

/** @req SWS_Port_00204 Not including Port_Cbk.c*/
/** @req SWS_Port_00129 Additional types are included from other files */

#include "Port.h"
#include "bcm2835.h"
#if defined(USE_DET)
#include "Det.h"
#endif
#include "Cpu.h"
#include <string.h>
#include "SchM_Port.h"

#define NUM_OF_GPIO_PINS 54
#define FUNCTION_SELECT_MASK 0x03
#define RISING_EDGE_DETECT_ENABLE (1 << 3)
#define FALLING_EDGE_DETECT_ENABLE (1 << 4)
#define HIGH_LEVEL_DETECT_ENABLE (1 << 5)
#define LOW_LEVEL_DETECT_ENABLE (1 << 6)
#define ASYNC_RISING_EDGE_DETECT_ENABLE (1 << 5)
#define ASYNC_FALLING_EDGE_DETECT_ENABLE (1 << 5)
#define ENABLE_PULL_UP (1 << 9)
#define ENABLE_PULL_DOWN (1 << 10)

typedef enum {
    PORT_UNINITIALIZED = 0u, //!< PORT_UNINITIALIZED
    PORT_INITIALIZED,      //!< PORT_INITIALIZED
} Port_StateType;

static Port_StateType Port_state = PORT_UNINITIALIZED;
static const Port_ConfigType * Port_configPtr = NULL;
uint64_t pullUpPins = 0, pullDownPins = 0;

/* Set SFR registers of Port properly.*/
typedef volatile struct SLCR_reg Mcu_SlcrHwRegType;

#if defined(USE_DET)
#if (PORT_DEV_ERROR_DETECT)
#define VALIDATE_PARAM_CONFIG(_ptr,_api) \
    if( (_ptr)==((void *)0) ) { \
        (void)Det_ReportError(PORT_MODULE_ID, 0, _api, PORT_E_PARAM_CONFIG ); \
        return; \
    }

#define VALIDATE_STATE_INIT(_api)\
    if(PORT_INITIALIZED!=Port_state){\
        (void)Det_ReportError(PORT_MODULE_ID, 0, _api, PORT_E_UNINIT ); \
        return; \
    }

#define VALIDATE_PARAM_PIN(_pin, _api)\
    if(_pin>= NUM_OF_GPIO_PINS){\
        (void)Det_ReportError(PORT_MODULE_ID, 0, _api, PORT_E_PARAM_PIN ); \
        return; \
    }

#define VALIDATE_PARAM_POINTER(_pointer, _api)\
    if(_pointer == ((void *)0) ) {\
        (void)Det_ReportError(PORT_MODULE_ID, 0, _api, PORT_E_PARAM_POINTER ); \
        return; \
    }

#else
#define VALIDATE_PARAM_CONFIG(_ptr,_api)
#define VALIDATE_STATE_INIT(_api)
#define VALIDATE_PARAM_PIN(_pin, _api)
#define VALIDATE_PARAM_POINTER(_pointer,_api)
#endif
#else
#define VALIDATE_PARAM_CONFIG(_ptr,_api)
#define VALIDATE_STATE_INIT(_api)
#define VALIDATE_PARAM_PIN(_pin, _api)
#define VALIDATE_PARAM_POINTER(_pointer,_api)
#endif

void Internal_RefreshPin(uint8_t pin) {
	// Set port pin function
	bcm2835_GpioFnSel(Port_configPtr->PortConfig[pin].gpioPin, FUNCTION_SELECT_MASK & Port_configPtr->PortConfig[pin].config);

	// Set rising edge detection
	if (Port_configPtr->PortConfig[pin].config & RISING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPREN0, Port_configPtr->PortConfig[pin].gpioPin);

	// Set falling edge detection
	if (Port_configPtr->PortConfig[pin].config & FALLING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPFEN0, Port_configPtr->PortConfig[pin].gpioPin);

	// Set high level detection
	if (Port_configPtr->PortConfig[pin].config & HIGH_LEVEL_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPHEN0, Port_configPtr->PortConfig[pin].gpioPin);

	// Set low level detection
	if (Port_configPtr->PortConfig[pin].config & LOW_LEVEL_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPLEN0, Port_configPtr->PortConfig[pin].gpioPin);

	// Set async. rising edge detection
	if (Port_configPtr->PortConfig[pin].config & ASYNC_RISING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPAREN0, Port_configPtr->PortConfig[pin].gpioPin);

	// Set async. falling edge detection
	if (Port_configPtr->PortConfig[pin].config & ASYNC_FALLING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPAFEN0, Port_configPtr->PortConfig[pin].gpioPin);
}

void Internal_SetPull(uint8_t pullValue, uint64_t pinsMask) {
	GPPUD = pullValue;
	bcm2835_Sleep(150);
	GPPUDCLK0 = (uint32_t) (pinsMask & 0xFFFFFFFF);
	GPPUDCLK1 = (uint32_t) (pinsMask >> 32);
	bcm2835_Sleep(150);
	GPPUD = 0;
	GPPUDCLK0 = 0;
	GPPUDCLK1 = 0;
}


/** @req SWS_Port_00001 */
/** @req SWS_Port_00043 */
/** @req SWS_Port_00214 All I/O registers are initialized by port */
/** @req SWS_Port_00215 */
/** @req SWS_Port_00217 Only I/O registers are initialized by port */
/** @req SWS_Port_00218 Only I/O registers are initialized by port */
/** @req SWS_Port_00042 */
/** @req SWS_Port_00041 */
/** !req SWS_Port_00055 */
/** !req SWS_Port_00005 */

void Port_Init(const Port_ConfigType *config) {

    /* @req SWS_Port_00105 */
    VALIDATE_PARAM_CONFIG(config, PORT_INIT_ID);

    Port_configPtr = config;

    SchM_Enter_Port_EA_0();  // Disable interrupts

	// TODO: Optimize. Write only once to the registers, i.e. not in every iteration.
	for (uint8 pinIter = 0; config->PortConfig[pinIter].gpioPin != PORT_INVALID_REG; pinIter++)	{
		if (config->PortConfig[pinIter].gpioPin != GPIO16_PAD) {					// GPIO16_PAD is used by sys_tick.c
			Internal_RefreshPin(pinIter);
			// Update pull variables
			if (ENABLE_PULL_UP & config->PortConfig[pinIter].config) {
				pullUpPins |= (1 << config->PortConfig[pinIter].gpioPin);
			}
			else if (ENABLE_PULL_DOWN & config->PortConfig[pinIter].config) {		// 'Else' makes sure that none of the pins are both pull up and pull down
				pullDownPins |= (1 << config->PortConfig[pinIter].gpioPin);
			}
		}
	}

	// Set pull-downs
	Internal_SetPull(1, pullDownPins);

	// Set pull-ups
	Internal_SetPull(2, pullUpPins);

	// Set no pull up or down
	Internal_SetPull(0, ~(pullUpPins | pullDownPins));

    SchM_Exit_Port_EA_0();  // Enable interrupts

	// Set pin out data
	for (uint8 pinIter = 0; config->OutConfig[pinIter].gpioPin != PORT_INVALID_REG; pinIter++)	{
		if (config->OutConfig[pinIter].gpioPin != GPIO16_PAD) {					// GPIO16_PAD is used by sys_tick.c
			if (config->OutConfig[pinIter].level == 0)
				bcm2835_SetPinInGpioReg(&GPCLR0, config->OutConfig[pinIter].gpioPin);
			else
				bcm2835_SetPinInGpioReg(&GPSET0, config->OutConfig[pinIter].gpioPin);
		}
	}	

    // @req SWS_Port_00002
    Port_state = PORT_INITIALIZED;
}

#if ( PORT_SET_PIN_DIRECTION_API == STD_ON )
/** @req SWS_Port_00138 */
/** @req SWS_Port_00054 */
void Port_SetPinDirection( Port_PinType pin, Port_PinDirectionType direction )
{
    VALIDATE_STATE_INIT(PORT_SET_PIN_DIRECTION_ID);
    VALIDATE_PARAM_PIN(pin, PORT_SET_PIN_DIRECTION_ID);

   	// @req SWS_Port_00063
	if (direction==PORT_PIN_IN) {
		bcm2835_GpioFnSel(pin, GPFN_IN);
	}
	else {
		bcm2835_GpioFnSel(pin, GPFN_OUT);
	}
}
#endif

/** !req SWS_Port_00066 */
void Port_RefreshPortDirection(void) {
    VALIDATE_STATE_INIT(PORT_REFRESH_PORT_DIRECTION_ID);

    SchM_Enter_Port_EA_0();  // Disable interrupts

	// TODO: Optimize. Write only once to the registers, i.e. not in every iteration.
	for (uint8 pinIter = 0; Port_configPtr->PortConfig[pinIter].gpioPin != PORT_INVALID_REG; pinIter++)	{
		if (Port_configPtr->PortConfig[pinIter].gpioPin != GPIO16_PAD) {				// GPIO16_PAD is used by sys_tick.c
			Internal_RefreshPin(pinIter);
		}
	}

	// Set pull-downs
	Internal_SetPull(1, pullDownPins);

	// Set pull-ups
	Internal_SetPull(2, pullUpPins);

	// Set No pull up or down
	Internal_SetPull(0, ~(pullUpPins | pullDownPins));

    SchM_Exit_Port_EA_0();  // Enable interrupts
}

#if PORT_VERSION_INFO_API == STD_ON
/** @req SWS_Port_00225 */
/** @req SWS_Port_00087 */

void Port_GetVersionInfo(Std_VersionInfoType* versioninfo)
{
    VALIDATE_PARAM_POINTER(versioninfo, PORT_GET_VERSION_INFO_ID);
    versioninfo->vendorID = PORT_VENDOR_ID;
    versioninfo->moduleID = PORT_MODULE_ID;
    versioninfo->sw_major_version = PORT_SW_MAJOR_VERSION;
    versioninfo->sw_minor_version = PORT_SW_MINOR_VERSION;
    versioninfo->sw_patch_version = PORT_SW_PATCH_VERSION;
}
#endif

#if (PORT_SET_PIN_MODE_API == STD_ON)
/** @req SWS_Port_00128 */
/** !req SWS_Port_00077*/
/** @req SWS_Port_00212 */
/** @req SWS_Port_00087 PORT_E_MODE_UNCHANGEABLE and PORT_E_PARAM_INVALID_MODE not available */
void Port_SetPinMode(Port_PinType Pin, Port_PinModeType Mode)
{
    // @req SWS_Port_00125
    VALIDATE_STATE_INIT(PORT_SET_PIN_MODE_ID);
    VALIDATE_PARAM_PIN(Pin, PORT_SET_PIN_MODE_ID);

    SchM_Enter_Port_EA_0();  // Disable interrupts

	// Set port pin function
	bcm2835_GpioFnSel(Pin, FUNCTION_SELECT_MASK & Mode);

	// Set rising edge detection
	if (Mode & RISING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPREN0, Pin);

	// Set falling edge detection
	if (Mode & FALLING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPFEN0, Pin);

	// Set high level detection
	if (Mode & HIGH_LEVEL_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPHEN0, Pin);

	// Set low level detection
	if (Mode & LOW_LEVEL_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPLEN0, Pin);

	// Set async. rising edge detection
	if (Mode & ASYNC_RISING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPAREN0, Pin);

	// Set async. falling edge detection
	if (Mode & ASYNC_FALLING_EDGE_DETECT_ENABLE)
		bcm2835_SetPinInGpioReg(&GPAFEN0, Pin);

	if (ENABLE_PULL_UP & Mode) {
		Internal_SetPull(2, pullUpPins & (1 << Pin));
	} else if (ENABLE_PULL_DOWN & Mode) {
		Internal_SetPull(1, pullDownPins & (1 << Pin));
	} else {
		Internal_SetPull(0, (1 << Pin));
	}

    SchM_Exit_Port_EA_0();  // Enable interrupts
}
#endif
