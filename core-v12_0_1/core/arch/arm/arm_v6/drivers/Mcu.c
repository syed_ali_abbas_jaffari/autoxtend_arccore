/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * Mcu.c
 *
 *  Created on: May 17, 2016
 *     Author: Ali Syed
 *
 *  Comment: Designed for BCM2835 (Raspberry Pi)
 */

#include <string.h>
#include "Std_Types.h"
#include "SchM_Mcu.h"
#include "Mcu.h"
#include "Mcu_Arc.h"
#if defined(USE_DET)
#include "Det.h"
#endif
#if defined(USE_DEM)
#include "Dem.h"
#endif
#include "Cpu.h"
#include "io.h"
#if defined(USE_KERNEL)
#include "irq.h"
#endif
#if defined(USE_TTY_SERIAL_BCM2835)
#include "device_serial.h"
#include "serial_dbg_bcm2835.h"
void bcm2835InitDbg();
#endif

/* Global requirements */
/* !req SWS_Mcu_00130 */
/* @req SWS_Mcu_00237 Mcu_ModeType specifies the identification mode */
/* @req SWS_Mcu_00239 Mcu_RamSectionType specifies the identification of ram section */
/* @req SWS_Mcu_00232 Mcu_ClockType defines the identification of clock type*/
/* @req SWS_Mcu_00226 Production Errors shall not be used as the return value of the called function */

#include "bcm2835.h"

/* ----------------------------[private define]------------------------------*/
/* ----------------------------[private macro]-------------------------------*/

/* Development error macros. */
/* @req SWS_Mcu_00163 */
#if ( MCU_DEV_ERROR_DETECT == STD_ON )

/* Sanity check */
#if !defined(USE_DET)
#error DET not configured when MCU_DEV_ERROR_DETECT is set
#endif

#define VALIDATE(_exp,_api,_err ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(MCU_MODULE_ID,0,_api,_err); \
          return; \
        }

#define VALIDATE_W_RV(_exp,_api,_err,_rv ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(MCU_MODULE_ID,0,_api,_err); \
          return (_rv); \
        }
#else
#define VALIDATE(_exp,_api,_err )
#define VALIDATE_W_RV(_exp,_api,_err,_rv )
#endif

/* ----------------------------[private typedef]-----------------------------*/

/**
 * Type that holds all global data for Mcu
 */
typedef struct
{
	boolean initRun;		// Set if Mcu_Init() has been called
	const Mcu_ConfigType *config;	// Saved pointer to the configuration from Mcu_Init()
	Mcu_ClockType clockSetting;	// Saved clock setting from Mcu_InitClock()
} Mcu_GlobalType;

/* ----------------------------[private function prototypes]-----------------*/
/* ----------------------------[private variables]---------------------------*/

/*lint -save -e785 Misra 2012 9.3 Too few initializers.
 * Just the necessary record are initialized. */
Mcu_GlobalType Mcu_Global =
{
	.initRun = FALSE,
	.config = &McuConfigData[0],
};

/* ----------------------------[private functions]---------------------------*/

/* @req SWS_Mcu_00052*/
Mcu_ResetType Mcu_Arc_GetResetReason(void)
{
	// Not known how to detect reset reason or is it possible
	return MCU_POWER_ON_RESET;
}


/* ----------------------------[public functions]----------------------------*/


/* @req SWS_Mcu_00026  */
/* @req SWS_Mcu_00116  */
/* @req SWS_Mcu_00244  */
/* @req SWS_Mcu_00245  */
/* @req SWS_Mcu_00246  */
/* @req SWS_Mcu_00247  */
/* @req SWS_Mcu_00153  */
void Mcu_Init(const Mcu_ConfigType *configPtr)
{
	VALIDATE( ( NULL != configPtr ), MCU_INIT_SERVICE_ID, MCU_E_PARAM_CONFIG );

	Irq_Enable();

	Mcu_Global.config = configPtr;
	Mcu_Global.initRun = TRUE;

#if defined(USE_TTY_SERIAL_BCM2835)
	bcm2835InitDbg();
#endif
}


void Mcu_Arc_DeInit()
{
	Mcu_Global.initRun = FALSE;	// Very simple Deinit. Should we do more?
}


Std_ReturnType Mcu_InitRamSection(const Mcu_RamSectionType RamSection)
{
	/* @req SWS_Mcu_00021 */
	/* @req SWS_Mcu_00125 */
	/* @req SWS_Mcu_00154 */
	VALIDATE_W_RV( ( TRUE == Mcu_Global.initRun ), MCU_INITRAMSECTION_SERVICE_ID, MCU_E_UNINIT, E_NOT_OK );
	VALIDATE_W_RV( ( RamSection <= Mcu_Global.config->McuRamSectors ), MCU_INITRAMSECTION_SERVICE_ID, MCU_E_PARAM_RAMSECTION, E_NOT_OK );

	/* @req SWS_Mcu_00011 */
	/*lint -save -e923 cast from unsigned int to pointer Misra 2004 11.1,2004 11.3,2012 11.1,2012 11.4, 2012 11.6
	 * Its suppression is necessary because function signature of memset is fixed*/
	//memset(	(void *)Mcu_Global.config->McuRamSectorSettingConfig[RamSection].McuRamSectionBaseAddress, (sint32)Mcu_Global.config->McuRamSectorSettingConfig[RamSection].McuRamDefaultValue,		(size_t)Mcu_Global.config->McuRamSectorSettingConfig[RamSection].McuRamSectionSize);
	/*lint -restore*/

	/* NOT SUPPORTED, reason: not sure if support for external RAM is required in any application*/

	return E_OK;
}


/* @req SWS_Mcu_00137  */
/* @req SWS_Mcu_00210  */
#if ( MCU_INIT_CLOCK == STD_ON )
/* @req SWS_Mcu_00155  */
Std_ReturnType Mcu_InitClock(const Mcu_ClockType ClockSetting)
{
	VALIDATE_W_RV( ( 1 == Mcu_Global.initRun ), MCU_INITCLOCK_SERVICE_ID, MCU_E_UNINIT, E_NOT_OK );
	VALIDATE_W_RV( ( ClockSetting < Mcu_Global.config->McuClockSettings ), MCU_INITCLOCK_SERVICE_ID, MCU_E_PARAM_CLOCK, E_NOT_OK );

	Mcu_Global.clockSetting = ClockSetting;
	// There should be some PLL settings (see BCM2835 datasheet Section "Clock Manager General Purpose Clocks Control"), but cannot find the registers in the datasheets (bcm2835 peripherals or ARM1176 datasheet).
	return E_OK;
}
#endif /* #if ( MCU_INIT_CLOCK == STD_ON )*/

Std_ReturnType Mcu_DistributePllClock(void)
{
	VALIDATE_W_RV( ( TRUE == Mcu_Global.initRun ), MCU_DISTRIBUTEPLLCLOCK_SERVICE_ID, MCU_E_UNINIT, E_NOT_OK );

	// NOT IMPLEMENTED: There should be some PLL settings (see BCM2835 datasheet Section "Clock Manager General Purpose Clocks Control"), but cannot find the registers in the datasheets (bcm2835 peripherals or ARM1176 datasheet).

	return E_OK;
}

Mcu_PllStatusType Mcu_GetPllStatus(void)
{
	VALIDATE_W_RV( ( TRUE == Mcu_Global.initRun ), MCU_GETPLLSTATUS_SERVICE_ID, MCU_E_UNINIT, MCU_PLL_STATUS_UNDEFINED );

	// NOT IMPLEMENTED: There should be some PLL settings (see BCM2835 datasheet Section "Clock Manager General Purpose Clocks Control"), but cannot find the registers in the datasheets (bcm2835 peripherals or ARM1176 datasheet).

	return MCU_PLL_LOCKED;
}

/* @req SWS_Mcu_00158 */
Mcu_ResetType Mcu_GetResetReason(void)
{
	/* @req SWS_Mcu_00133 */
	/* @req SWS_Mcu_00125 */
	/* @req SWS_Mcu_00005 */
	VALIDATE_W_RV( ( TRUE == Mcu_Global.initRun ), MCU_GETRESETREASON_SERVICE_ID, MCU_E_UNINIT, MCU_RESET_UNDEFINED );

	// NOT IMPLEMENTED: Unable to find the registers in the datasheets (bcm2835 peripherals or ARM1176 datasheet).

	return MCU_POWER_ON_RESET;
}

/* @req SWS_Mcu_00235 */
/* @req SWS_Mcu_00006 */
/* @req SWS_Mcu_00159 */
Mcu_RawResetType Mcu_GetResetRawValue(void)
{
	/* @req SWS_Mcu_00135 */
	/* @req SWS_Mcu_00125 */
	VALIDATE_W_RV( ( TRUE == Mcu_Global.initRun ), MCU_GETRESETREASON_SERVICE_ID, MCU_E_UNINIT, MCU_GETRESETRAWVALUE_UNINIT_RV );

	// NOT IMPLEMENTED: Unable to find the registers in the datasheets (bcm2835 peripherals or ARM1176 datasheet).

	return MCU_POWER_ON_RESET;
}

void Mcu_SetMode( Mcu_ModeType McuMode)
{
	/* @req SWS_Mcu_00125 */
	/* @req SWS_Mcu_00020 */
	VALIDATE( ( TRUE == Mcu_Global.initRun ), MCU_SETMODE_SERVICE_ID, MCU_E_UNINIT );
	VALIDATE( ( (mcuMode <= Mcu_Global.config->McuNumberOfMcuModes)), MCU_SETMODE_SERVICE_ID, MCU_E_PARAM_MODE );

	/* Low Power Mode
	* --------------------------------------
	*   APU
	*   SCU
	*   L2
	*   Peripherals
	*   Clock
	* */

	/* Shutdown core 2 (if on) */

	/* NOT_IMPLEMENTED: Sleep mode not supported yet */

}

/* @req SWS_Mcu_00162 */
void Mcu_GetVersionInfo( Std_VersionInfoType* versioninfo) {

	/* @req SWS_Mcu_00204 */
	VALIDATE( ( NULL != versioninfo ), MCU_GETVERSIONINFO_SERVICE_ID, MCU_E_PARAM_POINTER);

	versioninfo->vendorID = MCU_VENDOR_ID;
	versioninfo->moduleID = MCU_MODULE_ID;
	versioninfo->sw_major_version = MCU_SW_MAJOR_VERSION;
	versioninfo->sw_minor_version = MCU_SW_MINOR_VERSION;
	versioninfo->sw_patch_version = MCU_SW_PATCH_VERSION;

}

/**
 * Get frequency of the oscillator
 */
uint32 Mcu_Arc_GetClockReferencePointFrequency(void)
{
	return Mcu_Global.config->McuClockSettingConfig[Mcu_Global.clockSetting].McuClockReferencePointFrequency;
}

/********************************************************************
 * Mcu_Arc_InitZero
 *
 * Function that perform all necessary initialization needed to to run software, such as disabling watchdog,
 * init ECC RAM, setup exception vectors etc
 ********************************************************************/
void Mcu_Arc_InitZero(void) {
	/********************************************************************************
	* NOTE
	*
	* This function will be called before BSS and DATA are initialized.
	* Ensure that you do not access any global or static variables before
	* BSS and DATA is initialized
	********************************************************************************/

	Mcu_Arc_PlatformInit();

#if defined(USE_KERNEL)
	Irq_Init();
#endif

}

/**
 * Get the system clock in Hz. It calculates the clock from the
 * different register settings in HW.
 */
uint32_t Mcu_Arc_GetSystemClock(void)
{
	return Mcu_Global.config->McuClockSettingConfig[Mcu_Global.clockSetting].McuClockReferencePointFrequency;
}

