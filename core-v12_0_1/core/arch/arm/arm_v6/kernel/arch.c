/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * arch.c
 *
 *  Created on:  Mar 4, 2013
 *      Author:  Zhang Shuzhou
 *  Reviewed on: May 17, 2016
 *     Reviewer: Ali Syed
 *
 */

/* ----------------------------[includes]------------------------------------*/
#include "os_i.h"
#include "arch_stack.h"
//#include "sys.h"
#include "Cpu.h"
#if defined(CFG_BCM2835)
#include "bcm2835.h"
#endif

/* ----------------------------[private define]------------------------------*/
/* ----------------------------[private macro]-------------------------------*/
/* ----------------------------[private typedef]-----------------------------*/
/* ----------------------------[private function prototypes]-----------------*/
/* ----------------------------[private variables]---------------------------*/
/* ----------------------------[private functions]---------------------------*/
/* ----------------------------[public functions]----------------------------*/

void Os_Arc_Panic(uint32_t exception, void *pData) {

    (void)exception;
    (void)pData;

    /* No protection hook */
    /* @req OS107 */ /* If not protection hook, call ShutdownOS() */

    /* This is done regardless of if the protection
     * hook is called or not
     */
    ShutdownOS(E_OS_PANIC);
}

void Os_ArchFirstCall( void )
{
	// IMPROVEMENT: make switch here... for now just call func.
	Irq_Enable();
	OS_SYS_PTR->currTaskPtr->constPtr->entry();
}

void *Os_ArchGetStackPtr( void ) {
	 void *x;
	 asm volatile ("mov  %0, sp": "=r" (x));
	 return x;
}

unsigned int Os_ArchGetScSize( void ) {

	return SC_SIZE;
}

void Os_ArchSetTaskEntry(OsTaskVarType *pcbPtr ) {
	// TODO: Add lots of things here, see ppc55xx
	uint32_t *context = (uint32_t *)pcbPtr->stack.curr;

	context[C_CONTEXT_OFFS/4] = SC_PATTERN;

	// Set LR to start function
	if( pcbPtr->constPtr->proc_type == PROC_EXTENDED ) {
			context[VGPR_LR_OFF/4] = (uint32_t)Os_TaskStartExtended;
	} else if( pcbPtr->constPtr->proc_type == PROC_BASIC ) {
			context[VGPR_LR_OFF/4] = (uint32_t)Os_TaskStartBasic;
	}
}

void Os_ArchSetupContext( OsTaskVarType *pcbPtr ) {
	// TODO: Add lots of things here, see ppc55xx

	// uint32_t *context = (uint32_t *)pcb->stack.curr;

}

void Os_ArchInit( void ) {
#if (OS_SC3 == STD_ON) || (OS_SC4 == STD_ON)
	Os_MMInit();
#endif
}

void Os_ArchSetStackPointer(void *sp) {

}

void Os_ArchTest(void *stack_p){

}
