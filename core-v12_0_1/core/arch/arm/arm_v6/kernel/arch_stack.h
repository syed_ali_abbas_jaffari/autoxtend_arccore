/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * arch_stack.h
 *
 *  Created on:  Mar 4, 2013
 *      Author:  Zhang Shuzhou
 *  Reviewed on: May 17, 2016
 *     Reviewer: Ali Syed
 *
 */
#ifndef ARCH_STACK_H_
#define ARCH_STACK_H_




#define SC_PATTERN		0xde		/* Small context */
#define LC_PATTERN		0xad		/* Large context */

/* Minimum alignment req */
#define ARCH_ALIGN		4

/* Small context (task swap==function call) */
//#define SAVE_NVGPR(_x,_y)
//#define RESTORE_NVGPR(_x,_y)

/* Save volatile regs, NOT preserved by function calls */
//#define SAVE_VGPR(_x,_y)
//#define RESTORE_VGPR(_x,_y)

/* Large context (interrupt) */
//#define SAVE_ALL_GPR(_x,_y)
//#define RESTORE_ALL_GPR(_x,_y)


// NVREGS: r0+r1+r2+r3+r4+r5+r6+r7+r8+r9+r10+r11+r12+lr = 14*4 = 40
#define NVGPR_SIZE		56
// VGPR: 9*4 = 36
//#define VGPR_SIZE		36
// SP + context
#define C_SIZE			8
#define VGPR_LR_OFF		(C_SIZE+NVGPR_SIZE-4)
#define C_CONTEXT_OFFS  4
#define C_SP_OFF 		0
#define SC_SIZE			(NVGPR_SIZE+C_SIZE)
/*
 * FULL_PATTERN is used indicate that a full register context is on the stack.
 * FUNC_PATTERN indicates that a function register context is  on the stack
 */
//#define ISR_SPE_PATTERN		0x12
//#define FUNC_SPE_PATTERN	0x32
//#define ISR_PATTERN			0xde
//#define FUNC_PATTERN		0xad

#if !defined(_ASSEMBLER_)
#include <stdint.h>


typedef struct {
	uint32_t r0;
	uint32_t r1;
	uint32_t r2;

	uint32_t r3;
	uint32_t r4;
	uint32_t r5;
	uint32_t r6;

	uint32_t r7;
	uint32_t r8;
	uint32_t r9;
	uint32_t r10;

	uint32_t r11;
	uint32_t r12;		// REG_SAVE
	uint32_t lr;		// REG_SAVE, lr (link register, receives the return address when a branch and link instruction is executed, general purpose, otherwise)
	uint32_t pc;		// rfeia + next address
	uint32_t psr;
} Os_IsrFrameType;


#endif /* !defined(_ASSEMLBER_) */
#endif /* ARCH_STACK_H_ */
