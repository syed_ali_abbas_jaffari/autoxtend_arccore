/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * irq.c
 *
 *  Created on:  Mar 4, 2013
 *      Author:  Zhang Shuzhou
 *  Reviewed on: May 17, 2016
 *     Reviewer: Ali Syed
 * Comment: So far implements only slow hardware interrupts
 */

#include "os_i.h"
#include "isr.h"
#include "irq_types.h"
#include "bcm2835.h"

// As the hardware doesn't provide control over priorities of the interrupts, we have to implement priorities ourselves.
// Priorities go from 0 (the lowest) to 31 (the highest)
uint32_t prioMask[32][3];		// uint32_t holds priority bits for 32 interrupt sources. 
								// The first array index '32' indicates priority levels (explained in the above comment). 
								// The second array index '3' indicates number of bytes required to define priority for all 72 interrupts (note some bits are not used)
									// 0 - Basic interrupt register, 1 - interrupt 1 register, 2 - interrupt 2 register
uint16_t Irq_StackPrio[16] ;	// 16 nested interrupts are supported
uint16_t Irq_Index =  0;

// Lookup table to get the first least significant set bit
static const uint8_t MultiplyDeBruijnBitPosition[32] = 
{
  0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8, 
  31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
};

// Called from Os_IsrInit() in os_isr.c. Used to initialize IRQ registers before starting to register them.
void Irq_Init( void ) {

}

/**
 * Called before starting an interrupt service routine (Called from Os_Isr() in os_isr.c before calling the interrupt handler.)
 * 
 * @param priority The priority of the ISR we are about to start
 */
void Irq_SOI3( uint8_t priority ) {
	// Disable lower prio interrupts
	Irq_StackPrio[++Irq_Index] = priority; 	// save priority

	/* It seems that we need to clear the enable of the interrupt here
	 * because not doing it and enabling interrupts will cause a Data Exception.
	 * At the same time we must also set the priority mask according to the
	 * priority of this ISR.
	 * */

	IRQ_DISABLE_BASIC = (IRQ_DISABLE_BASIC ^ prioMask[priority][0]);
	IRQ_DISABLE1 = (IRQ_DISABLE1 ^ prioMask[priority][1]);
	IRQ_DISABLE2 = (IRQ_DISABLE2 ^ prioMask[priority][2]);
}

// Called after ending the interrupt (Called from Os_Isr() in os_isr.c after calling the interrupt handler.)
void Irq_EOI( void ) {
	// Enable higher prio interrupts
	--Irq_Index;
	IRQ_ENABLE_BASIC = prioMask[Irq_StackPrio[Irq_Index]][0];
	IRQ_ENABLE1 = prioMask[Irq_StackPrio[Irq_Index]][1];
	IRQ_ENABLE2 = prioMask[Irq_StackPrio[Irq_Index]][2];
}

// Called from IrqHandler in arch_krn.sx. Used to execute the Os_Isr based on the interrupt type.
void *Irq_Entry(void *stack_p) {
	IrqType vector = BCM2835_IRQ_INVALID;

	vector = MultiplyDeBruijnBitPosition[((uint32_t)((IRQ_BASIC_PEND & (-IRQ_BASIC_PEND)) * 0x077CB531U)) >> 27];
	if (vector >= BCM2835_IRQ_INVALID && vector < BCM2835_IRQ_SYS_TIMER0) {
		// Not possible, since these bits are don't care
		return stack_p;
	} else if (vector == BCM2835_IRQ_PENDING1) {
		vector = 32 + MultiplyDeBruijnBitPosition[((uint32_t)((IRQ_PEND1 & (-IRQ_PEND1)) * 0x077CB531U)) >> 27];
	} else if (vector == BCM2835_IRQ_PENDING2) {
		vector = 64 + MultiplyDeBruijnBitPosition[((uint32_t)((IRQ_PEND2 & (-IRQ_PEND2)) * 0x077CB531U)) >> 27];
	}
	
	Os_Isr(stack_p, vector);

	return stack_p;
}

// Enables an interrupt. Called from Os_IsrAddWithId in os_isr.c
void Irq_EnableVector( int16_t vector,  uint8_t priority, int core ) {

	if (vector < NUMBER_OF_INTERRUPTS_AND_EXCEPTIONS) {

		/* Priority is from 0 (lowest) to 31 (highest)
		 * The mask should be set on all lower priorities than this one
		 * */
		for(int i=0; i < priority ; i++  ) {
			prioMask[i][vector/32] |= (1 << (vector%32));
		}

		/* Assume this is done in non-interrupt context, set to lowest prio */
		IRQ_ENABLE_BASIC = prioMask[0][0];
		IRQ_ENABLE1 = prioMask[0][1];
		IRQ_ENABLE2 = prioMask[0][2];

	} else {
		assert(0);			// Invalid vector!
	}
}

/** TODO CHECK IF REALLY VALID FOR RASPBERRY PI
 * NVIC prio have priority 0-31, 0-highest priority.
 * Autosar does it the other way round, 0-Lowest priority
 * NOTE: prio 255 is reserved for SVC and PendSV
 *
 * Autosar    NVIC
 *   31        0
 *   30        1
 *   ..
 *   0         31
 * @param prio
 * @return
 */
static inline int osPrioToCpuPio( uint8_t prio ) {
	assert(prio<32);
	prio = 31 - prio;
	return 0;
}

/**
 * Generates a soft interrupt, ie sets pending bit.
 * This could also be implemented using ISPR regs.
 *
 * @param vector
 */
void Irq_GenerateSoftInt( IrqType vector ) {

}

void Irq_AckSoftInt( IrqType vector ) {

}

/**
 * Get the current priority from the interrupt controller.
 * @param cpu
 * @return
 */
uint8_t Irq_GetCurrentPriority( Cpu_t cpu) {

	 //uint8_t prio = 0;

	// SCB_ICSR contains the active vector
	return 0;
}

/**
 * Attach an ISR type 1 to the interrupt controller. Not implemented.
 * Perhaps called by the application. It is not called by the core.
 *
 * @param entry
 * @param int_ctrl
 * @param vector
 * @param prio
 */
void Irq_AttachIsr1( void (*entry)(void), void *int_ctrl, uint32_t vector, uint8_t prio) {

	// IMPROVEMENT: Use NVIC_InitVector(vector, osPrioToCpuPio(pcb->prio)); here
}

