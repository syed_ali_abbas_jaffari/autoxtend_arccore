/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * irq_types.h
 *
 *  Created on:  Mar 4, 2013
 *      Author:  Zhang Shuzhou
 *  Reviewed on: May 17, 2016
 *     Reviewer: Ali Syed
 *
 */
#ifndef IRQ_TYPES_H
#define IRQ_TYPES_H

// To avoid implicit declaration warning while compiling.
#define Irq_SOI()

// BCM2835 interrupts (see table on p.113 in BCM2835-ARM-Peripherals.pdf)
// Help: https://github.com/raspberrypi/linux/blob/rpi-3.6.y/arch/arm/mach-bcm2708/include/mach/platform.h
#define NUMBER_OF_INTERRUPTS_AND_EXCEPTIONS 96					// Total number of interrupts and exceptions (72 + rest for don't cares, see the enum below)
typedef enum {
	BCM2835_IRQ_ARM_TIMER	      		= 0,					// In IRQ_BASIC bit 0
	BCM2835_IRQ_PENDING1	      		= 8,					// In IRQ_BASIC bit 8
	BCM2835_IRQ_PENDING2	      		= 9,					// In IRQ_BASIC bit 9
	BCM2835_IRQ_INVALID					= 21,					// Used to represent invalid value (Unused in the datasheet)
	BCM2835_IRQ_SYS_TIMER0      		= 32,					// In IRQ_1 bit 0
	BCM2835_IRQ_SYS_TIMER1      		= 33,					// In IRQ_1 bit 1
	BCM2835_IRQ_SYS_TIMER2      		= 34,					// In IRQ_1 bit 2
	BCM2835_IRQ_SYS_TIMER3      		= 35,					// In IRQ_1 bit 3
	BCM2835_IRQ_VC_USB 	     	    	= 41,					// In IRQ_1 bit 9
	BCM2835_IRQ_AUX						= 61,					// In IRQ_1 bit 29
	BCM2835_IRQ_INVALID2				= 64,					// Used to represent invalid value (Unused in the datasheet)
	BCM2835_IRQ_GPIO0 	     			= 81,					// In IRQ_2 bit 17
	BCM2835_IRQ_GPIO1 	     			= 82,					// In IRQ_2 bit 18
	BCM2835_IRQ_GPIO2 	     			= 83,					// In IRQ_2 bit 19
	BCM2835_IRQ_GPIO3 	     			= 84,					// In IRQ_2 bit 20
	BCM2835_IRQ_VC_I2C 	      		   	= 85,					// In IRQ_2 bit 21
	BCM2835_IRQ_VC_SPI 	       		  	= 86,					// In IRQ_2 bit 22
	BCM2835_IRQ_VC_UART					= 89					// In IRQ_2 bit 25
} IrqType;

/* Offset from start of exceptions to interrupts
 * Exceptions have negative offsets while interrupts have positive
 */
#define IRQ_INTERRUPT_OFFSET 0

// Not sure if needed.
typedef enum {
	 PERIPHERAL_CLOCK_AHB,
	 PERIPHERAL_CLOCK_APB1,
	 PERIPHERAL_CLOCK_APB2,
} Mcu_Arc_PeriperalClock_t;

typedef enum {
	CPU_0=0,
} Cpu_t;

#endif /* IRQ_H_ */
