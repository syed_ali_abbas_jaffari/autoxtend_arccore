/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

#include "bcm2835.h"

/**
 * Select which function a certain GPIO pin should have (pp. 91-94 in BCM2835-ARM-Peripherals.pdf).
 *
 * There are 54 GPIO pins on BCM2835. Each of them has at least
 * two alternative functions, in addition to acting simply as an
 * input or an output pin.
 *
 * There are 6 registers for pin function selection (GPFSELx), each
 * handling at most 10 pins (for example, GPFSEL3 handles pins 30-39).
 * Further, each pin is represented by three bits in these registers
 * (for example, pin 34 is represented by bits 12-14 in GPFSEL3).
 * Bit values have the following meaning:
 * 		000 = GPIO Pin x is an input
 * 		001 = GPIO Pin x is an output
 * 		100 = GPIO Pin x takes alternate function 0
 * 		101 = GPIO Pin x takes alternate function 1
 * 		110 = GPIO Pin x takes alternate function 2
 * 		111 = GPIO Pin x takes alternate function 3
 * 		011 = GPIO Pin x takes alternate function 4
 * 		010 = GPIO Pin x takes alternate function 5
 *
 * @param gpio_pin	    	-----    GPIO pin number
 * @param gpio_fn	       	-----    3 bits describing which function to select (as above)
 */
void bcm2835_GpioFnSel(uint32 gpio_pin, uint32 gpio_fn)
{
  uint32 regOffset = gpio_pin / 10; 							/* 10 pins per GPIO select register (GPFSELx) */
  uint32 bitOffset = (gpio_pin % 10) * 3; 						/* 3 bits per pin (or FSELx) in each GPFSELx */
  volatile uint32 *gpfnsel = &GPFSEL0 + regOffset;				/* Get the right GPFSELx address */

  *gpfnsel &= ~(0x07 << bitOffset); 							/* Clear all previous selection settings for this pin */
  *gpfnsel |= (gpio_fn << bitOffset); 							/* Set the pin to the new value  */
}

/**
 * Set one bit to 1 in 32-bit write-only GPIO-related registers.
 *
 * In this function, the pin bit is written directly, since we cannot read
 * the register value. By definition, 0's don't affect the actual values
 * of write-only register.
 *
 * There are several GPIO control registers, with one one bit per GPIO pin.
 * Typically, there are two registers per control functionality (to cover all 54 pins).
 * Some examples:
 * 		Set output pin (GPSETn)
 * 		Clear output pin (GPCLRn)
 *
 * @param baseReg	    	-----    address of the first control register for this functionality
 * 									 (the second one follows 32-bit later)
 * @param gpio_pin	    	-----    GPIO pin number (0-53)
 */
void bcm2835_SetPinInGpioReg(volatile uint32* baseReg, uint32 gpio_pin)
{
	uint32 regOffset = gpio_pin / 32;							/* 32 pins per register */
	uint32 bitOffset = gpio_pin % 32;							/* One bit per pin within a register */

	*(baseReg + regOffset) |= (1 << bitOffset);					/* Set the pin bit in the corresponding register to the new value  */
}

/**
 * Read the bit corresponding to a given GPIO pin from one of its control registers.
 *
 * There are several GPIO control registers, with one bit per GPIO pin.
 * Typically, there are two registers per control functionality (to cover all 54 pins).
 * Examples:
 * 		Read if an event (rising or falling edge) has been detected (GPEDSn)
 * 		Read pin level (GPLEVn)
 *
 * @param baseReg	    	----- address of the first control register for this functionality
 * @param gpio_pin	    	----- GPIO pin number (0-53)
 * @return value			----- Binary (ON/OFF) value on this GPIO pin
 */
uint32 bcm2835_ReadGpioPin(uint32 gpio_pin)
{
	uint32 regOffset = gpio_pin / 32;							/* 32 pins per register */
	uint32 bitOffset = gpio_pin % 32;							/* One bit per pin within a register */

	uint32 value = *(&GPLEV0 + regOffset);						/* Get the pin value from the right register */

	return (value & (1 << bitOffset)) ? HIGH : LOW;				/* Convert the return value to a boolean */
}

uint32 bcm2835_ReadGpioPort(uint8 gpio_port)
{
	uint32 value = *(&GPLEV0 + gpio_port);
	return value;
}

void bcm2835_WriteGpioPort(uint8 port_id, uint32 value)
{
	*(&GPSET0 + port_id) |= value;
	*(&GPCLR0 + port_id) |= !value;
}

/**
 * Sleep for some microseconds
 *
 * Loop, doing nothing, until the system timer register says that the
 * specified delay has passed.
 *
 * @param micros	    	----- number of microseconds to wait in a loop
 */
void bcm2835_Sleep(uint64_t micros)
{
    uint64_t start = CURRENT_TIME;
    while (CURRENT_TIME < (start + micros));
}

