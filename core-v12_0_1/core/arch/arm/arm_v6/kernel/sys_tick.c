/* ----------------------------- Copyright Notice ---------------------------
 * Copyright (C) 2016, Ali Syed, Germany
 * Contact: syed.ali.abbas.jaffari@gmail.com
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * See <https://www.gnu.org/licenses/gpl.txt> for more details.
 * ---------------------------- Copyright Notice ---------------------------*/

/*
 * sys_tick.h
 *
 *  Created on:  Mar 4, 2013
 *      Author:  Zhang Shuzhou
 *  Reviewed on: May 17, 2016
 *     Reviewer: Ali Syed
 *
 */

#include "Os.h"
#include "os_i.h"
#include "bcm2835.h"
#include "irq_types.h"
#include "isr.h"
#include "arc.h"
#include "os_counter_i.h"
#include "Mcu.h"

uint32 aliveSignalCallCount = 0;
uint32 gpio_tick_period = 500;

//#define ACK_LED GPIO16_PAD		// RPi 2
#define ACK_LED GPIO47_PAD			// RPi Zero

#define ALIVE_SIGNAL_PATTERN 0x0000121f
uint32 current_pattern = 0;

/*
 * Creates alive signal on GPIO16 pin (ACK LED on Raspberry Pi)
 * Blink once per 500ms with pattern ALIVE_SIGNAL_PATTERN
 */
static void aliveSignal(void){
	if (current_pattern == 0)
		current_pattern = ALIVE_SIGNAL_PATTERN;

	gpio_tick_period = 1000/16*(current_pattern&0xf);
	aliveSignalCallCount++;

	if (aliveSignalCallCount / gpio_tick_period == 1) {
		if (bcm2835_ReadGpioPin(ACK_LED) == HIGH){
			// turn off
			bcm2835_SetPinInGpioReg(&GPCLR0, ACK_LED);
			aliveSignalCallCount = 0;
		}
		else
		{
			// turn on
			bcm2835_SetPinInGpioReg(&GPSET0, ACK_LED);
			aliveSignalCallCount = 0;
		}
		current_pattern >>= 4;
	}
}

void Bcm2835OsTick(void) {
	// Clear timer interrupt otherwise this will hit again as soon as interrupts are re-enabled.
	ARM_TIMER_CLI = 0;

	// Call regular OsTick.
	OsTick();

	// Indicate alive signal
	aliveSignal();
}

/**
 * Init of free running timer.
 */
void Os_SysTickInit( void ) {
	// Set the gpio directions
    bcm2835_GpioFnSel(ACK_LED, GPFN_OUT);
	// Enable OsTick ISR with priority 6. Priorities are implemented statically for a hardware but might be different from the priorities in the hardware datasheet.
	ISR_INSTALL_ISR2("OsTick", Bcm2835OsTick, BCM2835_IRQ_ARM_TIMER, 6, 0);		// ISR_INSTALL_ISR2(Name, Entry, Vector, Priority, Application)
}

/**
 * Start the Sys Tick timer
 *
 * @param period_ticks - How long the period in timer ticks should be.
 *                     - Ratio of Mcu_Arc_GetSystemClock(), i.e. 250000000Hz, and required frequency of the systick timer
 *                     - Will never exceed 250000000Hz/65535 (constraint from Design tool meta-model template)
 */
void Os_SysTickStart(uint32_t period_ticks) {

	ARM_TIMER_LOD = (period_ticks / 50)-1;
	ARM_TIMER_RLD = (period_ticks / 50)-1;		// The value is decremented once per 0.2us (see below comment)
	ARM_TIMER_DIV = 49;							// Source 250Mhz, Divide by 50, Get 5Mhz
	ARM_TIMER_CLI = 0;
	IRQ_ENABLE_BASIC = 1;
	ARM_TIMER_CTL = ARM_TIMER_ENABLE;	// 23-bit counter, pre-scale is clock/1 (No pre-scale), timer interrupt enabled, Timer enabled, Timers keeps running in debug halted mode, Free running counter Disabled
}

