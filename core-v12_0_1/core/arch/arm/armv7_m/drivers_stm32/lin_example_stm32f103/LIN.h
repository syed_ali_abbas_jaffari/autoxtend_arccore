#ifndef __LIN_H
#define __LIN_H

#define   LIN_MODE       1
#define   LIN_CHECKSUM_MODE   1
#define   LIN_ID_NUM       2
#define   LIN_LEN_MODE   1
#define ID_LIFE_BELT       0xf0
#define ID_ACK                 0x50

enum SlaveWorkMode{
    SLAVE_IDLE,
    SLAVE_RX_MODE,
    SLAVE_TX_MODE
};

typedef struct
{
    INT8U CMD_ID;
    void (*pFunc)(void);
} CMD_LINType;

extern  OS_EVENT *LINMutex;
extern  OS_EVENT *LINRxSem;  
extern  LINDataType  LINTempData; 
#ifdef   LIN_MODULE

#if (LIN_LEN_MODE == 0)
const INT8U LIN_ID_List[LIN_ID_NUM][2]={
    {ID_LIFE_BELT, SLAVE_RX_MODE},
    {ID_ACK, SLAVE_TX_MODE}
};
#else
const INT8U LIN_ID_List[LIN_ID_NUM][3]={
    {ID_LIFE_BELT, SLAVE_RX_MODE, 8},
    {ID_ACK, SLAVE_TX_MODE, 4}
};
#endif
#endif

extern  void BSP_LIN_Init(void);
extern  void LinSend(INT8U *ComputeData);
extern  void LIN_DataAnalyse(void);
#endif

