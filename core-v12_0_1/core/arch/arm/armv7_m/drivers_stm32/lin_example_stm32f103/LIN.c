
#define  LIN_MODULE

#include <LIN.h>

#define BIT(A,B)      ((A>>B)&0x01)   
#define LIN_RXCMD_BUF_SIZE  5

enum LinState{
    IDLE,
    SYNCH,
    ID_LEN,
    DATA_GET,
    CHECKSUM
};

enum LinErrState{
    NO_ERR,
    SYNC_ERR,
    ID_ERR,
    CHKSUM_ERR
};

static INT8U AnalysePlus4 = IDLE;
#if (LIN_LEN_MODE == 0)
static const INT8U LIN_Len[4]={2, 2, 4, 8};
#endif
static LINDataType TempLinData;
LINDataType LINRxMsgBuf[LIN_RXCMD_BUF_SIZE];
COMM_LIN_Q LIN_RxMsg;
extern CMD_LINType  const  LIN_CMD[LIN_ID_NUM];
static INT8U  SendDataLen =0;
static INT8U  LinSendBuf[9]={0,0,0,0,0,0,0,0,0};

static void  LIN_IRQ_Handler(void);
static void  LIN_Rx_Analyse(INT8U LIN_Data);

void BSP_LIN_Init(void)
{
    USART_InitTypeDef  USART_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    INT8U i,j;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, ENABLE);
    USART_InitStructure.USART_BaudRate = 19200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(UART4, &USART_InitStructure);

    USART_LINBreakDetectLengthConfig(UART4, USART_LINBreakDetectLength_11b);
    USART_LINCmd(UART4, ENABLE);
    USART_Cmd(UART4, ENABLE);

    USART_ITConfig(UART4, USART_IT_RXNE, ENABLE);
    USART_ITConfig(UART4, USART_IT_TXE, DISABLE);
    USART_ITConfig(UART4, USART_IT_LBD, ENABLE);

    BSP_IntVectSet(BSP_INT_ID_UART4, LIN_IRQ_Handler);
    BSP_IntPrioSet(BSP_INT_ID_UART4, 1);
    BSP_IntEn(BSP_INT_ID_UART4);

    TempLinData.ErrorType = NO_ERR;
    for(i=0;i<LIN_RXCMD_BUF_SIZE;i++)
    {
        LINRxMsgBuf[i].CMD_ID = 0;
        LINRxMsgBuf[i].Len = 0;
        for(j=0;j<8;j++)
            LINRxMsgBuf[i].Data[j]=0;
    }
    COMM_LIN_QCreate(&LIN_RxMsg, LINRxMsgBuf, LIN_RXCMD_BUF_SIZE);
}

static void  LIN_IRQ_Handler(void)
{
    if(USART_GetITStatus(UART4, USART_IT_LBD) != RESET)
    {
        USART_ClearITPendingBit(UART4, USART_IT_LBD);
        AnalysePlus4 = SYNCH;
        TempLinData.ErrorType = NO_ERR;
        TempLinData.WorkMode = SLAVE_IDLE;
        USART_ITConfig(UART4, USART_IT_TXE, DISABLE);
    }

    if(USART_GetITStatus(UART4, USART_IT_RXNE) != RESET)
    {
        USART_ClearITPendingBit(UART4, USART_IT_RXNE);
        LIN_Rx_Analyse((INT8U)USART_ReceiveData(UART4));
    }
    if(USART_GetFlagStatus(UART4, USART_FLAG_ORE)==SET)
    {
        USART_ClearFlag(UART4, USART_FLAG_ORE);
        USART_ReceiveData(UART4);
    }

    if(USART_GetITStatus(UART4, USART_IT_TXE) != RESET)
    {
        USART_ClearITPendingBit(UART4, USART_IT_TXE);
        if((LINTempData.WorkMode == SLAVE_TX_MODE)&&(SendDataLen != 0))
        {
            SendDataLen--;
            USART_SendData(UART4, ((u16) LinSendBuf[SendDataLen]));
        }else{
            SendDataLen = 0;
            LINTempData.WorkMode = SLAVE_IDLE;
            USART_ITConfig(UART4, USART_IT_TXE, DISABLE);
        }
    }
}

static void LIN_Rx_Analyse(INT8U LIN_Data)
{
    INT8U  TempData = 0, TempNum = 0;
    static INT8U TempLen = 0;
    static INT16U TempCheckSum = 0;

    switch(AnalysePlus4)
    {
    case 1:
        if(LIN_Data == 0x55)
        {
            AnalysePlus4 =  ID_LEN;
        }else{
            AnalysePlus4 = IDLE;
            TempLinData.ErrorType = SYNC_ERR;
        }
        break;

    case 2:
        TempData = (~(BIT(LIN_Data,1)^BIT(LIN_Data,3)^BIT(LIN_Data,4)^BIT(LIN_Data,5)))<<7;
        TempData |= (BIT(LIN_Data,0)^BIT(LIN_Data,1)^BIT(LIN_Data,2)^BIT(LIN_Data,4))<<6;
        if(TempData == (LIN_Data&0xC0))
        {
            TempLinData.CMD_ID = LIN_Data;
#if (LIN_CHECKSUM_MODE == 0)
            TempCheckSum = 0;
#else
            TempCheckSum = TempLinData.CMD_ID;
#endif
            for(TempNum = 0; TempNum < LIN_ID_NUM; TempNum++)
            {
                if(TempLinData.CMD_ID == LIN_ID_List[TempNum][0])
                {
                    TempLinData.WorkMode = LIN_ID_List[TempNum][1];
#if (LIN_LEN_MODE == 0)
                    TempLinData.Len = LIN_Len[((LIN_Data>>4)&0x03)];
#else
                    TempLinData.Len = LIN_ID_List[TempNum][2];
#endif
                    if(TempLinData.WorkMode == SLAVE_RX_MODE)
                    {
                        TempLen = 1;
                        AnalysePlus4 = DATA_GET;
                    }else if(TempLinData.WorkMode == SLAVE_TX_MODE)
                    {
                        AnalysePlus4 = IDLE;
                        COMM_LIN_QPushStr(&LIN_RxMsg, &TempLinData);
                        OSSemPost(LINRxSem);
                    }
                    return;
                }
            }
            AnalysePlus4 = IDLE; TempLinData.CMD_ID = 0; TempLinData.Len = 0;
        }else{
            AnalysePlus4 = IDLE;
            TempLinData.ErrorType = ID_ERR;
        }
        break;

    case DATA_GET:
        TempLinData.Data[(TempLen-1)] = LIN_Data;
        TempCheckSum += LIN_Data;
        if(TempCheckSum&0xFF00)
            TempCheckSum = (TempCheckSum&0x00FF)+1;
        if(TempLinData.Len > TempLen)
        {
            TempLen++;
        }else{
            AnalysePlus4 = CHECKSUM;
            TempLinData.CheckSum = (INT8U)((~TempCheckSum)&0x00ff);
            TempLen = 0;
        }
        break;

    case CHECKSUM:
        AnalysePlus4 = IDLE;
        if(TempLinData.CheckSum == LIN_Data)
        {
            COMM_LIN_QPushStr(&LIN_RxMsg, &TempLinData);
            TempLinData.CMD_ID = 0;  TempLinData.Len = 0; TempLinData.CheckSum = 0;
            OSSemPost(LINRxSem);
        }else{
            TempLinData.CMD_ID = 0;   TempLinData.ErrorType = CHKSUM_ERR;
            TempLinData.Len = 0;      TempLinData.CheckSum = 0;
        }
        break;

    default:
        break;
    }
}

void LIN_DataAnalyse(void)
{
    COMM_Q_Status Q_Status_Data;
    INT8U i=0;

    Q_Status_Data = COMM_LIN_QQuery(&LIN_RxMsg);
    if((Q_Status_Data == COMM_Q_DATA)||(Q_Status_Data == COMM_Q_FULL))
    {
        if(COMM_LIN_QPopStr(&LIN_RxMsg, &LINTempData) == COMM_Q_OK)
        {
            for(i=0;i<LIN_ID_NUM;i++)
            {
                if(LINTempData.CMD_ID == LIN_CMD[i].CMD_ID)
                {
                    LIN_CMD[i].pFunc();
                    break;
                }
            }
        }
    }
}

void  LinSend(INT8U *ComputeData)
{
    INT8U  i = 0;
#if (LIN_CHECKSUM_MODE == 0)
    INT16U  TempData = 0;
#else	
    INT16U  TempData = LINTempData.CMD_ID;
#endif

    if((LINTempData.WorkMode != SLAVE_TX_MODE)||(SendDataLen != 0))
        return;

    SendDataLen = LINTempData.Len+1;
    for(i = 0; i<LINTempData.Len; i++)
    {
        LinSendBuf[(LINTempData.Len-i)] = ComputeData[i];
        TempData += ComputeData[i];
        if(TempData&0xFF00)
            TempData = (TempData&0x00FF)+1;
    }
    LinSendBuf[0] = (INT8U)((~TempData)&0x00ff);
    USART_ITConfig(UART4, USART_IT_TXE, ENABLE);
}
