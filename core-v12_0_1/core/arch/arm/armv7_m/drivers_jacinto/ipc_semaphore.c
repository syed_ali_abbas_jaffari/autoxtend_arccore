/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "ipc_semaphore.h"
#include "Os.h"
#include "timer.h"

sem_struct* Semaphore_Create( void )
{
	sem_struct* ptr =(sem_struct*)malloc(sizeof(sem_struct));
	ptr->val = 1;

	return ptr;
}

void Semaphore_Wait_Block(sem_struct* semaphore)
{
	while (semaphore->val > 0);
}

 int Semaphore_Busy(sem_struct* semaphore)
{
	 int rv;
	 if ( semaphore->val == 0) {
		 rv = 0;
	 } else {
		 rv = 1; 	/* Semaphore busy */
	 }
	 return rv;
}


/* Returns E_OK if the semaphore is free, E_NOT_OK if it timed out */
Std_ReturnType Semaphore_Wait_Timeout(sem_struct* semaphore, uint32_t timeout)
{
	uint32_t startTicks = Timer_GetTicks();
	/* timeout is in ms, timer operates in us */
	uint32_t waitTicks = TIMER_US2TICK(timeout * 1000);
	while (semaphore->val > 0)
	{
		Sleep(0);
		if (Timer_GetTicks() - startTicks >= waitTicks) return E_NOT_OK;
	}
	return E_OK;
}

void Semaphore_Signal(sem_struct* semaphore)
{
	if (semaphore->val > 0)
	{
		semaphore->val--;
	}
}


