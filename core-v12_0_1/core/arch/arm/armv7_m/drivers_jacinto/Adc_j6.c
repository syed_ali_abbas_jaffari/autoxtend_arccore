/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/** @tagSettings DEFAULT_ARCHITECTURE=JACINTO6 */
/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */

/* !req SWS_Adc_00082  IoHwAb not supported with external hardware*/
/* !req SWS_Adc_00083  IoHwAb not supported with external hardware*/
/* @req SWS_Adc_00085  call-backs defined during configuration*/
/* @req SWS_Adc_00104  Each group has the possibility to have separate call-backs*/
/* !req SWS_Adc_00342  not implemented on top level architecture*/
/* !req SWS_Adc_00416  Not possible to change notification settings during runtime*/
/* @req SWS_Adc_00056  Initialization done in top level and in SPI drivers*/

/* ----------------------------[includes]------------------------------------*/
#include <string.h>

#include "Adc.h"
#include "Spi.h"
#if defined(USE_DET)
#include "Det.h"
#endif
#include "isr.h"
#include "irq_types.h"
#include "Adc_j6_main.h"

/* ----------------------------[private define]------------------------------*/
#define MASK_12BIT_ADC                  0xFFFuL

#define VALIDATE_NO_RV(_exp,_api,_err ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(ADC_MODULE_ID,0,_api,_err); \
          return; \
        }

#define DET_REPORTERROR(_x,_y,_z,_q) (void)Det_ReportError(_x, _y, _z, _q)
#define   ADC_HW_MAIN_SPI_INIT                           0x1Cu

/* ----------------------------[private macro]-------------------------------*/

#define EXTERNAL_JOB(_x)		ExternalSpiDevices[_x]->SpiJobAssignment
#define EXTERNAL_CHANNEL(_x)	ExternalSpiDevices[_x].SpiSequence->SpiJobAssignment[0]->SpiChannelAssignment[0]->SpiChannelId
#define GROUP_MODE(_x)		AdcGroupConfiguration_AdcHwUnit[_x].conversionMode
#define CURRENT_GROUP(_x) 	AdcGroupConfiguration_AdcHwUnit[_x]


/* ----------------------------[private typedef]-----------------------------*/
typedef struct {
	/** Reference to SPI sequence (required for external ADC drivers). */
	const Spi_SequenceConfigType* SpiSequence;

} Adc_ExternalDriver;




/* ----------------------------[private function prototypes]-----------------*/
/* ----------------------------[private variables]---------------------------*/


static uint8 adcResultBuffer[1024];	/*Global array for conversion storage*/
Adc_StateType adcInternalState = ADC_STATE_UNINIT;
Adc_ExternalDriver ExternalSpiDevices[ADC_NBR_OF_ADCHWUNIT_GROUPS];
boolean runConversion = FALSE;
Adc_GroupType currentGroupIndex;


/* ----------------------------[private functions]---------------------------*/


/* ----------------------------[public functions]----------------------------*/

void Adc_handler(void);
void Adc_Hw_DeInit(void);
void Adc_Hw_StartGroupConversion(Adc_GroupType group);
/**
 * Used to initialize hardware
 * @param ConfigPtr
 */
void Adc_Hw_Init(const Adc_ConfigType* ConfigPtr){
	 VALIDATE_NO_RV( (NULL != ConfigPtr), ADC_INIT_ID, ADC_E_PARAM_POINTER);
	 adcInternalState = ADC_STATE_INIT;
}

#if (ADC_DEINIT_API == STD_ON)
void Adc_Hw_DeInit(void){
    /* @req SWS_Adc_00110 */
	adcInternalState = ADC_STATE_UNINIT;
}
#endif

#if (ADC_ENABLE_START_STOP_GROUP_API == STD_ON)
void Adc_Hw_StartGroupConversion (Adc_GroupType group){
    const Adc_GroupDefType* groupPtr = &AdcConfig[0].groupConfigPtr[group];
    uint16 bit = 1;
    uint16 channel;
    uint16 sample;
    uint8 adcAdressBuffer[((groupPtr->numberOfChannels * groupPtr->streamNumSamples) * 2) + 4]; /*Array to store command bytes for external ADC*/
    Std_ReturnType retVal = E_NOT_OK;

    VALIDATE_NO_RV( NULL != groupPtr , ADC_STARTGROUPCONVERSION_ID, ADC_E_PARAM_POINTER);
    currentGroupIndex = group;

    memset(adcResultBuffer, 0x00u, sizeof(adcResultBuffer));
    memset(adcAdressBuffer, 0x00u, (groupPtr->numberOfChannels*2*groupPtr->streamNumSamples) + 4);

    /*External ADC requires command bytes in bit 5-3, values 0-7
     *Each read sequence is two bytes long, ext ADC requires command bytes to be sent first */
	for (channel = 0; channel < groupPtr->numberOfChannels; channel++) {
		for(sample = 0; sample <= (groupPtr->streamNumSamples - 1); sample++){
			adcAdressBuffer[bit] = (((AdcChannelConfiguration_AdcHwUnit[groupPtr->channelMappingList[channel]].Adc_Channel)) << 3);
			bit = bit+2;
		}
	}


    /* Start group conversion */
    /* @req SWS_Adc_00061 */
   retVal = Spi_SetupEB(groupPtr->ExtChannelId, adcAdressBuffer, adcResultBuffer, ((groupPtr->numberOfChannels * groupPtr->streamNumSamples)+1));
   if(retVal == E_OK){
	   retVal = Spi_SetAsyncMode(SPI_INTERRUPT_MODE);
	   if(retVal == E_OK) {
		   (void)Spi_AsyncTransmit(groupPtr->ExtSequenceId);
	   }
   }
}

void Adc_Hw_StopGroupConversion (Adc_GroupType group){
	runConversion = FALSE;
}
#endif

void Adc_Hw_EnableHardwareTrigger(Adc_GroupType Group){
	/*NOT SUPPORTED*/
}

void Adc_Hw_DisableHardwareTrigger(Adc_GroupType Group){
	/*NOT SUPPORTED*/
}

void Adc_handler(void) {
	const Adc_GroupDefType* groupPtr = &AdcConfig[0].groupConfigPtr[currentGroupIndex];
	uint16 msbLsb = 2;
	uint16 sample;
	uint16 channel;
	uint16 conversionData[(groupPtr->numberOfChannels * groupPtr->streamNumSamples)];
	 memset(conversionData, 0x00u, sizeof(conversionData));
	for (channel = 0; channel < groupPtr->numberOfChannels; channel++) {
		for(sample = 0; sample <= (groupPtr->streamNumSamples - 1); sample++){
			conversionData[(channel * groupPtr->streamNumSamples) + sample] = ((adcResultBuffer[msbLsb] << 8) | adcResultBuffer[msbLsb + 1]);
			msbLsb = msbLsb+2;
		}
	}
	msbLsb = 0;

	/* @req SWS_Adc_00122 */
	for (channel = 0; channel < groupPtr->numberOfChannels; channel++) {
		for(sample = 0; sample <= (groupPtr->streamNumSamples - 1); sample++){
			groupPtr->status->resultBufferPtr[(channel * groupPtr->streamNumSamples) + sample] = conversionData[msbLsb] & MASK_12BIT_ADC;
			++msbLsb;
		}
	}
	/* @req SWS_Adc_00080 */
	/* @req SWS_Adc_00060 */
	Adc_Internal_Isr(groupPtr);
}


void Adc_Hw_IsrPost(const Adc_GroupDefType * groupPtr, boolean enableInt) {
    	if (ADC_TRIGG_SRC_HW == groupPtr->triggerSrc) {
    		runConversion = FALSE;
    	} else {
    		runConversion = enableInt;
    	}

}


