/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef ADC_MAIN_H
#define ADC_MAIN_H


/** API service ID's */
#define ADC_MAINFUNCTION_ID 0x0Du


/** Adc state enum*/
typedef enum
{
  ADC_STATE_UNINIT,
  ADC_STATE_INIT,
}Adc_StateType;

/** Adc main function to be scheduled in OS to enable multiple samples and multiple channels in one group */
void Adc_HW_MainFunction();


#endif
