/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef IPC_SEMAPHORE_H_
#define IPC_SEMAPHORE_H_
#include "Std_Types.h"

typedef struct
{
	uint8 val;
} sem_struct;

typedef sem_struct* Arc_Ipc_Semaphore_Handle;
sem_struct* Semaphore_Create();

void Semaphore_Wait_Block(sem_struct* semaphore);
Std_ReturnType Semaphore_Wait_Timeout(sem_struct* semaphore, uint32_t timeout);
void Semaphore_Signal(sem_struct* semaphore);
int Semaphore_Busy(sem_struct* semaphore);
#endif
