/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "Adc.h"
#include "Spi.h"
#include "Adc_j6_main.h"
#if defined(USE_DET)
#include "Det.h"
#endif

extern boolean runConversion;
extern Adc_GroupType currentGroupIndex;
extern Adc_StateType adcInternalState;

#define VALIDATE_NO_RV(_exp,_api,_err ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(ADC_MODULE_ID,0,_api,_err); \
          return; \
        }

#define DET_REPORTERROR(_x,_y,_z,_q) (void)Det_ReportError(_x, _y, _z, _q)


void Adc_HW_MainFunction( void ) {

	const Adc_GroupDefType* groupPtr = &AdcConfig[0].groupConfigPtr[currentGroupIndex];

	VALIDATE_NO_RV( NULL != groupPtr , ADC_MAINFUNCTION_ID, ADC_E_PARAM_POINTER);

	if  (adcInternalState == ADC_STATE_INIT) {
		if ((runConversion == TRUE ) && ((Spi_GetSequenceResult(groupPtr->ExtSequenceId) == SPI_SEQ_OK))) {
			Adc_Hw_StartGroupConversion(currentGroupIndex);
		}
	} else {
		/** DO NOTHING **/
	}

}
