/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/
#ifndef ETH_JACINTO_H_
#define ETH_JACINTO_H_

#include "Arc_Types.h"

#define GMAC_SW_BASE           (0x48484000U + L3_OFFSET)
#define GMAC_SW_CPPI_RAM_LEN   0x2000u

/*lint -save -e834 -e9050 */ /* This is for clarity */
/* GMAC_SW registers */
struct GmacSw_reg {
	struct{
		vuint32_t CPSW_ID_VER; /* R 32 0x0000 0000 0x4848 4000 */
		vuint32_t CPSW_CONTROL; /*  RW 32 0x0000 0004 0x4848 4004 */
		vuint32_t CPSW_SOFT_RESET; /* RW 32 0x0000 0008 0x4848 4008 */
		vuint32_t CPSW_STAT_PORT_EN; /*RW 32 0x0000 000C 0x4848 400C */
		vuint32_t CPSW_PTYPE; /* RW 32 0x0000 0010 0x4848 4010 */
		vuint32_t CPSW_SOFT_IDLE; /* RW 32 0x0000 0014 0x4848 4014 */
		vuint32_t CPSW_THRU_RATE;  /* RW 32 0x0000 0018 0x4848 4018 */
		vuint32_t CPSW_GAP_THRESH; /* RW 32 0x0000 001C 0x4848 401C */
		vuint32_t CPSW_TX_START_WDS; /* RW 32 0x0000 0020 0x4848 4020 */
		vuint32_t CPSW_FLOW_CONTROL; /* RW 32 0x0000 0024 0x4848 4024 */
		vuint32_t CPSW_VLAN_LTYPE; /* RW 32 0x0000 0028 0x4848 4028 */
		vuint32_t CPSW_TS_LTYPE; /* RW 32 0x0000 002C 0x4848 402C */
		vuint32_t CPSW_DLR_LTYPE; /* RW 32 0x0000 0030 0x4848 4030 */
		vuint32_t CPSW_EEE_PRESCALE; /* RW 32 0x0000 0034 0x4848 4034 */
	}SS;
	vuint32_t SS_Padding[(0x4100 - 0x4034 - 0x4)/4];
	struct{
		vuint32_t CONTROL; /*  RW 32 0x0000 0000 0x4848 4100 */
		vuint32_t CONTROL_Padding[(0x4108 - 0x4100 - 0x4)/4];
		vuint32_t MAX_BLKS; /*  RW 32 0x0000 0008 0x4848 4108 */
		vuint32_t BLK_CNT; /*  RW 32 0x0000 000C 0x4848 410C */
		vuint32_t TX_IN_CTL; /*  RW 32 0x0000 0010 0x4848 4110 */
		vuint32_t PORT_VLAN; /*  RW 32 0x0000 0014 0x4848 4114 */
		vuint32_t TX_PRI_MAP; /*   RW 32 0x0000 0018 0x4848 4118 */
		vuint32_t CPDMA_TX_PRI_MAP; /*   RW 32 0x0000 001C 0x4848 411C */
		vuint32_t CPDMA_RX_CH_MAP; /*   RW 32 0x0000 0020 0x4848 4120 */
		vuint32_t CPDMA_RX_CH_MAP_Padding[(0x4130 - 0x4120 - 0x4)/4];
		vuint32_t RX_DSCP_PRI_MAP0; /*   RW 32 0x0000 0030 0x4848 4130 */
		vuint32_t RX_DSCP_PRI_MAP1; /*   RW 32 0x0000 0034 0x4848 4134 */
		vuint32_t RX_DSCP_PRI_MAP2; /*   RW 32 0x0000 0038 0x4848 4138 */
		vuint32_t RX_DSCP_PRI_MAP3; /*   RW 32 0x0000 003C 0x4848 413C */
		vuint32_t RX_DSCP_PRI_MAP4; /*   RW 32 0x0000 0040 0x4848 4140 */
		vuint32_t RX_DSCP_PRI_MAP5; /*   RW 32 0x0000 0044 0x4848 4144 */
		vuint32_t RX_DSCP_PRI_MAP6; /*   RW 32 0x0000 0048 0x4848 4148 */
		vuint32_t RX_DSCP_PRI_MAP7; /*   RW 32 0x0000 004C 0x4848 414C */
		vuint32_t IDLE2LPI; /*  RW 32 0x0000 0050 0x4848 4150 */
		vuint32_t LPI2WAKE; /*  RW 32 0x0000 0054 0x4848 4154 */
	}PORT0;
	vuint32_t PORT0_Padding[(0x4200 - 0x4154 - 0x4)/4];
	struct{
		vuint32_t CONTROL; /* RW 32 0x0000 0100 0x4848 4200           */
		vuint32_t CONTROL_Padding[(0x4208 - 0x4200 - 0x4)/4];
		vuint32_t MAX_BLKS; /* RW 32 0x0000 0108 0x4848 4208          */
		vuint32_t BLK_CNT; /* RW 32 0x0000 010C 0x4848 420C           */
		vuint32_t TX_IN_CTL; /* RW 32 0x0000 0110 0x4848 4210         */
		vuint32_t PORT_VLAN; /* RW 32 0x0000 0114 0x4848 4214         */
		vuint32_t TX_PRI_MAP; /* RW 32 0x0000 0118 0x4848 4218        */
		vuint32_t TS_SEQ_MTYPE; /* RW 32 0x0000 011C 0x4848 421C      */
		vuint32_t SA_LO; /* RW 32 0x0000 0120 0x4848 4220             */
		vuint32_t SA_HI; /* RW 32 0x0000 0124 0x4848 4224             */
		vuint32_t SEND_PERCENT; /* RW 32 0x0000 0128 0x4848 4228      */
		vuint32_t SEND_PERCENT_Padding[(0x4230 - 0x4228 - 0x4)/4];
		vuint32_t RX_DSCP_PRI_MAP0; /* RW 32 0x0000 0130 0x4848 4230  */
		vuint32_t RX_DSCP_PRI_MAP1; /* RW 32 0x0000 0134 0x4848 4234  */
		vuint32_t RX_DSCP_PRI_MAP2; /* RW 32 0x0000 0138 0x4848 4238  */
		vuint32_t RX_DSCP_PRI_MAP3; /* RW 32 0x0000 013C 0x4848 423C  */
		vuint32_t RX_DSCP_PRI_MAP4; /* RW 32 0x0000 0140 0x4848 4240  */
		vuint32_t RX_DSCP_PRI_MAP5; /* RW 32 0x0000 0144 0x4848 4244  */
		vuint32_t RX_DSCP_PRI_MAP6; /* RW 32 0x0000 0148 0x4848 4248  */
		vuint32_t RX_DSCP_PRI_MAP7; /* RW 32 0x0000 014C 0x4848 424C  */
		vuint32_t IDLE2LPI; /* RW 32 0x0000 0150 0x4848 4250          */
		vuint32_t LPI2WAKE; /* RW 32 0x0000 0154 0x4848 4254          */
	}PORT1;
	vuint32_t PORT1_Padding[(0x4300 - 0x4254 - 0x4)/4];
	struct{
		vuint32_t CONTROL; /* RW 32 0x0000 0200 0x4848 4300                */
		vuint32_t CONTROL_Padding[(0x4308 - 0x4300 - 0x4)/4];
		vuint32_t MAX_BLKS; /* RW 32 0x0000 0208 0x4848 4308               */
		vuint32_t BLK_CNT; /* RW 32 0x0000 020C 0x4848 430C                */
		vuint32_t TX_IN_CTL; /* RW 32 0x0000 0210 0x4848 4310              */
		vuint32_t PORT_VLAN; /* RW 32 0x0000 0214 0x4848 4314              */
		vuint32_t TX_PRI_MAP; /* RW 32 0x0000 0218 0x4848 4318             */
		vuint32_t TS_SEQ_MTYPE; /* RW 32 0x0000 021C 0x4848 431C           */
		vuint32_t SA_LO; /* RW 32 0x0000 0220 0x4848 4320                  */
		vuint32_t SA_HI; /* RW 32 0x0000 0224 0x4848 4324                  */
		vuint32_t SEND_PERCENT; /* RW 32 0x0000 0228 0x4848 4328           */
		vuint32_t SEND_PERCENT_Padding[(0x4330 - 0x4328 - 0x4)/4];
		vuint32_t RX_DSCP_PRI_MAP0; /* RW 32 0x0000 0230 0x4848 4330       */
		vuint32_t RX_DSCP_PRI_MAP1; /* RW 32 0x0000 0234 0x4848 4334       */
		vuint32_t RX_DSCP_PRI_MAP2; /* RW 32 0x0000 0238 0x4848 4338       */
		vuint32_t RX_DSCP_PRI_MAP3; /* RW 32 0x0000 023C 0x4848 433C       */
		vuint32_t RX_DSCP_PRI_MAP4; /* RW 32 0x0000 0240 0x4848 4340       */
		vuint32_t RX_DSCP_PRI_MAP5; /* RW 32 0x0000 0244 0x4848 4344       */
		vuint32_t RX_DSCP_PRI_MAP6; /* RW 32 0x0000 0248 0x4848 4348       */
		vuint32_t RX_DSCP_PRI_MAP7; /* RW 32 0x0000 024C 0x4848 434C       */
		vuint32_t IDLE2LPI; /* RW 32 0x0000 0250 0x4848 4350               */
		vuint32_t LPI2WAKE; /* RW 32 0x0000 0254 0x4848 4354               */
	}PORT2;
	vuint32_t PORT2_Padding[(0x4800 - 0x4354 - 0x4)/4];
	struct{
		vuint32_t CPDMA_TX_IDVER; /* R 32 0x0000 0000 0x4848 4800                     */
		vuint32_t CPDMA_TX_CONTROL; /* RW 32 0x0000 0004 0x4848 4804                  */
		vuint32_t CPDMA_TX_TEARDOWN; /* RW 32 0x0000 0008 0x4848 4808                 */
		vuint32_t CPDMA_TX_TEARDOWN_Padding[(0x4810 - 0x4808 - 0x4)/4];
		vuint32_t CPDMA_RX_IDVER; /* R 32 0x0000 0010 0x4848 4810                     */
		vuint32_t CPDMA_RX_CONTROL; /* RW 32 0x0000 0014 0x4848 4814                  */
		vuint32_t CPDMA_RX_TEARDOWN; /* RW 32 0x0000 0018 0x4848 4818                 */
		vuint32_t CPDMA_SOFT_RESET; /* RW 32 0x0000 001C 0x4848 481C                  */
		vuint32_t CPDMA_DMACONTROL; /* RW 32 0x0000 0020 0x4848 4820                  */
		vuint32_t CPDMA_DMASTATUS; /* R 32 0x0000 0024 0x4848 4824                    */
		vuint32_t CPDMA_RX_BUFFER_OFFSET; /* RW 32 0x0000 0028 0x4848 4828            */
		vuint32_t CPDMA_EMCONTROL; /* RW 32 0x0000 002C 0x4848 482C                   */
		vuint32_t CPDMA_TX_PRI0_RATE; /* RW 32 0x0000 0030 0x4848 4830                */
		vuint32_t CPDMA_TX_PRI1_RATE; /* RW 32 0x0000 0034 0x4848 4834                */
		vuint32_t CPDMA_TX_PRI2_RATE; /* RW 32 0x0000 0038 0x4848 4838                */
		vuint32_t CPDMA_TX_PRI3_RATE; /* RW 32 0x0000 003C 0x4848 483C                */
		vuint32_t CPDMA_TX_PRI4_RATE; /* RW 32 0x0000 0040 0x4848 4840                */
		vuint32_t CPDMA_TX_PRI5_RATE; /* RW 32 0x0000 0044 0x4848 4844                */
		vuint32_t CPDMA_TX_PRI6_RATE; /* RW 32 0x0000 0048 0x4848 4848                */
		vuint32_t CPDMA_TX_PRI7_RATE; /* RW 32 0x0000 004C 0x4848 484C                */
		vuint32_t CPDMA_TX_PRI7_RATE_Padding[(0x4880 - 0x484C - 0x4)/4];
		vuint32_t CPDMA_TX_INTSTAT_RAW; /* R 32 0x0000 0080 0x4848 4880               */
		vuint32_t CPDMA_TX_INTSTAT_MASKED; /* R 32 0x0000 0084 0x4848 4884            */
		vuint32_t CPDMA_TX_INTMASK_SET; /* W 32 0x0000 0088 0x4848 4888               */
		vuint32_t CPDMA_TX_INTMASK_CLEAR; /* W 32 0x0000 008C 0x4848 488C             */
		vuint32_t CPDMA_IN_VECTOR; /* R 32 0x0000 0090 0x4848 4890                    */
		vuint32_t CPDMA_EOI_VECTOR; /* RW 32 0x0000 0094 0x4848 4894                  */
		vuint32_t CPDMA_EOI_VECTOR_Padding[(0x48A0 - 0x4894 - 0x4)/4];
		vuint32_t CPDMA_RX_INTSTAT_RAW; /* R 32 0x0000 00A0 0x4848 48A0               */
		vuint32_t CPDMA_RX_INTSTAT_MASKED; /* R 32 0x0000 00A4 0x4848 48A4            */
		vuint32_t CPDMA_RX_INTMASK_SET; /* RW 32 0x0000 00A8 0x4848 48A8              */
		vuint32_t CPDMA_RX_INTMASK_CLEAR; /* RW 32 0x0000 00AC 0x4848 48AC            */
		vuint32_t CPDMA_DMA_INTSTAT_RAW; /* R 32 0x0000 00B0 0x4848 48B0              */
		vuint32_t CPDMA_DMA_INTSTAT_MASKED; /* R 32 0x0000 00B4 0x4848 48B4           */
		vuint32_t CPDMA_DMA_INTMASK_SET; /* W 32 0x0000 00B8 0x4848 48B8              */
		vuint32_t CPDMA_DMA_INTMASK_CLEAR; /* RW 32 0x0000 00BC 0x4848 48BC           */
		vuint32_t CPDMA_RX0_PENDTHRESH; /* RW 32 0x0000 00C0 0x4848 48C0              */
		vuint32_t CPDMA_RX1_PENDTHRESH; /* RW 32 0x0000 00C4 0x4848 48C4              */
		vuint32_t CPDMA_RX2_PENDTHRESH; /* RW 32 0x0000 00C8 0x4848 48C8              */
		vuint32_t CPDMA_RX3_PENDTHRESH; /* RW 32 0x0000 00CC 0x4848 48CC              */
		vuint32_t CPDMA_RX4_PENDTHRESH; /* RW 32 0x0000 00D0 0x4848 48D0              */
		vuint32_t CPDMA_RX5_PENDTHRESH; /* RW 32 0x0000 00D4 0x4848 48D4              */
		vuint32_t CPDMA_RX6_PENDTHRESH; /* RW 32 0x0000 00D8 0x4848 48D8              */
		vuint32_t CPDMA_RX7_PENDTHRESH; /* RW 32 0x0000 00DC 0x4848 48DC              */
		vuint32_t CPDMA_RX0_FREEBUFFER; /* W 32 0x0000 00E0 0x4848 48E0               */
		vuint32_t CPDMA_RX1_FREEBUFFER; /* W 32 0x0000 00E4 0x4848 48E4               */
		vuint32_t CPDMA_RX2_FREEBUFFER; /* W 32 0x0000 00E8 0x4848 48E8               */
		vuint32_t CPDMA_RX3_FREEBUFFER; /* W 32 0x0000 00EC 0x4848 48EC               */
		vuint32_t CPDMA_RX4_FREEBUFFER; /* W 32 0x0000 00F0 0x4848 48F0               */
		vuint32_t CPDMA_RX5_FREEBUFFER; /* W 32 0x0000 00F4 0x4848 48F4               */
		vuint32_t CPDMA_RX6_FREEBUFFER; /* W 32 0x0000 00F8 0x4848 48F8               */
		vuint32_t CPDMA_RX7_FREEBUFFER; /* W 32 0x0000 00FC 0x4848 48FC               */
	}CPDMA;
	vuint32_t CPDMA_Padding[(0x4A00 - 0x48FC - 0x4)/4];
	struct{
		vuint32_t TX0_HDP; /* RW 32 0x0000 0000 0x4848 4A00                           */
		vuint32_t TX1_HDP; /* RW 32 0x0000 0004 0x4848 4A04                           */
		vuint32_t TX2_HDP; /* RW 32 0x0000 0008 0x4848 4A08                           */
		vuint32_t TX3_HDP; /* RW 32 0x0000 000C 0x4848 4A0C                           */
		vuint32_t TX4_HDP; /* RW 32 0x0000 0010 0x4848 4A10                           */
		vuint32_t TX5_HDP; /* RW 32 0x0000 0014 0x4848 4A14                           */
		vuint32_t TX6_HDP; /* RW 32 0x0000 0018 0x4848 4A18                           */
		vuint32_t TX7_HDP; /* RW 32 0x0000 001C 0x4848 4A1C                           */
		vuint32_t RX0_HDP; /* RW 32 0x0000 0020 0x4848 4A20                           */
		vuint32_t RX1_HDP; /* RW 32 0x0000 0024 0x4848 4A24                           */
		vuint32_t RX2_HDP; /* RW 32 0x0000 0028 0x4848 4A28                           */
		vuint32_t RX3_HDP; /* RW 32 0x0000 002C 0x4848 4A2C                           */
		vuint32_t RX4_HDP; /* RW 32 0x0000 0030 0x4848 4A30                           */
		vuint32_t RX5_HDP; /* RW 32 0x0000 0034 0x4848 4A34                           */
		vuint32_t RX6_HDP; /* RW 32 0x0000 0038 0x4848 4A38                           */
		vuint32_t RX7_HDP; /* RW 32 0x0000 003C 0x4848 4A3C                           */
		vuint32_t TX0_CP; /* RW 32 0x0000 0040 0x4848 4A40                            */
		vuint32_t TX1_CP; /* RW 32 0x0000 0044 0x4848 4A44                            */
		vuint32_t TX2_CP; /* RW 32 0x0000 0048 0x4848 4A48                            */
		vuint32_t TX3_CP; /* RW 32 0x0000 004C 0x4848 4A4C                            */
		vuint32_t TX4_CP; /* RW 32 0x0000 0050 0x4848 4A50                            */
		vuint32_t TX5_CP; /* RW 32 0x0000 0054 0x4848 4A54                            */
		vuint32_t TX6_CP; /* RW 32 0x0000 0058 0x4848 4A58                            */
		vuint32_t TX7_CP; /* RW 32 0x0000 005C 0x4848 4A5C                            */
		vuint32_t RX0_CP; /* RW 32 0x0000 0060 0x4848 4A60                            */
		vuint32_t RX1_CP; /* RW 32 0x0000 0064 0x4848 4A64                            */
		vuint32_t RX2_CP; /* RW 32 0x0000 0068 0x4848 4A68                            */
		vuint32_t RX3_CP; /* RW 32 0x0000 006C 0x4848 4A6C                            */
		vuint32_t RX4_CP; /* RW 32 0x0000 0070 0x4848 4A70                            */
		vuint32_t RX5_CP; /* RW 32 0x0000 0074 0x4848 4A74                            */
		vuint32_t RX6_CP; /* RW 32 0x0000 0078 0x4848 4A78                            */
		vuint32_t RX7_CP; /* RW 32 0x0000 007C 0x4848 4A7C                            */
	}STATERAM;
	vuint32_t STATERAM_Padding[(0x4C00 - 0x4A7C - 0x4)/4];
	struct{
		vuint32_t CPTS_IDVER; /* R 32 0x0000 0000 0x4848 4C00                         */
		vuint32_t CPTS_CONTROL; /* RW 32 0x0000 0004 0x4848 4C04                      */
		vuint32_t CPTS_CONTROL_Padding[(0x4C0C - 0x4C04 - 0x4)/4];
		vuint32_t CPTS_TS_PUSH; /* W 32 0x0000 000C 0x4848 4C0C                       */
		vuint32_t CPTS_TS_LOAD_VAL; /* RW 32 0x0000 0010 0x4848 4C10                  */
		vuint32_t CPTS_TS_LOAD_EN; /* W 32 0x0000 0014 0x4848 4C14                    */
		vuint32_t CPTS_TS_LOAD_EN_Padding[(0x4C20 - 0x4C14 - 0x4)/4];
		vuint32_t CPTS_INTSTAT_RAW; /* RW 32 0x0000 0020 0x4848 4C20                  */
		vuint32_t CPTS_INTSTAT_MASKED; /* R 32 0x0000 0024 0x4848 4C24                */
		vuint32_t CPTS_INT_ENABLE; /* RW 32 0x0000 0028 0x4848 4C28                   */
		vuint32_t CPTS_INT_ENABLE_Padding[(0x4C30 - 0x4C28 - 0x4)/4];
		vuint32_t CPTS_EVENT_POP; /* W 32 0x0000 0030 0x4848 4C30                     */
		vuint32_t CPTS_EVENT_LOW; /* R 32 0x0000 0034 0x4848 4C34                     */
		vuint32_t CPTS_EVENT_HIGH; /* R 32 0x0000 0038 0x4848 4C38                    */
	}CPTS;
	vuint32_t CPTS_Padding[(0x4D00 - 0x4C38 - 0x4)/4];
	struct{
		vuint32_t ALE_IDVER; /* R 32 0x0000 0000 0x4848 4D00                          */
		vuint32_t ALE_IDVER_Padding[(0x4D08 - 0x4D00 - 0x4)/4];
		vuint32_t ALE_CONTROL; /* RW 32 0x0000 0008 0x4848 4D08                       */
		vuint32_t ALE_CONTROL_Padding[(0x4D10 - 0x4D08 - 0x4)/4];
		vuint32_t ALE_PRESCALE; /* RW 32 0x0000 0010 0x4848 4D10                      */
		vuint32_t ALE_PRESCALE_Padding[(0x4D18 - 0x4D10 - 0x4)/4];
		vuint32_t ALE_UNKNOWN_VLAN; /* RW 32 0x0000 0018 0x4848 4D18                  */
		vuint32_t ALE_UNKNOWN_VLAN_Padding[(0x4D20 - 0x4D18 - 0x4)/4];
		vuint32_t ALE_TBLCTL; /* RW 32 0x0000 0020 0x4848 4D20                        */
		vuint32_t ALE_TBLCTL_Padding[(0x4D34 - 0x4D20 - 0x4)/4];
		vuint32_t ALE_TBLW2; /* RW 32 0x0000 0034 0x4848 4D34                         */
		vuint32_t ALE_TBLW1; /* RW 32 0x0000 0038 0x4848 4D38                         */
		vuint32_t ALE_TBLW0; /* RW 32 0x0000 003C 0x4848 4D3C                         */
		vuint32_t ALE_PORTCTL0; /* RW 32 0x0000 0040 0x4848 4D40                      */
		vuint32_t ALE_PORTCTL1; /* RW 32 0x0000 0044 0x4848 4D44                      */
		vuint32_t ALE_PORTCTL2; /* RW 32 0x0000 0048 0x4848 4D48                      */
		vuint32_t ALE_PORTCTL3; /* RW 32 0x0000 004C 0x4848 4D4C                      */
		vuint32_t ALE_PORTCTL4; /* RW 32 0x0000 0050 0x4848 4D50                      */
		vuint32_t ALE_PORTCTL5; /* RW 32 0x0000 0054 0x4848 4D54                      */
	}ALE;
	vuint32_t ALE_Padding[(0x4D80 - 0x4D54 - 0x4)/4];
	struct{
		vuint32_t SL_IDVER; /* R 32 0x0000 0000 0x4848 4D80 0x4848 4DC0               */
		vuint32_t SL_MACCONTROL; /* RW 32 0x0000 0004 0x4848 4D84 0x4848 4DC4         */
		vuint32_t SL_MACSTATUS; /* R 32 0x0000 0008 0x4848 4D88 0x4848 4DC8           */
		vuint32_t SL_SOFT_RESET; /* RW 32 0x0000 000C 0x4848 4D8C 0x4848 4DCC         */
		vuint32_t SL_RX_MAXLEN; /* RW 32 0x0000 0010 0x4848 4D90 0x4848 4DD0          */
		vuint32_t SL_BOFFTEST; /* RW 32 0x0000 0014 0x4848 4D94 0x4848 4DD4           */
		vuint32_t SL_RX_PAUSE; /* R 32 0x0000 0018 0x4848 4D98 0x4848 4DD8            */
		vuint32_t SL_TX_PAUSE; /* R 32 0x0000 001C 0x4848 4D9C 0x4848 4DDC            */
		vuint32_t SL_EMCONTROL; /* RW 32 0x0000 0020 0x4848 4DA0 0x4848 4DE0          */
		vuint32_t SL_RX_PRI_MAP; /* RW 32 0x0000 0024 0x4848 4DA4 0x4848 4DE4         */
		vuint32_t SL_TX_GAP; /* RW 32 0x0000 0028 0x4848 4DA8 0x4848 4DE8             */
	}SL1;
	vuint32_t SL1_Padding[(0x4DC0 - 0x4DA8 - 0x4)/4];
	struct{
		vuint32_t SL_IDVER; /* R 32 0x0000 0000 0x4848 4D80 0x4848 4DC0               */
		vuint32_t SL_MACCONTROL; /* RW 32 0x0000 0004 0x4848 4D84 0x4848 4DC4         */
		vuint32_t SL_MACSTATUS; /* R 32 0x0000 0008 0x4848 4D88 0x4848 4DC8           */
		vuint32_t SL_SOFT_RESET; /* RW 32 0x0000 000C 0x4848 4D8C 0x4848 4DCC         */
		vuint32_t SL_RX_MAXLEN; /* RW 32 0x0000 0010 0x4848 4D90 0x4848 4DD0          */
		vuint32_t SL_BOFFTEST; /* RW 32 0x0000 0014 0x4848 4D94 0x4848 4DD4           */
		vuint32_t SL_RX_PAUSE; /* R 32 0x0000 0018 0x4848 4D98 0x4848 4DD8            */
		vuint32_t SL_TX_PAUSE; /* R 32 0x0000 001C 0x4848 4D9C 0x4848 4DDC            */
		vuint32_t SL_EMCONTROL; /* RW 32 0x0000 0020 0x4848 4DA0 0x4848 4DE0          */
		vuint32_t SL_RX_PRI_MAP; /* RW 32 0x0000 0024 0x4848 4DA4 0x4848 4DE4         */
		vuint32_t SL_TX_GAP; /* RW 32 0x0000 0028 0x4848 4DA8 0x4848 4DE8             */
	}SL2;
	vuint32_t SL2_Padding[(0x5000 - 0x4DE8 - 0x4)/4];
	struct{
		vuint32_t MDIO_VER; /* RW 32 0x0000 0000 0x4848 5000                          */
		vuint32_t MDIO_CONTROL; /* RW 32 0x0000 0004 0x4848 5004                      */
		vuint32_t MDIO_ALIVE; /* RW 32 0x0000 0008 0x4848 5008                        */
		vuint32_t MDIO_LINK; /* R 32 0x0000 000C 0x4848 500C                          */
		vuint32_t MDIO_LINKINTRAW; /* RW 32 0x0000 0010 0x4848 5010                   */
		vuint32_t MDIO_LINKINTMASKED; /* RW 32 0x0000 0014 0x4848 5014                */
		vuint32_t MDIO_LINKINTMASKED_Padding[(0x5020 - 0x5014 - 0x4)/4];
		vuint32_t MDIO_USERINTRAW; /* RW 32 0x0000 0020 0x4848 5020                   */
		vuint32_t MDIO_USERINTMASKED; /* RW 32 0x0000 0024 0x4848 5024                */
		vuint32_t MDIO_USERINTMASKSET; /* RW 32 0x0000 0028 0x4848 5028               */
		vuint32_t MDIO_USERINTMASKCLR; /* RW 32 0x0000 002C 0x4848 502C               */
		vuint32_t MDIO_USERINTMASKCLR_Padding[(0x5080 - 0x502C - 0x4)/4];
		vuint32_t MDIO_USERACCESS0; /* RW 32 0x0000 0080 0x4848 5080                  */
		vuint32_t MDIO_USERPHYSEL0; /* RW 32 0x0000 0084 0x4848 5084                  */
		vuint32_t MDIO_USERACCESS1; /* RW 32 0x0000 0088 0x4848 5088                  */
		vuint32_t MDIO_USERPHYSEL1; /* RW 32 0x0000 008C 0x4848 508C                  */
	}MDIO;
	vuint32_t MDIO_Padding[(0x5200 - 0x508C - 0x4)/4];
	struct{
		vuint32_t WR_IDVER; /* R 32 0x0000 0000 0x4848 5200                           */
		vuint32_t WR_SOFT_RESET; /* RW 32 0x0000 0004 0x4848 5204                     */
		vuint32_t WR_CONTROL; /* RW 32 0x0000 0008 0x4848 5208                        */
		vuint32_t WR_INT_CONTROL; /* RW 32 0x0000 000C 0x4848 520C                    */
		vuint32_t WR_C0_RX_THRESH_EN; /* RW 32 0x0000 0010 0x4848 5210                */
		vuint32_t WR_C0_RX_EN; /* RW 32 0x0000 0014 0x4848 5214                       */
		vuint32_t WR_C0_TX_EN; /* RW 32 0x0000 0018 0x4848 5218                       */
		vuint32_t WR_C0_MISC_EN; /* RW 32 0x0000 001C 0x4848 521C                     */
		vuint32_t WR_C0_MISC_EN_Padding[(0x5240 - 0x521C - 0x4)/4];
		vuint32_t WR_C0_RX_THRESH_STAT; /* R 32 0x0000 0040 0x4848 5240               */
		vuint32_t WR_C0_RX_STAT; /* R 32 0x0000 0044 0x4848 5244                      */
		vuint32_t WR_C0_TX_STAT; /* R 32 0x0000 0048 0x4848 5248                      */
		vuint32_t WR_C0_MISC_STAT; /* R 32 0x0000 004C 0x4848 524C                    */
		vuint32_t WR_C0_MISC_STAT_Padding[(0x5270 - 0x524C - 0x4)/4];
		vuint32_t WR_C0_RX_IMAX; /* RW 32 0x0000 0070 0x4848 5270                     */
		vuint32_t WR_C0_TX_IMAX; /* RW 32 0x0000 0074 0x4848 5274                     */
		vuint32_t WR_C0_TX_IMAX_Padding[(0x5288 - 0x5274 - 0x4)/4];
		vuint32_t WR_RGMII_CTL; /* R 32 0x0000 0088 0x4848 5288                       */
	}WR;
	vuint32_t WR_Padding[(0x6000 - 0x5288 - 0x4)/4];
	vuint8_t *CPPI_RAM; /* 0x4848 6000 8 KiB */
} __attribute__((packed));

/*lint -restore*/

typedef enum{
	ALE_FREE_ENTRY,
	ALE_ADRS_ENTRY,
	ALE_VLAN_ENTRY,
	ALE_VLAN_ADRS_ENTRY
}Eth_AleTablEntryType;

typedef enum{
	ALE_UNI_ADRS_NOT_AGEABLE,
	ALE_UNI_ADRS_AGEABLE_NOT_TOUCHED,
	ALE_UNI_ADRS_OUI_NOT_AGEABLE,
	ALE_UNI_ADRS_AGEABLE_TOUCHED
}Eth_AleTablUnicastType;

typedef enum{
	FIFO_NORMAL_PRIO,
	FIFO_DUAL_MAC,
	FIFO_RATE_LIMIT
}Eth_FifoPrioType;


typedef volatile struct GmacSw_reg GmacSw_HwRegType; /* HW register type */

#endif /* ETH_JACINTO_H_ */
