/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "arc.h"
#include "irq_types.h"

#include "core_cr4.h"

#if defined(CFG_DEBUG_EXCEPTIONS)

/**
 * Function to extract stacked registers
 *
 * @param stackPtr
 */
static void handleException( uint32_t type , uint32_t *stackPtr )
{
    uint32_t r0;
    uint32_t r1;
    uint32_t r2;
    uint32_t r3;
    uint32_t r12;
    uint32_t lr;
    uint32_t pc;
    uint32_t xpsr;
    uint32_t ifar;
    uint32_t ifsr;
    uint32_t dfar;
    uint32_t dfsr;

    r0 = stackPtr[ 0 ];
    r1 = stackPtr[ 1 ];
    r2 = stackPtr[ 2 ];
    r3 = stackPtr[ 3 ];

    r12 = stackPtr[ 12 ];
    lr = stackPtr[ 13 ];
    pc = stackPtr[ 14 ];
    xpsr = stackPtr[ 15 ];

    switch(type) {
    case 0ul:
        /* Prefetch abort */

        /* IFAR holds the faulting address of a Prefetch Abort exception */
        ifar = CoreGetIFAR();

        /*  PMSAv7 IFSR encodings
         * IFSR[3:0]
         *  0001  - Alignment fault
         *  0000  - Background fault, MPU
         *  1101  - Permission fault, MPU
         *  0010  - Debug event
         *  1000  - Synchronous external abort
         *  etc.
         * */
        ifsr = CoreGetIFSR();

        (void)ifar;
        (void)ifsr;

        while(1) {};

    	break;
    case 1ul:
        /* Data abort */

        /* IFAR holds the faulting address of a Prefetch Abort exception */
        dfar = CoreGetDFAR();

        /*   PMSAv7 DFSR encodings
         * DFSR[3:0]
         *  0001  - Alignment fault
         *  0000  - Background fault, MPU
         *  1101  - Permission fault, MPU
         *  0010  - Debug event
         *  1000  - Synchronous external abort
         *  etc.
         * */
        dfsr = CoreGetDFSR();

        (void)dfar;
        (void)dfsr;

        while(1) {};

        break;
    default:
    	/* We should not get here for the other modes */
    	while(1) {};
    	break;
    }

    (void)r0;
    (void)r1;
    (void)r2;
    (void)r3;
    (void)r12;
    (void)pc;
    (void)xpsr;
    (void)lr;
    (void)ifar;

    while(1) {};
}
#endif

void Mcu_Arc_HandleException( uint32_t type, uint32_t *stackPtr )
{
#if defined(CFG_DEBUG_EXCEPTIONS)
    handleException(type,stackPtr);
#else
    Os_Arc_Panic(type,stackPtr);
#endif
}


