/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.3 */

#ifndef XCP_CONFIGTYPES_H_
#define XCP_CONFIGTYPES_H_

#define XCP_IDENTIFICATION_ABSOLUTE              0x0
#define XCP_IDENTIFICATION_RELATIVE_BYTE         0x1
#define XCP_IDENTIFICATION_RELATIVE_WORD         0x2
#define XCP_IDENTIFICATION_RELATIVE_WORD_ALIGNED 0x3

/* ERROR CODES */
typedef enum {
    XCP_ERR_CMD_SYNCH         = 0x00,
    XCP_ERR_CMD_BUSY          = 0x10,
    XCP_ERR_DAQ_ACTIVE        = 0x11,
    XCP_ERR_PGM_ACTIVE        = 0x12,

    XCP_ERR_CMD_UNKNOWN       = 0x20,
    XCP_ERR_CMD_SYNTAX        = 0x21,
    XCP_ERR_OUT_OF_RANGE      = 0x22,
    XCP_ERR_WRITE_PROTECTED   = 0x23,
    XCP_ERR_ACCESS_DENIED     = 0x24,
    XCP_ERR_ACCESS_LOCKED     = 0x25,
    XCP_ERR_PAGE_NOT_VALID    = 0x26,
    XCP_ERR_MODE_NOT_VALID    = 0x27,
    XCP_ERR_SEGMENT_NOT_VALID = 0x28,
    XCP_ERR_SEQUENCE          = 0x29,
    XCP_ERR_DAQ_CONFIG        = 0x2A,

    XCP_ERR_MEMORY_OVERFLOW   = 0x30,
    XCP_ERR_GENERIC           = 0x31,
    XCP_ERR_VERIFY            = 0x32,
} Xcp_ErrorType;

typedef enum {
	XCP_TIMESTAMP_UNIT_1NS = 0x00,
	XCP_TIMESTAMP_UNIT_10NS = 0x01,
	XCP_TIMESTAMP_UNIT_100NS = 0x02,
	XCP_TIMESTAMP_UNIT_1US = 0x03,
	XCP_TIMESTAMP_UNIT_10US = 0x04,
	XCP_TIMESTAMP_UNIT_100US = 0x05,
	XCP_TIMESTAMP_UNIT_1MS = 0x06,
	XCP_TIMESTAMP_UNIT_10MS = 0x07,
	XCP_TIMESTAMP_UNIT_100MS = 0x08,
	XCP_TIMESTAMP_UNIT_1S = 0x09,
    XCP_TIMESTAMP_UNIT_1PS = 0x0A,
    XCP_TIMESTAMP_UNIT_10PS = 0x0B,
    XCP_TIMESTAMP_UNIT_100PS = 0x0C,
} Xcp_TimestampUnitType;

typedef enum  {
    XCP_CHECKSUM_ADD_11      = 0x01,
    XCP_CHECKSUM_ADD_12      = 0x02,
    XCP_CHECKSUM_ADD_14      = 0x03,
    XCP_CHECKSUM_ADD_22      = 0x04,
    XCP_CHECKSUM_ADD_24      = 0x05,
    XCP_CHECKSUM_ADD_44      = 0x06,
    XCP_CHECKSUM_CRC_16      = 0x07,
    XCP_CHECKSUM_CRC_16_CITT = 0x08,
    XCP_CHECKSUM_CRC_32      = 0x09,
    XCP_CHECKSUM_USERDEFINE  = 0xFF,
} Xcp_ChecksumType;


typedef struct Xcp_SegmentStandardInfoReturnType {
	uint8 maxPages;
	uint8 addressExtension;
	uint8 maxMapping;
	uint8 compressionMethod;
	uint8 encryptionMethod;
} Xcp_SegmentStandardInfoReturnType;

typedef struct Xcp_GetSectorInfo_Mode0_1_ReturnType {
	uint8 clearSeqNum;
	uint8 programSeqNum;
	uint8 programMethod;
	uint32 sectorInfo;
} Xcp_GetSectorInfo_Mode0_1_ReturnType;

typedef struct Xcp_MtaType {
	uint8   (*get)  (struct Xcp_MtaType* mta);
    void    (*write)(struct Xcp_MtaType* mta, uint8* data, int len);
    void    (*read) (struct Xcp_MtaType* mta, uint8* data, int len);
    void    (*flush)(struct Xcp_MtaType* mta);
	uint32   address;
    uint8    extension;
} Xcp_MtaType;

typedef struct Xcp_OdtEntryType {
	struct Xcp_OdtEntryType *XcpNextOdtEntry;
	uint32 XcpOdtEntryAddress;
	uint8  XcpOdtEntryLength;
	uint8  XcpOdtEntryNumber; /* 0 .. 254 */
	uint8  BitOffSet;
	uint8  XcpOdtEntryExtension;
} Xcp_OdtEntryType;

struct Xcp_BufferType;

typedef struct Xcp_OdtType {
	struct Xcp_OdtType *XcpNextOdt;
	struct Xcp_BufferType *XcpStim;
	Xcp_OdtEntryType* XcpOdtEntry; /* 0 .. * */
	uint8 XcpMaxOdtEntries; /* XCP_MAX_ODT_ENTRIES */
	uint8 XcpOdtEntriesCount; /* 0 .. 255 */
//	uint8 XcpOdtEntryMaxSize; /* 0 .. 254 */
	uint8 XcpOdtNumber; /* 0 .. 251 */
	uint8 XcpDtoPid; /* 0 .. 251 */
	uint8 XcpOdtEntriesValid; /* Number of non zero entries */
} Xcp_OdtType;

typedef enum {
	XCP_DAQLIST_MODE_SELECTED = 1 << 0,
	XCP_DAQLIST_MODE_STIM = 1 << 1,
	XCP_DAQLIST_MODE_TIMESTAMP = 1 << 4,
	XCP_DAQLIST_MODE_PIDOFF = 1 << 5,
	XCP_DAQLIST_MODE_RUNNING = 1 << 6,
	XCP_DAQLIST_MODE_RESUME = 1 << 7,
} Xcp_DaqListModeEnum;

typedef enum {
	XCP_DAQLIST_PROPERTY_PREDEFINED = 1 << 0,
	XCP_DAQLIST_PROPERTY_EVENTFIXED = 1 << 1,
	XCP_DAQLIST_PROPERTY_DAQ = 1 << 2,
	XCP_DAQLIST_PROPERTY_STIM = 1 << 3
} Xcp_DaqListPropertyEnum;

typedef struct {
	uint8 Prescaler;
	uint8 Priority;
	Xcp_DaqListModeEnum     Mode; /**< bitfield for the current mode of the DAQ list */
	Xcp_DaqListPropertyEnum Properties; /**< bitfield for the properties of the DAQ list */
	uint16 EventChannel; /* NOTE: Fixed channel vs current */
} Xcp_DaqListParams;

typedef struct Xcp_DaqListType {
	/**
	 * Pointer to an array of ODT structures this DAQ list will use
	 *   [USER]: With static DAQ lists, this needs to be set
	 *           to an array of XcpMaxOdt size.
	 *   [INTERNAL]: With dynamic DAQ configuration.
	 */
	Xcp_OdtType *XcpOdt; /**< reference to an array of Odt's configured for this Daq list */

	/**
	 * Pointer to next allocated DAQ list
	 *   [INTERNAL]
	 */
	struct Xcp_DaqListType *XcpNextDaq;

	/**
	 * Index number of DAQ list
	 *   [INTERNAL]
	 *
	 * 0 .. 65534
	 */
	uint16 XcpDaqListNumber; /* 0 .. 65534 */

	/**
	 * Maximum number of ODT's in XcpOdt array
	 *   [USER]    : When static DAQ configuration
	 *   [INTERNAL]: Dynamic DAQ configuration.
	 * 0 .. 252
	 */
	uint8 XcpMaxOdt;

	/**
	 * Number of currently configured ODT's
	 *   [USER]    : If you have predefined DAQ lists
	 *   [INTERNAL]: If daq lists are configured by master
	 */
	uint8 XcpOdtCount; /* 0 .. 252 */

	/**
	 * Holds parameters for the DAQ list
	 *   [INTERNAL/USER]
	 *   IMPROVEMENT: Move the parameters into the DAQ list structure instead
	 */
	Xcp_DaqListParams XcpParams;

} Xcp_DaqListType;

typedef enum {
	XCP_EVENTCHANNEL_PROPERTY_DAQ  = 1 << 2,
	XCP_EVENTCHANNEL_PROPERTY_STIM = 1 << 3,
	XCP_EVENTCHANNEL_PROPERTY_ALL  = XCP_EVENTCHANNEL_PROPERTY_DAQ | XCP_EVENTCHANNEL_PROPERTY_STIM,
} Xcp_EventChannelPropertyEnum;

typedef struct {
	/**
	 * Pointer to an array of pointers to daqlists
	 *   [USER]
	 */
	Xcp_DaqListType** XcpEventChannelTriggeredDaqListRef;

	/**
	 * Set to the name of the eventchannel or NULL
	 *   [USER]
	 */
	const char* XcpEventChannelName;

	/**
	 * Event channel number.
	 *   [USER]
	 *
	 * Should match the order in the array of event channels
	 */
	const uint16 XcpEventChannelNumber; /**< 0 .. 65534 */

	/**
	 * Priority of event channel (0 .. 255)
	 *   [IGNORED]
	 */
	const uint8 XcpEventChannelPriority;

	/**
	 * Maximum number of entries in XcpEventChannelTriggeredDaqListRef
	 *   [USER]
	 *
	 * 1 .. 255
	 * 0 = Unlimited
	 */
	const uint8 XcpEventChannelMaxDaqList;

	/**
	 * Bitfield defining supported features
	 *   [USER]
	 */
	const Xcp_EventChannelPropertyEnum XcpEventChannelProperties;

	/**
	 * Cycle unit of event channel
	 *   [USER]
	 *
	 * Set to 0 (XCP_TIMESTAMP_UNIT_1NS) if channel is not
	 * cyclic.
	 */
	const Xcp_TimestampUnitType XcpEventChannelUnit;

	/**
	 * Number of cycle units between each trigger of event
	 *   [USER]
	 *
	 * 0 .. 255
	 * 0 -> non cyclic
	 */
	const uint8 XcpEventChannelRate;

	/**
	 * Counter used to implement event cycle counting
	 *   [INTERNAL]
	 */
	uint8 XcpEventChannelRateCounter;

	/**
	 * Counter used to implement prescaling
	 *   [INTERNAL]
	 */
	uint8 XcpEventChannelCounter;

	/**
	 * Number of daq lists currently assigned to event channel
	 *   [INTERNAL]
	 */
	uint8 XcpEventChannelDaqCount;

} Xcp_EventChannelType;

typedef enum {
	XCP_ACCESS_ECU_ACCESS_WITHOUT_XCP = 1 << 0,
	XCP_ACCESS_ECU_ACCESS_WITH_XCP = 1 << 1,
	XCP_ACCESS_XCP_READ_ACCESS_WITHOUT_ECU = 1 << 2,
	XCP_ACCESS_READ_ACCESS_WITH_ECU = 1 << 3,
	XCP_ACCESS_XCP_WRITE_ACCESS_WITHOUT_ECU = 1 << 4,
	XCP_ACCESS_XCP_WRITE_ACCESS_WITH_ECU = 1 << 5,
	XCP_ACCESS_ALL = 0x3f
} Xcp_AccessFlagsType;

typedef enum {
	XCP_PROTECT_NONE = 0,
	XCP_PROTECT_CALPAG = 1 << 0,
	XCP_PROTECT_DAQ = 1 << 2,
	XCP_PROTECT_STIM = 1 << 3,
	XCP_PROTECT_PGM = 1 << 4,
} Xcp_ProtectType;

typedef enum {
	XCP_COMPRESSION_METHOD_NONE = 0,
} Xcp_CompressType;

typedef enum {
	XCP_ENCRYPTION_METHOD_NONE = 0,
} Xcp_EncryptionType;

typedef struct {
	uint32 XcpSrc;
	uint32 XcpDst;
	uint32 XcpLen;
} Xcp_MemoryMappingType;

typedef struct {
	Xcp_MemoryMappingType* XcpMapping;
	uint32 XcpAddress;
	uint32 XcpLength;
	uint32 XcpMaxMapping;
	uint8  XcpMaxPage;
	uint8  XcpPageXcp;
	uint8  XcpPageEcu;
	uint8  XcpExtension;
	const  Xcp_AccessFlagsType XcpAccessFlags;
	Xcp_CompressType   XcpCompression;
	Xcp_EncryptionType XcpEncryption;
} Xcp_SegmentType;

typedef struct {
	const char* XcpCaption; /**< ASCII text describing device [USER] */
	const char* XcpMC2File; /**< ASAM-MC2 filename without path and extension [USER] */
	const char* XcpMC2Path; /**< ASAM-MC2 filename with path and extension [USER] */
	const char* XcpMC2Url; /**< ASAM-MC2 url to file [USER] */
	const char* XcpMC2Upload; /**< ASAM-MC2 file to upload [USER] */
} Xcp_InfoType;

/** Collection of runtime parameter*/
typedef struct {

	/** Pointer to the statically allocated DAQ Lists*/
	Xcp_DaqListType *XcpDaqList;

	/** Statically allocated DAQ Lists size*/
	uint16 XcpDaqListSize;

} Xcp_RunTimeType;

/** Xcp Ram*/
typedef struct {

#if	(XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC)
	/** Daq      counter which shows the dynamically allocated number of Daqs */
	uint16 cntrDynamicDaq;

	/** Odt      counter which shows the dynamically allocated number of Odts */
	uint8  cntrDynamicOdt;

	/** OdtEntry counter which shows the dynamically allocated number of OdtEntries */
	uint8  cntrDynamicOdtEntry;
#endif /*XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC*/

	/** Runtime parameter*/
	uint16 XcpMaxDaq; /* 0 .. 65535, XCP_MAX_DAQ */

	/** Runtime parameter*/
	Xcp_ProtectType XcpProtect;

} Xcp_RamType;

/** @req 4.1.3/SWS_Xcp_00845 *//*Xcp_ConfigType definition - can not be tested with conventional module tests*/
typedef struct {
	/** Collection of runtime parameter*/
	Xcp_RunTimeType *rt;

#if(XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC)

	/** Daq      references for dynamic Daq allocation */
	Xcp_DaqListType*  ptrDynamicDaq;

	/** Odt      references for dynamic Daq allocation */
	Xcp_OdtType*      ptrDynamicOdt;

	/** OdtEntry references for dynamic Daq allocation */
	Xcp_OdtEntryType* ptrDynamicOdtEntry;

#endif /*XCP_DAQ_CONFIG_TYPE == DAQ_DYNAMIC*/

	/**
	 * Function is used to initialize MTA (Memory Transfer Address) and its proper functions to read and write
	 * that memory sections
	 * @param mta (output) Memory Transfer Address
	 * @param address (input) This value is sent by XCP Master tool
	 * @param extension (input) This value is sent by XCP Master tool
	 */
	void (*XcpMtaInit)(
			Xcp_MtaType* mta,
			uint32 address,
			uint8 extension);

	/**
	 * Function used for Seed & Key unlock
	 * @param res is the resource requested to be unlocked
	 * @param seed is the seed that was sent to the master
	 * @param seed_len is the length of @param seed
	 * @param key is the key sent from master
	 * @param key_len is the length of @param key
	 * @return E_OK for success, E_ERR for failure
	 */
	Std_ReturnType (*XcpUnlockFn)(
			Xcp_ProtectType res,
			const uint8* seed,
			uint8 seed_len,
			const uint8* key,
			uint8 key_len);
	/**
	 * Function called to retrieve seed that should be
	 * sent to master in a GetSeed/Unlock exchange
	 * @param res is the resource requested
	 * @param seed pointer to buffer that will hold seed (255 bytes max)
	 * @return number of bytes in seed
	 */
	uint8 (*XcpSeedFn)(
			Xcp_ProtectType res,
			uint8* seed);

	/**
	 * Function called for a XCP user defined call from the master
	 * @param data data recieved from master (excluding the preceding 0xF1 for user defined)
	 * @param len  length of the data in buffer
	 * @return
	 */
	Std_ReturnType (*XcpUserFn)(
			void* data,
			int len);


#if(XCP_FEATURE_CALPAG == STD_ON)

	/**
	 *
	 * @param mode
	 * @param segm
	 * @param page
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpSetCalPage)(
			const uint8 mode,
			const uint8 segm,
			const uint8 page,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param mode
	 * @param segm
	 * @param page
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetCalPage)(
			const uint8 mode,
			const uint8 segm,
			      uint8 *page,
			      Xcp_ErrorType *error);

	/**
	 *
	 * @param sourceSegm
	 * @param sourcePage
	 * @param destSegm
	 * @param destPage
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpCopyCalPage)(
			const uint8 sourceSegm,
			const uint8 sourcePage,
			const uint8 destSegm,
			const uint8 destPage,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param maxSegment
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetPagProcessorInfo)(
			uint8 *maxSegment,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param segmentNumber
	 * @param segmentInfo
	 * @param basicInfo
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetSegmentInfo_GetBasicAddress)(
			const uint8 segmentNumber,
			const uint8 segmentInfo,
			      uint32 *basicInfo,
			      Xcp_ErrorType *error);

	/**
	 *
	 * @param segmentNumber
	 * @param responseData
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetSegmentInfo_GetStandardInfo)(
			const uint8 segmentNumber,
			      Xcp_SegmentStandardInfoReturnType *responseData,
			      Xcp_ErrorType *error);

	/**
	 *
	 * @param segmentNumber
	 * @param segmentInfo
	 * @param mappingInfo
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetSegmentInfo_GetAddressMapping)(
			const uint8 segmentNumber,
			const uint8 segmentInfo,
			      uint32 *mappingInfo,
			      Xcp_ErrorType *error);

	/**
	 *
	 * @param segmentNumber
	 * @param pageNumber
	 * @param pageProperties
	 * @param initSegment
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetPageInfo)(
			const uint8 segmentNumber,
			const uint8 pageNumber,
			      uint8 *pageProperties,
			      uint8 *initSegment,
			      Xcp_ErrorType *error);

	/**
	 *
	 * @param mode
	 * @param segmentNumber
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpSetSegmentMode)(
			const uint8 mode,
			const uint8 segmentNumber,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param segmentNumber
	 * @param mode
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetSegmentMode)(
			const uint8 segmentNumber,
			      uint8 *mode,
			      Xcp_ErrorType *error);

#endif /*XCP_FEATURE_CALPAG == STD_ON*/

#if(XCP_FEATURE_PGM == STD_ON)
	/**
	 *
	 * @param clearRange
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgramClear_FunctionalAccess)(
			const uint32 clearRange,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param mta
	 * @param clearRange
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgramClear_AbsoluteAccess)(
			Xcp_MtaType* mta,
			uint32 clearRange,
			Xcp_ErrorType *error);
    /**
     *
     * @param error
     * @return
     */
	Std_ReturnType (*XcpProgramReset)(
			Xcp_ErrorType *error);

	/**
	 *
	 * @param pgmProperties
	 * @param maxSector
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetPgmProcessorInfo)(
			uint8 *pgmProperties,
			uint8 *maxSector,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param mode
	 * @param sectorNumber
	 * @param responseData
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpGetSectorInfo_Mode0_1)(
			const uint8 mode,
			const uint8 sectorNumber,
			Xcp_GetSectorInfo_Mode0_1_ReturnType *responseData,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param sectorNameLength
	 * @param error
	 * @return
	 */
    Std_ReturnType (*XcpGetSectorInfo_Mode2)(
    		uint8 *sectorNameLength,
    		Xcp_ErrorType *error);

	/**
	 *
	 * @param mta
	 * @param codeSize
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgramPrepare)(
			Xcp_MtaType* mta,
			uint16 codeSize,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param compressionMethod
	 * @param encryptionMethod
	 * @param programmingMethod
	 * @param accessMethod
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgramFormat)(
			const uint8 compressionMethod,
			const uint8 encryptionMethod,
			const uint8 programmingMethod,
			const uint8 accessMethod,
			Xcp_ErrorType *error);

	/**
	 *
	 * @param mode
	 * @param type
	 * @param value
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgramVerify)(
			const uint8 mode,
			const uint8 type,
			const uint8 value,
			Xcp_ErrorType *error);

	/**
	 * Flash algorithm must be place here, Post increment of the MTA shall be performed here
	 * @param mta
	 * @param data
	 * @param len
	 * @param error
	 * @return
	 */
	Std_ReturnType (*XcpProgram)(
			Xcp_MtaType* mta,
			uint8* data,
			uint32 len,
			Xcp_ErrorType *error);

#endif /*XCP_FEATURE_PGM == STD_ON*/



	/**
	 * This function is responsible to calculate the intended checksum algorithm, at the given example
	 * XCP_CHECKSUM_ADD_11 is used
	 * @param mtaPtr
	 * @param block (input) length of the block where the CRC is calculated
	 * @param type (output) type of the CRC calculation.
	 * @param response (output) the result of the CRC calculation over the block
	 */
	void (*XcpBuildChecksum)(
			Xcp_MtaType*      mtaPtr,
			uint32            block,
			Xcp_ChecksumType* type,
			uint32*           response);

	/** Config and Runtime parameter*/
	Xcp_EventChannelType *XcpEventChannel;

	/** Config parameter*/
	const uint16 XcpMaxEventChannel; /* 0 .. 65535, XCP_MAX_EVENT_CHANNEL */

	/** Config parameter*/
	const Xcp_InfoType XcpInfo;

	/** Config parameter*/
	const Xcp_ProtectType XcpOriginalProtect; /**< Bitfield with currently locked features (Xcp_ProtectType) */

} Xcp_ConfigType;


extern const Xcp_ConfigType XcpConfig;

#endif /* XCP_CONFIGTYPES_H_ */
