/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.3 */

#ifndef XCP_INTERNAL_H_
#define XCP_INTERNAL_H_


#include "Xcp.h"
#include "SchM_Xcp.h"
#include <string.h>
#include <stdlib.h>

#if(XCP_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif

#include "Os.h"
#include "ComStack_Types.h"

#include "debug.h"
#undef  DEBUG_LVL
#define DEBUG_LVL DEBUG_HIGH

#if !defined(HIGH_BYTE_FIRST)
#	define HIGH_BYTE_FIRST     0U
#endif

#if !defined(LOW_BYTE_FIRST)
#	define LOW_BYTE_FIRST      1U
#endif

#if !defined(CPU_BYTE_ORDER)
#	error Unknown machine endian
#endif

/* This definitions are coming from ASR XCP Spec to identify Api Ids for Det report*/
#define XCP_API_ID_XCP_INIT                0x00u
#define XCP_API_ID_XCP_GETVERSIONINFO      0x01u
#define XCP_API_ID_XCP_RX_INDICATION       0x03u
#define XCP_API_ID_XCP_TX_CONFIRMATION     0x02u
#define XCP_API_ID_XCP_TRIGGERTRANSMIT     0x41u
#define XCP_API_ID_XCP_SETTRANSMISSIONMODE 0x05u
#define XCP_API_ID_XCP_MAINFUNCTION        0x04u

/* This definitions are necessary to identify additional Api Ids for Det report*/
#define XCP_API_ID_XCP_FIFO_GET            0xFFu
#define XCP_API_ID_XCP_FIFO_PUT            0xFEu
#define XCP_API_ID_XCP_FIFO_FREE           0xFDu
#define XCP_API_ID_XCP_FIFO_INIT           0xFBu
#define XCP_API_ID_XCP_PROCESSDAQ          0xFAu
#define XCP_API_ID_XCP_PROCESSCHANNEL      0xF9u

/* HELPER DEFINES */
#define RETURN_ERROR(code, ...) do {      \
        DEBUG(DEBUG_HIGH,## __VA_ARGS__ );\
        Xcp_TxError(code);                \
        return E_NOT_OK;                  \
    } while(0)

#define RETURN_SUCCESS() do { \
        Xcp_TxSuccess();      \
        return E_OK;          \
    } while(0)

#if(XCP_DEV_ERROR_DETECT == STD_ON)
#   define DET_VALIDATE(_exp, ApiId, ErrorId) \
        if( !(_exp) ) { \
          (void)Det_ReportError(XCP_MODULE_ID, 0, ApiId, ErrorId); \
        }
#   define DET_VALIDATE_NRV(_exp, ApiId, ErrorId) \
        if( !(_exp) ) { \
          (void)Det_ReportError(XCP_MODULE_ID, 0, ApiId, ErrorId); \
          return; \
        }
#   define DET_VALIDATE_RV(_exp, ApiId, ErrorId, Return) \
        if( !(_exp) ) { \
          (void)Det_ReportError(XCP_MODULE_ID, 0, ApiId, ErrorId); \
          return Return; \
        }
#   define DET_REPORTERROR(ApiId,ErrorId) (void)Det_ReportError(XCP_MODULE_ID, 0, ApiId, ErrorId)
#else
#   define DET_VALIDATE(_exp, ApiId, ErrorId)
#   define DET_VALIDATE_NRV(_exp, ApiId, ErrorId )
#   define DET_VALIDATE_RV(_exp, ApiId, ErrorId, Return)
#   define DET_REPORTERROR(ApiId,ErrorId)
#endif

#define XCP_ELEMENT_OFFSET(base) (XCP_ELEMENT_SIZE - 1 - ( (base)+XCP_ELEMENT_SIZE-1 ) % XCP_ELEMENT_SIZE )

#if (XCP_MAX_DTO > XCP_MAX_CTO)
#	define XCP_MAX_BUFFER XCP_MAX_DTO
#else
#   define XCP_MAX_BUFFER XCP_MAX_CTO
#endif

/*********************    COMMANDS    *****************/

/* STANDARD COMMANDS */
                                                    /* OPTIONAL */
#define XCP_PID_CMD_STD_CONNECT                 0xFF    // N
#define XCP_PID_CMD_STD_DISCONNECT              0xFE    // N
#define XCP_PID_CMD_STD_GET_STATUS              0xFD    // N
#define XCP_PID_CMD_STD_SYNCH                   0xFC    // N
#define XCP_PID_CMD_STD_GET_COMM_MODE_INFO      0xFB    // Y
#define XCP_PID_CMD_STD_GET_ID                  0xFA    // Y
#define XCP_PID_CMD_STD_SET_REQUEST             OxF9    // Y
#define XCP_PID_CMD_STD_GET_SEED                0xF8    // Y
#define XCP_PID_CMD_STD_UNLOCK                  0xF7    // Y
#define XCP_PID_CMD_STD_SET_MTA                 0xF6    // Y
#define XCP_PID_CMD_STD_UPLOAD                  0xF5    // Y
#define XCP_PID_CMD_STD_SHORT_UPLOAD            0xF4    // Y
#define XCP_PID_CMD_STD_BUILD_CHECKSUM          0xF3    // Y
#define XCP_PID_CMD_STD_TRANSPORT_LAYER_CMD     0xF2    // Y
#define XCP_PID_CMD_STD_USER_CMD                0xF1    // Y

/* CALIBRATION COMMANDS */
                                                    /* OPTIONAL */
#define XCP_PID_CMD_CAL_DOWNLOAD                0xF0    // N
#define XCP_PID_CMD_CAL_DOWNLOAD_NEXT           0xEF    // Y
#define XCP_PID_CMD_CAL_DOWNLOAD_MAX            0xEE    // Y
#define XCP_PID_CMD_CAL_SHORT_DOWNLOAD          0xED    // Y
#define XCP_PID_CMD_CAL_MODIFY_BITS             0xEC    // Y

/* PAGE SWITCHING COMMANDS */
                                                    /* OPTIONAL */
#define XCP_PID_CMD_PAG_SET_CAL_PAGE            0xEB    // Y
#define XCP_PID_CMD_PAG_GET_CAL_PAGE            0xEA    // Y
#define XCP_PID_CMD_PAG_GET_PAG_PROCESSOR_INFO  0xE9    // Y
#define XCP_PID_CMD_PAG_GET_SEGMENT_INFO        0xE8    // Y
#define XCP_PID_CMD_PAG_GET_PAGE_INFO           0xE7    // Y
#define XCP_PID_CMD_PAG_SET_SEGMENT_MODE        0xE6    // Y
#define XCP_PID_CMD_PAG_GET_SEGMENT_MODE        0xE5    // Y
#define XCP_PID_CMD_PAG_COPY_CAL_PAGE           0xE4    // Y

/* DATA SCQUISITION AND STIMULATION COMMANDS */
                                                    /* OPTIONAL */
#define XCP_PID_CMD_DAQ_CLEAR_DAQ_LIST          0xE3    // N
#define XCP_PID_CMD_DAQ_SET_DAQ_PTR             0xE2    // N
#define XCP_PID_CMD_DAQ_WRITE_DAQ               0xE1    // N
#define XCP_PID_CMD_DAQ_SET_DAQ_LIST_MODE       0xE0    // N
#define XCP_PID_CMD_DAQ_GET_DAQ_LIST_MODE       0xDF    // N
#define XCP_PID_CMD_DAQ_START_STOP_DAQ_LIST     0xDE    // N
#define XCP_PID_CMD_DAQ_START_STOP_SYNCH        0xDD    // N
#define XCP_PID_CMD_DAQ_GET_DAQ_CLOCK           0xDC    // Y
#define XCP_PID_CMD_DAQ_READ_DAQ                0xDB    // Y
#define XCP_PID_CMD_DAQ_GET_DAQ_PROCESSOR_INFO  0xDA    // Y
#define XCP_PID_CMD_DAQ_GET_DAQ_RESOLUTION_INFO 0xD9    // Y
#define XCP_PID_CMD_DAQ_GET_DAQ_LIST_INFO       0xD8    // Y
#define XCP_PID_CMD_DAQ_GET_DAQ_EVENT_INFO      0xD7    // Y
#define XCP_PID_CMD_DAQ_FREE_DAQ                0xD6    // Y
#define XCP_PID_CMD_DAQ_ALLOC_DAQ               0xD5    // Y
#define XCP_PID_CMD_DAQ_ALLOC_ODT               0xD4    // Y
#define XCP_PID_CMD_DAQ_ALLOC_ODT_ENTRY         0xD3    // Y

/* NON-VOLATILE MEMORY PROGRAMMING COMMANDS */
                                                    /* OPTIONAL */
#define XCP_PID_CMD_PGM_PROGRAM_START           0xD2    // N
#define XCP_PID_CMD_PGM_PROGRAM_CLEAR           0xD1    // N
#define XCP_PID_CMD_PGM_PROGRAM                 0xD0    // N
#define XCP_PID_CMD_PGM_PROGRAM_RESET           0xCF    // N
#define XCP_PID_CMD_PGM_GET_PGM_PROCESSOR_INFO  0xCE    // Y
#define XCP_PID_CMD_PGM_GET_SECTOR_INFO         0xCD    // Y
#define XCP_PID_CMD_PGM_PROGRAM_PREPARE         0xCC    // Y
#define XCP_PID_CMD_PGM_PROGRAM_FORMAT          0xCB    // Y
#define XCP_PID_CMD_PGM_PROGRAM_NEXT            0xCA    // Y
#define XCP_PID_CMD_PGM_PROGRAM_MAX             0xC9    // Y
#define XCP_PID_CMD_PGM_PROGRAM_VERIFY          0xC8    // Y

/* STIM LISTS */
#define XCP_PID_CMD_STIM_LAST                   0xBF    // Y


/* Exclusive areas */
#define SchM_Enter_Xcp(_area) SchM_Enter_Xcp_##_area()
#define SchM_Exit_Xcp(_area) SchM_Exit_Xcp_##_area()

#define XCP_UNUSED(x) (void)(x)

#define GET_UINT8(data, offset)  (*((uint8* )(data)+(offset)))
#define GET_UINT16(data, offset) (*(uint16*)((uint8*)(data)+(offset)))
#define GET_UINT32(data, offset) (*(uint32*)((uint8*)(data)+(offset)))

#define SET_UINT8(data, offset, value) do {           \
         (*(uint8* )((uint8*)(data)+(offset))) = (value); \
        } while(0)

#define SET_UINT16(data, offset, value) do {            \
         (*(uint16*)((uint8*)(data)+(offset))) = (value); \
        } while(0)

#define SET_UINT32(data, offset, value) do {            \
         (*(uint32*)((uint8*)(data)+(offset))) = (value); \
        } while(0)

#define FIFO_GET_WRITE(fifo, it) \
    for(Xcp_BufferType* it = Xcp_Fifo_Get(fifo.free); it; Xcp_Fifo_Put(&fifo, it), it = NULL)

#define FIFO_FOR_READ(fifo, it) \
    for(Xcp_BufferType* it = Xcp_Fifo_Get(&fifo); it; Xcp_Fifo_Free(&fifo, it), it = Xcp_Fifo_Get(&fifo))

#define FIFO_ADD_U8(fifo, value) \
    do { SET_UINT8(fifo->data, fifo->len, value); fifo->len+=1; } while(0)

#define FIFO_ADD_U16(fifo, value) \
    do { SET_UINT16(fifo->data, fifo->len, value); fifo->len+=2; } while(0)

#define FIFO_ADD_U32(fifo, value) \
    do { SET_UINT32(fifo->data, fifo->len, value); fifo->len+=4; } while(0)

/* REPLY TYPES */
typedef enum {
    XCP_PID_RES  = 0xFF,
    XCP_PID_ERR  = 0xFE,
    XCP_PID_EV   = 0xFD,
    XCP_PID_SERV = 0xFC,
} Xcp_ReplyType;

/* EVENT CODES */
typedef enum {
    XCP_EV_RESUME_MODE        = 0x00,
    XCP_EV_CLEAR_DAQ          = 0x01,
    XCP_EV_STORE_DAQ          = 0x02,
    XCP_EV_STORE_CAL          = 0x03,
    XCP_EV_CMD_PENDING        = 0x05,
    XCP_EV_DAQ_OVERLOAD       = 0x06,
    XCP_EV_SESSION_TERMINATED = 0x07,
    XCP_EV_USER               = 0xFE,
    XCP_EV_TRANSPORT          = 0xFF,
} Xcp_EventType;


/* COMMAND LIST FUNCTION CALLBACK */

typedef Std_ReturnType (*Xcp_CmdFuncType)(uint8, void*, int);
typedef void           (*Xcp_CmdWorkType)(void);

typedef struct {
    Xcp_CmdFuncType fun;  /**< pointer to function to use */
    uint8           len;  /**< minimum length of command  */
    uint8           lock; /**< locked by following types  (Xcp_ProtectType) */
} Xcp_CmdListType;



/* INTERNAL STATE STRUCTURES */

typedef enum {
    XCP_DYNAMIC_STATE_UNDEFINED = 0,
    XCP_DYNAMIC_STATE_FREE_DAQ,
    XCP_DYNAMIC_STATE_ALLOC_DAQ,
    XCP_DYNAMIC_STATE_ALLOC_ODT,
    XCP_DYNAMIC_STATE_ALLOC_ODT_ENTRY,
} Xcp_DaqListConfigStateEnum;

typedef struct {
    Xcp_OdtEntryType*   ptr;
    Xcp_OdtType*        odt;
    Xcp_DaqListType*    daq;
    Xcp_DaqListConfigStateEnum dyn;
} Xcp_DaqPtrStateType;

typedef struct {
    int         len; /**< Original upload size */
    int         rem; /**< Remaining upload size */
} Xcp_TransferType;


typedef struct {
    uint8           seed[255];
    uint8           seed_len;
    uint8           seed_rem;
    uint8           key[255];
    uint8           key_len;
    uint8           key_rem;
    Xcp_ProtectType res;
} Xcp_UnlockType;


/* RX/TX FIFO */

typedef struct Xcp_BufferType {
    struct Xcp_BufferType* next;
	uint32                 len;
    uint8                  data[XCP_MAX_BUFFER];
} Xcp_BufferType;

typedef struct Xcp_FifoType {
    Xcp_BufferType*        front;
    Xcp_BufferType*        back;
    struct Xcp_FifoType*   free;
    void*                  lock;
} Xcp_FifoType;

/* INTERNAL GLOBAL VARIABLES */
extern       Xcp_FifoType    Xcp_FifoRxCto;
extern       Xcp_FifoType    Xcp_FifoTxCto;
extern       Xcp_MtaType     Xcp_Mta;
extern       uint8           Xcp_Inited;
extern       uint8           Xcp_Connected;
extern const Xcp_ConfigType* xcpPtr;

/* This function is used to enter exclusive area to avoid data correction.*/
static inline void Xcp_Fifo_Lock(void)
{
	SchM_Enter_Xcp_EA_0();
}

/* This function is used to exit from exclusive area.*/
static inline void Xcp_Fifo_Unlock(void)
{
	SchM_Exit_Xcp_EA_0();
}

/* This function retrieves the fist element from the FIFO q.*/
static inline Xcp_BufferType* Xcp_Fifo_Get(Xcp_FifoType* q)
{
	DET_VALIDATE_RV(q, XCP_API_ID_XCP_FIFO_GET, XCP_E_NULL_POINTER,NULL);

    Xcp_Fifo_Lock();
    Xcp_BufferType* b = q->front;
    if(b == NULL) {
    	/*FIFO is empty*/
        Xcp_Fifo_Unlock();
        return NULL;
    }

    /*retrieve b from the FIFO*/
    q->front = b->next;
    if(q->front == NULL) {
        q->back = NULL;
    }
    b->next = NULL;
    Xcp_Fifo_Unlock();
    return b;
}

/* This function add the new element b to the FIFO q.*/
static inline void Xcp_Fifo_Put(Xcp_FifoType* q, Xcp_BufferType* b)
{
	DET_VALIDATE_NRV(q, XCP_API_ID_XCP_FIFO_PUT, XCP_E_NULL_POINTER);
	DET_VALIDATE_NRV(b, XCP_API_ID_XCP_FIFO_PUT, XCP_E_NULL_POINTER);

    Xcp_Fifo_Lock();
    b->next = NULL;
    if(q->back) {
        q->back->next = b; /*add pointer to b from the previous element*/
    } else {
    	q->front = b; /*if there is no previous element add as first element*/
    }
    q->back = b;
    Xcp_Fifo_Unlock();
}

/* This function release one element b by adding it to q->free FIFO.*/
static inline void Xcp_Fifo_Free(Xcp_FifoType* q, Xcp_BufferType* b)
{
	DET_VALIDATE_NRV(q, XCP_API_ID_XCP_FIFO_FREE, XCP_E_NULL_POINTER);
	DET_VALIDATE_NRV(b, XCP_API_ID_XCP_FIFO_FREE, XCP_E_NULL_POINTER);

    if(b) {
        b->len = 0;
        Xcp_Fifo_Put(q->free, b);
    }
}

/* This function initialize a FIFO q by the content defined by b and e pointer as start
 * and end pointer respectively.*/
static inline void Xcp_Fifo_Init(Xcp_FifoType* q, Xcp_BufferType* b, Xcp_BufferType* e)
{
	DET_VALIDATE_NRV(q, XCP_API_ID_XCP_FIFO_INIT, XCP_E_NULL_POINTER);
	DET_VALIDATE_NRV(b, XCP_API_ID_XCP_FIFO_INIT, XCP_E_NULL_POINTER);
	DET_VALIDATE_NRV(e, XCP_API_ID_XCP_FIFO_INIT, XCP_E_NULL_POINTER);

    q->front = NULL;
    q->back  = NULL;
    q->lock  = NULL;
    for(;b != e; b++) {
    	Xcp_Fifo_Put(q, b);
    }
}

/* This kind of memory extension is used by XCP.c for reading or writing some general information
 * from XCP reserved memory (Ram), or reading from XCP Config (Flash) */
#define XCP_MTA_EXTENSION_MEMORY_INTERNAL_FIXED 0xFFu

/* MTA HELPER FUNCTIONS */
static inline void  Xcp_MtaFlush(Xcp_MtaType* mta)                       { if(mta->flush) mta->flush(mta); } /**< Will flush any remaining data to write */
static inline void  Xcp_MtaWrite(Xcp_MtaType* mta, uint8* data, int len) { mta->write(mta, data, len); }
static inline void  Xcp_MtaRead (Xcp_MtaType* mta, uint8* data, int len) { mta->read(mta, data, len);}
static inline uint8 Xcp_MtaGet  (Xcp_MtaType* mta)                       { return mta->get(mta); }


/* PROGRAMMING COMMANDS */
extern Std_ReturnType Xcp_CmdProgramStart(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramClear(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgram(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramReset(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramInfo(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetPgmProcessorInfo(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetSectorInfo(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramPrepare(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramFormat(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdProgramVerify(uint8 pid, void* data, int len);

/* ONLINE CALIBRATION COMMANDS*/
extern Std_ReturnType Xcp_CmdSetSegmentMode(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetSegmentMode(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdSetCalPage(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetCalPage(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetPageInfo(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdCopyCalPage(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetPagProcessorInfo(uint8 pid, void* data, int len);
extern Std_ReturnType Xcp_CmdGetSegmentInfo(uint8 pid, void* data, int len);

/* CALLBACK FUNCTIONS */
extern void           Xcp_RxIndication(const void* data, int len);
extern void           Xcp_TxConfirmation(void);
extern Std_ReturnType Xcp_Transmit    (const void* data, int len);
extern Std_ReturnType Xcp_CmdTransportLayer(uint8 pid, void* data, int len);
extern void 		  Xcp_TxError(Xcp_ErrorType code);
extern void 	      Xcp_TxSuccess(void);
#if 0
extern void Xcp_TxEvent(Xcp_EventType code);
#endif

/* GENERIC HELPER FUNCTIONS */
extern void           Xcp_MemCpy(void * dst, void const * src, size_t len);

#endif /* XCP_INTERNAL_H_ */
