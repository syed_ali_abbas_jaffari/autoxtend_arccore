/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 * 
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with  
 * the terms contained in the written license agreement between you and ArcCore, 
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as 
 * published by the Free Software Foundation and appearing in the file 
 * LICENSE.GPL included in the packaging of this file or here 
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "Std_Types.h"
#include "Lin.h"
#include "LinIf.h"
#include "LinSM.h"
#include "LinSM_Cbk.h"
#include "LinIf_Types.h"
#if defined(USE_COM)
#include "Com.h"
#endif
#include "ComM_Types.h"
#include "ComStack_Types.h"
#include "Com_Types.h"
#include "ComM_BusSM.h"
#include "Det.h"
#if defined(USE_BSWM)
#include "BswM_LinSM.h"
#endif
#include "Mcu.h"
#include "LinSM_ConfigTypes.h"
/*==================[macros]==================================================*/

/* Development error macros. */
#if ( LINSM_DEV_ERROR_DETECT == STD_ON )
#define LINSM_DET_REPORT_ERROR(_api, _err)  (void)Det_ReportError(MODULE_ID_LINSM,0,_api,_err)

#define VALIDATE(_exp,_api,_err ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(MODULE_ID_LINSM,0,_api,_err); \
          return; \
        }

#define VALIDATE_W_RV(_exp,_api,_err,_rv ) \
        if( !(_exp) ) { \
          (void)Det_ReportError(MODULE_ID_LINSM,0,_api,_err); \
          return (_rv); \
        }
#else
#define LINSM_DET_REPORT_ERROR(_api, _err)
#define VALIDATE(_exp,_api,_err )
#define VALIDATE_W_RV(_exp,_api,_err,_rv )
#endif

#if defined(USE_BSWM)
#define BSWM_LINSM_CURRENTSCHEDULE(_ch, _sch) \
    BswM_LinSM_CurrentSchedule(_ch,_sch); \

#define BSWM_LINSM_CURRENTSTATE(_ch,_state) \
    BswM_LinSM_CurrentState(_ch,_state); \

#else
#define BSWM_LINSM_CURRENTSCHEDULE(_ch, _sch)
#define BSWM_LINSM_CURRENTSTATE(_ch,_state)
#endif

/*==================[internal data]===========================================*/

/* Timer values for every LinIf channel. These timers are used to monitor the
 * confirmation of a specific command after calling LinIf_GotoSleep,
 * LinIf_Wakeup or LinIf_ScheduleRequest. The arrays are indexed by the LinSM
 * channel index (NOT the LinIf channel id).
 *
 * See Specification of LIN State Manager, R4.0 Rev 3,
 * 7.1.8 Timeout of requests. */
/** @req LINSM175 */
static uint32 ScheduleRequestTimer [LINSM_CHANNEL_CNT];
static uint32 GoToSleepTimer       [LINSM_CHANNEL_CNT];
static uint32 WakeUpTimer          [LINSM_CHANNEL_CNT];

static void DecrementTimer(uint32 *timer);

/* LinSM module state */
static LinSM_StatusType LinSMStatus = LINSM_UNINIT;

/* Channel states. The arrays are indexed by the LinSM
 * channel index. */
static LinSM_StatusType LinSMChannelStatus[LINSM_CHANNEL_CNT];
static LinIf_SchHandleType LinSMSchTablCurr[LINSM_CHANNEL_CNT];
static LinIf_SchHandleType LinSMSchTablNew[LINSM_CHANNEL_CNT];

static const LinSM_ConfigType *LinSMConfigPtr;

/*=========================[Local functions]===========================*/
/**
 * Get the index (in LinSM configuration) for a Lin channel
 * @param channel
 * @param channelIndex
 * @return TRUE: index found, FALSE: invalid Lin channel
 */
static Std_ReturnType GetChannelIndex(NetworkHandleType channel, uint32 *channelIndex)
{
    Std_ReturnType ret = E_NOT_OK;
    for (uint32 i = 0; (i < LinSMConfigPtr->LinSMChannels_Size) && (E_OK != ret); i++) {
        if(channel == LinSMConfigPtr->LinSMChannels[i].LinSMComMNetworkHandleRef) {
            *channelIndex = i;
            ret = E_OK;
        }
    }
    return ret;
}

#if ( LINSM_DEV_ERROR_DETECT == STD_ON )
/**
 * Checks if a schedule is configured for a specific channel
 * @param channelIndex
 * @param schedule
 * @return TRUE: valid schedule, FALSE: schedule not configured for this channel
 */
static boolean CheckScheduleValid(uint32 channelIndex, LinIf_SchHandleType schedule)
{
    boolean isValid = FALSE;
    const LinSM_ScheduleType *schedulePtr = LinSMConfigPtr->LinSMChannels[channelIndex].LinSMSchedules;
    for(uint8 schIndex = 0; (schIndex < LinSMConfigPtr->LinSMChannels[channelIndex].LinSMSchedule_Cnt) && !isValid; schIndex++) {
        /* IMPROVEMNT: Is it ok to request the NULL schedule? */
        if((schedule == schedulePtr->LinSMScheduleIndex) || (0 == schedule)) {
            isValid = TRUE;
        }
        schedulePtr++;
    }
    return isValid;
}
#endif
/*==================[Implementation of LinSM interface]=======================*/


/*------------------[Initialization]------------------------------------------*/


void LinSM_Init(const LinSM_ConfigType* ConfigPtr)
{
	uint32 i;

	/** @req LINSM217 LINSM218 */
    VALIDATE( (ConfigPtr!=NULL), LINSM_INIT_SERVICE_ID, LINSM_E_PARAMETER_POINTER );
    LinSMConfigPtr = ConfigPtr;
	for (i = 0; i < LinSMConfigPtr->LinSMChannels_Size; i++)
	{
	    /** @req LINSM160 */
		LinSMChannelStatus[i] = LINSM_NO_COMMUNICATION;
		/** @req LINSM0216 */
		LinSMSchTablCurr[i]= 0; //NULL Schedule
		LinSMSchTablNew[i]=0;
		ScheduleRequestTimer[i] = 0;
		GoToSleepTimer[i] = 0;
		WakeUpTimer[i] = 0;
	}
	LinSMStatus = LINSM_INIT;
}

/*------------------[Deinitialization]----------------------------------------*/


void LinSM_DeInit()
{
	LinSMStatus = LINSM_UNINIT;
}

/*------------------[Schedule Request]----------------------------------------*/

Std_ReturnType LinSM_ScheduleRequest(NetworkHandleType channel,LinIf_SchHandleType schedule)
{
    Std_ReturnType rv;
    uint32 channelIndex = 0;
    /** @req LINSM116 */
	VALIDATE_W_RV( (LinSMStatus != LINSM_UNINIT), LINSM_SCHEDULE_REQUEST_SERVICE_ID, LINSM_E_UNINIT, E_NOT_OK);
	/** @req LINSM114 */
	rv = GetChannelIndex(channel, &channelIndex);
	VALIDATE_W_RV( (E_OK == rv), LINSM_SCHEDULE_REQUEST_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK, E_NOT_OK);
	/** @req LINSM115 */
	VALIDATE_W_RV( CheckScheduleValid(channelIndex, schedule), LINSM_SCHEDULE_REQUEST_SERVICE_ID, LINSM_E_PARAMETER, E_NOT_OK);
    /** @req LINSM163 LINSM211 */
    if ((ScheduleRequestTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD) || (LINSM_FULL_COMMUNICATION != LinSMChannelStatus[channelIndex])) {
        return E_NOT_OK;
    }
    else {
        /* Set the Schedule Request confirmation timer for the specified channel. */
        /** @req LINSM100 */
        ScheduleRequestTimer[channelIndex] = LinSMConfigPtr->LinSMChannels[channelIndex].LinSMConfirmationTimeout;
        LinSMSchTablNew[channelIndex] = schedule;
        /** @req LINSM164 */
        rv = LinIf_ScheduleRequest(channel, schedule);
        if (rv != E_OK) {
            /** @req LINSM0213 */
            BSWM_LINSM_CURRENTSCHEDULE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, LinSMSchTablCurr[channelIndex]);
            LinSMSchTablNew[channelIndex] = LinSMSchTablCurr[channelIndex];
            ScheduleRequestTimer[channelIndex] = 0;
        }
        return rv;
    }
}

/*------------------[Get current COM mode]------------------------------------*/


Std_ReturnType LinSM_GetCurrentComMode(NetworkHandleType network,ComM_ModeType* mode)
{
    Std_ReturnType ret;
    uint32 channelIndex = 0;
    /** @req LINSM182 */
    /* If active state is LINSM_UNINIT the state
     * COMM_NO_COMMUNICATION shall be returned. */
    if ((LinSMStatus == LINSM_UNINIT) && (mode != NULL_PTR)) {
        *mode= COMM_NO_COMMUNICATION;
    }

    /** @req LINSM125 */
    VALIDATE_W_RV( (LinSMStatus != LINSM_UNINIT), LINSM_GET_CURRENT_COM_MODE_SERVICE_ID, LINSM_E_UNINIT, E_NOT_OK);
	/** @req LINSM123 */
    ret = GetChannelIndex(network, &channelIndex);
	VALIDATE_W_RV( (E_OK == ret), LINSM_GET_CURRENT_COM_MODE_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK, E_NOT_OK);
	/** @req LINSM124 */
	VALIDATE_W_RV( (mode != NULL_PTR), LINSM_GET_CURRENT_COM_MODE_SERVICE_ID, LINSM_E_PARAMETER_POINTER, E_NOT_OK);

	switch (LinSMChannelStatus[channelIndex]) {
        case LINSM_FULL_COMMUNICATION:
            /** @req LINSM181 */
            *mode= COMM_FULL_COMMUNICATION;
            break;
        default:
            /** @req LINSM180 */
            *mode= COMM_NO_COMMUNICATION;
            break;
	}
	return E_OK;
}

/*------------------[Request new COM mode]------------------------------------*/


Std_ReturnType LinSM_RequestComMode(NetworkHandleType network,ComM_ModeType mode)
{
	Std_ReturnType res;
	uint32 channelIndex = 0;
	boolean tempflg;

	/** @req LINSM128 */
	VALIDATE_W_RV( (LinSMStatus != LINSM_UNINIT), LINSM_REQUEST_COM_MODE_SERVICE_ID, LINSM_E_UNINIT, E_NOT_OK);
	/** @req LINSM127 */
	res = GetChannelIndex(network, &channelIndex);
	VALIDATE_W_RV( (E_OK == res), LINSM_REQUEST_COM_MODE_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK, E_NOT_OK);
    /** @req LINSM191 */
    VALIDATE_W_RV( ((mode == COMM_NO_COMMUNICATION) || (mode == COMM_SILENT_COMMUNICATION) || (mode == COMM_FULL_COMMUNICATION)), LINSM_REQUEST_COM_MODE_SERVICE_ID, LINSM_E_PARAMETER_POINTER, E_NOT_OK);

    res = E_NOT_OK;
    tempflg = (boolean)(((COMM_NO_COMMUNICATION == mode) && (LINSM_NO_COMMUNICATION == LinSMChannelStatus[channelIndex])) ||
                        ((COMM_FULL_COMMUNICATION == mode) && (LINSM_FULL_COMMUNICATION == LinSMChannelStatus[channelIndex])));
    /** @req LINSM0210 LINSM174 */
    if (tempflg || (WakeUpTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD)) {
        return res;
    }

    switch(mode)
	{
		case COMM_NO_COMMUNICATION:
		    /* Set the GotoSleep confirmation timer for the specified channel. */
		    /** @req LINSM100 */
		    GoToSleepTimer[channelIndex] = LinSMConfigPtr->LinSMChannels[channelIndex].LinSMConfirmationTimeout;
		    /** @req LINSM164 */
			if (E_OK == LinIf_GotoSleep(network)){
			    /** @req LINSM223 */
				LinSMChannelStatus[channelIndex] = LINSM_NO_COMMUNICATION;
				res = E_OK;
			}
			else {
			    /** @req LINSM177 */
			    BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, LINSM_FULL_COM);
			    ComM_ModeType ComMode;
			    ComMode = COMM_FULL_COMMUNICATION;
			    ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
			    GoToSleepTimer[channelIndex] = 0;
			}
			break;
		case COMM_SILENT_COMMUNICATION:
			// Standard say nothing about this case.
		    /** @req LINSM183 */
			break;
		case COMM_FULL_COMMUNICATION:
		    /* Set the Wakeup confirmation timer for the specified channel. */
		    /** @req LINSM100 */
		    WakeUpTimer[channelIndex] = LinSMConfigPtr->LinSMChannels[channelIndex].LinSMConfirmationTimeout;
		    /** @req LINSM047 *//** @req LINSM164 */
			if (E_OK == LinIf_WakeUp(network)){
				res = E_OK;
			}
			else
			{
			    /** @req LINSM0202 */
			    BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_NO_COM);
			    ComM_ModeType ComMode;
			    ComMode = COMM_NO_COMMUNICATION;
			    ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
				WakeUpTimer[channelIndex] = 0;
			}
			break;
		default:
			break;
	}
	return res;
}

/*------------------[Schedule request confirmation]---------------------------*/


void LinSM_ScheduleRequest_Confirmation(NetworkHandleType channel)
{
    Std_ReturnType ret;
    uint32 channelIndex = 0;
    /** @req LINSM131 */
	VALIDATE( (LinSMStatus != LINSM_UNINIT), LINSM_SCHEDULE_REQUEST_CONF_SERVICE_ID, LINSM_E_UNINIT);
	/** @req LINSM130 */
	ret = GetChannelIndex(channel, &channelIndex);
	VALIDATE( (E_OK == ret), LINSM_SCHEDULE_REQUEST_CONF_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK);

	if (ScheduleRequestTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD) {

	    LinSMSchTablCurr[channel] = LinSMSchTablNew[channelIndex];
	    /** @req LINSM206 */
	    BSWM_LINSM_CURRENTSCHEDULE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, LinSMSchTablCurr[channelIndex]);
	}
	/** @req LINSM154 */
	ScheduleRequestTimer[channelIndex] = 0;

}

/*------------------[Wakeup confirmation]-------------------------------------*/


void LinSM_WakeUp_Confirmation(NetworkHandleType channel,boolean success)
{
    Std_ReturnType ret;
    uint32 channelIndex = 0;
    /** @req LINSM134 */
	VALIDATE( (LinSMStatus != LINSM_UNINIT), LINSM_WAKEUP_CONF_SERVICE_ID, LINSM_E_UNINIT);
	/** @req LINSM133 */
	ret = GetChannelIndex(channel, &channelIndex);
	VALIDATE( (E_OK == ret), LINSM_WAKEUP_CONF_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK);

    /** @req LINSM172 */
    if(WakeUpTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD){
        if(success)
        {
            ComM_ModeType ComMode = COMM_FULL_COMMUNICATION;
            ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
            LinSMChannelStatus[channelIndex] = LINSM_FULL_COMMUNICATION;
            /** @req LINSM192 */
            BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_FULL_COM);
        }
        else {
            /** @req LINSM0202 */
            BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_NO_COM);
            ComM_ModeType ComMode;
            ComMode = COMM_NO_COMMUNICATION;
            ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
        }
    }

    /** @req LINSM154 */
    WakeUpTimer[channelIndex] = 0;
}

/*------------------[GotoSleep confirmation]----------------------------------*/


void LinSM_GotoSleep_Confirmation(NetworkHandleType channel,boolean success)
{
    Std_ReturnType ret;
    uint32 channelIndex = 0;
    /** @req LINSM137 */
	VALIDATE( (LinSMStatus != LINSM_UNINIT), LINSM_GOTO_SLEEP_CONF_SERVICE_ID, LINSM_E_UNINIT);
	/** @req LINSM136 */
	ret = GetChannelIndex(channel, &channelIndex);
	VALIDATE( (E_OK == ret), LINSM_GOTO_SLEEP_CONF_SERVICE_ID, LINSM_E_NONEXISTENT_NETWORK);

    /** @req LINSM172 */
    if(GoToSleepTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD){
        if(success)
        {
            LinSMChannelStatus[channelIndex] = LINSM_NO_COMMUNICATION;
            ComM_ModeType ComMode = COMM_NO_COMMUNICATION;
            ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
            /** @req LINSM193 */
            BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_NO_COM);
        }
        else {
            /** @req LINSM177 */
            BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_FULL_COM);
            ComM_ModeType ComMode;
            ComMode = COMM_FULL_COMMUNICATION;
            ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
        }
    }

    /** @req LINSM154 */
    GoToSleepTimer[channelIndex] = 0;
}

/*==================[Main function]==========================================*/

void LinSM_MainFunction(void)
{
	uint32 channelIndex;
	ComM_ModeType ComMode;

    /** @req LINSM179 */
    VALIDATE( (LinSMStatus != LINSM_UNINIT), LINSM_MAIN_FUNCTION_SERVICE_ID, LINSM_E_UNINIT);
    /** @req LINSM157 */
	for(channelIndex = 0; channelIndex < LinSMConfigPtr->LinSMChannels_Size; channelIndex++){
		// Check timers

	    if(ScheduleRequestTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD){

	        DecrementTimer(&ScheduleRequestTimer[channelIndex]);
            if (ScheduleRequestTimer[channelIndex] < LINSM_MAIN_PROCESSING_PERIOD) {
                /** @req LINSM0214 */
                BSWM_LINSM_CURRENTSCHEDULE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, LinSMSchTablCurr[channelIndex]);
                /** @req LINSM102 */
                LINSM_DET_REPORT_ERROR(LINSM_MAIN_FUNCTION_SERVICE_ID, LINSM_E_CONFIRMATION_TIMEOUT);
            }
	    }
	    if(GoToSleepTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD){

	        DecrementTimer(&GoToSleepTimer[channelIndex]);
            if (GoToSleepTimer[channelIndex] < LINSM_MAIN_PROCESSING_PERIOD) {
                ComMode = COMM_FULL_COMMUNICATION;
                /** @req LINSM170 */
                ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
                /** @req LINSM215 */
                BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, LINSM_FULL_COM);
                /** @req LINSM102 */
                LINSM_DET_REPORT_ERROR(LINSM_MAIN_FUNCTION_SERVICE_ID, LINSM_E_CONFIRMATION_TIMEOUT);
            }
	    }

        if(WakeUpTimer[channelIndex] >= LINSM_MAIN_PROCESSING_PERIOD){

            DecrementTimer(&WakeUpTimer[channelIndex]);
            if (WakeUpTimer[channelIndex] < LINSM_MAIN_PROCESSING_PERIOD) {
                ComMode = COMM_NO_COMMUNICATION;
                /** @req LINSM170 */
                ComM_BusSM_ModeIndication(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef, &ComMode);
                /** @req LINSM215 */
                BSWM_LINSM_CURRENTSTATE(LinSMConfigPtr->LinSMChannels[channelIndex].LinSMComMNetworkHandleRef,LINSM_NO_COM);
                /** @req LINSM102 */
                LINSM_DET_REPORT_ERROR(LINSM_MAIN_FUNCTION_SERVICE_ID, LINSM_E_CONFIRMATION_TIMEOUT);
            }

        }
	}
}

/** @req LINSM162*/
static void DecrementTimer(uint32 *timer) {
    /** @req LINSM159*/
    *timer = *timer - LINSM_MAIN_PROCESSING_PERIOD;
    /** @req LINSM101*/
    if(*timer < LINSM_MAIN_PROCESSING_PERIOD){
        /** @req LINSM102*/
        (void)Det_ReportError(MODULE_ID_LINSM,0,LINSM_MAIN_FUNCTION_SERVICE_ID,LINSM_E_CONFIRMATION_TIMEOUT);
    }
}

/*==================[end of file]=============================================*/
