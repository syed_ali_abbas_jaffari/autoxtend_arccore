/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 * 
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with  
 * the terms contained in the written license agreement between you and ArcCore, 
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as 
 * published by the Free Software Foundation and appearing in the file 
 * LICENSE.GPL included in the packaging of this file or here 
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/


//lint -esym(960,8.7)	PC-Lint misunderstanding of Misra 8.7 for Com_SystenEndianness and endianess_test




#include <stdlib.h>
#include <string.h>
#include "PduR.h"
#include "Com_Arc_Types.h"
#include "Com.h"
#include "Com_Internal.h"
#include "Com_misc.h"
#include "debug.h"
#include "Det.h"
#include "Cpu.h"
#include "SchM_Com.h"
#include <assert.h>

#if defined(USE_LINOS)
#include "logger.h" /* Logger functions */
#endif

/* Declared in Com_Cfg.c */
extern const ComRxIPduCalloutType ComRxIPduCallouts[];
extern const ComNotificationCalloutType ComNotificationCallouts[];
extern const ComTxIPduCalloutType ComTriggerTransmitIPduCallouts[];

Com_BufferPduStateType Com_BufferPduState[COM_MAX_N_IPDUS];

uint8 Com_SendSignal(Com_SignalIdType SignalId, const void *SignalDataPtr) {
	/* @req COM334 */ /* Shall update buffer if pdu stopped, should not store trigger */
	uint8 ret = E_OK;
	boolean dataChanged = FALSE;

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_SENDSIGNAL_ID, COM_E_UNINIT);
		return COM_SERVICE_NOT_AVAILABLE;
	}

	if(SignalId >= ComConfig->ComNofSignals) {
		DET_REPORTERROR(COM_SENDSIGNAL_ID, COM_E_PARAM);
		return COM_SERVICE_NOT_AVAILABLE;
	}

	// Store pointer to signal for easier coding.
	const ComSignal_type * Signal = GET_Signal(SignalId);
#if defined(USE_LINOS)
	logger(LOG_INFO, "Com_SendSignal SignalDataPtr [%s] ComBitSize [%d]", 
			logger_format_hex((char*)SignalDataPtr, (Signal->ComBitSize / 8)),
			Signal->ComBitSize
	);
#endif

    if (Signal->ComIPduHandleId == NO_PDU_REFERENCE) {
        /* Return error if signal is not connected to an IPdu*/
        return COM_SERVICE_NOT_AVAILABLE;
    }


    if (isPduBufferLocked(Signal->ComIPduHandleId)) {
        return COM_BUSY;
    }
	//DEBUG(DEBUG_LOW, "Com_SendSignal: id %d, nBytes %d, BitPosition %d, intVal %d\n", SignalId, nBytes, signal->ComBitPosition, (uint32)*(uint8 *)SignalDataPtr);
    Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(Signal->ComIPduHandleId);

    /* @req COM624 */
    uint8 *comIPduDataPtr = Arc_IPdu->ComIPduDataPtr;
	SchM_Enter_Com_EA_0();
    Com_Misc_WriteSignalDataToPdu(
    		(const uint8 *)SignalDataPtr,
    		Signal->ComSignalType,
    		comIPduDataPtr,
    	    Signal->ComBitPosition,
    	    Signal->ComBitSize,
    	    Signal->ComSignalEndianess,
    		&dataChanged);
    // If the signal has an update bit. Set it!
    /* @req COM061 */
    if (Signal->ComSignalArcUseUpdateBit) {
        SETBIT(comIPduDataPtr, Signal->ComUpdateBitPosition);
    }

    /* Assign the number of repetitions based on Transfer property */
    if( !Com_Misc_TriggerTxOnConditions(Signal->ComIPduHandleId, dataChanged, Signal->ComTransferProperty) ) {
        ret = COM_SERVICE_NOT_AVAILABLE;
    }
    SchM_Exit_Com_EA_0();

    return ret;
}

uint8 Com_ReceiveSignal(Com_SignalIdType SignalId, void* SignalDataPtr) {

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_RECEIVESIGNAL_ID, COM_E_UNINIT);
		return COM_SERVICE_NOT_AVAILABLE;
	}
	if(SignalId >= ComConfig->ComNofSignals) {
		DET_REPORTERROR(COM_RECEIVESIGNAL_ID, COM_E_PARAM);
		return COM_SERVICE_NOT_AVAILABLE;
	}
	DEBUG(DEBUG_LOW, "Com_ReceiveSignal: SignalId %d\n", SignalId);
	const ComSignal_type * Signal = GET_Signal(SignalId);

    if (Signal->ComIPduHandleId == NO_PDU_REFERENCE) {
        /* Return error init value signal if signal is not connected to an IPdu*/
        memcpy(SignalDataPtr, Signal->ComSignalInitValue, SignalTypeToSize(Signal->ComSignalType, Signal->ComBitSize/8));
        return E_OK;
    }

    const ComIPdu_type *IPdu = GET_IPdu(Signal->ComIPduHandleId);
    Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(Signal->ComIPduHandleId);

    uint8 ret = E_OK;
    const void* pduBuffer = 0;
    if ((IPdu->ComIPduSignalProcessing == COM_DEFERRED) && (IPdu->ComIPduDirection == COM_RECEIVE)) {
    	pduBuffer = Arc_IPdu->ComIPduDeferredDataPtr;
    } else {
        if (isPduBufferLocked(Signal->ComIPduHandleId)) {
            ret = COM_BUSY;
        }
        pduBuffer = Arc_IPdu->ComIPduDataPtr;
    }
    /* @req COM631 */
    Com_Misc_ReadSignalDataFromPdu(
			pduBuffer,
			Signal->ComBitPosition,
			Signal->ComBitSize,
			Signal->ComSignalEndianess,
			Signal->ComSignalType,
			SignalDataPtr);
    if( !Arc_IPdu->Com_Arc_IpduStarted && (E_OK == ret) ) {
        ret = COM_SERVICE_NOT_AVAILABLE;
    }
    return ret;
}

uint8 Com_ReceiveDynSignal(Com_SignalIdType SignalId, void* SignalDataPtr, uint16* Length) {
    /* IMPROVEMENT: Validate signal id?*/
    if( COM_INIT != Com_GetStatus() ) {
        DET_REPORTERROR(COM_RECEIVEDYNSIGNAL_ID, COM_E_UNINIT);
        return COM_SERVICE_NOT_AVAILABLE;
    }
    const ComSignal_type * Signal = GET_Signal(SignalId);
    Com_Arc_IPdu_type    *Arc_IPdu   = GET_ArcIPdu(Signal->ComIPduHandleId);
    const ComIPdu_type   *IPdu       = GET_IPdu(Signal->ComIPduHandleId);

    Com_SignalType signalType = Signal->ComSignalType;
    /* @req COM753 */
    if (signalType != COM_UINT8_DYN) {
        return COM_SERVICE_NOT_AVAILABLE;
    }
    uint8 ret = E_OK;
    SchM_Enter_Com_EA_0();
    /* @req COM712 */
    if( *Length >= Arc_IPdu->Com_Arc_DynSignalLength ) {
        if (*Length > Arc_IPdu->Com_Arc_DynSignalLength) {
            *Length = Arc_IPdu->Com_Arc_DynSignalLength;
        }
        uint8 startFromPduByte = (Signal->ComBitPosition) / 8;
        const void* pduDataPtr = 0;
        if ((IPdu->ComIPduSignalProcessing == COM_DEFERRED) && (IPdu->ComIPduDirection == COM_RECEIVE)) {
            pduDataPtr = Arc_IPdu->ComIPduDeferredDataPtr;
        } else {
            if (isPduBufferLocked(getPduId(IPdu))) {
                ret = COM_BUSY;
            }
            pduDataPtr = Arc_IPdu->ComIPduDataPtr;
        }
        /* @req COM711 */
        memcpy(SignalDataPtr, (uint8 *)pduDataPtr + startFromPduByte, *Length);

        SchM_Exit_Com_EA_0();
    } else {
        /* @req COM724 */
        *Length = Arc_IPdu->Com_Arc_DynSignalLength;
        ret = E_NOT_OK;
    }
    if( !Arc_IPdu->Com_Arc_IpduStarted && (E_OK == ret) ) {
        ret = COM_SERVICE_NOT_AVAILABLE;
    }
    return ret;
}


/**
 * Send a dynamic signal (ie ComSignalType=COM_UINT8_DYN)
 *
 * @param SignalId			- The signal ID
 * @param SignalDataPtr		- Pointer to the data.
 * @param Length			- The signal length in bytes
 * @return
 */
uint8 Com_SendDynSignal(Com_SignalIdType SignalId, const void* SignalDataPtr, uint16 Length) {
    /* IMPROVEMENT: validate SignalId */
    /* !req COM629 */
    /* @req COM630 */
    if( COM_INIT != Com_GetStatus() ) {
        DET_REPORTERROR(COM_SENDDYNSIGNAL_ID, COM_E_UNINIT);
        return COM_SERVICE_NOT_AVAILABLE;
    }
    const ComSignal_type * Signal = GET_Signal(SignalId);
    Com_Arc_IPdu_type    *Arc_IPdu   = GET_ArcIPdu(Signal->ComIPduHandleId);
    const ComIPdu_type   *IPdu       = GET_IPdu(Signal->ComIPduHandleId);
    uint8 ret = E_OK;
    Com_SignalType signalType = Signal->ComSignalType;
    boolean dataChanged = FALSE;

    /* @req COM753 */
    if (signalType != COM_UINT8_DYN) {
        return COM_SERVICE_NOT_AVAILABLE;
    }
    if (isPduBufferLocked(getPduId(IPdu))) {
        return COM_BUSY;
    }

    /* Get signalLength in bytes,
     * Note!, ComBitSize is set to ComSignalLength in case of COM_UINT8_DYN or COM_UINT8_N */
    uint32 signalLength = Signal->ComBitSize / 8;
    Com_BitPositionType bitPosition = Signal->ComBitPosition;

    /* Check so that we are not trying to send a signal that is
     * bigger that the actual space */
    if (signalLength < Length) {
        return E_NOT_OK;
    }
    uint8 startFromPduByte = bitPosition / 8;

    SchM_Enter_Com_EA_0();
    if( (Arc_IPdu->Com_Arc_DynSignalLength != Length) ||
            (0 != memcmp((void *)((uint8 *)Arc_IPdu->ComIPduDataPtr + startFromPduByte), SignalDataPtr, Length)) ) {
        dataChanged = TRUE;
    }
    /* @req COM628 */
    memcpy((void *)((uint8 *)Arc_IPdu->ComIPduDataPtr + startFromPduByte), SignalDataPtr, Length);
    /* !req COM757 */ //Length of I-PDU?
    Arc_IPdu->Com_Arc_DynSignalLength = Length;
    // If the signal has an update bit. Set it!
    if (Signal->ComSignalArcUseUpdateBit) {
        SETBIT(Arc_IPdu->ComIPduDataPtr, Signal->ComUpdateBitPosition);
    }

    /* Assign the number of repetitions based on Transfer property */
    if( !Com_Misc_TriggerTxOnConditions(Signal->ComIPduHandleId, dataChanged, Signal->ComTransferProperty) ) {
        ret = COM_SERVICE_NOT_AVAILABLE;
    }
    SchM_Exit_Com_EA_0();

    return ret;
}

Std_ReturnType Com_TriggerTransmit(PduIdType TxPduId, PduInfoType *PduInfoPtr) {
	/* @req COM475 */
	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_TRIGGERTRANSMIT_ID, COM_E_UNINIT);
		return E_NOT_OK;
	}
	if( TxPduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_TRIGGERTRANSMIT_ID, COM_E_PARAM);
		return E_NOT_OK;
	}
	/*
	 * !req COM260: This function must not check the transmission mode of the I-PDU
	 * since it should be possible to use it regardless of the transmission mode.
	 * Only for ComIPduType = NORMAL.
	 */

	const ComIPdu_type *IPdu = GET_IPdu(TxPduId);
	Com_Arc_IPdu_type    *Arc_IPdu   = GET_ArcIPdu(TxPduId);

	SchM_Enter_Com_EA_0();

	/* @req COM766 */
	if( (IPdu->ComTriggerTransmitIPduCallout != COM_NO_FUNCTION_CALLOUT) && (ComTriggerTransmitIPduCallouts[IPdu->ComTriggerTransmitIPduCallout] != NULL) ) {
	    /* @req COM395 */
	    (void)ComTriggerTransmitIPduCallouts[IPdu->ComTriggerTransmitIPduCallout](TxPduId, Arc_IPdu->ComIPduDataPtr);
	}

    /* @req COM647 */
    /* @req COM648 */ /* Interrups disabled */
    memcpy(PduInfoPtr->SduDataPtr, Arc_IPdu->ComIPduDataPtr, IPdu->ComIPduSize);

    SchM_Exit_Com_EA_0();

	PduInfoPtr->SduLength = IPdu->ComIPduSize;
	if( !Arc_IPdu->Com_Arc_IpduStarted ) {
		return E_NOT_OK;
	} else {
		return E_OK;
	}
}

void Com_TriggerIPDUSend(PduIdType PduId) {
	/* !req	COM789 */
	/* !req	COM662 */ /* May be supported, but when is buffer locked?*/

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_TRIGGERIPDUSEND_ID, COM_E_UNINIT);
		return;
	}
	if( PduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_TRIGGERIPDUSEND_ID, COM_E_PARAM);
		return;
	}

	(void)Com_Misc_TriggerIPDUSend(PduId);

}

void Com_RxIndication(PduIdType RxPduId, PduInfoType* PduInfoPtr) {

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_RXINDICATION_ID, COM_E_UNINIT);
		return;
	}
	if( RxPduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_RXINDICATION_ID, COM_E_PARAM);
		return;
	}

	const ComIPdu_type *IPdu = GET_IPdu(RxPduId);
	Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(RxPduId);
	SchM_Enter_Com_EA_0();


	/* @req COM649 */ /* Interrups disabled */
	// If Ipdu is stopped
	/* @req COM684 */
	if (!Arc_IPdu->Com_Arc_IpduStarted) {
		SchM_Exit_Com_EA_0();
		return;
	}


	// Check callout status
	/* @req COM555 */
	/* @req COM700 */
	if ((IPdu->ComRxIPduCallout != COM_NO_FUNCTION_CALLOUT) && (ComRxIPduCallouts[IPdu->ComRxIPduCallout] != NULL)) {
		if (!ComRxIPduCallouts[IPdu->ComRxIPduCallout](RxPduId, PduInfoPtr->SduDataPtr)) {
			// IMPROVMENT: Report error to DET.
			// Det_ReportError();
			/* !req COM738 */
			SchM_Exit_Com_EA_0();
			return;
		}
	}
	/* !req COM574 */
	/* !req COM575 */

#if(COM_IPDU_COUNTING_ENABLE ==  STD_ON )
	if (ComConfig->ComIPdu[RxPduId].ComIpduCounterRef != NULL) {

	    // Ignore the IPdu if errors detected
	    if (!(Com_Misc_validateIPduCounter(ComConfig->ComIPdu[RxPduId].ComIpduCounterRef,PduInfoPtr->SduDataPtr))){
	        /* @req COM726 *//* @req COM727 */
	        if (ComConfig->ComIPdu[RxPduId].ComIpduCounterRef->ComIPduCntErrNotification != COM_NO_FUNCTION_CALLOUT) {
	            (void)ComNotificationCallouts[ComConfig->ComIPdu[RxPduId].ComIpduCounterRef->ComIPduCntErrNotification](); //Trigger error callback
	        }

	        /* @req COM590 */
	        return;
	    }
	}
#endif

	// Copy IPDU data
    memcpy(Arc_IPdu->ComIPduDataPtr, PduInfoPtr->SduDataPtr, IPdu->ComIPduSize);
	Com_Misc_RxProcessSignals(IPdu,Arc_IPdu);

	SchM_Exit_Com_EA_0();

	return;
}

void Com_TpRxIndication(PduIdType PduId, NotifResultType Result) {
	/* @req COM720 */

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_TPRXINDICATION_ID, COM_E_UNINIT);
		return;
	}
	if( PduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_RXINDICATION_ID, COM_E_PARAM);
		return;
	}

	const ComIPdu_type *IPdu = GET_IPdu(PduId);
	Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(PduId);
	/* @req COM651 */ /* Interrups disabled */
	SchM_Enter_Com_EA_0();

	// If Ipdu is stopped
	/* @req COM684 */
	if (!Arc_IPdu->Com_Arc_IpduStarted) {
		Com_Misc_UnlockTpBuffer(getPduId(IPdu));
		SchM_Exit_Com_EA_0();
		return;
	}
	if (Result == NTFRSLT_OK) {
		if (IPdu->ComIPduSignalProcessing == COM_IMMEDIATE) {
			// irqs needs to be disabled until signal notifications have been called
			// Otherwise a new Tp session can start and fill up pdus
			Com_Misc_UnlockTpBuffer(getPduId(IPdu));
		}
		// In deferred mode, buffers are unlocked in mainfunction
		Com_Misc_RxProcessSignals(IPdu,Arc_IPdu);
	} else {
		Com_Misc_UnlockTpBuffer(getPduId(IPdu));
	}
	SchM_Exit_Com_EA_0();

}

void Com_TpTxConfirmation(PduIdType PduId, NotifResultType Result) {
	/* !req COM713 */
	/* !req COM662 */ /* Tp buffer unlocked but buffer is never locked */
	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_TPTXCONFIRMATION_ID, COM_E_UNINIT);
		return;
	}
	if( PduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_RXINDICATION_ID, COM_E_PARAM);
		return;
	}
	(void)Result; // touch

	SchM_Enter_Com_EA_0();
	Com_Misc_UnlockTpBuffer(PduId);
	SchM_Exit_Com_EA_0();
}

void Com_TxConfirmation(PduIdType TxPduId) {
	/* !req COM469 */
	/* !req COM053 */
	/* @req COM652 */ /* Function does nothing.. */

	/* Need to implement COM305 */

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_TXCONFIRMATION_ID, COM_E_UNINIT);
		return;
	}
	if( TxPduId >= ComConfig->ComNofIPdus ) {
		DET_REPORTERROR(COM_RXINDICATION_ID, COM_E_PARAM);
		return;
	}

    /* @req COM468 */
    /* Call all notifications for the PDU */
    const ComIPdu_type *IPdu = GET_IPdu(TxPduId);
    Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(TxPduId);

    if (IPdu->ComIPduDirection == COM_RECEIVE) {
        DET_REPORTERROR(COM_TXCONFIRMATION_ID, COM_INVALID_PDU_ID);
    }
    else {
	    /* take care DM handling */
    	if(Arc_IPdu->Com_Arc_TxDeadlineCounter){ /* if DM configured */
    		Com_Misc_TxHandleDM(IPdu,Arc_IPdu);
    		/* IMPROVEMENT - handle also in Com_TpTxConfirmation */
    	}
    	if (IPdu->ComIPduSignalProcessing == COM_IMMEDIATE) {
        /* If immediate, call the notification functions  */
        for (uint16 i = 0; IPdu->ComIPduSignalRef[i] != NULL; i++) {
            const ComSignal_type *signal = IPdu->ComIPduSignalRef[i];
            if ((signal->ComNotification != COM_NO_FUNCTION_CALLOUT) &&
                (ComNotificationCallouts[signal->ComNotification] != NULL) ) {
                ComNotificationCallouts[signal->ComNotification]();
            }
          }
        }
        else {
            /* If deferred, set status and let the main function call the notification function */
            Com_Misc_SetTxConfirmationStatus(IPdu, TRUE);
        }    	
     }
}


uint8 Com_SendSignalGroup(Com_SignalGroupIdType SignalGroupId) {
	/* Validate signalgroupid?*/
	/* @req COM334 */
	uint8 ret = E_OK;
	boolean dataChanged = FALSE;
	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_SENDSIGNALGROUP_ID, COM_E_UNINIT);
		return COM_SERVICE_NOT_AVAILABLE;
	}
	const ComSignal_type * Signal = GET_Signal(SignalGroupId);

    if (Signal->ComIPduHandleId == NO_PDU_REFERENCE) {
        return COM_SERVICE_NOT_AVAILABLE;
    }

    Com_Arc_IPdu_type *Arc_IPdu = GET_ArcIPdu(Signal->ComIPduHandleId);
    const ComIPdu_type *IPdu = GET_IPdu(Signal->ComIPduHandleId);

    if (isPduBufferLocked(getPduId(IPdu))) {
        return COM_BUSY;
    }

    // Copy shadow buffer to Ipdu data space

    SchM_Enter_Com_EA_0();
    /* @req COM635 */
    /* @req COM050 */
    Com_Misc_CopySignalGroupDataFromShadowBufferToPdu(SignalGroupId,
                                                 (IPdu->ComIPduSignalProcessing == COM_DEFERRED) &&
                                                 (IPdu->ComIPduDirection == COM_RECEIVE),
                                                 &dataChanged);

    // If the signal has an update bit. Set it!
    /* @req COM061 */
    if (Signal->ComSignalArcUseUpdateBit) {
        SETBIT(Arc_IPdu->ComIPduDataPtr, Signal->ComUpdateBitPosition);
    }
    /* Assign the number of repetitions based on Transfer property */
    if( !Com_Misc_TriggerTxOnConditions(Signal->ComIPduHandleId, dataChanged, Signal->ComTransferProperty) ) {
        ret = COM_SERVICE_NOT_AVAILABLE;
    }
    SchM_Exit_Com_EA_0();

    return ret;
}


uint8 Com_ReceiveSignalGroup(Com_SignalGroupIdType SignalGroupId) {

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_RECEIVESIGNALGROUP_ID, COM_E_UNINIT);
		return COM_SERVICE_NOT_AVAILABLE;
	}

	const ComSignal_type * Signal = GET_Signal(SignalGroupId);

    if (Signal->ComIPduHandleId == NO_PDU_REFERENCE) {
        return COM_SERVICE_NOT_AVAILABLE;
    }

    const ComIPdu_type *IPdu = GET_IPdu(Signal->ComIPduHandleId);

    if (isPduBufferLocked(getPduId(IPdu))) {
        return COM_BUSY;
    }
    // Copy Ipdu data buffer to shadow buffer.
    /* @req COM638 */
    /* @req COM461 */
    /* @req COM051 */
    imask_t irq_state;
    Irq_Save(irq_state);
    Com_Misc_CopySignalGroupDataFromPduToShadowBuffer(SignalGroupId);
    Irq_Restore(irq_state);

    if( !GET_ArcIPdu(Signal->ComIPduHandleId)->Com_Arc_IpduStarted ) {
        return COM_SERVICE_NOT_AVAILABLE;
    } else {
        return E_OK;
    }
}

void Com_UpdateShadowSignal(Com_SignalIdType SignalId, const void *SignalDataPtr) {

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_UPDATESHADOWSIGNAL_ID, COM_E_UNINIT);
		return;
	}

    if( SignalId >= ComConfig->ComNofGroupSignals ) {
        DET_REPORTERROR(COM_UPDATESHADOWSIGNAL_ID, COM_E_PARAM);
        return;
    }

    Com_Arc_GroupSignal_type *Arc_GroupSignal = GET_ArcGroupSignal(SignalId);

    if (Arc_GroupSignal->Com_Arc_ShadowBuffer != NULL) {
        Com_Misc_UpdateShadowSignal(SignalId, SignalDataPtr);
    }
}

void Com_ReceiveShadowSignal(Com_SignalIdType SignalId, void *SignalDataPtr) {

	if( COM_INIT != Com_GetStatus() ) {
		DET_REPORTERROR(COM_RECEIVESHADOWSIGNAL_ID, COM_E_UNINIT);
		return;
	}

    if( SignalId >= ComConfig->ComNofGroupSignals ) {
        DET_REPORTERROR(COM_RECEIVESHADOWSIGNAL_ID, COM_E_PARAM);
        return;
    }

    const ComGroupSignal_type *GroupSignal = GET_GroupSignal(SignalId);
	Com_Arc_GroupSignal_type *Arc_GroupSignal = GET_ArcGroupSignal(SignalId);

    /* Get default value if unconnected */
    if (Arc_GroupSignal->Com_Arc_ShadowBuffer == NULL) {
        memcpy(SignalDataPtr, GroupSignal->ComSignalInitValue, SignalTypeToSize(GroupSignal->ComSignalType, GroupSignal->ComBitSize/8));
    }
    else {
	/* @req COM640 */
    	Com_Misc_ReadSignalDataFromPdu(
				Arc_GroupSignal->Com_Arc_ShadowBuffer,
				GroupSignal->ComBitPosition,
				GroupSignal->ComBitSize,
				GroupSignal->ComSignalEndianess,
				GroupSignal->ComSignalType,
				SignalDataPtr);
    }

}



