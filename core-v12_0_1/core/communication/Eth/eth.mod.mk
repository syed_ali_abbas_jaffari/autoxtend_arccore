#Ethernet
obj-$(USE_ETH) += Eth.o
obj-$(USE_ETH) += Eth_PBcfg.o
obj-$(USE_ETH)-$(CFG_JAC6) += Eth_jacinto.o
obj-$(USE_ETH)-$(CFG_RH850F1H) += Eth_rh850f1h.o
obj-$(USE_ETH)-$(CFG_ZYNQ)     += Eth_zc702.o
vpath-$(USE_ETH) += $(ROOTDIR)/communication/Eth
inc-$(USE_ETH) += $(ROOTDIR)/communication/Eth
