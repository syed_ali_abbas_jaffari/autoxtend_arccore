/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#include "EthTSyn.h"
#include "EthTSyn_Internal.h"
#include <string.h>

/* @req 4.2.2/SWS_EthTSyn_00028 */ /* Message format derived from IEEE 802.1As */

extern EthTSyn_InternalType EthTSyn_Internal;
#ifdef HOST_TEST
/*lint -esym(9003, testBuffers) */
EthTsyn_Internal_MsgSync testBuffers[2];
#endif

#define TWO_STEP_CLOCK      2u         /* Follow up msg supported */
#define TRANSPORT_SPECIFIC  (1u << 4u) /* Set transportSpecific = 1 for 802.1AS */

extern const EthTSyn_ConfigType* EthTSyn_ConfigPointer;
/*lint -save -e572 -e778 -e9032 */
/**
 *
 * @param bufPtr
 * @param i
 */
void EthTSyn_Internal_MsgPackPdelayReqMsg(EthTsyn_Internal_MsgPDelayReq *bufPtr, uint8 i)
{
    /* PTP Header */
	memset(bufPtr, 0, V2_PDELAY_REQ_LENGTH); /* Reset packet to all zeros */
    /* Message type, length, flags, Sequence, Control, log mean message interval */
    bufPtr->headerMsg.transportSpecificAndMessageType = TRANSPORT_SPECIFIC;     /* Set transportSpecific = 1 and Clear previous Message type */
    bufPtr->headerMsg.transportSpecificAndMessageType |= V2_PDELAY_REQ_MESSAGE;
    bufPtr->headerMsg.reserved1AndVersionPTP = V2_VERSION_PTP;
    bufPtr->headerMsg.messageLength = ETHTSYN_HEADER_PDELAY_REQ_LENGTH_BE;

    memcpy(bufPtr->headerMsg.sourcePortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.port_clock_identity, CLOCK_IDENTITY_LENGTH);
    bufPtr->headerMsg.sourcePortId.portNumber = ETHTSYN_HEADER_DEFAULT_PORT_NUMBER_BE;   /* Ordinary clock so its 1 */
    bufPtr->headerMsg.sequenceId = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.last_pdelay_req_tx_sequence_number) ;
    bufPtr->headerMsg.control = V2_ALL_OTHERS_CONTROL;
    bufPtr->headerMsg.logMeanMessageInterval = LOGMEAN_PDELAY_REQ;
#ifdef HOST_TEST
    memcpy(&testBuffers[0], bufPtr, V2_PDELAY_REQ_LENGTH);
#endif
}
/**
 *
 * @param bufPtr
 * @param i
 */
void EthTSyn_Internal_MsgPackPdelayRespMsg(EthTsyn_Internal_MsgPDelayResp *bufPtr, uint8 i)
{
    /* PTP Header */
	memset(bufPtr, 0, V2_PDELAY_RESP_LENGTH);/* Reset packet to all zeros */
    /* Message type, length, flags, Sequence, Control, log mean message interval */
    bufPtr->headerMsg.transportSpecificAndMessageType = TRANSPORT_SPECIFIC;                   /* Set transportSpecific = 1 and Clear previous Message type */
    bufPtr->headerMsg.transportSpecificAndMessageType |= V2_PDELAY_RESP_MESSAGE;
    bufPtr->headerMsg.reserved1AndVersionPTP = V2_VERSION_PTP;
    bufPtr->headerMsg.messageLength = ETHTSYN_HEADER_PDELAY_RESP_LENGTH_BE;
    bufPtr->headerMsg.flags[0] = TWO_STEP_CLOCK;
    memcpy(bufPtr->headerMsg.sourcePortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.port_clock_identity, CLOCK_IDENTITY_LENGTH);
    bufPtr->headerMsg.sourcePortId.portNumber = ETHTSYN_HEADER_DEFAULT_PORT_NUMBER_BE; /* Ordinary clock so its 1 */

    bufPtr->headerMsg.sequenceId = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.last_pdelay_resp_tx_sequence_number);
    bufPtr->headerMsg.control = V2_ALL_OTHERS_CONTROL;
    bufPtr->headerMsg.logMeanMessageInterval = LOGMEAN_PDELAY_RESP;

    bufPtr->receiveTimestamp.nanoseconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.nanoseconds);
    bufPtr->receiveTimestamp.seconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.seconds);
    bufPtr->receiveTimestamp.secondsHi = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.secondsHi);

    memcpy(bufPtr->requestingPortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.requestingPortIdentity.clockIdentity,CLOCK_IDENTITY_LENGTH);
    bufPtr->requestingPortId.portNumber = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.requestingPortIdentity.portNumber);
#ifdef HOST_TEST
    memcpy(&testBuffers[0], bufPtr, V2_PDELAY_RESP_LENGTH);
#endif
}
/**
 *
 * @param bufPtr
 * @param i
 */
void EthTSyn_Internal_MsgPackPdelayRespFollowUpMsg(EthTsyn_Internal_MsgPDelayRespFollowUp *bufPtr, uint8 i)
{
    /* PTP Header */
	memset(bufPtr, 0, V2_PDELAY_RESP_FOLLOWUP_LENGTH);/* Reset packet to all zeros */
    /* Message type, length, flags, Sequence, Control, log mean message interval */
    bufPtr->headerMsg.transportSpecificAndMessageType = TRANSPORT_SPECIFIC;                   /* Set transportSpecific = 1 and Clear previous Message type */
    bufPtr->headerMsg.transportSpecificAndMessageType |= V2_PDELAY_RESP_FOLLOWUP_MESSAGE;
    bufPtr->headerMsg.reserved1AndVersionPTP = V2_VERSION_PTP;
    bufPtr->headerMsg.messageLength = ETHTSYN_HEADER_PDELAY_RESP_FOLLOWUP_LENGTH_BE;

    memcpy(bufPtr->headerMsg.sourcePortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.port_clock_identity, CLOCK_IDENTITY_LENGTH);
    bufPtr->headerMsg.sourcePortId.portNumber = ETHTSYN_HEADER_DEFAULT_PORT_NUMBER_BE;  /* Ordinary clock so its 1 */

    bufPtr->headerMsg.sequenceId = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.last_pdelay_resp_tx_sequence_number);
    bufPtr->headerMsg.control = V2_ALL_OTHERS_CONTROL;
    bufPtr->headerMsg.logMeanMessageInterval = LOGMEAN_PDELAY_RESP_FOLLOWUP;

    bufPtr->responseOriginTimestamp.nanoseconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.nanoseconds);
    bufPtr->responseOriginTimestamp.seconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.seconds);
    bufPtr->responseOriginTimestamp.secondsHi = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.t2PdelayReqRxTime.secondsHi);

    memcpy(bufPtr->requestingPortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.requestingPortIdentity.clockIdentity,CLOCK_IDENTITY_LENGTH);
    bufPtr->requestingPortId.portNumber = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.requestingPortIdentity.portNumber);

#ifdef HOST_TEST
    memcpy(&testBuffers[0], bufPtr, V2_PDELAY_RESP_FOLLOWUP_LENGTH);
#endif
}
/**
 *
 * @param bufPtr
 * @param i
 */
void EthTSyn_Internal_MsgPackSyncMsg( EthTsyn_Internal_MsgSync *bufPtr, uint8 i)
{
    /* PTP Header */
	memset(bufPtr, 0, V2_SYNC_LENGTH);/* Reset packet to all zeros */
    /* Message type, length, flags, Sequence, Control, log mean message interval */
    bufPtr->headerMsg.transportSpecificAndMessageType = TRANSPORT_SPECIFIC;   /* Set transportSpecific = 1 and Clear previous Message type */
    bufPtr->headerMsg.transportSpecificAndMessageType |= V2_SYNC_MESSAGE;
    bufPtr->headerMsg.reserved1AndVersionPTP = V2_VERSION_PTP;
    bufPtr->headerMsg.messageLength = ETHTSYN_HEADER_SYNC_LENGTH_BE;

    bufPtr->headerMsg.flags[0] = TWO_STEP_CLOCK;

    memcpy(bufPtr->headerMsg.sourcePortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.port_clock_identity, CLOCK_IDENTITY_LENGTH);
    bufPtr->headerMsg.sourcePortId.portNumber = ETHTSYN_HEADER_DEFAULT_PORT_NUMBER_BE;  /* Ordinary clock so its 1 */

    bufPtr->headerMsg.sequenceId = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.last_sync_tx_sequence_number);
    bufPtr->headerMsg.control = V2_SYNC_CONTROL;
    /* bufPtr->headerMsg.logMeanMessageInterval = (sint8)log2f((float32)((float32)(EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxPeriod) * ETHTSYN_MAIN_FUNCTION_PERIOD_LOG)); */ /* Need to check */
    bufPtr->headerMsg.logMeanMessageInterval = (sint8)EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxPeriod * ETHTSYN_MAIN_FUNCTION_PERIOD_LOG;  /* Need to check */

#ifdef HOST_TEST
    memcpy(&testBuffers[0], bufPtr, V2_SYNC_LENGTH);
#endif
}

/**
 *
 * @param bufPtr
 * @param i
 */
void EthTSyn_Internal_MsgPackSyncFollow(EthTsyn_Internal_MsgFollowUp *bufPtr, uint8 i)
{
    /* PTP Header */
	memset(bufPtr, 0, V2_FOLLOWUP_LENGTH);/* Reset packet to all zeros */
    /* Message type, length, flags, Sequence, Control, log mean message interval */
    bufPtr->headerMsg.transportSpecificAndMessageType = TRANSPORT_SPECIFIC;                   /* Clear previous Message type */
    bufPtr->headerMsg.transportSpecificAndMessageType |= V2_FOLLOWUP_MESSAGE;
    bufPtr->headerMsg.reserved1AndVersionPTP = V2_VERSION_PTP;
    bufPtr->headerMsg.messageLength =  ETHTSYN_HEADER_FOLLOWUP_LENGTH_BE;

    memcpy(bufPtr->headerMsg.sourcePortId.clockIdentity, EthTSyn_Internal.timeDomain[i].ptpCfgData.port_clock_identity, 8);
    bufPtr->headerMsg.sourcePortId.portNumber = ETHTSYN_HEADER_DEFAULT_PORT_NUMBER_BE;  /* Ordinary clock so its 1 */

    bufPtr->headerMsg.sequenceId = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.last_sync_tx_sequence_number);
    bufPtr->headerMsg.control = V2_FOLLOWUP_CONTROL;

 /* bufPtr->headerMsg.logMeanMessageInterval = (sint8)log2f((float32)(((float32)EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxFollowUpOffset) * ETHTSYN_MAIN_FUNCTION_PERIOD_LOG) ); */ /* Need to check */;
    bufPtr->headerMsg.logMeanMessageInterval = (sint8)EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxFollowUpOffset * ETHTSYN_MAIN_FUNCTION_PERIOD_LOG;
    bufPtr->preciseOriginTimestamp.nanoseconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t1SyncTxTime.nanoseconds) ;
    bufPtr->preciseOriginTimestamp.seconds = bswap32(EthTSyn_Internal.timeDomain[i].ptpCfgData.t1SyncTxTime.seconds);
    bufPtr->preciseOriginTimestamp.secondsHi = bswap16(EthTSyn_Internal.timeDomain[i].ptpCfgData.t1SyncTxTime.secondsHi);
#ifdef HOST_TEST
    memcpy(&testBuffers[0], bufPtr, V2_FOLLOWUP_LENGTH);
#endif
}

/**
 *
 * @param X
 * @return
 */
uint64 convert_To_NanoSec(Eth_TimeStampType X)
{
    uint64 result;
    uint64 second48;

    second48 = ((((uint64)X.secondsHi << 32u)) | X.seconds);
    result = (1000000000 * second48) + X.nanoseconds;
    return (result);
}

/**
 *
 * @param X
 * @return
 */
uint64 convert_To_NanoSec_Stbm(StbM_TimeStampType X)
{
    uint64 result;
    uint64 second48;

    second48 = ((((uint64)X.secondsHi << 32u)) | X.seconds);
    result = (1000000000 * second48) + X.nanoseconds;
    return (result);
}

/**
 *
 * @param value
 * @return
 */
EthTSyn_Internal_TimeNanoType absolute(sint64 value) {
    EthTSyn_Internal_TimeNanoType tmp_val;
  if (value < 0) {
      tmp_val.master_to_slave_delay = (uint64)-value;
      tmp_val.sign = TRUE;
  }
  else {
      tmp_val.master_to_slave_delay = (uint64)value;
      tmp_val.sign = FALSE;
  }
  return tmp_val;
}
/**
 *
 * @param i
 */
void EthTSyn_Internal_UpdateTimer(uint8 i)
{
    /* Sync Interval time counter */
    if((TRUE == EthTSyn_Internal.timeDomain[i].ptpCfgData.syncIntervalTimerStarted)){
        if (EthTSyn_Internal.timeDomain[i].ptpCfgData.syncIntervalTimer < (EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxPeriod)){
            EthTSyn_Internal.timeDomain[i].ptpCfgData.syncIntervalTimer++;
            EthTSyn_Internal.timeDomain[i].ptpCfgData.syncIntervalTimeout = FALSE;
        }else {
            EthTSyn_Internal.timeDomain[i].ptpCfgData.syncIntervalTimeout = TRUE;
        }
    }

    /* Follow Up Timer counter */    
    if((TRUE == EthTSyn_Internal.timeDomain[i].ptpCfgData.followUpTimerStarted)){
        if((EthTSyn_Internal.timeDomain[i].ptpCfgData.followUpTimer < EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxFollowUpOffset)){
            EthTSyn_Internal.timeDomain[i].ptpCfgData.followUpTimer++;
            EthTSyn_Internal.timeDomain[i].ptpCfgData.followUpTimerTimeout = FALSE;
        }else {
            EthTSyn_Internal.timeDomain[i].ptpCfgData.followUpTimerTimeout = TRUE;
        }
    }

    /* Reception Timeout counter */
    if(TRUE == EthTSyn_Internal.timeDomain[i].ptpCfgData.RxFollowUpTimerStarted){
        if((EthTSyn_Internal.timeDomain[i].ptpCfgData.RxFollowUpTimer < (EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynGlobalTimeFollowUpTimeout - 1))){
            EthTSyn_Internal.timeDomain[i].ptpCfgData.RxFollowUpTimer++;
            EthTSyn_Internal.timeDomain[i].ptpCfgData.receptionTimeout = FALSE;
        }else{
            EthTSyn_Internal.timeDomain[i].ptpCfgData.receptionTimeout = TRUE;
        }
    }

    /* Pdelay Req interval counter */
    if(TRUE == EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayTimerStarted){
        EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayIntervalTimer++;
        if(EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayIntervalTimer < (EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxPdelayReqPeriod -1)){
            EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayIntervalTimeout = FALSE;
        }else {
            EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayIntervalTimeout = TRUE;
        }
    }

    /* Pdelay Resp interval counter */
    if((TRUE == EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayRespTimerStarted)){
        if(EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayRespIntervalTimer < EthTSyn_ConfigPointer->EthTSynGlobalTimeDomain[i].EthTSynTimeDomain->EthTSynGlobalTimeTxFollowUpOffset){
            EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayRespIntervalTimer++;
            EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayRespTimeout = FALSE;
        }else {
                EthTSyn_Internal.timeDomain[i].ptpCfgData.pdelayRespTimeout = TRUE;
        }
    }
}

/*lint -restore */
