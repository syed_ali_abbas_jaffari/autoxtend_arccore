/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.2.2 */

#include "SD.h"
#include "SD_Internal.h"
#include "SoAd.h"
#include "string.h"

//lint -w2
/*lint -e793 Ignore over 31 significant characters in SoAd.h */

extern const Sd_ConfigType *SdCfgPtr; /*lint -e526 */

typedef struct  {
	TcpIp_SockAddrType ipaddress;
	PduInfoType *msg;
	Sd_InstanceType *svcInstance;
	boolean multicast;
} MsgType;

static const TcpIp_SockAddrType wildcard = {
		(TcpIp_DomainType) TCPIP_AF_INET,
		TCPIP_PORT_ANY,
		{TCPIP_IPADDR_ANY, TCPIP_IPADDR_ANY, TCPIP_IPADDR_ANY, TCPIP_IPADDR_ANY }
};

/* -----------------Receive queue (client/server) --------------- */
#define NO_OF_QUEUES 2u  /* Client and Server queues */
#define RECEIVE_MAX 50u  /* IMPROVEMENT:  Make this configurable */

static MsgType* receive_queue_array [NO_OF_QUEUES] [RECEIVE_MAX];
static uint8 rec_rear[NO_OF_QUEUES] = {0,0};
static uint8 rec_front[NO_OF_QUEUES] = {0,0};

static boolean rec_insert(MsgType *item, uint8 queue)
{
	if (queue > 1){
		return FALSE;  // quit with an error
	}

	uint8 rec_next = rec_rear[queue] + 1;
	if (rec_next >= RECEIVE_MAX){
		rec_next = 0;
	}

	// Circular buffer is full
	if (rec_next == rec_front[queue]){
		return FALSE;  // quit with an error
	}

	receive_queue_array[queue][rec_rear[queue]] = item;
	rec_rear[queue] = rec_next;
	return TRUE;

}


static boolean rec_delete(MsgType **item, uint8 queue)
{
	// Sanity check of queue parameter
	if (queue > 1){
		return FALSE;  // quit with an error
	}

    // if the head isn't ahead of the tail, we don't have any characters
    if (rec_rear[queue] == rec_front[queue]){
        return FALSE;  // quit with an error
    }

    *item = receive_queue_array[queue][rec_front[queue]];

    uint8 rec_next = rec_front[queue] + 1;
    if(rec_next >= RECEIVE_MAX){
        rec_next = 0;
    }

    rec_front[queue] = rec_next;

    return TRUE;

}

static boolean rec_put_back_latest(uint8 queue)
{
	// Sanity check of queue parameter
	if (queue > 1){
		return FALSE;  // quit with an error
	}

	if (rec_front[queue] > 0) {
		rec_front[queue] -= 1;
	}
	else {
		rec_front[queue] = RECEIVE_MAX-1;
	}

	return TRUE;
}

/* Memory pool for MsgType */

#define PAYLOAD_MAX 1000 /* IMPROVEMENT:  Determine maximum length of an SD_Message, make this configurable */
#define POOL_MAX (RECEIVE_MAX * NO_OF_QUEUES)

const TcpIp_SockAddrType Empty_TcpIp_SockAddrType = { 0,0,{0,0,0,0}};

static MsgType Message_Pool[POOL_MAX];
static PduInfoType PduInfo_Pool[POOL_MAX];
static uint8 Payload_Pool[POOL_MAX][PAYLOAD_MAX];

static uint32 nextMessage = 0;
static uint32 lastMessage = 0;


void InitMessagePool()
{
	for (uint32 i=0; i<POOL_MAX;i++){
		for (uint32 j=0; j<PAYLOAD_MAX; j++){
			Payload_Pool[i][j] = 0;
		}
		PduInfo_Pool[i].SduLength = 0;
		PduInfo_Pool[i].SduDataPtr = Payload_Pool[i];
		Message_Pool[i].ipaddress = Empty_TcpIp_SockAddrType;
		Message_Pool[i].msg = &PduInfo_Pool[i];
	}
}

boolean PoolFull ()
{
	uint32 size = 0;
	if (lastMessage < nextMessage) {
		size = nextMessage - lastMessage;
	}else if (lastMessage > nextMessage) {
		size = nextMessage + POOL_MAX - lastMessage;
	}
	if (size == (POOL_MAX - 1)){
		return TRUE;
	} else
	{
		return FALSE;
	}
}
static MsgType* NewMessage()
{
	MsgType *result;

	if (PoolFull()){
		/* No messages left */
		result = NULL;
	}
	else {

		result = &Message_Pool[nextMessage];
		nextMessage++;
		if (nextMessage == POOL_MAX) {
			nextMessage = 0;
		}
	}

	return result;
}

void FreeMessage(void) {
	/* Clear data */
	if (nextMessage != lastMessage) {
		Message_Pool[lastMessage].ipaddress = Empty_TcpIp_SockAddrType;
		Message_Pool[lastMessage].msg->SduLength = 0;
		for (int j = 0; j < PAYLOAD_MAX; j++) {
			Message_Pool[lastMessage].msg->SduDataPtr[j] = 0;
		}
		lastMessage++;
		if (lastMessage == POOL_MAX) {
			lastMessage = 0;
		}
	}
}


void Handle_RxIndication(PduIdType RxPduId, const PduInfoType* PduInfoPtr) {

#define ENTRY_TYPE_INDEX 16u

	Sd_InstanceType *svcInstance = NULL;
	boolean is_multicast = FALSE;

	/** @req SWS_SD_00482 */
	/* Determine service instance. */
	for (uint16 i = 0; i <= SD_NUMBER_OF_INSTANCES; i++) {
		if (SdCfgPtr != NULL) {
			if (SdCfgPtr->Instance[i].MulticastRxPduId == RxPduId) {
				is_multicast = TRUE;
				svcInstance = (Sd_InstanceType *) &SdCfgPtr->Instance[i];
				break;
			} else if (SdCfgPtr->Instance[i].UnicastRxPduId == RxPduId) {
				is_multicast = FALSE;
				svcInstance = (Sd_InstanceType *) &SdCfgPtr->Instance[i];
				break;
			}
		}
	}

	if (svcInstance == NULL) {
		/* No matching service - IMPROVEMENT: Add error handling */
		return;
	}

	/* save Message and address */
	MsgType *received_message = NewMessage();
	if (received_message == NULL) {
		/* Message Pool empty -  IMPROVEMENT: Add error handling */
		return;
	}

	received_message->ipaddress = wildcard; // Set at higher level.
	received_message->msg->SduLength = PduInfoPtr->SduLength;
	received_message->multicast = is_multicast;
	received_message->svcInstance = svcInstance;
	memcpy(received_message->msg->SduDataPtr, PduInfoPtr->SduDataPtr,
			PduInfoPtr->SduLength);

	/** @req SWS_SD_00708 */
	/* IMPROVEMENT: Check consistency */

	/* Peek the type of message */
	/* IMPROVEMENT: Currently only supports one entry per message */
	uint8 entry_type = received_message->msg->SduDataPtr[ENTRY_TYPE_INDEX];
	uint8 queue = CLIENT_QUEUE;
	if ((entry_type == FIND_SERVICE_TYPE)
			|| (entry_type == SUBSCRIBE_EVENTGROUP_TYPE)) {
		queue = SERVER_QUEUE;
	} else if (entry_type == OFFER_SERVICE_TYPE)
/*			|| (entry_type == SUBSCRIBE_EVENTGROUP_ACK_TYPE)*/
	{
		queue = CLIENT_QUEUE;
	} else if  /*(entry_type == OFFER_SERVICE_TYPE)
			|| */ (entry_type == SUBSCRIBE_EVENTGROUP_ACK_TYPE) {
		queue = CLIENT_QUEUE;
	}

	if (!rec_insert(received_message, queue)) {
		/* Buffer full Error */
		/* IMPROVEMENT: Add error handling */

	}

}

/* Fetch the next received SD message from the queue
 * If the queue is empty FALSE is returned.
 * otherwise 0 is returned, and the Sd_Message is found in the msg parameter.
 */
boolean ReceiveSdMessage(Sd_Message *msg,  TcpIp_SockAddrType *ipaddress, uint8 queue, Sd_InstanceType **server_svc, boolean *is_multicast)
{
	MsgType *received_message;

	/* Fetch next item in queue */
	if (!rec_delete(&received_message, queue)) {
		/* Queue empty */
		return FALSE;
	}

	if (received_message == NULL) {
		return FALSE;
	}
	else {
		*ipaddress = received_message->ipaddress;
		*server_svc = received_message->svcInstance;
		*is_multicast = received_message->multicast;
		DecodeMessage(msg, received_message->msg->SduDataPtr, received_message->msg->SduLength);
	}
	return TRUE;
}

boolean PutBackSdMessage(uint8 queue)
{
	return rec_put_back_latest(queue);
}

static uint8 message [1000];  /* TBD: Calculate Max length */
static uint8 options[500];    /* TBD: Calculate Max length */


//static uint16 sessionID = 1; /* IMPROVEMENT: Handle this according to req. SD_00036 and SD_00035. Declared as const for now.  */


/* TransmitSdMessage assembles and transmits one SD message of any type, both client and server messages
 * Parameters:
 * instance - ref to the current instance
 * client - ref to the current client service instance dynamic data structure
 * server - ref to the current server service instance dynamic data structure
 * subscribe_entry - Used for SD_SUBSCRIBE_EVENTGROUP_ACK/NACK messages. ref to subscribe entry to reply to.
 * event_index - Used for SD_SUBSCRIBE_EVENTGROUP/STOP_SUBSCRIBE and SD_SUBSCRIBE_EVENTGROUP_ACK/NACK messages.
 *               Contains the index of the ConsumedEventGroup that should be subscribed (SUBSCRIBE) or
 *               the index of the EvvneHandler thit is subscribed (SUBSCRIBE_ACK)
 * ipaddress - unicast address to set if applicable
 */
void TransmitSdMessage(Sd_DynInstanceType *instance,
					   Sd_DynClientServiceType *client,
					   Sd_DynServerServiceType *server,
					   const Sd_Entry_Type2_EventGroups *subscribe_entry,
					   uint8 event_index,
					   Sd_EntryType entry_type,
					   TcpIp_SockAddrType *ipaddress)
{
	/** @req SWS_SD_00480 */
	/** @req SWS_SD_00478 */
	/** @req SWS_SD_00701 */

	/* IMPROVEMENT: We need to check that parameters are OK, to avoid null pointer reference bugs.
	 * i.e. server parameters is not null and is used only for server entries etc.
	 * or maybe divide it for client and server messages. */

	Sd_Message sd_msg;
	uint8 entry_type1_array[ENTRY_TYPE_1_SIZE];
	uint8 entry_type2_array[ENTRY_TYPE_2_SIZE];
	boolean send_by_multicast = FALSE;

	PduIdType pdu;
	PduInfoType pduinfo;
	uint32  messagelength;
	uint32 optionslength = 0;
	uint8 no_of_options = 0;

	/* IMPROVEMENT: Handling of flags according to chapter 7.3.6. Reboot_Flag and Unicast_Flag is const until then. */
	/** @req SWS_SD_00151 **/
	const uint8 Reboot_Flag = 1u; /* Default value after reboot */
	/** @req SWS_SD_00153 **/
	const uint8 Unicast_Flag = 1u; /* Supports Unicast messages */
	/** @req SWS_SD_00154 **/
	const uint8 Flags = 0u;
	const uint8 NoOfEntries = 1; /* IMPROVEMENT:Change this later.*/

	/** @req SWS_SD_00032 **/
	sd_msg.RequestID = (uint32) ((CLIENT_ID << 16)); /* SessionID is filled in later */ /*lint !e835 Want to use CLIENT_ID even though zero as left argument */
	sd_msg.ProtocolVersion = (uint8) PROTOCOL_VERSION;
	/** @req SWS_SD_00143 **/
    sd_msg.InterfaceVersion = (uint8) INTERFACE_VERSION;
    sd_msg.MessageType = (uint8) MESSAGE_TYPE;
    sd_msg.ReturnCode = (uint8) RETURN_CODE;
	/** @req SWS_SD_00150 **/
	/** @req SWS_SD_00152 **/
    sd_msg.Flags =  (uint8) (Flags | (Reboot_Flag << 7u) |  (Unicast_Flag << 6u));
	/** @req SWS_SD_00156 **/
    sd_msg.Reserved = 0u;

	/* Create the options array */
	sd_msg.LengthOfOptionsArray = 0;
	sd_msg.OptionsArray = NULL;
    BuildOptionsArray(entry_type, client, server, event_index, options, &optionslength, &no_of_options,instance->InstanceCfg->HostName);
    if (optionslength > 0) {
    	sd_msg.OptionsArray = &options[0];
    	sd_msg.LengthOfOptionsArray = optionslength;
    } else {
    	sd_msg.OptionsArray = NULL;
    	sd_msg.LengthOfOptionsArray = 0;
    }

    /* Create entries array */
    /* NB: Currently only one entry per SD_Message is used.
     * IMPROVEMENT: Handle packing of more than one entry in each message. */
    switch (entry_type) {
	case SD_OFFER_SERVICE:
	case SD_FIND_SERVICE:
	case SD_STOP_OFFER_SERVICE:
		/** @req SWS_SD_00158 **/
	    sd_msg.LengthOfEntriesArray = (uint32) ENTRY_TYPE_1_SIZE  * NoOfEntries;
		BuildServiceEntry(entry_type, client, server, entry_type1_array, no_of_options);
		sd_msg.EntriesArray = entry_type1_array;
		break;
	case SD_SUBSCRIBE_EVENTGROUP:
	case SD_STOP_SUBSCRIBE_EVENTGROUP:
	case SD_SUBSCRIBE_EVENTGROUP_ACK:
	case SD_SUBSCRIBE_EVENTGROUP_NACK:
		/** @req SWS_SD_00694 **/
	    sd_msg.LengthOfEntriesArray = (uint32) ENTRY_TYPE_2_SIZE  * NoOfEntries;
		BuildEventGroupsEntry(entry_type, client, subscribe_entry, event_index, entry_type2_array, no_of_options);
		sd_msg.EntriesArray = entry_type2_array;
		break;
	default:
		/* Error */
		break;
	}


	if (client != NULL) {
		/* set remote address for client call */
	    switch (entry_type) {
		case SD_FIND_SERVICE:
			send_by_multicast = TRUE;
			break;
		case SD_SUBSCRIBE_EVENTGROUP:
			/** @req SWS_SD_00702 */
			if (client->ClientServiceCfg->ConsumedEventGroup[event_index].TcpActivationRef != ACTIVATION_REF_NOT_SET) {
				(void)SoAd_EnableSpecificRouting (client->ClientServiceCfg->ConsumedEventGroup[event_index].TcpActivationRef,
					client->ClientServiceCfg->TcpSocketConnectionGroupId);
			}
			/** @req SWS_SD_00703 */
			else if (client->ClientServiceCfg->ConsumedEventGroup[event_index].UdpActivationRef != ACTIVATION_REF_NOT_SET) {
				(void)SoAd_EnableSpecificRouting (client->ClientServiceCfg->ConsumedEventGroup[event_index].UdpActivationRef,
					client->ClientServiceCfg->UdpSocketConnectionGroupId);
			}
			send_by_multicast = FALSE;
			if ((client->UdpEndpoint.valid != FALSE) || (client->TcpEndpoint.valid != FALSE)) {
				/* Use Unicast. Set remote address to address retrieved in incoming option parameter, but use port number from SD multicast socket.
				 * ipaddress was set to the correct value when receiving OFFER_SERVICE. */
				SoAd_SoConIdType tx_socket = instance->TxSoCon;

				SoAd_SoConIdType rx_socket = instance->MulticastRxSoCon;
				TcpIp_SockAddrType sd_remoteaddress;
				sd_remoteaddress.domain = TCPIP_AF_INET;
				(void)SoAd_GetRemoteAddr(rx_socket, &sd_remoteaddress);
				ipaddress->port = sd_remoteaddress.port;

				(void)SoAd_SetRemoteAddr(tx_socket, ipaddress);

				/* Assign sessionID for this SD messsage */
				/** @req SWS_SD_00032 **/
				sd_msg.RequestID |= client->UnicastSessionID;
				/** @req SWS_SD_00035 */
				/** @req SWS_SD_00036 */
				if (client->UnicastSessionID == 0xFFFF){
					client->UnicastSessionID = 1u;
				}
				else {
					client->UnicastSessionID++;
				}
			}
			break;
		case SD_STOP_SUBSCRIBE_EVENTGROUP:
			send_by_multicast = FALSE;
			if ((client->UdpEndpoint.valid != FALSE) || (client->TcpEndpoint.valid != FALSE)) {
				/* Use Unicast. Set remote address to address retreived in previous option to OFFER_SERVICE,
				 * but use port number from SD multicast socket. */
				SoAd_SoConIdType tx_socket = instance->TxSoCon;

				if (client->TcpEndpoint.valid != FALSE) {
					ipaddress->addr[0] = ((client->TcpEndpoint.IPv4Address & 0xFF000000) >> 24);
					ipaddress->addr[1] = ((client->TcpEndpoint.IPv4Address & 0x00FF0000) >> 16);
					ipaddress->addr[2] = ((client->TcpEndpoint.IPv4Address & 0x0000FF00) >> 8);
					ipaddress->addr[3] = (client->TcpEndpoint.IPv4Address & 0x000000FF);
					ipaddress->domain = TCPIP_AF_INET;
				} else if (client->UdpEndpoint.valid != FALSE) {
					ipaddress->addr[0] = ((client->UdpEndpoint.IPv4Address & 0xFF000000) >> 24);
					ipaddress->addr[1] = ((client->UdpEndpoint.IPv4Address & 0x00FF0000) >> 16);
					ipaddress->addr[2] = ((client->UdpEndpoint.IPv4Address & 0x0000FF00) >> 8);
					ipaddress->addr[3] = (client->UdpEndpoint.IPv4Address & 0x000000FF);
					ipaddress->domain = TCPIP_AF_INET;
				}

	            /* Fetch port number from SD RX_Multicast socket. */
	            SoAd_SoConIdType rx_socket = instance->MulticastRxSoCon;
				TcpIp_SockAddrType sd_remoteaddress;
				sd_remoteaddress.domain = TCPIP_AF_INET;
				(void)SoAd_GetRemoteAddr(rx_socket, &sd_remoteaddress);
				ipaddress->port = sd_remoteaddress.port;

				(void)SoAd_SetRemoteAddr(tx_socket, ipaddress);

				/* Assign sessionID for this SD messsage */
				/** @req SWS_SD_00032 **/
				sd_msg.RequestID |= client->UnicastSessionID;
				/** @req SWS_SD_00035 */
				/** @req SWS_SD_00036 */
				if (client->UnicastSessionID == 0xFFFF){
					client->UnicastSessionID = 1u;
				}
				else {
					client->UnicastSessionID++;
				}
			}
			break;
		default:
			/* Should not occur */
			break;
		}

	}

	if (server != NULL) {
		/* set remote address for server call */
	    switch (entry_type) {
		case SD_OFFER_SERVICE:
		case SD_STOP_OFFER_SERVICE:
			send_by_multicast = TRUE;
			break;
		case SD_SUBSCRIBE_EVENTGROUP_ACK:
		case SD_SUBSCRIBE_EVENTGROUP_NACK:
			send_by_multicast = FALSE;
			/* Set remote address to address retrieved in incoming option parameter.
			 * ipaddress was not set in this case.
			 * If no option parameter, use ipaddress as inparameter */
			/** @req SWS_SD_00421 **/
			if (server->EventHandlers[event_index].UdpEndpoint.valid != FALSE) {
				   ipaddress->addr[0] = ((server->EventHandlers[event_index].UdpEndpoint.IPv4Address & 0xFF000000) >> 24);
				   ipaddress->addr[1] = ((server->EventHandlers[event_index].UdpEndpoint.IPv4Address & 0x00FF0000) >> 16);
				   ipaddress->addr[2] = ((server->EventHandlers[event_index].UdpEndpoint.IPv4Address & 0x0000FF00) >> 8);
				   ipaddress->addr[3] = (server->EventHandlers[event_index].UdpEndpoint.IPv4Address & 0x000000FF);
				   ipaddress->domain = TCPIP_AF_INET;
				   ipaddress->port = server->EventHandlers[event_index].UdpEndpoint.PortNumber;
			} else if (server->EventHandlers[event_index].TcpEndpoint.valid != FALSE) {
				   ipaddress->addr[0] = ((server->EventHandlers[event_index].TcpEndpoint.IPv4Address & 0xFF000000) >> 24);
				   ipaddress->addr[1] = ((server->EventHandlers[event_index].TcpEndpoint.IPv4Address & 0x00FF0000) >> 16);
				   ipaddress->addr[2] = ((server->EventHandlers[event_index].TcpEndpoint.IPv4Address & 0x0000FF00) >> 8);
				   ipaddress->addr[3] = (server->EventHandlers[event_index].TcpEndpoint.IPv4Address & 0x000000FF);
				   ipaddress->domain = TCPIP_AF_INET;
				   ipaddress->port = server->EventHandlers[event_index].TcpEndpoint.PortNumber;
			}

			/* Use Unicast. Set the remote address before sending */
			if (ipaddress != NULL) {
				SoAd_SoConIdType tx_socket = instance->TxSoCon;
				(void)SoAd_SetRemoteAddr(tx_socket, ipaddress);

				/* Assign sessionID for this SD messsage */
				/** @req SWS_SD_00032 **/
				sd_msg.RequestID |= server->UnicastSessionID;
				/** @req SWS_SD_00035 */
				/** @req SWS_SD_00036 */
				if (server->UnicastSessionID == 0xFFFF){
					server->UnicastSessionID = 1u;
				}
				else {
					server->UnicastSessionID++;
				}
			}
			break;
		default:
			/* Should not occur */
			break;
		}

	}

	if (send_by_multicast) {
	    /* Use Multicast. Retreive multicast address from Multicast RxPdu. */
		SoAd_SoConIdType multicast_rx_socket = instance->MulticastRxSoCon;
		SoAd_SoConIdType tx_socket = instance->TxSoCon;
		TcpIp_SockAddrType destination;
		uint8 netmask;
		TcpIp_SockAddrType default_router;

		destination.domain = TCPIP_AF_INET;
		default_router.domain = TCPIP_AF_INET;
		(void)SoAd_GetLocalAddr(multicast_rx_socket,  &destination, &netmask, &default_router);
		/* Set the remote multicast address before sending */
		(void)SoAd_SetRemoteAddr(tx_socket, &destination);

		/* Assign sessionID for this SD messsage */
		/** @req SWS_SD_00032 **/
		sd_msg.RequestID |= instance->MulticastSessionID;
		/** @req SWS_SD_00035 */
		/** @req SWS_SD_00036 */
		if (instance->MulticastSessionID == 0xFFFF){
			instance->MulticastSessionID = 1u;
		}
		else {
			instance->MulticastSessionID++;
		}
	}


	/* Combine entries and options to sd message */
	memset(message,0,1000);
	FillMessage(sd_msg, message, &messagelength);
	pduinfo.SduDataPtr = message;
	pduinfo.SduLength = (uint16) messagelength;

	pdu = instance->InstanceCfg->TxPduId;

	/** @req SWS_SD_00039 */
	/** @req SWS_SD_00709 */
	(void)SoAd_IfTransmit(pdu,&pduinfo);

	/** @req SWS_SD_00705 */
	(void)SoAd_SetRemoteAddr(instance->TxSoCon, &wildcard);

}

