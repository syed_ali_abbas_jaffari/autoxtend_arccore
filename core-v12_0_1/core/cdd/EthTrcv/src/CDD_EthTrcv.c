/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 * 
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with  
 * the terms contained in the written license agreement between you and ArcCore, 
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as 
 * published by the Free Software Foundation and appearing in the file 
 * LICENSE.GPL included in the packaging of this file or here 
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

/** @reqSettings DEFAULT_SPECIFICATION_REVISION=NONE */
/** @tagSettings DEFAULT_ARCHITECTURE=RH850F1H|ZYNQ|JACINTO6 */

#include "Eth.h"
#include "CDD_EthTrcv.h"
#if defined(CFG_JAC6)
#include "Eth_jacinto.h" /* Can directly access the ETH registers for the link status  in J6 */
#endif
#include "LwIpAdp.h"

#define ETHTRCV_MAX_ADDRESS              32u

#define BMCR_REGISTER    			     0u
#define BMSR_REGISTER    			     1u
#define AUTONEG_ADVERTISE_REGISTER	     4u
#define PHY_SOFT_RESET                	 0x8000u
#define ENABLE_AUTO_NEGOTIATION          0x1000u
#define ENABLE_100MBPS_DUPLEX            0x2100u
#if defined(CFG_ETH_PHY_DP83848C)
#define PHY_STATUS_REGISTER     	     0x10u
#define PHY_STATUS_LINK                  0x0001u
#define PHY_STATUS_NO_AUTO_100           0x0005u
#define PHY_STATUS_LOOPBACK              0x0008u
#elif defined (CFG_ETH_PHY_TJA1100)
#define PHY_STATUS_REGISTER     	     0x01u
#define PHY_EXTENDED_STATUS_REGISTER     15u
#define PHY_EXTENDED_CONTROL_REGISTER    17u
#define PHY_CONFIG_REGISTER1             18u
#define PHY_STATUS_LINK                  0x0004u
#define PHY_STATUS_NO_AUTO_100           0xE600u /*100BASE-T4,100BASE-X_FD,100BASE-X_HD,100BASE-T2_FD,100BASE-T2_HD*/
#define PHY_EXTENDED_STATUS_NO_AUTO_100  0x0080u
#define PHY_CONFIG_EN_BIT                (2u)
#define PHY_CONFIG_MASTER_SLAVE_BIT      (15u)
#else /* CFG_ETH_PHY_DP83865 */
#define PHY_STATUS_REGISTER     	     0x11u
#define PHY_STATUS_LINK                  0x0004u
#define PHY_STATUS_NO_AUTO_100           0x000Eu
#define PHY_STATUS_LOOPBACK              0x0040u
#define RGMII_INBAND_STATUS_EN           0x0100U
#endif

#ifdef CFG_BRD_ZYNQ_ZC702
#define PORT_MIO_52                      0xF80007D0
#define PORT_MIO_53                      0xF80007D4
#endif

#if defined(CFG_JAC6)
#define GMAC_SW_MDIO_LINKSEL             0x00000080U
#define GMAC_SW_MDIO_LINKINTENB          0x00000040U
#define GMAC_SW_MDIO_PHYADDRMON_MASK     0x0000001FU
static GmacSw_HwRegType * const hwPtr = (GmacSw_HwRegType*) GMAC_SW_BASE;
#endif

/* PHY device - MARVELL-88E1111 */
#define RGMII_TXRX_CLK_DELAY    	     0x0030
#define ASYMMETRIC_PAUSE    		     0x0800
#define PAUSE   					     0x0400
#define LINKSPEED_1000MBPS		         0x0040
#define LINKSPEED_100MBPS			     0x2000
#define LINKSPEED_10MBPS			     0x0000

/* Max wait cycles for actions to occur in busy wait***** */
#define ETH_MAX_BUSY_WAIT_CYCLES        (uint32)1000

#define ETH_MDIO_WAIT_CYCLES             50u

/* Macro for busy waiting for a while */
#define BUSY_WAIT_A_WHILE(_regPtr, _mask, _val, _retPtr)\
    *(_retPtr) = E_NOT_OK;\
    for( uint32 i = 0; i < ETH_MAX_BUSY_WAIT_CYCLES; i++) {\
        if( (*(_regPtr) & (_mask)) == (_val) ) {\
            *(_retPtr) = E_OK;\
            break;\
        }\
    }\

static uint8  PhyAddress = ETHTRCV_MAX_ADDRESS; /* Invalid address or Phy is not initialised in this context */
LinkState  linkStatus = NO_LINK;


#define ETH_LINK_CHECK  /* internal defines for the internal config and test */

/**
 * Fetch PHY address:: mock up function for ETH tranciever
 * @param ctrlIdx
 * @return phyAddr
 */
static uint8 findPhyAdrs(uint8 CtrlIdx)
{
    uint8 phyAddr;
  	for (phyAddr = 0; phyAddr < ETHTRCV_MAX_ADDRESS; phyAddr++) {
#if !defined(CFG_JAC6)
  	    uint8 Status;
  	    uint16 Reg1;
  	    uint16 Reg2;
  		Status =  Eth_ReadMii(CtrlIdx,phyAddr, 2, &Reg1);
  		Status |= Eth_ReadMii(CtrlIdx,phyAddr, 3, &Reg2); /* change it */

  		if ((Status == ETH_OK) &&
  		    (Reg1 > 0x0000) && (Reg1 < 0xffff) &&
  		    (Reg2 > 0x0000) && (Reg2 < 0xffff)) {
  			return phyAddr;
  		}
#else
  		for(uint32 i = 0; i < ETH_MDIO_WAIT_CYCLES; i++); /* Provide some time for MDIO module to find the PHYs available */
  		if(hwPtr->MDIO.MDIO_ALIVE & (0x1u << phyAddr)){
  			return phyAddr;
  		}
#endif
  	}
  	return phyAddr;
}

/**
 * Link Setting up - Needed to be called from a cyclic routine
 * precondition - phyAddress is valid
 * @param CtrlIdx
 * @return
 */
void EthTrcv_TransceiverLinkUp(uint8 CtrlIdx)
{
	if(PhyAddress != ETHTRCV_MAX_ADDRESS){
		uint16 statusValue = 0;
#if !defined(CFG_JAC6)
		uint16 regValue;
		if(linkStatus == NO_LINK)
		{
			/* Set auto neg advert register */
			(void)Eth_WriteMii(CtrlIdx,PhyAddress, AUTONEG_ADVERTISE_REGISTER, 0x01e1);
			// Enable and start auto-negotiation
			Eth_ReadMii(CtrlIdx,PhyAddress,BMCR_REGISTER, &regValue);
			Eth_WriteMii(CtrlIdx,PhyAddress,BMCR_REGISTER, (regValue | 0x1200));
			linkStatus = AUTONEGOTIATE_RESTART;
		}
		// Wait for Auto-Neg complete flag
		Eth_ReadMii(CtrlIdx,PhyAddress,BMSR_REGISTER,&statusValue);
		if((statusValue & 0x0020 )== 0x0020){
			linkStatus = AUTONEGOTIATE_COMPLETE;
		}
		Eth_ReadMii(CtrlIdx,PhyAddress,PHY_STATUS_REGISTER,&statusValue);
		if((statusValue & PHY_STATUS_LINK ) != 0){
			linkStatus = LINK_UP;
		}else{
			linkStatus = LINK_DOWN;
		}
		LwIpAdp_LinkStateUpdate(linkStatus);
#else
		Eth_ReadMii(CtrlIdx,PhyAddress,PHY_STATUS_REGISTER,&statusValue);
		if((statusValue & PHY_STATUS_LINK ) != 0){
			linkStatus = LINK_UP;
		}else{
			linkStatus = LINK_DOWN;
		}
		LwIpAdp_LinkStateUpdate(linkStatus);
#endif
	}else{
		PhyAddress =  findPhyAdrs(CtrlIdx);
		if(PhyAddress != ETHTRCV_MAX_ADDRESS){
			 (void)EthTrcv_TransceiverInit(CtrlIdx);
		}
	}
}

/**
  * @brief  Mock up function for ETH Tranceiver : Initialise tranciever required
  * @param  ctrlIdx
  * @retval Std_ReturnType
  */
Std_ReturnType EthTrcv_TransceiverInit(uint8 CtrlIdx)
{
#if defined(ETHTRCV_ADRS)
	PhyAddress = ETHTRCV_ADRS;
#else
	PhyAddress = findPhyAdrs(CtrlIdx);
#endif
	if(PhyAddress == ETHTRCV_MAX_ADDRESS){ /* Could not find the Phy connected */
		return E_NOT_OK;
	}

#if defined(CFG_ETH_PHY_DP83848C) || defined(CFG_ETH_PHY_DP83865) || defined(CFG_ETH_PHY_TJA1100)
    uint16 phyStatus;
    uint16 timeout;
#if defined(CFG_ETH_PHY_TJA1100)
    uint16 extdPhyStatus;
#endif
    uint16 regValue;
    /* Wait for completion */
    timeout = 10000;
	/*  Reset phy */
	(void)Eth_WriteMii(CtrlIdx,PhyAddress, BMCR_REGISTER, PHY_SOFT_RESET);
	for(volatile uint16 i=0; i < 1000; i++){};

#if	defined(CFG_JAC6)  /*  ETH_NO_AUTO_NEG */
	hwPtr->MDIO.MDIO_USERPHYSEL0 |=  GMAC_SW_MDIO_LINKSEL;
	hwPtr->MDIO.MDIO_USERPHYSEL0 |= (GMAC_SW_MDIO_PHYADDRMON_MASK & PhyAddress);

	(void)Eth_ReadMii (CtrlIdx,PhyAddress, BMCR_REGISTER, &regValue);
	regValue &= ~ENABLE_AUTO_NEGOTIATION; // disable AN
	(void)Eth_WriteMii(CtrlIdx,PhyAddress, BMCR_REGISTER, (regValue | ENABLE_100MBPS_DUPLEX));
	for(volatile uint16 i=0; i < ETH_MDIO_WAIT_CYCLES; i++){};
	do
	{
		for(volatile uint16 i=0; i < ETH_MDIO_WAIT_CYCLES; i++){};
		if ((timeout--) == 0) {
			return E_NOT_OK;
		}
		Eth_ReadMii(CtrlIdx,PhyAddress, PHY_STATUS_REGISTER, &phyStatus);
#if defined (CFG_ETH_PHY_TJA1100)
		Eth_ReadMii(CtrlIdx,PhyAddress, PHY_EXTENDED_STATUS_REGISTER, &extdPhyStatus);
#endif
	}while
#if !defined (CFG_ETH_PHY_TJA1100)
	((phyStatus & PHY_STATUS_NO_AUTO_100) != PHY_STATUS_NO_AUTO_100);
#else
	(((phyStatus & PHY_STATUS_NO_AUTO_100) == 0u) && ((extdPhyStatus & PHY_EXTENDED_STATUS_NO_AUTO_100) != PHY_EXTENDED_STATUS_NO_AUTO_100));
#endif

#else //CFG_JAC6(ETH_NO_AUTO_NEG) end

#ifdef ETH_LINK_CHECK
	Eth_TransHw_linkUp(CtrlIdx);
#else
	/* Set auto neg advert register */
	(void)Eth_WriteMii(CtrlIdx,PhyAddress, AUTONEG_ADVERTISE_REGISTER, 0x01e1);

	/* enable auto-negotiation */
	(void)Eth_ReadMii (CtrlIdx,PhyAddress, BMCR_REGISTER, &regValue);
	(void)Eth_WriteMii(CtrlIdx,PhyAddress, BMCR_REGISTER, (regValue | 0x1200));


	do {
		for(volatile uint16 i=0; i < 1000; i++){
		}

		if ((timeout--) == 0) {
			return E_NOT_OK;
		}

		if (Eth_ReadMii(CtrlIdx,PhyAddress, BMSR_REGISTER, &phyStatus) != 0) {
			return E_NOT_OK;
		}
		if(phyStatus == 0xffff){
			return E_NOT_OK;
		}
	} while (!(phyStatus & 0x0020)); // Should be 0x786D
#endif //ETH_LINK_CHECK end
#endif /* Auto negotiate end */


#ifdef ETH_PHYLOOPBACK /* physical line test */

	(void)Eth_ReadMii (CtrlIdx,PhyAddress, BMCR_REGISTER, &regValue);
	(void)Eth_WriteMii(CtrlIdx,PhyAddress, BMCR_REGISTER, (regValue | 0x6100)); // loopback with speed 100
	do
	{
		for(volatile uint16 i=0; i < 1000; i++){};
		if ((timeout--) == 0) {
			return E_NOT_OK;
		}
		Eth_ReadMii(CtrlIdx,PhyAddress, PHY_STATUS_REGISTER, &phyStatus);
	}while (!(phyStatus & PHY_STATUS_LOOPBACK));
#endif                   /* physical line test */

#else /* other phy */
	uint16 Val = 0;
	uint32_t i;
	enum{
	   ETH_SPEED_10MBPS,
	   ETH_SPEED_100MBPS,
	   ETH_SPEED_1000MBPS
	};
	uint16 speed = ETH_SPEED_100MBPS;

	Eth_WriteMii(CtrlIdx,PhyAddress, 22, 2); //page adres
	Eth_ReadMii(CtrlIdx,PhyAddress,21, &Val);
	Val |= RGMII_TXRX_CLK_DELAY;
	Eth_WriteMii(CtrlIdx,PhyAddress, 21, Val);

	Eth_WriteMii(CtrlIdx,PhyAddress, 22, 0 );// page adrs

	Eth_ReadMii(CtrlIdx,PhyAddress, AUTONEG_ADVERTISE_REGISTER, &Val);
	Val |= RGMII_TXRX_CLK_DELAY;/* ASYMMETRIC_PAUSE ?? */
	Val |= PAUSE;
	Eth_WriteMii(CtrlIdx,PhyAddress, AUTONEG_ADVERTISE_REGISTER, Val);

	Eth_ReadMii(CtrlIdx, PhyAddress, BMCR_REGISTER, &Val);
	Val &= ~LINKSPEED_1000MBPS;
	Val &= ~LINKSPEED_100MBPS;
	Val &= ~LINKSPEED_10MBPS;

	if(speed == ETH_SPEED_100MBPS)	{
		Val |=  LINKSPEED_100MBPS;
	}
	else if (speed == ETH_SPEED_1000MBPS)    {
		Val |=  LINKSPEED_1000MBPS;
	}
	else {
		Val |=  LINKSPEED_10MBPS;
	}

	Eth_WriteMii( CtrlIdx,PhyAddress, 9, 0); /* Dont advertise PHY speed of 1000 Mbps */
	Eth_WriteMii(CtrlIdx,PhyAddress, AUTONEG_ADVERTISE_REGISTER,  0x0100 |0x0080); // don't advertise 100mbps full duplex and 100mbps half resp

	Eth_WriteMii(CtrlIdx,PhyAddress,BMCR_REGISTER, Val | 0x8000); // reset mask added
	for ( i=0; i < 100000; i++);
#endif
#if 0 /* Speed is set statically and configured */
	Eth_Hw_SetSpeed(speed);
#endif
	linkStatus = LINK_UP;
	LwIpAdp_LinkStateUpdate(LINK_UP);
    return E_OK;
}
