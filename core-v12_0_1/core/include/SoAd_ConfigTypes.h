/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/

#ifndef SOAD_CONFIGTYPES_H
#define SOAD_CONFIGTYPES_H
#include "Std_Types.h"
#include "ComStack_Types.h"
#include "SoAd_Types.h"

/*
 * Callback function prototypes
 */

typedef void (*SoAd_CallbackTpCopyRxDataFncType)(PduIdType rxPduId, PduInfoType* pduInfoPtr, PduLengthType* rxBufferSizePtr);	/** @req SOAD139 */
typedef void (*SoAd_CallbackTpStartofReceptionFncType)(PduIdType rxPduId, PduLengthType sduLength, PduLengthType* rxBufferSizePtr);	/** @req SOAD138 */
typedef void (*SoAd_CallbackTpGetAvailableTxBufferFncType)(PduIdType txPduId, PduLengthType, PduLengthType* txBufferSizePtr);
typedef void (*SoAd_CallbackTpCopyTxDataFncType)(PduIdType txPduId, PduInfoType** txPduPtr, RetryInfoType* retryInfoPtr, PduLengthType txDataCntPtr);	/** @req SOAD137 */

typedef void (*SoAd_CallbackPdurIfRxIndicationFncType)(PduIdType rxPduId, PduInfoType* pduInfoPtr);	/** @req SOAD106 */
typedef void (*SoAd_CallbackPdurTpRxIndicationFncType)(PduIdType rxPduId, NotifResultType result);	/** @req SOAD180 */
typedef void (*SoAd_CallbackUdpnmIfRxIndicationFncType)(PduIdType rxPduId, const uint8* udpSduPtr);

typedef void (*SoAd_CallbackPdurIfTxConfirmationFncType)(PduIdType txPduId);	/** @req SOAD107 */
typedef void (*SoAd_CallbackPdurTpTxConfirmationFncType)(PduIdType txPduId, NotifResultType result);	/** @req SOAD181 */
typedef void (*SoAd_CallbackUdpnmIfTxConfirmationFncType)(PduIdType txPduId);

typedef Std_ReturnType (*SoAd_CallbackDoIpPowerModeFncType)(SoAd_DoIp_PowerMode* powermode);

typedef union {
	SoAd_CallbackPdurIfRxIndicationFncType	PdurIfRxIndicationFnc;
	SoAd_CallbackPdurTpRxIndicationFncType	PdurTpRxIndicationFnc;
	SoAd_CallbackUdpnmIfRxIndicationFncType	UdpnmIfRxIndicationFnc;
} SoAd_CallbackRxIndicationFncType;

typedef union {
	SoAd_CallbackPdurIfTxConfirmationFncType	PdurIfTxConfirmationFnc;
	SoAd_CallbackPdurTpTxConfirmationFncType	PdurTpTxConfirmationFnc;
	SoAd_CallbackUdpnmIfTxConfirmationFncType	UdpnmIfTxConfirmationFnc;
} SoAd_CallbackTxConfirmationFncType;

// IMPROVEMENT: Where shall these enums be defined?
typedef enum {
	SOAD_SOCKET_PROT_TCP,
	SOAD_SOCKET_PROT_UDP
} SoAd_SocketProtocolType;

typedef enum {
	SOAD_AUTOSAR_CONNECTOR_CDD,
	SOAD_AUTOSAR_CONNECTOR_DOIP,
	SOAD_AUTOSAR_CONNECTOR_PDUR,
	SOAD_AUTOSAR_CONNECTOR_XCP
} SoAd_AutosarConnectorType;

typedef enum {
	SOAD_UL_CDD,
	SOAD_UL_PDUR,
	SOAD_UL_UDPNM,
	SOAD_UL_XCP
} SoAd_ULType;

// 10.2.5 SoAdSocketConnection
// Information required to receive and transmit data via the TCP/IP stack on a particular connection.
typedef struct {
	uint16						SocketId;
	char*						SocketLocalIpAddress;
	uint16						SocketLocalPort;
	char*						SocketRemoteIpAddress;
	uint16						SocketRemotePort;
	SoAd_SocketProtocolType		SocketProtocol;
	boolean						SocketTcpInitiate;
	boolean						SocketTcpNoDelay;
	boolean						SocketUdpListenOnly;
	SoAd_AutosarConnectorType	AutosarConnectorType;
	boolean						PduHeaderEnable;
	boolean						SocketAutosarApi;
	boolean 					ResourceManagementEnable;
	boolean						PduProvideBufferEnable;
} SoAd_SocketConnectionType;

// 10.2.6 SoAdSocketRoute (RX direction)
// Describes the path of a PDU from a socket in the TCP/IP stack to the PDU Router after reception in the TCP/IP Stack
typedef struct {
	const SoAd_SocketConnectionType*	SourceSocketRef;
	uint32								SourceId;				/* Only used when having PDU header enabled */
//	???									DestinationPduRef;		// NOTE: Replaced by following line, ok?
	uint16								DestinationPduId;		// NOTE: Ok?
	uint64								DestinationSduLength;
	SoAd_ULType							UserRxIndicationUL;
//	SoAd_CallbackRxIndicationFncType	RxIndicationUL;			// Only used when UserRxIndicationUL = CDD
} SoAd_SocketRouteType;

// 10.2.7 SoAdPduRoute (TX direction)
// Describes the path of a PDU from the PDU Router to the socket in the TCP/IP stack for transmission.
typedef struct {
	uint16									SourcePduId;
	uint16									SourceSduLength;
//	???										SourcePduRef;			// NOTE: Is not SOAD031 enough?
	const SoAd_SocketConnectionType*		DestinationSocketRef;
	uint32									DestinationId;			/* Only used when having PDU header enabled */
	SoAd_ULType								UserTxConfirmationUL;
//	SoAd_CallbackTxConfirmationFncType		TxConfirmationUL;		// Only used when UserTxConfirmationUL = CDD
} SoAd_PduRouteType;

// 10.2.8 SoAdDoIpConfig
// This container contains all global configuration parameters of the DoIP plug-in.
typedef struct {
	uint32		DoIpAliveCheckresponseTimeMs;
	uint32		DoIpControlTimeoutMs;
	uint32		DoIpGenericInactiveTimeMs;
	uint8*		DoIpHostNameOpt;
	uint32		DoIpInitialInactiveTimeMs;
	uint32		DoIpResponseTimeoutMs;
	uint32		DoIpVidAnnounceIntervalMs;
	uint32		DoIpVidAnnounceMaxWaitMs;
	uint32		DoIpVidAnnounceMinWaitMs;
	uint32		DoIpVidAnnounceNum;
	SoAd_CallbackDoIpPowerModeFncType DoipPowerModeCallback;
} SoAd_DoIpConfigType;

// 10.2.9 SoAdDoIpRoute
// A SoAd_DoIP_Route allocates a PDU ID to a combination of a DoIP source and a DoIP target address.
typedef struct {
	uint64								DoIpSourceAddress;
	uint64								DoIpTargetAddress;
	const SoAd_SocketConnectionType*	DoIpSocketConnectionRef;
	uint16								PduRouteIndex;
	uint16								SocketRouteIndex;
} SoAd_DoIpRouteType;


typedef void (*DoIp_AuthenticationCallbackType)(void);
typedef void (*DoIp_ConfirmationCallbackType)(void);

typedef struct {
	uint16 addressValue;
	uint16 txPdu;
	uint16 rxPdu;
} DoIp_TargetAddressConfigType;

typedef struct {
	uint8 activationNumber;
	DoIp_AuthenticationCallbackType authenticationCallback;
	DoIp_ConfirmationCallbackType confirmationCallback;
} DoIp_RoutingActivationConfigType;

typedef struct {
	uint16 routingActivation;
	uint16 target;
} DoIp_RoutingActivationToTargetAddressMappingType;

typedef struct {
	uint16 address;
	uint16 numBytes;
} DoIp_TesterConfigType;

typedef struct {
	uint16 tester;
	uint16 routingActivation;
} DoIp_TesterToRoutingActivationMapType;

typedef struct {
	const SoAd_SocketConnectionType*	SocketConnection;
	const SoAd_SocketRouteType*			SocketRoute;
	const SoAd_PduRouteType*			PduRoute;
	const SoAd_DoIpConfigType*			DoIpConfig;

	const SoAd_DoIp_ArcNodeTypeType 	DoIpNodeType;
	const uint16 						DoIpNodeLogicalAddress;

	const DoIp_TargetAddressConfigType* 					DoIpTargetAddresses;
	const DoIp_RoutingActivationConfigType*					DoIpRoutingActivations;
	const DoIp_RoutingActivationToTargetAddressMappingType* DoIpRoutingActivationToTargetAddressMap;
	const DoIp_TesterConfigType*							DoIpTesters;
	const DoIp_TesterToRoutingActivationMapType*			DoIpTesterToRoutingActivationMap;
} SoAd_ConfigType;



/*
 * Make the SoAd_Config visible for others.
 */
extern const SoAd_ConfigType SoAd_Config;


#endif
