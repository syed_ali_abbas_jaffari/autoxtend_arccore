
# ARCH defines
ARCH=arm_v6
ARCH_FAM=arm
ARCH_MCU=bcm2835

#
# CFG (y/n) macros
# 

CFG=ARM ARM_V6

# Add our board  
CFG+=BCM2835

# What buildable modules does this board have, 
# default or private

# MCAL
MOD_AVAIL+=MCU KERNEL PORT DIO LIN

MOD_USE += MCU KERNEL

# Default cross compiler
DEFAULT_CROSS_COMPILE = /opt/arm-none-eabi/bin/arm-none-eabi-
